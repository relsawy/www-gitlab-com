---
layout: markdown_page
title: Product Stage Direction - Plan
description: "The Plan stage enables teams to effectively plan features and projects in a single application"
canonical_path: "/direction/plan/"
---

- TOC
{:toc}

## Overview

As the broader GitLab organization pursues its vision of replacing disparate DevOps toolchains with a single application, the Plan stage aspires to build robust planning tools that meet the needs of our customers and tie directly into the broader DevOps lifecycle.  This document outlines the overall direction of our stage, the market we’re after, and our vision of a loveable DevOps planning solution within GitLab.

## Scope and Context

Planning tools are a diverse and complex product segment with far-reaching niches that solve unique problems for different types and sizes of teams. Today most GitLab customers arrive and adopt Create (source code management) or Verify (continuous integration) first. Since the primary customer is typically a software development organization and GitLab’s branding, marketing strategy, and adoption patterns align with those personas, Plan will focus on building a solution for software teams. That said, we've seen great success with non-software teams adopting Plan to meet their needs. Our own Marketing team dogfoods Plan to successfuly manage large marketing programs. 

Today our stage is organized into two groups: Project Management and Product Planning. For the purposes of this page, we’re going to scope our vision around our Jobs to Be Done which may cross-section multiple groups and categories. Overtime we may shift and evolve our groups and categories to attack this ambitious vision but this framing should keep us rooted in the capabilities our product needs to deliver for our customers. 

Today many users already find a lot of value within our Planning capabilities. Customers across different industries have adopted Plan within their organizations and are already reducing cycle time, increasing PM and developer happiness, and extracting value from the deep integration with their primary SCM and CI/CD tooling. They’re bought in and love the fully integrated toolchain. That said, we still have a long road to travel. In some areas we fall short and need to mature on the basics, in others we'll look to lead and innovate leveraging GitLab's unique position in the market. While the basic building blocks for the Plan stage have existed since the very beginning, we feel like we're just getting started. 


## Problem and Jobs to be Done

So… what is Plan and what problems are we really trying to solve? Plan facilitates a connection between Dev and Ops. It can start simply as an issue that turns into a code change via an MR, merged, deployed and eventually delivered to a customer. As organizations and products grow, what goes into deciding what to change and how to change it becomes much more complex. Product Managers and Development Managers need to weigh tradeoffs, effectively prioritize, understand their velocity and realize the value they end up delivering to their customers. This is where Plan can contribute to a high performing team, and where the problems and challenges to solve for our customers become complex. 

To best frame these problems we’ve compiled a set of Jobs To Be Done (JTBD) that represent the jobs our users are hiring us for. If you’re unfamiliar with JTBDs take a look at [this article](https://hbr.org/2016/09/know-your-customers-jobs-to-be-done). 

<%= partial("direction/jtbd-list", locals: { stage_key: "Plan" }) %>

You’ll notice these Jobs to be Done don’t line up with a specific feature or tool that we or one of our competitors offer. This is intentional - customers are not hiring us to be a better issue tracker, they’re hiring us to help them deliver value to their customers faster with more consistent results in conjunction with our other Stages across the DevOps Lifecycle. That said, there are common tools and feature sets that the industry has widely adopted (hired) to complete these jobs. We don’t see a departure from issue trackers coming anytime soon, and teams will still need a way to organize their work on a backlog for a given iteration. That said, we must evaluate ourselves against how well we meet the needs of that set of jobs. If we do well, the details will fade into the background and our customers will love GitLab’s planning tools because they ‘just work’. 


## The Plan Market

When you look across the landscape of planning tools you’ll find a few things. First, there are some large incumbents. Along the lines of Jira and Rally, these solutions have been in the market for many years and have very large customer bases. It’s often difficult to find customers of these tools that say they ‘love’ one of these competitors - but they’ll quickly rattle off the capabilities they need that only that competitor provides. This uncovers both a unique challenge and opportunity. When customers don’t love who they currently hire for a job the opportunity is ripe for disruption, but with the nuance of planning methodologies and adherence to specific capabilities, it can be challenging to break through. 

You’ll also find countless upstarts who aim to make tools that software teams won’t hate. Companies like Clubhouse or Wrike take a less software-oriented approach to planning (but still sell heavily into software teams). The wildcards are the startups like Airtable and Notion that avoid a strict point of view on methodologies and let teams configure their tools to solve numerous use cases for their business. 

After reviewing reports and analysis from numerous sources we believe the Total Addressable Market(TAM) for Plan is $14.8B in 2020, growing to $20.0B by 2025 - a Compounded Annual Growth Rate (CAGR) of 10.7%. This encompasses tooling across the entire Plan product segment from task tracking and bug tracking, to enterprise planning tools and Application Lifecycle Management (ALM). These segments represent 48% of GitLab’s estimated Serviceable Addressable Market (SAM) and 40% of our TAM; which is not inclusive of the rest of the DevOps lifecycle tooling such as SCM, CICD, or other related tools. 

As GitLab continues to mature and gain market share across our existing Stages, we see a strong opportunity to extend the value proposition of a “single application for the entire DevOps lifecycle” into the $35B Enterprise Resource Planning (ERP) and $48.2B Customer Relationship Management markets.

### Competitive Landscape

There are several entrenched market leaders as well as new startups entering the space on a regular basis. Among the largest competitors are:

- **Atlassian (TEAM)** - Average annual revenue growth of 40% over the last five years and $1.2B total revenue in 2019 have propelled TEAM to a $22.3B market capitalization. Instead of relying on a direct sales model to drive revenue growth, TEAM has consistently increased its market footprint through strategic acquisitions that would allow its complimentary products to organically self-propagate through enterprises. While they have many products, the Plan stage is specifically seeking to gain market share from Jira Software (Software Project Management), Jira Align (Portfolio Management), Confluence (Wiki),  Trello (Project Management), Jira Core (Business Project Management), and Jira Service Desk (ITSM Software). 

- **Microsoft (MSFT)** - With the acquisition of GitHub, Microsoft has started the process of combining the best from one of the world’s most popular SCM solutions with key features from Azure DevOps. Microsoft Project is their leading plan solution that currently owns 15-25% of the planning market. 

- **CollabNet VersionOne** - In 2019, TPG Capital purchased CollabNet VersionOne from Vector Capital as part of TPG’s $500M equity capital investment to build an integrated DevOps company called Digital.ai, which also includes XebiaLabs an Arxan Technologies, to focus on end-to-end solutions for Global 5,000 enterprises. 

- **Broadcom Rally** - Rally was the de facto market leader at one point in time but has seen its overall market share decline substantially. While it is still entrenched in many enterprises, many speculate that Broadcom has placed Rally on life support.

- **Planview (Private)** - Named as one of the Enterprise Agile Planning Tools (EAPT) market leaders, Planview provides a range of enterprise grade project, product, and portfolio management capabilities. Lead by investments from Venture Partners in 2013 and Thoma Bravo in 2017, Planview has been steadily acquiring competitors such as Troux Technologies, Innotas, LeanKit, and Spigit to expand its reach into different markets.

- **Targetprocess** - One of the smaller and younger competitors in the plan space, Targetprocess has rapidly increased its market footprint and has been recognized by Gartner as a leader in the EAPT market. 


## Our Vision of a Loveable Solution

### The Power of Planning within a Single DevOps Application

As an end to end DevOps platform, GitLab is uniquely positioned to deliver a planning suite that enables business leaders to drive their vision and development teams to quickly deliver value while improving how they work iteration by iteration. Our unification of the DevOps process allows us to interlink data across every stage of development; from initial analysis, to planning, implementation, deployment, and monitoring. 

For enterprises and growing SMBs, the complexities and difficulties of managing multiple teams and projects across your portfolio are monumental. Not only do organizations need access to detailed information on the progress of individual teams, they need to aggregate it up to consumable views for leaders to review and make informed decisions against.

This shared environment allows us to directly relate individual contributions and merge requests to larger company goals and directives. Leveraging these relations, GitLab aspires to provide planning functions that bridges the gap between individual contributors and the executive suite. We aim to surface the right information at the right level, empowering your business to quickly adapt to changing market conditions without multiple integrations and licenses across a suite of bloated solutions.

In order for teams to accelerate software delivery and have a greater ability to manage changing priorities, team-level planning tools and methodologies need to focus on helping teams collaborate effectively in order to decrease their time to value. We believe efficiency can be improved in two ways. The first way is to improve the efficiency of work that teams flow through existing value streams. This focuses on continuously optimizing the planning and execution processes to make them as fast as possible while maintaining high quality standards to enable faster innovation cycles in the future. The second way is to question and change the value stream into higher value-added activities at each step of the software delivery process. GitLab’s vision for Project Management is to help teams answer both of these questions: “Am I doing things fast enough?” and “Am I doing the right things?”

We are deliberately investing in improving GitLab to address the unique challenges that project, delivery, and product managers face -- including feature management, mapping to business value and outcomes, and synthesizing input and feedback from dozens of disparate channels into outcome driven, actionable roadmaps that are tightly aligned to the larger portfolio objectives and key results with their organization.


## 2020 Strategy

### Near Term (FY21) Strategy

**1. Importing from Jira without losing required data**: We continue to iterate on an enterprise-class planning tool integrated directly into the DevOps lifecycle. As we refine and add additional capabilities we know many customers need a solution to migrate from their current tooling (most commonly Jira) to GitLab. Importing from Jira without losing required data means just that; GitLab must provide the proper objects, fields, and workflows in order for our customers to view GitLab as a viable replacement for Jira. Enforced workflows, custom fields, and realtime functionality are likely requirements for this goal to be met, although we are still conducting research with our customers to understand how we can provide *substantially more value* than Jira so it's easy for organizations to justify the tangible and cognitive overhead for switching tools. Our goal is to provide a value-added differentiation with an effortless migration path. We've made significant progress against this initiative in [12.10](/releases/2020/04/22/gitlab-12-10-released/#import-issues-from-jira-to-gitlab) by allowing Jira issues to be imported directly into GitLab. Our [follow-on iteration](https://gitlab.com/groups/gitlab-org/-/epics/2826) will enable 1:1 user mapping and parsing of Jira flavored Markdown into GitLab Markdown. 

Growth driver: Expansion

**2. Best in class Kanban boards**: Current project management tools are capable, but [few enjoy the experience](https://twitter.com/arnvald/status/1219915577688543232?s=20). Trello made significant gains by focusing on the user experience. Unfortunately, Trello chose to be a general tool which left some software teams wanting features designed specifically to help with software development and delivery. GitLab has an opportunity to re-design board-based workflows for product development teams; think of how Jira could work if it were designed by Trello as opposed to the other way around. Our boards need to evolve to be a primary interface, an experience where: 

- Everyone who is looking at a particular Board is seeing the same thing (updated in realtime), with rich interactions without having to leave the Board.
- This may include introducing capabilities such as having short summaries, first-class checklists, quick filters, editing fields directly on the board cards, hiding lables, horizontal swim lanes for epics, and integrated analytics.
- In addition, we will focus on optimizing Boards for common workflows that modern product teams have such as issue triage, daily standup, sprint planning, product increment planning, and executive reporting. GitLab should provide templated Boards for common Agile methodolies such as XP, Scrum, Kanban, and lean software development.
- While we work on expanding the use cases that Boards support, we must maintain the simplicity and flexibility that our current Plan users have come to love and value.  

Growth driver: Retention with a transition to initial purchase

**3. Enhancing Portfolio and Group Roadmaps**: Provide easy-to-use, cross-team roadmaps at the portfolio, project, and epic level that allow users across the organization to see how work is progressing and identify dependencies and blockers. Organize and prioritize work though dynamic roadmaps in real time. Our current roadmap product works fine for epics, but only provides basic functionality. Roadmaps need to include all the necessary information to assess and react to your development plans (milestones, progress, timeboxes, dependencies, etc.) We've made improvements on this initiative by making milestones and weights available on the roadmap view.

Growth driver: Expansion

**4. Strategic Planning with Initiatives and Epics**: Enhanced portfolio management experience allowing customers to start planning from the top; creating strategic initiatives, projects, and epics while laying them out on a roadmap prior to issues and milestones being created. Provide analytics at each level, and allow linking of each object to surface deeper dependency mapping across multiple teams and projects. Enable users to manage strategic initiatives, assign work, measure impact, and allocate resources to each to help deliver tangible business results. 

Growth driver: Expansion

**5. Requirements & Quality Management**: Many regulated customers desire to use GitLab for requirements mapping, dependencies, and process management. In 12.10, we released our MVC of requirements management, providing users the ability to create and archive requirements and are continuing to iterate on tying these requirements to test. Additionally, we'll be launching the MVC of Quality Management in Q2, focusing on dogfooding of quality management features by our own quality team.

We aim to draw on the strengths of a single DevOps platform by creating a unified approach requirements management and quality management. 

Being able to clearly document your products requirements should be simple and intuitive and will enable users to show certification to the guidelines, standards, and potential regulations imposed by your organization or customer. These requirements should be easily to organize and allow for flexible classifications. It should be simple for architects and engineers to link requirements to other higher and lower level requirements to create an easy to visualize hierarchy.

In parallel with implementation, we will allow for testing to be organized in a hierarchical manner utilizing test suites, test groups, and test cases. Utilizing automated means, test sessions can be created and run, with status reported in a clean and efficient manner. 

To unify the certify package, requirements will be able to trace to test cases. This will allow for requirements to automatically be identified as satisfied when the linked testing has completed and passed. In this way, showing requirements verification will be greatly simplified, saving precious and resources compared to current manual offerings. This will also allow for simplified auditing as selecting a requirement will show traced code files, traced tests, as well as the status of these tests.

Growth driver: Initial purchase

**6. Reporting and Analytics**: Provide dashboarding and analytics for project and portfolio management, allowing business to track and communicate progress on work in flight, capacity of teams and projects, and overall efficiency across their full portfolio. Board work in progress limits were the first step here, but expansion of the milestone page to have more historically accurate insights into sprint velocity, scope movement, and plans vs. actuals are important for product development teams.

Growth driver: Expansion


## Plan Categories

<%= partial("direction/categories-content", locals: { :stageKey => "plan" }) %>

## Plan Stage Pricing Strategy

The Plan stage pricing strategy balances growing usage adoption across all personas while delivering the enterprise-level capabilities our larger customers demand. This approach enables small teams to be successful with GitLab Plan without upgrading to a higher tier. Teams of any size should be able to effectively plan work local to their team and deliver more value to their customers with core features like issue tracking and labels. As organizations grow and mature their adoption of the DevOps lifecycle we offer robust enterprise-grade planning capabiliites that align closely to their needs. Features like Requirements Management and Multi-Level epics offer feature functionality that uniquely benefit larger organizations. 

As we continue to mature our categories we'll consider three major pricing principles: 

1. **We aim to have an offering for each category at each tier. This aligns closely with GitLab's [buyer model](/handbook/ceo/pricing/#four-tiers). That said for Plan, the majority of our features aim to make 'teams' more successful within the DevOps lifecycle, this could lead to more improvements made at the Starter and up tiers as we mature our categories.

1. **If a feature adds unique value to more complex planning strategies we should aim towards a higher tier**. A good example of this are Roadmaps. Roadmaps are often deployed with multi team organizations that need to plan work across disparate functions and sub-organizations. Roadmaps offers a unique view that benefits executive teams and product leaders to better understand the delivery timelines of various Epics across an organization. They are available at Premium. 

1. - **We build value upon existing primitives that start at a lower tier**, and as the product matures we consider moving its primitive capabilities down. Issues are a great example, as most features in Premium/Ultimate tiers require broad adoption of the issue primitive. 

### Core/Free

**An Individual Contributor, Personal Projects** Core/Free tier should be considered a primary driver for adoption (measured in GMAU or SMAU). If a feature aligns with a basic planning action that all teams would benefit from, then target core. Any individual should be able to get up and running easily with some basic planning features and find value within the first few weeks without needing to upgrade. This includes importing issues, along with core issue management features like labels, assignees, and templates. 

If we're able to deliver meaningful value at Core we'll see continued growth and adoption as measured by SMAU.

### Starter/Bronze

**Small Teams** The Starter tier begins to unlock capabilities that make sense for managing complex projects across a single team. Targeting this tier still has the opportunity to drive adoption through a land and expand model as teams begin to adopt Plan. Especially in the case where an organization's first Stage is Create, as they move repos over and find value in managing their code they'll benefit from the ability to tie in planning activities within the same tooling. This tier should make available anything an individual team needs to effectively develop software with basic Agile methodologies like Scrum or Kanban.

### Premium/Silver

**SMBs and larger enterprises planning work across multiple teams** Premium is the first enterprise-level paid tier for Plan. We use this tier to offer unique functionality that aligns closely with the needs of enterprises and the varying organization structures that can exist within them. Features at this tier will enable product teams to effectively plan broadly across teams, map dependencies, and report on business outcomes as they deliver value to their customers. 

### Ultimate

**Mature enterprises who have adopted advanced planning methodologies** The Ultimate tier targets enterprise customers who have adopted Portfolio SAFe (or equivalent framework) and need to align teams across many departments. The Plan stage has only begun to bring value to this tier so we are still early in building out maturity, with a great roadmap in front of us. This year we've begun to deliver capabilities like Requirements Management, enhancing our Roadmaps and Epics, enabling enterprises to plan at the portfolio level. 

This tier has the most IACV upside and requires us to build mature offerings for core capabilities to capture that value. 

