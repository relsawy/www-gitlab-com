---
layout: markdown_page
title: "Category Direction - Internationalization"
description: "As GitLab evolves, we'd like anyone around the world to be able to use the application and contribute to it. Learn more here!"
canonical_path: "/direction/manage/internationalization/"
---

- TOC
{:toc}

Last Reviewed: 2020-09-28

## Introduction

Thanks for visiting the direction page for Internationalization in GitLab. This page belongs to the [Import](https://about.gitlab.com/handbook/product/product-categories/#import-group) group of the [Manage](https://about.gitlab.com/direction/manage/) stage and is maintained by [Haris Delalić](https://gitlab.com/hdelalic) who can be contacted directly via [email](mailto:hdelalic@gitlab.com). This vision is a work in progress and everyone can contribute. If you'd like to provide feedback on this page or contribute to this vision, please feel free to open a merge request for this page or comment in the [corresponding epic](https://gitlab.com/groups/gitlab-org/-/epics/2722) for this category.

## Mission

As GitLab evolves, we'd like anyone around the world to be able to use the application and contribute to it. In order for us to accomplish this together, our goal is to translate the GitLab application into many languages. To support this vision, the product should be built with internationalization in mind as the default (e.g. by continually externalizing strings). 

We also would like to localize GitLab for as many cultures as possible. To support this vision, in addition to translating the product, we should present data, such as numbers, date, and time, in the locally expected format.

### Target Languages
The following languages have been identified as our primary targets for translations:
* Spanish
* Chinese (China)
* Russian
* French
* Japanese
* Portuguese (Brazil)
* German
* Korean

These languages have been targeted due to usage (as measured on GitLab.com) and country profile (large number of native speakers and a strong domestic software development industry).
While these target languages will get [additional focus](https://gitlab.com/gitlab-org/manage/general-discussion/-/issues/16393), we will continue to support any language community that wants to translate GitLab.

## Maturity Plan

Internationalization is a **non-marketable category**, and is therefore not assigned a maturity level. However, we use [GitLab's Maturity framework](https://about.gitlab.com/direction/maturity/) to visualize the current state of the category and discuss the future roadmap.

The Internationalization category is currently a **Viable** feature in GitLab. Most of the strings are externalized and available for translations. Several languages are mostly translated (Japanese, Chinese, Ukrainian, Spanish). New translations are being merged on a [montly cadence](https://gitlab.com/groups/gitlab-org/-/epics/3571).  

We are now working on achieving the **Complete** maturity level for Internationalization. This includes externalizing all the strings and [automating](https://gitlab.com/gitlab-org/gitlab/-/issues/19896) most of the process for merging new translations.

## What's next & why

Internationalization is a community-driven effort maintained by the [Import group](https://about.gitlab.com/handbook/product/product-categories/#import-group). While the GitLab team regularly schedules [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AInternationalization) to support this project, no significant changes to our Crowdin program are currently planned. Although we may consider hiring an internationalization company to translate GitLab in the future, internationalization remains a community-driven effort that can only succeed with the involvement of the wider community.

While large internationalization improvements are not being actively prioritized in the Import group, we intend to prioritize several key improvements for the remainder of 2020:
* [Externalize all strings in GitLab](https://gitlab.com/groups/gitlab-org/-/epics/846). We've made significant improvements in our ability to detect non-externalized strings; we plan on finishing this work and are planning a hackathon to complete string externalization in the application.
* [Regular monthly import of the latest translations](https://gitlab.com/groups/gitlab-org/-/epics/3571). We have started importing the latest translations from Crowdin on a regular monthly cadence in April 2020 and intend to continue this process until it is fully automated and continuous.
* [Automate merge of Crowdin translations](https://gitlab.com/gitlab-org/gitlab/-/issues/19896). We are looking into the necessary changes to remove any manual steps from the Crowdin translations merge process.
* [Active support for the translation community](https://gitlab.com/groups/gitlab-org/-/epics/2722). We have already more actively engaged the translation community and plan to step up this engagement even more in order to remove roadblocks for contributing to our translation effort. This includes providing active guidance on translation decisions, awarding swag to star contributors and encouraging language communities to increase their translation levels. 

## What is not planned right now

The translation of our documentation and contribution guidance has been discussed on several occasions. However, we currently [do not have a goal to include GitLab documentation](https://gitlab.com/gitlab-org/gitlab/-/issues/15471#note_214823504) into our crowd-sourced translation effort. While this would be a valuable resource for our international contributors, we do not feel that we can ensure the quality, nor the timely updates to our documentation. 

## How you can help

We're always looking for contributions to help us translate GitLab. [Here's how you can help](https://docs.gitlab.com/ee/development/i18n/#how-to-contribute):
1. Externalize strings in GitLab. Before we can translate a string, we first need to mark it so the application is able to retrieve an appropriate translation in another language.
1. Contribute translations at [Crowdin](https://translate.gitlab.com/). You can help contribute new translations to a variety of languages and vote up/down on existing translations.
1. Become a [proof reader](https://docs.gitlab.com/ee/development/i18n/proofreader.html). All translations are read by a [proof reader](https://docs.gitlab.com/ee/development/i18n/proofreader.html) before being accepted, and we're always looking for help across a number of languages.

## Current translation status

This is the overall translation level for all target languages:

[![Translation Status](https://badges.crowdin.net/gitlab-ee/localized.svg "Current Status")](https://translate.gitlab.com/) 
