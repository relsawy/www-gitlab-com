---
layout: markdown_page
title: "Category Direction - Compliance Management"
description: "The goal of Compliance Management is to change the current paradigm for compliance to create an experience that's simple and friendly."
canonical_path: "/direction/manage/compliance-management/"
---

- TOC
{:toc}

## Compliance Management

| **Stage** | **Maturity** | **Content Last Reviewed** |
| --- | --- | --- |
| [Manage](/direction/dev/#manage) | [Minimal](/direction/maturity/) | `2020-10-19` |

## Introduction and how you can help

Thanks for visiting this direction page on Compliance Management in GitLab. If you'd like to provide feedback or contribute to this vision, please feel free to open a merge request for this page or comment in the [corresponding epic](https://gitlab.com/groups/gitlab-org/-/epics/720) for this category.

Compliance is a concept that has historically been complex and unfriendly. It evokes feelings of stress, tedium, and a desire to avoid the work entirely. There is often a disconnect between your company's policies and the features and settings that exist within a service provider like GitLab. Further compounding this challenge is a lack of visibility provided about the state of your account groups, projects, teams, etc within an external service or application. Compliance Management aims to bring compliance-specific data and features that focus on raising awareness and visibility of the compliance state of your GitLab groups and projects to help you make data-informed decisions about your organization's use of GitLab.

### Overview

The goal of **Compliance Management** is to change the current paradigm for compliance to create an experience that's simple and friendly. Compliance with GitLab should just happen in the background. Managing your compliance program should be easy and give you a sense of pride in your organization, not a stomach ache.

This category focuses on building features and experiences to enable compliance professionals to easily view compliance data for groups and projects; focus their time only on groups or projects that require attention; and define their organization's policies through settings and configuration.

##### Use Cases

* [Cameron (Compliance Manager)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#cameron-compliance-manager) needs a single place to view key compliance data (e.g. segregation of duties, pipelines and project security grades) about their GitLab groups and projects to ensure their teams are operating appropriately, so the organization can maintain a good compliance posture with their use of GitLab.
* [Cameron (Compliance Manager)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#cameron-compliance-manager) needs to implement [separation of duties controls](https://about.gitlab.com/direction/manage/#separation-of-duties) within GitLab to prevent the risk of a single individual having the ability to carry out multiple job functions, so the organization can mitigate risk and meet the compliance controls of a multitude of compliance frameworks.
* [Cameron (Compliance Manager)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#cameron-compliance-manager) needs a way to identify when a project is subject to compliance requirements to ensure appropriate policies and procedures are in place, so they can be managed easily and reported on with minimal effort.

#### Target Audience

* [Cameron (Compliance Manager)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#cameron-compliance-manager)
* [Sam (Security Analyst)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sam-security-analyst)
* [Sidney (Systems Administrator)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sidney-systems-administrator)
* [Rachel (Release Manager)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#rachel-release-manager)

#### Challenges to address

Separation of duties is a core concept among all compliance frameworks and is generally a risk management practice for organizations. Within GitLab, there are many areas where [separation of duties](https://about.gitlab.com/direction/manage/#separation-of-duties) is required, such as for merge requests, within CI/CD pipelines, or even changing certain project settings. This is a broad area to cover, but it's also one of the most important requirements our customers have. The implementation of separation of duties features will require collaboration across multiple GitLab product groups to ensure we've covered all of our customers' needs.

* What success looks like: GitLab should have features and experiences in place that allow compliance-minded organizations to configure and enforce separation of duties within GitLab. Customers should be able to require more than a single individual be involved with certain actions, such as merge requests or within CI/CD pipelines, to manage risk for their organization and meet their compliance requirements. This can manifest as [two-person approvals](https://gitlab.com/groups/gitlab-org/-/epics/3839), a specific [deployer role](https://docs.gitlab.com/ee/ci/environments/protected_environments.html#deployment-only-access-to-protected-environments), and bringing merge request approvals to the [group level](https://gitlab.com/groups/gitlab-org/-/epics/4367) to prevent maintainers from modifying these settings.

Enterprises operating in regulated environments need to ensure the technology they use complies with their internal company policies and procedures, which are largely based on the requirements of a particular legal or regulatory framework (e.g. GDPR, SOC 2, ISO, PCI-DSS, SOX, COBIT, HIPAA, FISMA, NIST, FedRAMP) governing their industry. Customers need features that enable them to comply with these frameworks beginning with defining the rules, or controls, for their GitLab environment.

* What success looks like: Using [compliance framework project labels](https://docs.gitlab.com/ee/user/project/settings/#compliance-framework), administrators and group owners should be able to clearly label their GitLab projects. These labels should be [customizable](https://gitlab.com/gitlab-org/gitlab/-/issues/231247) and support many aspects of a project's configuration. This association should apply the configured, sensible default settings to a project and provide enforcement capabilities for these settings. These labels should also help users in a GitLab namespace know when a project has special requirements for compliance and give [Cameron (Compliance Manager)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#cameron-compliance-manager) peace of mind knowing these projects have enforcement capabilities to prevent unauthorized changes. These labels should evolve to be tied to certain compliance controls for auditing and reporting purposes.

Compliance-minded organizations are data-informed and need visibility into all of their business operations to make the best decisions. Currently, GitLab does not aggregate the specific information organizations need to make these compliance decisions or monitor compliance status for their groups and projects. The information exists in many cases, but is simply not consolidated and presented in a way that makes compliance a simple, friendly task. Organizations have to dig for information in many disparate areas of the GitLab application and then need to build custom API-driven solutions to extract the data they need for internal compliance management or for reporting to auditors.

* What success looks like: GitLab provides a native, in-app experience for data and insights centered around compliance for organizations. This could manifest as a group-level dashboard to track the compliance progress and status of [projects](https://gitlab.com/gitlab-org/gitlab/-/issues/230826), [changes (MRs)](https://gitlab.com/gitlab-org/gitlab/-/issues/12424), commits, etc. in the context of a specific compliance framework or program. These insights should be derived by measuring activities in GitLab against specific compliance controls, such as separation of duties, access controls, SDLC policy, and more.

Managing large numbers of users with expansive group and project footprints can be difficult to do without the proper insight and tooling. In many cases, customers are building custom scripts or services to enforce things like credential rotation. Credential management is an important element of compliance because the number of people who have access, the systems they have access to, their type of access, and the layered controls in place to mitigate risk of breaches or credential misuse can have far-reaching affects on an organization in the event of a security incident.

In organizations with complex systems and approval structures, there is sometimes a need to bypass an approval flow for emergencies or to override a process that may be particularly burdensome in the moment. These bypasses are normally infrequent, but important. For example, an organization that needs to deploy an emergency fix to their production environment because their website or application is down, and the workflows they've built around deploying that change may require much more time than is available to them.

When organizations adopt GitLab, they are incorporating it into a complex and comprehensive set of systems that are already in operation. These complex systems cover a broad range of functions and exist to support business functions outside of GitLab. Those systems, however, are critical to an organizations' use of GitLab because it could be anything from ITSM to internal reporting systems that generate outputs necessary for the GitLab users to meet company requirements. These systems are involved in the decision process for changes made within GitLab to a production environment. Connecting those systems to GitLab is time consuming, complicated, and comes with inherent challenges since they're not natively supported in GitLab. 

### Where we are Headed

The [Compliance Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2537) continues to evolve to meet the needs of our customers managing their compliance programs. Our goal is to ensure this dashboard can answer as many questions as possible, saving you the time and effort of digging through individual projects. We want to surface all of the key compliance signals (e.g. segregation of duties, pipelines and project security grades) for you throughout a group so you can immediately, and more efficiently, hone in on the areas that actually require attention.

This dashboard currently focuses on the most recent merged MR and we'll be pivoting to focus on the broader concept of [change management](https://gitlab.com/gitlab-org/gitlab/-/issues/12424). We'll also be introducing a [projects view](https://gitlab.com/gitlab-org/gitlab/-/issues/230826) to abstract up a layer and highlight the most important project-level data. As we focus on compliance management, we want to build features and experiences that enable you to monitor your GitLab compliance posture much easier, saving you time and headache from doing things the traditional way.

Adding coverage for projects to the compliance dashboard means leveraging [project compliance framework labels](https://docs.gitlab.com/ee/user/project/settings/#compliance-framework) to designate the regulated projects we should track. Currently, the compliance dashboard reports on all recently merged MRs from every project, including unregulated projects. Iterating towards our vision to reduce the complexity and time required of compliance management in GitLab means reducing the total scope of projects you need to monitor in the first place. For those organizations that need only monitor specific, regulated projects, we can surface specific insights about these projects based on your feedback.

The [Compliance Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2537) should be your "one stop shop" for everything you need to know to focus your efforts and save time. The more questions we can answer within this dashboard, the less time you're spending digging through logs, projects, settings, and other areas. Compliance should be simple and friendly and that starts with a single, easy-to-use place to start your journey.

## What's Next & Why

Compliance Management will initially be focused on the SOC2, SOX, and HIPAA compliance frameworks because these three frameworks appear to be some of the most common among GitLab customers. Additionally, the features we build for these frameworks will inherently add value to other organizations who are managing compliance with other frameworks due to the fundamental nature of many requirements the various frameworks share. For example, adding access control features to support HIPAA could potentially satisfy requirements for PCI-DSS.

We'll be introducing better control at the [group level](https://gitlab.com/groups/gitlab-org/-/epics/2187) by bringing more project settings, such as MR approvals, to the group level. We'll also be providing an experience to create [custom compliance framework labels](https://gitlab.com/gitlab-org/gitlab/-/issues/231247). These would be defined at the group level, but applied to each project that has specific compliance requirements. This allows us to manage group-level setting inheritance and enforcement in a much more tactical way without applying broad, disruptive controls across an entire namespace.

We will also be focused on providing [compliance pipeline configurations](https://gitlab.com/groups/gitlab-org/-/epics/3156) that can be defined at the group-level using the custom compliance labels feature. This experience would allow you to centrally manage a compliance pipeline configuration that is inherited by regulated (labeled) projects and still allows project maintainers and developers to extend their own `.gitlab-ci.yml` for their local environment. This will provide a way to standardized and centrally manage your compliance pipelines without having to do as much custom, heavy lifting for workarounds.

We will continue to iterate on the [Compliance Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2537) to bring necessary compliance context into a single view for easy analysis and action.

| Compliance Dashboard (Projects) | Compliance Dashboard (Merge Requests) |
| ------ | ------ |
| ![](https://about.gitlab.com/images/direction/manage/compliance_dashboard_project.png) | ![](https://about.gitlab.com/images/direction/manage/compliance_dashboard-02.png) |

## Maturity

Compliance Management is currently in the **minimal** state. This is because we now have an MVC of the [Compliance Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2537), you can [designate regulated vs. unregulated projects](https://docs.gitlab.com/ee/user/project/settings/#compliance-framework) and we have some existing settings that serve as compliance controls, but these features are not yet used by significant numbers of users to solve real problems.

* You can read more about GitLab's [maturity framework here](https://about.gitlab.com/direction/maturity/), including an approximate timeline.

In order to bring Compliance Management to the **viable** state, we will be implementing features that provide GitLab group owners and administrators with more, and better, compliance controls for their GitLab environments. These controls should make it easier to manage compliance within GitLab, such as maintaining separation of duties, establishing clear change control workflows, and preventing changes to these controls by unauthorized users except for emergency or exception situations.

* You can follow the **viable** maturity plan in [this epic](https://gitlab.com/groups/gitlab-org/-/epics/2423).

Once we've achieved a **viable** version of Compliance Management, achieving a **complete** level of maturity will involve collecting customer feedback and evolving the [Compliance Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2537) further and ensuring a more complete coverage of group-level and instance-level compliance controls that enable you to confidently and effectively manage the compliance posture of your GitLab environment. Assuming we're on the right track, we'll continue to focus on these areas:

* Increase the number of settings and features you can affect by assigning a framework to a group or project.
* Increase the comprehensiveness of "out-of-the-box" Compliace Management to incorporate evidence collection, reporting, and automation of auditing tasks.
* Improve the visibility and consolidation of compliance-relevant data points in the Compliance Dashboard.
* Simplify repeatable workflows, such as evidence collection, audit project management, and project creation in a regulated context.

Finally, once we've achieved a ruleset that's sufficiently flexible and powerful for enterprises, it's not enough to be able to define these rules - we should be able to measure and confirm that they're being adhered to. Achieving **Lovable** maturity likely means further expansion of the two dimensions above, plus visualizing/reporting on the state of Compliance Management across the instance.

### User Success Metrics

We'll know we're on the right track with **Compliance Management** based on the following metrics:

* An increase in the [total number of compliance actions](https://about.gitlab.com/handbook/product/dev-section-performance-indicators/#managecompliance---other---total-compliance-actions) taken by users
* An increase in the amount of time spent viewing the [Compliance Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2537)
* An increase in the number of projects using [compliance framework labels](https://docs.gitlab.com/ee/user/project/settings/#compliance-framework)
* An increase in the number of [enterprise templates](https://docs.gitlab.com/ee/gitlab-basics/create-project.html#enterprise-templates) being created

### Competitive landscape

[Microsoft Compliance Manager](https://docs.microsoft.com/en-us/microsoft-365/compliance/compliance-manager?view=o365-worldwide) is the most direct competition with this category and our vision for the future. The [Compliance Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2537) is moving in this direction, but there are some key differences in each. Microsoft's Compliance Manager provides pre-built assessments for various compliance frameworks or certifications, introduces workflows to complete those assessments or risk assessments, offers "improvement actions" to improve an organization's compliance posture, and generates a compliance score.

<iframe width="560" height="315" src="https://www.youtube.com/embed/gjLP7ABGe90" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

After review, our impressions are:

* It's unclear what programmatic actions are being taken to automate compliance tasks
* It appears to require [manual effort](https://docs.microsoft.com/en-us/microsoft-365/compliance/compliance-score-calculation?view=o365-worldwide#initial-score-based-on-microsoft-365-data-protection-baseline) to manage the assessments beyond Microsoft's baseline capabilities
* The scoring mechanism is a gamification of the compliance program, which can help provide a sense of compliance posture for that program
* Creating custom assessment templates seems to require creating a spreadsheet that's formatted in a [specific way](https://docs.microsoft.com/en-us/microsoft-365/compliance/compliance-manager-templates?view=o365-worldwide#formatting-your-template-data-with-excel) to import into the compliance manager, which then renders that data in the UI
* There's no obvious clarification about whether a score can reach 100%, which could imply an attestation of compliance
* Ultimately, it seems this is a much broader Governance, Risk, and Compliance (GRC) tool that helps a Microsoft customer track their compliance programs within the Microsoft 365 ecosystem
* The documentation mentions a baseline compliance score based on a ["default Data Protection Baseline assessment provided to all organizations"](https://docs.microsoft.com/en-us/microsoft-365/compliance/compliance-score-calculation?view=o365-worldwide#initial-score-based-on-microsoft-365-data-protection-baseline), but does not provide specifics about this

There's an interesting, and valuable, feature that shows [Microsoft-managed controls](https://docs.microsoft.com/en-us/microsoft-365/compliance/compliance-manager?view=o365-worldwide#controls) versus customer-owned and shared controls. This would be helpful for identifying where Microsoft needs to take ownership to deliver the controls a customer inherently needs in their service provider.

Microsoft seems focused on building out a comprehensive, natively connected [compliance center](https://docs.microsoft.com/en-us/microsoft-365/compliance/microsoft-365-compliance-center?view=o365-worldwide) which is a massive undertaking and commits to a full-on GRC application within Microsoft 365. There doesn't seem to be a specific focus on DevOps, which suggests there's an opportunity for GitLab to provide this focus and supplement other GRC tools that seem to follow a similar pattern.

### Analyst landscape

The feedback we've received about this category has been supportive of our direction to save compliance professionals time and make compliance tasks easier. While there are companies and applications that exist as holistic GRC solutions, GitLab is well-positioned to make a lot of the compliance process easier and automated by enabling customers to leverage the data they're already generating in their usage of GitLab.

We spoken with some analysts about the `Compliance Management` direction and here's what they said:

* Automating traceability, and the retrieval of that data, will be valuable because it saves a lot of time
* We should provide a "glue" mechanism to tie our customers' various systems together in a way that
lets us streamline and automate reporting
* We should endeavor to build generic integrations and not specific ones to allow maximum flexibility
to customers connecting their external compliance systems
* Realize we're solving for more than employee dissatisfaction; we're helping customers save money and avoid downtime too
* Save time from checking and double-checking audit data; that time can be reallocated for other value-adding tasks
* We should build an experience that connects to the [Value Stream Management](https://about.gitlab.com/direction/manage/value_stream_management/)
direction with compliance data
* We should be focusing our messaging and efforts on automation and ensuring our users understand that value
* Leverage the concept of "Continuous Compliance" and help organizations break away from their waterfall methods
* Partnering with auditors on the [Compliance Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2537) should help build credibility and drive adoption
* C-level executives will take an interest in the dashboard particularly if it helps them mitigate the risk of monetary fines from non-compliance with regulatory requirements

### Top Customer Success/Sales issue(s)

### Top user issue(s)

* [Allow group owners to define compliance pipeline configurations at the group level](https://gitlab.com/groups/gitlab-org/-/epics/3156)
* [Configure Protected Branches at the Group and Instance Level](https://gitlab.com/gitlab-org/gitlab/-/issues/18488)
* [Support default approvers setting at group level](https://gitlab.com/groups/gitlab-org/-/epics/4367)

### Top Vision issue(s)

* [Compliance Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/2537)
