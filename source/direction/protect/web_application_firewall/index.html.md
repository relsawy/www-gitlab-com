---
layout: secure_and_protect_direction
title: "Category Direction - Web Application Firewall"
description: "By being able to see what is being sent to your systems, GitLab wants to empower you to either block malicious traffic or to otherwise act on it."
canonical_path: "/direction/protect/web_application_firewall/"
---

- TOC
{:toc}


| | |
| --- | --- |
| Stage | [Protect](/direction/protect) |
| Maturity | [Minimal](/direction/maturity/) |
| Content Last Reviewed | `2020-09-15` |

### Introduction and how you can help
<!-- Introduce yourself and the category. Use this as an opportunity to point users to the right places for contributing and collaborating with you as the PM -->

<!--
<EXAMPLE>
Thanks for visiting this category direction page on Snippets in GitLab. This page belongs to the [Editor](/handbook/product/product-categories/#editor-group) group of the Create stage and is maintained by <PM NAME>([E-Mail](mailto:<EMAIL@gitlab.com>) [Twitter](https://twitter.com/<TWITTER>)).

This direction page is a work in progress, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=snippets) and [epics]((https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=snippets) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your need for snippets, we'd especially love to hear from you.
</EXAMPLE>
-->
Thanks for visiting this category direction page on the Web Application Firewall (WAF) in GitLab. This page belongs to the Container Security group of the Protect stage and is maintained by Sam White ([swhite@gitlab.com](mailto:<swhite@gitlab.com>)).

This direction page is a work in progress, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AWAF) and [epics](https://gitlab.com/groups/gitlab-org/-/epics/795) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email or on a video call. If you're a GitLab user and have direct knowledge of your need for a web application firewall, we'd especially love to hear from you.

### Overview
A web application firewall (WAF) filters, monitors, and blocks web traffic to and from a web application. A WAF is differentiated from a regular firewall in that a WAF is able to filter the content of specific web applications, while regular firewalls serve as a safety gate between servers. By inspecting the contents of web traffic, it can prevent attacks stemming from web application security flaws, such as SQL injection, cross-site scripting (XSS), file inclusion, and security misconfigurations.

GitLab's WAF comes with a default out-of-the-box OWASP ruleset in detection only mode.

#### Target Audience
<!--
List the personas (https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas#user-personas) involved in this category.

Look for differences in user's goals or uses that would affect their use of the product. Separate users and customers into different types based on those differences that make a difference.
-->
* [Sam (Security Analyst)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#sam-security-analyst) and [Alex (Security Operations Engineer)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#alex-security-operations-engineer) are our primary target personas for any organizations that have an established security team
* [Devon (DevOps Engineer)](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer) could be a secondary persona for organizations without established security teams.  This needs to be researched more.

### Where we are Headed
<!--
Describe the future state for your category.
- What problems are we intending to solve?
- How will GitLab uniquely address them?
- What is the resulting benefits and value to users and their organizations?

Use narrative techniques to paint a picture of how the lives of your users will benefit from using this
category once your strategy is at least minimally realized. In order to challenge your level of ambition
(with the goal to make it sufficiently high), link to the current market leaders long-term vision and address how
we plan to displace them. -->

GitLab's WAF is focused on providing a base level of traffic filtering for data coming in and out of containerized environments.  The goal is not to become a general purpose WAF or to compete head-to-head with long-standing WAF vendors.  Rather the intent is to focus on containerized environments and optimize for the protection capabilities that can be applied at that layer.

Current WAF functionality is capable of filtering data at the L7 layer to protect against L7 DDoS attacks and many of the attacks listed on the OWASP Top 10.  Our WAF has some basic L4 DoS protection; however, another WAF that sits outside the containerized environment is required for organizations wanting to do L4 DDoS protection or more advanced L4 DoS protection.  For environments that are already running a WAF outside the cluster, the GitLab WAF is complementary and can provide for defense in depth with an additional layer of protection for any traffic that might circumvent or slip through a firewall further upstream.

### What's Next & Why
This category has currently achieved the [Minimal](https://about.gitlab.com/direction/maturity/) maturity level.  For the near-term, we plan to invest minimally into this category to allow for time to gather additional user feedback.  While we gather that feedback, our development efforts are currently focused on our [Container Network Security](/direction/protect/container_network_security) and [Container Host Security](/direction/protect/container_host_security) categories.

### FAQ

**Q:** What are the current plans to support high availability (HA) when using Ingress or the GitLab Web Application Firewall?

**A:** *Plans to add support for a [high availability Web Application Firewall](https://gitlab.com/gitlab-org/gitlab/-/issues/215028) have been delayed to allow development efforts to focus on the [Container Network Security](/direction/protect/container_network_security) and [Container Host Security](/direction/protect/container_host_security) categories.  For now, we advise customers with a strict requirement for high availability to not use the GitLab Web Application Firewall.  We welcome any contributions that the wider community would like to make in this area to help advance the feature and accelerate the timeline for when high availability can be supported for this feature.*

### Competitive Landscape
Some other vendors in the WAF market include the following:

- [Akamai](https://www.akamai.com/us/en/resources/application-firewall.jsp)
- [Alibaba Cloud](https://www.alibabacloud.com/product/waf)
- [Amazon Web Services](https://aws.amazon.com/waf/)
- [Barracuda Networks](https://www.barracuda.com/products/webapplicationfirewall)
- [Cloudflare](https://www.cloudflare.com/waf/)
- [F5 Networks](https://www.f5.com/products/security/advanced-waf)
- [Fortinet](https://www.fortinet.com/products/web-application-firewall/fortiweb.html)
- [Imperva](https://www.imperva.com/products/web-application-firewall-waf/)
- [Microsoft](https://docs.microsoft.com/en-us/azure/application-gateway/waf-overview)
- [Oracle](https://www.oracle.com/cloud/security/cloud-services/web-application-firewall.html)
- [Radware](https://www.radware.com/products/appwall/)
- [Signal Sciences](https://www.signalsciences.com/)

### Analyst Landscape
Gartner provides a Magic Quadrant for the [Web Application Firewall](https://www.gartner.com/en/documents/3964460) market.

### Related Categories
<!-- What's the most important thing to move your strategy forward?-->
*  [Container Network Security](/direction/protect/container_network_security)