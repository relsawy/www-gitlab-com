---
layout: markdown_page
title: "Category Direction - Container Scanning"
description: "Container Scanning tests your Docker images against known vulnerabilities that may affect software that is installed in the image. Learn more!"
canonical_path: "/direction/secure/composition-analysis/container-scanning/"
---

<!---  using https://gitlab.com/-/ide/project/gitlab-com/www-gitlab-com/edit/master/-/doc/templates/product/category_direction_template.html.md -->

- TOC
{:toc}

## Secure & Protect

| | |
| --- | --- |
| Stage | [Secure](/direction/secure/) |
| Maturity | [Viable](/direction/maturity/) |
| Content Last Reviewed | `2020-09-01` |
| Content Last Updated | `2020-09-01` |

### Introduction and how you can help
Thanks for visiting this category direction page on Container Scanning at GitLab. This page belongs to the [Composition Analysis](/handbook/product/product-categories/#composition-analysis-group) group of the [Secure](/direction/secure/) stage and is maintained by [Nicole Schwartz](https://gitlab.com/NicoleSchwartz).

#### Send Us Feedback
We welcome feedback, bug reports, feature requests, and community contributions.

Not sure how to get started?

- Upvote or comment on [proposed Category:Container Scanning issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AContainer%20Scanning) - when you find one similar to what you want, please leave a comment AND upvote it! This helps it to be prioritized, backlog items with few unique individuals commenting are unlikely to get reviewed and prioritized.
- Can't find an issue? Make a [feature proposal](https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Feature%20proposal) or a [bug report](https://gitlab.com/gitlab-org/gitlab/-/issues/new?&issuable_template=Bug). Please add the appropriate labels by adding this line to the bottom of your new issue `/label ~"devops::secure" ~"Category:Container Scanning" ~"group::composition analysis"`.
<!--- https://gitlab.com/gitlab-org/gitlab/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#) --->
- Consider signing up for [First Look](https://about.gitlab.com/community/gitlab-first-look/).
- Want to get directly into the user interview pool for Secure Stage? Sign up on this [google document form](https://docs.google.com/forms/d/e/1FAIpQLScCNWXVA_gBWQKB51amfmR4ccESsvZPudaEUrhQWVUZFaAf5Q/viewform?usp=sf_link).

Sharing your feedback directly on GitLab.com is the best way to contribute to our direction.

We believe [everyone can contribute](https://about.gitlab.com/company/strategy/#contribute-to-gitlab-application) and so if you wish to contribute [here is how to get started](https://about.gitlab.com/community/contribute/).

### Overview

Our best practices are to package applications into containers, so they can be deployed to Kubernetes.

Container Scanning checks your Docker images against known vulnerabilities that may affect software that is contained in the image. Users often use existing images as the base for their containers. It means that they rely on the security of those images and their preinstalled software. Unfortunately, as this software is subject to vulnerabilities, this may affect the security of the entire project.

Our goal is to provide Container Scanning as part of the standard development process. This means that Container Scanning is executed every time a new commit is pushed to a branch, and only vulnerabilities introduced within the merge request are shown. We also include Container Scanning as part of [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

In the future, another place where Container Scanning results would be useful is in the [GitLab Container Registry](https://docs.gitlab.com/ee/user/project/container_registry.html). Images built during pipelines are stored in the registry, and then used for deployments. Integrating Container Scanning into GitLab Container Registry will help to [monitor if it is safe to deploy a specific version of the app](https://gitlab.com/gitlab-org/gitlab-ee/issues/8790).

#### Target Audience
<!--
List the personas (https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas#user-personas) involved in this category.

Look for differences in user's goals or uses that would affect their use of the product. Separate users and customers into different types based on those differences that make a difference.
-->
Primary: Sasha (Software Developer) wants to know when adding a container if it has known vulnerabilities so alternate versions or containers can be considered.

Secondary: Sam (Security Analyst) wants to know what containers have known vulnerabilities (to reduce the OWASP A9 risk - Using Components with Known Vulnerabilities), to be alerted if a new vulnerability is published for an existing component, and how behind current version the components are.

Other: Cameron (Compliance Manager), Delaney (Development Team Lead), Devon (DevOps Engineer), Sidney (Systems Administrator)

#### Challenges to address
<!--
- What needs, goals, or jobs to be done do the users have?
- How do users address these challenges today? What products or work-arounds are utilized?

Provide links to UX Research issues, which validate these problems exist.
-->
We will be researching current user challenges in [this issue](https://gitlab.com/gitlab-org/ux-research/-/issues/297). Please feel free to comment!

### Key features

Currently we notify developers when they add containers with known vulnerabilities in a merge request, if [security approvals](https://docs.gitlab.com/ee/user/application_security/index.html#security-approvals-in-merge-requests) are configured, we will require an approval for critical, high or unknown findings. A summary of all findings for a project can be found in the [Security Dashboard](https://docs.gitlab.com/ee/user/application_security/security_dashboard/index.html) where Security Teams can quickly check the security status of projects. In some cases we are able to offer [automatic remediation](https://docs.gitlab.com/ee/user/application_security/container_scanning/#solutions-for-vulnerabilities-auto-remediation) for the findings.

- [Shows finding information in the merge request](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#overview)
- [Merge Request Approvals](https://docs.gitlab.com/ee/user/application_security/index.html#security-approvals-in-merge-requests)
- [Security Dashboard](https://docs.gitlab.com/ee/user/application_security/security_dashboard/index.html) available at the Project, Group, and Instance levels
- [Auto-remediation](https://docs.gitlab.com/ee/user/application_security/container_scanning/#solutions-for-vulnerabilities-auto-remediation) leverages Container Scanning to provide a solution for vulnerabilities that can be applied to fix the codebase.
- [Available offline](https://docs.gitlab.com/ee/user/application_security/offline_deployments/)

### Strategy

See [Secure 3 Year Strategy](https://about.gitlab.com/direction/secure/#3-year-strategy)

### Where we are Headed
<!--
Describe the future state for your category.
- What problems are we intending to solve?
- How will GitLab uniquely address them?
- What is the resulting benefits and value to users and their organizations?

Use narrative techniques to paint a picture of how the lives of your users will benefit from using this
category once your strategy is at least minimally realized. In order to challenge your level of ambition
(with the goal to make it sufficiently high), link to the current market leaders long-term vision and address how
we plan to displace them. -->

To be determined when focus returns to this category. Currently Container Scanning is being maintained but not actively pushed forward while the group concentrates on Dependency Scanning.

#### Roadmap

-[Dependency Scanning Epic Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&search=%22Container+Scanning+category+vision%22)

#### What's Next & Why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->

<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->

To be determined when focus returns to this category.

#### What is Not Planned Right Now
<!-- Often it's just as important to talk about what you're not doing as it is to
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand.-- > ----->

Anything other than bug fixes and improvements for internal customers.

#### Maturity Plan
<!-- It's important your users know where you're headed next. The maturity plan
section captures this by showing what's required to achieve the next level. The
section should follow this format:

This category is currently at the XXXX maturity level, and our next maturity target is YYYY (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/product-categories/maturity/#legend)).

- Link to maturity epic if you are using one, otherwise list issues with maturity::YYYY labels) -->
This category is currently at the Viable maturity level, and our next maturity target is not set (see our [definitions of maturity levels](https://about.gitlab.com/direction/maturity/)

 - [Container Scanning - Viable to Complete](https://gitlab.com/groups/gitlab-org/-/epics/299)

#### User success metrics
<!--
- What specific user behaviors are indicate that users are trying these features, and solving their problems?
- How will users discover these features?
-->
Currently we have very limited product analytics data, as a result we will be tracking number of times that our scans run.

In the future we hope that users will allow us to enhance our telemtry to be able to record information such as the number of findings that are dismissed vs. accepted. See our current metrics [here](https://about.gitlab.com/handbook/product/secure-and-protect-section-performance-indicators/#securecomposition-analysis---gmau---users-running-any-sca-scanners), [and other items being discussed in this issue](https://gitlab.com/groups/gitlab-org/-/epics/445).

#### Why is this important?
<!--
- Why is GitLab building this feature?
- What impact will it have on the broader devops workflow?
- How confident are we? What is the effort?
-->
In addition to being [A9 Using Components with Known Vulnerabilities](https://owasp.org/www-project-top-ten/OWASP_Top_Ten_2017/Top_10-2017_A9-Using_Components_with_Known_Vulnerabilities) in the OWASP top 10, keeping components up to date is code quality issue, and finally as the need for software bill of materials (SBoM) grows being able to list your dependencies will become a needed feature for all application developers.

### Competitive Landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/product-processes/#customer-meetings). We’re not aiming for feature parity with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

- [Black Duck](https://www.blackducksoftware.com/solutions/container-security)
- [Snyk](https://snyk.io/container-vulnerability-management)
- [Sonatype Nexus](https://www.sonatype.com/containers)
- [Qualys](https://www.qualys.com/apps/container-security/)
- [sysdig](https://sysdig.com/products/kubernetes-security/image-scanning/)
- [Aqua](https://www.aquasec.com/products/container-security/)
- [StackRox](https://www.stackrox.com/use-cases/vulnerability-management/)
- [Prisma Cloud - was TwistLock](https://www.paloaltonetworks.com/prisma/cloud/compute-security/container-security)

### Analyst Landscape
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

[GitLab was named a Niche Player in the 2020 Gartner Magic Quadrant for AST.](https://about.gitlab.com/resources/report-gartner-mq-ast/)

We continue to engage analysts so they remain aware that we offer Container Scanning, which is sometimes considered stand-alone and other times it is considered part of Application Security Testing (AST) or Software Composition Analysis (SCA) bundles as defined in our [Solutions](/handbook/product/product-categories/#solutions), since vulnerabilities for base images can be considered very similar to vulnerabilities for software dependencies.

We can get valuable feedback from analysts, and use it to drive our vision.

- [Gartner](https://www.gartner.com/doc/3888664/container-security--image-analysis)
- [Forrester](https://www.forrester.com/report/Now+Tech+Container+Security+Q4+2018/-/E-RES142078)

### Top Issue(s)

I base this on `popularity`, so please remember to comment AND upvote issues you would like to see.

#### Customer Success/Sales

[Full list](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AContainer%20Scanning&label_name[]=customer%20success)

If you don't see the `customer success` label on an issue yet, and you are a customer success team-member, feel free to add it!

#### Customer

[Full list](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Category%3AContainer+Scanning&label_name%5B%5D=customer&scope=all&sort=popularity&state=opened&utf8=%E2%9C%93)

If you don't see the `customer` label on an issue yet, feel free to add it if you are the first customer!

#### Internal customer

- [Full list](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AContainer%20Scanning&label_name[]=internal%20customer)

If you don't see the `internal customer` label on an issue yet, and you are a team-member, feel free to add it!

### Top Strategy Item(s)
<!-- What's the most important thing to move your strategy forward?-->

To be determined.