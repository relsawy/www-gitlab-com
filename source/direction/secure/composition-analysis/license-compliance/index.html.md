---
layout: markdown_page
title: "Category Direction - License Compliance"
description: "GitLab's goal is to provide License Compliance as part of the standard development process. Learn more!"
canonical_path: "/direction/secure/composition-analysis/license-compliance/"
---

<!---  using https://gitlab.com/-/ide/project/gitlab-com/www-gitlab-com/edit/master/-/doc/templates/product/category_direction_template.html.md -->

- TOC
{:toc}

## Secure & Protect

| | |
| --- | --- |
| Stage | [Secure](/direction/secure/) |
| Maturity | [Viable](/direction/maturity/) |
| Content Last Reviewed | `2020-09-01` |
| Content Last Updated | `2020-09-01` |

### Introduction and how you can help
Thanks for visiting this category direction page on License Compliance at GitLab. This page belongs to the [Composition Analysis](/handbook/product/product-categories/#composition-analysis-group) group of the [Secure](/direction/secure/) stage and is maintained by [Nicole Schwartz](https://gitlab.com/NicoleSchwartz).

#### Send Us Feedback
We welcome feedback, bug reports, feature requests, and community contributions.

Not sure how to get started?

- Upvote or comment on [proposed Category:License Compliance issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ALicense%20Compliance) - when you find one similar to what you want, please leave a comment AND upvote it! This helps it to be prioritized, backlog items with few unique individuals commenting are unlikely to get reviewed and prioritized.
- Can't find an issue? Make a [feature proposal](https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Feature%20proposal) or a [bug report](https://gitlab.com/gitlab-org/gitlab/-/issues/new?&issuable_template=Bug). Please add the appropriate labels by adding this line to the bottom of your new issue `/label ~"devops::secure" ~"Category:License Compliance" ~"group::composition analysis"`.
<!--- https://gitlab.com/gitlab-org/gitlab/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#) --->
- Consider signing up for [First Look](https://about.gitlab.com/community/gitlab-first-look/).
- Want to get directly into the user interview pool for Secure Stage? Sign up on this [google document form](https://docs.google.com/forms/d/e/1FAIpQLScCNWXVA_gBWQKB51amfmR4ccESsvZPudaEUrhQWVUZFaAf5Q/viewform?usp=sf_link).

Sharing your feedback directly on GitLab.com is the best way to contribute to our direction.

We believe [everyone can contribute](https://about.gitlab.com/company/strategy/#contribute-to-gitlab-application) and so if you wish to contribute [here is how to get started](https://about.gitlab.com/community/contribute/).

### Overview
<!-- A good description of what your category is today or in the near term. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Provide enough context that someone unfamiliar
with the details of the category can understand what is being discussed. -->

License Compliance analyses your application to track which licenses are used by third-party components, like libraries and external dependencies, and check that they are compatible with you policies.

Licenses can be incompatible with the chosen license model for the application, for example because of their redistribution rights.

Our goal is to provide License Compliance as part of the standard development process. This means that License Compliance is executed every time a new commit is pushed to a branch, identifying newly introduced licenses in the merge request. We also include License Compliance as part of [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

Licenses should also be included in a bill of materials (BOM), where all the components are listed with their licenses. See [this issue](https://gitlab.com/gitlab-org/gitlab-ee/issues/7476) for additional details.

#### Target Audience
<!--
List the personas (https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas#user-personas) involved in this category.

Look for differences in user's goals or uses that would affect their use of the product. Separate users and customers into different types based on those differences that make a difference.
-->
Primary: Sasha (Software Developer) wants to know when adding a dependency if it has permitted licenses.

Other: Cameron (Compliance Manager), Delaney (Development Team Lead)

#### Challenges to address
<!--
- What needs, goals, or jobs to be done do the users have?
- How do users address these challenges today? What products or work-arounds are utilized?

Provide links to UX Research issues, which validate these problems exist.
-->
We will be researching current user challenges in [this issue](https://gitlab.com/gitlab-org/ux-research/-/issues/1061). Please feel free to comment!

### Key features

- [10 package managers supported](https://docs.gitlab.com/ee/user/compliance/license_compliance/#supported-languages-and-package-managers)
- [Policy enforcement](https://docs.gitlab.com/ee/user/compliance/license_compliance/#policies) through [merge request approvals](https://docs.gitlab.com/ee/user/compliance/license_compliance/#enabling-license-approvals-within-a-project)
- [Available offline](https://docs.gitlab.com/ee/user/compliance/license_compliance/#running-license-compliance-in-an-offline-environment)

### Strategy

See [Secure 3 Year Strategy](https://about.gitlab.com/direction/secure/#3-year-strategy)

### Where we are Headed
<!--
Describe the future state for your category.
- What problems are we intending to solve?
- How will GitLab uniquely address them?
- What is the resulting benefits and value to users and their organizations?

Use narrative techniques to paint a picture of how the lives of your users will benefit from using this
category once your strategy is at least minimally realized. In order to challenge your level of ambition
(with the goal to make it sufficiently high), link to the current market leaders long-term vision and address how
we plan to displace them. -->
To be determined when focus returns to this category. Currently License Compliance is being maintained but not actively pushed forward while the group concentrates on Dependency Scanning.

#### Roadmap

-[License Compliance Epic Roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&search=%22License+Compliance+category+vision%22)

#### What's Next & Why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->

<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->
To be determined when focus returns to this category.

#### What is Not Planned Right Now
<!-- Often it's just as important to talk about what you're not doing as it is to
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand.-- > ----->
Anything other than bug fixes and improvements for internal customers.

#### Maturity Plan

<!-- It's important your users know where you're headed next. The maturity plan
section captures this by showing what's required to achieve the next level. The
section should follow this format:

This category is currently at the XXXX maturity level, and our next maturity target is YYYY (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/product-categories/maturity/#legend)).

- Link to maturity epic if you are using one, otherwise list issues with maturity::YYYY labels) -->
This category is currently at the Viable maturity level, and our next maturity target is not set (see our [definitions of maturity levels](https://about.gitlab.com/direction/maturity/)

 - [License Compliance - Viable to Complete](https://gitlab.com/groups/gitlab-org/-/epics/1662)

#### User success metrics
<!--
- What specific user behaviors are indicate that users are trying these features, and solving their problems?
- How will users discover these features?
-->
Currently we have very limited product analytics data, as a result we will be tracking number of times that our scans run.

In the future we hope that users will allow us to enhance our product analytics to be able to record information such as the number of findings that are dismissed vs. accepted. See our current metrics [here](https://about.gitlab.com/handbook/product/secure-and-protect-section-performance-indicators/#securecomposition-analysis---gmau---users-running-any-sca-scanners), [and other items being discussed in this issue](https://gitlab.com/groups/gitlab-org/-/epics/445).

#### Why is this important?
<!--
- Why is GitLab building this feature?
- What impact will it have on the broader devops workflow?
- How confident are we? What is the effort?
-->
Users have indicated they which to enforce license compliance policies as early as possible, in addition this contributes to the request that users have for producing a software bill of materials (SBoM).

### Competitive Landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/product-processes/#customer-meetings). We’re not aiming for feature parity with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

- [Black Duck](https://www.blackducksoftware.com/solutions/open-source-license-compliance)
- [Sonatype Nexus](https://www.sonatype.com/nexus-auditor)
- [Whitesource](https://www.whitesourcesoftware.com/open-source-license-compliance/)
- [snyk](https://snyk.io/product/open-source-license-compliance/)

### Analyst Landscape
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

[GitLab was named a Niche Player in the 2020 Gartner Magic Quadrant for AST.](https://about.gitlab.com/resources/report-gartner-mq-ast/)

The License Compliance topic is often coupled with Dependency Scanning in Software Composition Analysis (SCA). This is what analysts evaluate, and how it is bundled in other products. As defined in our [Solutions](https://about.gitlab.com/handbook/product/categories/index.html#solutions).

We should make sure that we can address the entire solution even if we consider these features as independent, and to leverage the single application nature of GitLab to provide a consistent experience in all of them.

We can get valuable feedback from analysts, and use it to drive our vision.

- [Forrester](https://www.forrester.com/report/The+Forrester+Wave+Software+Composition+Analysis+Q1+2017/-/E-RES136463)

### Top Issue(s)

I base this on `popularity`, so please remember to comment AND upvote issues you would like to see.

#### Customer Success/Sales

[Full list](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ALicense%20Compliance&label_name[]=customer%20success)

If you don't see the `customer success` label on an issue yet, and you are a customer success team-member, feel free to add it!

#### User

[Full list](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ALicense%20Compliance&label_name[]=customer)

If you don't see the `customer` label on an issue yet, feel free to add it if you are the first customer!

## Internal customer

[Full list](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ALicense%20Compliance&label_name[]=internal%20customer)

If you don't see the `internal customer` label on an issue yet, and you are a team-member, feel free to add it!

### Top Strategy Item(s)
<!-- What's the most important thing to move your strategy forward?-->
To be determined.
