---
layout: markdown_page
title: "Section Direction - Ops"
description: "The GitLab Ops Section comprises the Configure and Monitor stages of the DevOps lifecycle. Find more information here!"
canonical_path: "/direction/ops/"
---

- TOC
{:toc}

![Ops Overview](/images/direction/ops/ops-overview.png)

<!-- source: https://docs.google.com/presentation/d/1606LcicYP-QKMjHHzTEvZSrx5qkW9LpHiKQUWlggdEk/edit#slide=id.g82767e51d3_1_0 -->

## Ops Section Overview

<figure class="video_container">
    <iframe src="https://www.youtube.com/embed/MYIl4L71CXA" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

### Composition

The Ops Section is comprised of the [Verify](/direction/ops/#verify), [Package](/direction/ops/#package), [Release](/direction/ops/#release), [Configure](/direction/configure) and [Monitor](/direction/monitor) [stages of the DevOps lifecycle](/stages-devops-lifecycle/).
[Market analysts (internal - i)](https://drive.google.com/file/d/1B2KCSj_PF8ZhZqx_8Q6zFU2vldMu94lT/view) often describe these stages as automated software quality (ASQ, partially correlated with Verify, Package and Release) IT automation and configuration management (ITACM, correlated with Configure) and IT operations management (ITOM, partially correlated with Monitor). The section also covers CI/CD (Verify/Release), IT Infrastructure Monitoring/APM (Monitor), CDRA (Release), Infrastructure Configuration (Configure) and Package and Binary Management (Package) [analyst categories](/analysts/).

In some contexts, "ops" refers to operators. Operators were the counterparts to Developers represented in the original coining of the term [DevOps](https://en.wikipedia.org/wiki/DevOps). That definition highlighted the rift between the two groups and a need for collaboration. At GitLab, we use "ops" to mean operations - any component of the software delivery value stream after a developer commits code. Our [developer first perspective](/handbook/product/product-principles/#developer-first) means our use of the word ops is focused on enabling developers to configure tools and perform operations tasks **first**. We have ambitious plans to [support operators in the future](#medium-term-1-2-years).

### Market

The total addressable market (TAM) for **DevOps tools** targeting these stages was [$3.2B in 2018 and is expected to grow to $7.6B by 2022 (18.8% CAGR) (i)](https://docs.google.com/spreadsheets/d/1LO57cmXHDjE4QRf6NscRilpYA1mlXHeLFwM_tWlzcwI/edit?ts=5ddb7489#gid=947614257). This analysis is considered conservative as it focuses only on developers and doesn't [include additional potential users](/handbook/sales/tam/).
This is a deep value pool and represents a significant portion of GitLab's expanding addressable market.  As organizations further adopt DevOps, developer focused ops tools account for a larger share of a market. This market has traditionally targeted IT Architects, System Admins and Operators where large hardware budgets enabled expensive IT tools to flourish.

The [market is well established (i)](https://drive.google.com/file/d/1VvnJ5Q5PJzPKZ_oYBHGNuc6D7mtMmIZ_/view), but the participants are evolving rapidly. Existing leaders in single stages of the DevOps lifecycle are expanding to build [value stream delivery platforms (i)](https://drive.google.com/file/d/1HuRjVqH4D8qEuWkhv77XT6PEnnpml2gn/view?usp=sharing), what GitLab calls complete DevOps platforms delivered as a single application. These existing players are doing so via acquisition and product development. The leaders run the gamut from GitHub with their strong position against the Create stage and growing capabilities to [compete with Verify](/direction/verify/continuous_integration/#competitive-landscape), to [DataDog and New Relic with their strong positions against Monitor](/direction/monitor/#landscape). All are pivoting to pursue a value stream delivery platforms market.

Expanding market players such as GitHub are taking market share from traditional participants like CloudBees in the Verify stage. In the Package stage, JFrog is looking to expand their leadership position in artifact repositories into other stages of the DevOps lifecycle. In the Release stage Spinnaker is a new open-source, fast growing entrant enabling CD workflows. Harness has capabilities which span Verify, Release and they have taken an integrative approach covering the Configure and Monitor stages. Both Spinnaker and Harness are taking market share from traditional Application Release Orchestration vendors. Splunk, New Relic and Datadog are dominating market share in the Monitor stage and fiercely competing to be a central monitoring platform. IBM (+RedHat), HashiCorp, Puppet and Chef share a large chunk of the fragmented market in the Configure stage.

### Current Position
Despite many strong players, GitLab's market share in the Ops section is growing, especially in the Verify, Package and Release stages. For the Configure and Monitor stages, our unique perspective as a single and complete DevOps application positions us for growth.
Our customers utilize many [Ops Section stages](/handbook/product/ops-section-performance-indicators/#regular-performance-indicators) today.
Primarily they:
* Have adopted CI best practices and are defining their CI/CD pipelines as code
* Are adopting our Release stage features as a critical component of their DevOps journey, along with new technology platforms to realize the benefits of CD and progressive delivery
* Recognize Package stage features as essential to their delivery and are considering migrating from Artifactory
* Utilize GitLab's core features in Create and Verify to manage Infrastructure as Code
* Value our cluster integration with Kubernetes
* Are interested in bringing Incident & Alert Management processes into GitLab to encourage collaboration within their team
* Are interested in bringing monitoring of cloud native applications into GitLab

Our current R&D investment in Ops Section stages is [large](/company/team/?department=ops-section).
The maturity of each stage and category can be found in our [maturity page](/direction/maturity/). Our investment in the Ops section is critical to both enabling enterprise users to continue on their DevOps maturity journey and completing our [complete DevOps platform vision](/direction/#vision). Driving adoption across multiple Ops stages enables [early adopters](/handbook/product/product-principles/#prioritize-current-adopters) to recognize the [benefit of a single application](/handbook/product/gitlab-the-product/#single-application).

We would appreciate your feedback on this direction page. Please take [our survey](https://forms.gle/ex9zZ4sKmE1FKiHr9), [email Kenny Johnston](mailto:kenny@gitlab.com) or [propose an MR](https://gitlab.com/gitlab-com/www-gitlab-com/edit/master/source/direction/ops/template.html.md.erb)to this page!

## Challenges
GitLab is competing in a new market of [value stream delivery platforms (VSDP)(i - Gartner)](https://drive.google.com/file/d/1QAuhdt9xE761ItGJM3Jx6I6TdMAhHYrj/view). We were an early entrant in this market [before it was defined](/handbook/product/single-application/) but there are many, and will be more, fast followers. Key players include large established tech firms (Microsoft) as well as newly consolidated platforms from aquisition (JFrog, CollabNet/VersionOne/XebiaLabs). [By 2023, the number of companies switching to VSDPs is expected to quadruple (i)](https://drive.google.com/file/d/1HuRjVqH4D8qEuWkhv77XT6PEnnpml2gn/view?usp=sharing).

Beyond the key players, there is also rapid innovation. This makes for a market where there is proliferation of new technology concurrent to consolidation of the winning technologies into comprehensive platform players. Ease of deployment for CI/CD and operational tools has aided this expansion. Developers and operations teams can easily instrument and utilize new technologies alongside their existing software development pipelines with less risk, and in quicker time frames than under previous models where it required diligent planning and months of installation and configuration.

Competing tools are typically marketed as stand-alone point solutions. In contrast, GitLab's capabilities shine when customers use other stages of GitLab. This can create a narrow funnel for adoption with potential users coming only from the segment of the market already using, or willing to use, other stages of GitLab rather than broad-based market fit. Without developing more landing spots for new users of GitLab, this smaller adoption funnel will affect our rate of learning.

Few companies have been able to successfully bridge between Developer and Operator personas. Building tools that satisfy both sets of [jobs-to-be-done](/handbook/engineering/ux/jobs-to-be-done/getting-started-with-jobs-to-be-done/) is difficult. Without deeply understanding new personas and tasks, market entrants end up with a checklist of modules that don't represent a cohesive and improved experience for users.

In recent years we've seen the emergence of large [public cloud infrastructure providers](https://en.wikipedia.org/wiki/Cloud_computing#Public_cloud) moving from a focus on infrastructure services for operators towards developer tools. These providers could challenge current notions of [multi-cloud](https://en.wikipedia.org/wiki/Multicloud) by creating great, integrated experiences in their tools for developers to create, verify, and deploy applications to the provider's infrastructure. There is a possibility that these provider-centric ecosystems present organizations with a choice of investing in a best-of-breed platform with a small subset of projects or a just sufficient enough platform used by all.

Outside of market challenges we have some internal ones as well.
* In general, we are not [dogfooding](/handbook/values/#dogfooding) the Release, Monitor and Configure stage features sufficiently. This has the effect of [slowing our rate of learning](/handbook/values/#dogfooding), and putting us in the position of not having immediate real-world validation of our product investments.
* We've only recently begun [measuring our investments for effectiveness via our Product Performance indicators](/handbook/product/ops-section-performance-indicators/). Until we've developed an understanding of how to drive those metrics, there will remain room for improvement in the effectiveness of our R&D investment.

## Opportunities
Given these challenges, here are some key opportunities we must take advantage of:

* **DevOps Adoption Pains:** For many organizations their [DevOps journey is currently stalled as they attempt to adopt continous delivery in an effort to increase their deployment frequency](https://services.google.com/fh/files/misc/state-of-devops-2019.pdf). GitLab's [common method for defining CI and CD workflows](https://docs.gitlab.com/ee/ci/) allows organizations to use the same tools their teams love for CI when overcoming this new challenge. Even elite DevOps organizations struggle to bring measurement of their entire software delivery performance into one tool, GitLab's combination of Create, Verify, Release and Monitor allow our users to [measure all four aspects of their delivery performance](https://gitlab.com/groups/gitlab-org/-/epics/4358). DevOps transformations are hampered by a [lack of IT operations skills (i)](https://docs.google.com/presentation/d/1tt6-XqnhCmsx8Jfy39hit8OgggeaFUUxxU7TnNsG4Qk/edit#slide=id.p19). Most organizations have taken to creating [infrastructure platform teams](https://hackernoon.com/how-to-build-a-platform-team-now-the-secrets-to-successful-engineering-8a9b6a4d2c8) to [minimize the amount of operations expertise required by DevOps team members (i)](https://drive.google.com/file/d/1GexcUo4FzmhmV30tnjn7x3hyk1plEK4-/view). Those teams traditional operations tools are not integrated with development, and force an unneeded tradeoff between reliability and speed. This tradeoff results in increased down-time to problems with recovering from disaster - or slowed business agility. Value Stream Delivery Platforms like GitLab can [move an team from reactive to proactive reliability without sacraficing development velocity (i)](https://drive.google.com/file/d/1hJq-JKveNbxA2FryoBxcurMRZuYJ3b4e/view?usp=sharing).
* **Market Recognition:** The IT market has recognized the value of GitLab's core proposition, a [complete DevOps platform delivered as a single application](/handbook/product/single-application/). We know because single-function companies [are consolidating](https://azure.microsoft.com/en-us/blog/accelerating-devops-with-github-and-azure/), and analysts have [created a new categories - VSDP](https://www.gartner.com/en/documents/3979558/the-future-of-devops-toolchains-will-involve-maximizing-) - to describe that proposition. One key value driver for this market is the shifting-left of traditional Ops tasks ensuring reliability doesn't have to come at the expense of speed.
* **Increasing Developer buying power:** In the move to DevOps, [tools focused on enabling developers will garner increased share of wallet](http://videos.cowen.com/detail/videos/recent/video/6040056480001?autoStart=true). [71% of organizations (i)](https://docs.google.com/presentation/d/1tt6-XqnhCmsx8Jfy39hit8OgggeaFUUxxU7TnNsG4Qk/edit#slide=id.p4) will achieve full DevOps adoption on some projects within a year. [35% of DevOps implementations include Monitor stage capabilities, 30% include Configure stage ones (i)](https://docs.google.com/presentation/d/1tt6-XqnhCmsx8Jfy39hit8OgggeaFUUxxU7TnNsG4Qk/edit#slide=id.p14). This increasing buyer power is driven by the cultural and organizational changes (often called Digital Transformation Initiatives) needed for DevOps to deliver software faster.
* **Ahead of the game with Kubernetes:** Driven by software-defined infrastructure, cost management, and resiliency, organizations [are flocking to cloud-native application architectures (i)](https://drive.google.com/file/d/1ZAqTIiSfpHKyVFpgMnJoOBnyCQ-aF3ej/view). Kubernetes is the clear winner in container orchestration platforms. Kubernetes enables easy portability of code, and CI/CD platforms built on containers (like GitLab CI/CD) are leading the market. Kubernetes also provides a reliable, [multi-cloud abstration point](https://www.infoworld.com/article/3439824/kubernetes-is-bringing-multicloud-20.html) which can blunt the challenge from [public cloud providers](https://en.wikipedia.org/wiki/Cloud_computing#Public_cloud). [Gartner recommends (i)](https://drive.google.com/file/d/19MZsKTc8tvAAFQEWuxDGoTW1f_kCZ6GI/view) organizations choose vendors that support open-source software projects in the cloud-native ecosystem ([CNCF](https://www.cncf.io/)). Organizations are reporting [high ROI when moving to cloud-native development platforms](https://content.pivotal.io/blog/the-economics-of-cloud-native-go-much-deeper-than-sticker-price), and analyst firms predict [35% of applications will be cloud-native by 2022 (i)](https://drive.google.com/file/d/1em_oqA5ciPDmTxHGCHgwbv4t-38uJjt1/view).
* **IaC and GitOps are strong first steps towards AIOps:** AIOps strives to provide self-learning, self-healing IT operations massively impacting IT operations professionals and their tools. However, it is [early in the hype cycle (i)](https://drive.google.com/file/d/1GexcUo4FzmhmV30tnjn7x3hyk1plEK4-/view) and being successful will depend on DevOps teams first defining their infrastructure, operations, and observability as code. IT Operations teams are first adopting automation and programmable infrastructure in order to keep up with developer velocity. Experience with developer tools like source control and CI/CD tooling are becoming requirements for operations teams. GitLab's roots in SCM and CI and the entire GitOps, Infrastructure as Code, and Observability as Code movements will help set us up for future AIOps success.
* **Continuing commoditization of tooling:** Well funded technology companies (Google, Netflix, Lyft, Spotify etc.) that are focused on meeting scaling challenges have a history of releasing open source software that leap-frog vendor software. Examples include Kubernetes, Prometheus, and many of the foundational projects in CNCF. DevOps vendors have to continue to move up the value chain as previously specialized software gets commoditized.

## 3 Year Strategy
GitLab's Ops strategy follows that of [GitLab's company strategy](/company/strategy/). Just like our company [mission](/company/strategy/#mission), we will enable **everyone to contribute** beyond application code to other digital artifacts that increasingly define the performance, reliability, and resilience of the world's software. We will pursue that mission and capitalize on the opportunities (such as, developer buyer power, IT skills gaps, a move towards Kubernetes, and our roots in source-control and CI) by utilizing a [dual-flywheel](/company/strategy/#dual-flywheels) approach. This approach starts with attracting developers performing DevOps tooling and operations tasks to our Free/Core tier. As we build best of breed tools for them we will co-create with the community to drive product improvements. This will generate revenue for higher product tiers and additional investment for supporting the modern development teams.

More specifically, we will achieve this by enabling easy-to-discover workflows that support doing powerful, complex actions with a minimum of manual configuration. We want to take advantage of our single application so that, while each team may have their own views or dashboards in the product that support their day to day, the information about what is deployed where is available everywhere and to everyone, embedded naturally into the product where it's relevant. For example, a person thinking about upcoming releases may interact mostly with an environment-based overview that helps them see upcoming changes and alerts as they flow through environments, but that same information exists everywhere it is relevant:

* Testers looking at an individual issue can see which environment(s) that issue has been deployed to
* Developers reviewing a merge request have the Review App at their fingertips
* Operational tasks that ensure reliability, like chaos engineering and performance testing, are shifted left
* Feature flags link back to the issues and merge requests that introduced them for context, and provide views into the metrics needed to decide whether the feature was a success
* Developers can effect infrastructure changes with the same workflow as code changes
* Upcoming releases have burndown charts right in the overview
* Common deployment targets, including kubernetes and non-kubernetes, are native and have workflows enabled via executable runbooks
* Empowering ease of satisfying compliance and audit needs by collecting artifacts naturally during the release process
* At scale management of deployments with actionable analytics and dashboards to support management of development teams
* Incident responders can immediately see details of the last deploy, recent code changes and errors, traces and logs needed to help them get their service back up quickly
* Evidence collection for auditors happens automatically throughout the pipeline, with nothing special to set up

The end result is that even complex delivery flows become part of everyone's primary way of working. There isn't a context shift (or even worse, a switch into a different tool) needed to start thinking about delivery or operations - that information is there, in context, from your first commit. The centerpiece of this strategy is our [Get Things Done Easily theme](/direction/ops/#get-things-done-easily).

### Market Predictions
In three years the Ops market will:
* Consider Package management an integrated requirement for any CI/CD platform
* Provide seamless progressive delivery capabilities as a critical tool for enabling lean product-focused development
* Consolidate around Kubernetes as the de facto multi-cloud abstraction layer which application delivery teams interact with to deploy and maintain software
* While we're current building tools aligned to the separate nature of infrastructure platform delivery professionals and DevOps application delivery professionals, we are also [looking to a potential future where these can converge](https://www.youtube.com/watch?v=TwK7mReFks0).
* Have adapted DevOps processes to also include infrastructure and observability into CI/CD pipelines providing more responsive, more secure, and higher quality production applications

### GitLab Goals
As a result, in three years, the Ops stages in GitLab will:
* Be the market leader in CI/CD tooling, including making it easy to migrate to GitLab CI/CD from other tools and adding value to development leaders in large organizations with complex CI/CD requirements
* Build the first complete progressive delivery solution
* Be considered a critical tool for cloud-native development teams, and the infrastructure platform delivery teams which support them
* Enable strong collaboration between application and platform teams
* Enable Package and binary registries to play a central security, governance and control point in the software development process
* Provide easy adoption of IaC deployment patterns so DevOps teams have more insight and responsiveness to their application's production capabilities
* Be recognized for the flows that connect jobs-to-be-done across the product delivery process from commitment to customer
* Provide a best-in-class monitoring solution for observability use cases which competes with market leaders

## Themes
Our direction for Ops is to enable today's modern best practices in operations without the burden of specialized teams, multiple tools, and heavy workflows that are the largest barriers to adoption in these stages of the DevOps lifecycle.

Our goal is to empower DevOps teams to own their code's path to production as well as the performance of their production application itself. We also want to ensure they have the tools to contribute to feature development and the complete end-user experience of their application.

Our themes are directly aligned to our overall [Product Principles](/handbook/product/product-principles/) especially:
* [Convention over Configuration](https://about.gitlab.com/handbook/product/product-principles/#convention-over-configuration) - which was use to make [adopting GitLab's Ops Section capabilities](##user-adoption-journey) easy by making them configured for best practices out of the box
* [Single Application](/handbook/product/single-application/) - which we use to make the collaboration between development and operations tasks and personas delightful
* [Plays well with Others](/handbook/product/gitlab-the-product/#plays-well-with-others) - which drives us to ensure our [Runner works across platforms](/direction/verify/runner/#multi-platform-support), we support generic [Kubernetes integrations](/direction/configure/kubernetes_management/), build [native capabilities](/direction/release/continuous_delivery/#deployment-with-auto-devops) to support [common deployment patterns](/direction/configure/infrastructure_as_code/#collaboration-around-infrastructure-changes) and [we integrate generically with common alert management](/direction/monitor/debugging_and_health/alert_management/#challenges) and other [operational tools](/direction/monitor/apm/metrics/#getting-data-in).

In addition to our broad product themes there are some specific call-outs relevant for the Ops Section stages.

<%= partial("direction/ops/themes/speedy_pipelines") %>

<%= partial("direction/ops/themes/progressive_delivery") %>

<%= partial("direction/ops/themes/ops-as-code") %>

<%= partial("direction/ops/themes/ops-for-all") %>

<%= partial("direction/ops/themes/smart-feedback-loop") %>

## Performance Indicators
The performance of the Ops section is defined as the total adoption of its stages as measured by the [Ops Section Total Stages Monthly Active Users](/handbook/product/ops-section-performance-indicators/#ops---section-tmau---sum-of-ops-section-smau)) (Ops Section TMAU). Ops Section TMAU is a sum of all SMAUs across the five stages in the Ops Section (Verify, Package, Release, Configure & Monitor).

To increase the Ops Section TMAU, the product groups are focused on driving their stage adoption. We are especially focused on increasing adoption of the Verify and Release stages by driving CI usage from existing SCM users and driving CD usage from existing CI users. This will activate users into Ops Section TMAU and enable them to continue to adopt other Ops section stages as described on Ops section adoption journey (see also the [product-wide adoption journey](https://about.gitlab.com/handbook/product/growth/#adoption-journey)):

```mermaid
graph LR

    %% Define Styling
    classDef OpsStage fill:#11f,color:#fff
    classDef OtherStage fill:#ddd,color:#888

    %%% Define Stages
    P[Plan]:::OtherStage
    C[Create]:::OtherStage
    V[Verify]:::OpsStage
    Pa[Package]:::OpsStage
    R[Release]:::OpsStage
    Co[Configure]:::OpsStage
    M[Monitor]:::OpsStage
    S[Secure]:::OtherStage
    D[Protect]:::OtherStage

    %% Define Connections
    C -.- P
    P --> M
    V -.- S
    C ==> V
    V ==> R
    V ==> Pa
    V --> Co
    R ==> M
    R ==> Co
    Co -.- D
    Co ==> M
    M -.- D

    subgraph "Legend"
       Legend1[Ops Stage]:::OpsStage == Critical Path ==> Legend2[Other Stage]:::OtherStage
       Legend1 -. Additional Path .-> Legend2
    end
```

The majority of our [Ops section product groups](/handbook/product/product-categories/#ops-section) are focused on driving adoption first, and then monetization within their category scope. Some groups are well placed to focus on monetization right away by adding tier value for [buyer personas](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/buyer-persona/) (e.g. development manager, director) on top of existing stages which already have heavy usage. Those groups will adopt Stage Monthly Active Paid Users (SMAPU) as their North Star Metric.

See more details about our [Product Performance Indicators](/handbook/ceo/kpis/#layers-of-kpis) in our [Ops Section Product Performance Indicators page](/handbook/product/ops-section-performance-indicators/).

### Critical Jobs to Be Done
GitLab will focus on the [jobs to be done](/handbook/engineering/ux/ux-resources/#jobs-to-be-done) (JTBD) found in the workflows identified across all of our stages. Today we've identified those critical JTBD in the [Package](/handbook/engineering/development/ops/package/jtbd/), [Release Management](/handbook/engineering/development/ops/release/release-management/jtbd/), Configure and [Monitor](/direction/monitor/#workflows) stages.

The most critical are:
* Deploying [initial cloud-native infrastructure and CI/CD process](/direction/configure/#auto-devops)
* [Creating centralized cloud-native platforms](/direction/configure/kubernetes_management/) for development teams
* Managing [infrastructure change](/direction/configure/infrastructure_as_code/)
* [Triaging](/direction/monitor/workflows/triage/) application performance and availability degradations
* [Resolving](/direction/monitor/workflows/resolve/) application incidents

## Who is it for?
We identify the [personas](/handbook/marketing/product-marketing/roles-personas/#user-personas) the Ops section features are built for. In order to be transparent about personas we support today and personas we aim to support in the future we use the following categorization of personas listed in priority order.
* 🟩- Targeted with strong support
* 🟨- Targeted but incomplete support
* ⬜️- Not targeted but might find value

### Today
To capitalize on the [opportunities](#opportunities) listed above, the Ops section has features that make it useful to the following personas today.

1. 🟩 [Sasha - Software Developer](/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer)
1. 🟩 [Devon - DevOps Engineer](/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer)
1. 🟨 [Rachel - Release Manager](/handbook/marketing/product-marketing/roles-personas/#rachel-release-manager)
1. 🟨 [Simone - Software Engineer in Test](/handbook/marketing/product-marketing/roles-personas/#simone-software-engineer-in-test)
1. 🟨 [Allison - Application Ops](/handbook/marketing/product-marketing/roles-personas/#allison-application-ops)
1. 🟨 [Delaney - Devleopment Team Lead](/handbook/marketing/product-marketing/roles-personas/#delaney-development-team-lead)
1. 🟨 [Priyanka - Platform Engineer](/handbook/marketing/product-marketing/roles-personas/#priyanka-platform-engineer)
1. ⬜️ [Ops Teams](#composition)
1. ⬜️ Central IT / System Admins

### Medium Term (1-2 years)
As we execute our [3-year strategy](#3-year-strategy), our medium term (1-2 year) goal is to provide a single application that enables collaboration between cloud native development and platform teams.

1. 🟩 [Sasha - Software Developer](/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer)
1. 🟩 [Devon - DevOps Engineer](/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer)
1. 🟩 [Allison - Application Ops](/handbook/marketing/product-marketing/roles-personas/#allison-application-ops)
1. 🟩 [Simone - Software Engineer in Test](/handbook/marketing/product-marketing/roles-personas/#simone-software-engineer-in-test)
1. 🟩 [Rachel - Release Manager](/handbook/marketing/product-marketing/roles-personas/#rachel-release-manager)
1. 🟩 [Delaney - Devleopment Team Lead](/handbook/marketing/product-marketing/roles-personas/#delaney-development-team-lead)
1. 🟩 [Priyanka - Platform Engineer](/handbook/marketing/product-marketing/roles-personas/#priyanka-platform-engineer)
1. 🟨 [Ops Teams](#composition)
1. ⬜️ Central IT / System Admins

## One Year Plans

### Ops Section Plan

Our plan is quite simple. We see a common progression in all of our [GitLab stages and categories](/direction/maturity/). That progression is:
1. Create MVCs
1. Leverage the high rate of learning provided by dogfooding to achieve viability
1. Shift focus to awareness and aquisition of viable capabilities increasing SMAU
1. Begin building completeness and tier value as a result of increased SMAU

As a result our [performance indicators](#performance-indicators) and tier strategies are aligned to the maturity of each stage.

In doing so, over the next 12 months we will choose **NOT** to:

* Invest in Verify stage features like:
  * [Tracking accessibility results over time](https://gitlab.com/gitlab-org/gitlab/issues/36171).
  * [Directly connecting the IDE to the Visual Review App](https://gitlab.com/gitlab-org/gitlab/issues/119127).
  * [Automatic flaky test minimization](https://gitlab.com/gitlab-org/gitlab/issues/3583).
* Invest in some specific Package stage features like:
  * [Dependency Firewall](/direction/package/dependency_firewall), as we'll focus on making the [Dependency Proxy](/direction/package/dependency_proxy) functional and complete.
  * Package manager formats and features that are not NPM, Maven, C++, .NET, Linux, Ruby, Python, PHP, or Go or community contributed.
* Invest in specific Release stage features like:
  * [Service Now Integration](https://gitlab.com/gitlab-org/gitlab/issues/8373) to control rollbacks or other pipeline actions is not in the cards.
  * Pages improvements such as the one to [improve experience around GitLab Pages forking vs. templates ](https://gitlab.com/gitlab-org/gitlab/issues/197172)
  * [Mobile Publishing](https://gitlab.com/dashboard/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Arelease%20management&label_name[]=mobile%20publishing) will likely also not be leveled up, along with other mobile-focused features such as [feature-flags](https://gitlab.com/gitlab-org/gitlab/issues/24984) or [review apps specifically for mobile](https://gitlab.com/gitlab-org/gitlab/issues/199108).
* Invest heavily in ease of use and experience for individuals attaching Kubernetes clusters to projects. While there are use cases for project-level attachment, it is a higher priority to provide Infrastructure Platform teams with an enhanced cloud-native management experience.
* Invest heavily in enhanced out-of-the-box configuration of monitoring including auto-anomaly detection and synthetic monitoring - We need to focus first on dogfooding and the nitty-gritty of the three pillars of observability before moving to more advanced monitoring capabilities.
* Invest in app instrumentation across a number of legacy programming languages - We will pursue a modern first approach, meaning we will not focus on easy instrumentation of apps written in legacy programming languages.
* Invest heavily in automated cluster cost-optimization efforts - We will pursue this as our adoption by platform teams increases.

### Acquisition Plan

GitLab pursues [acquisitions](handbook/acquisitions/) which will help accelerate our product roadmap. There are a few categories across the Ops Section stages which have a [higher appetite for acquisition](/handbook/acquisitions/#acquisition-target-profile). We look for one of two things:
1. Planned future category maturity increments where the jump from minimal or viable to complete or lovable would nessecitate significant R&D investment and an acquisition would accelerate that maturity significantly.
1. Filling gaps in our [themes](#themes) that span our current categories.
As a result our top priorities are:

Top priorities:
1. CI Testing - Tools that immediately increase our capability in a Verify:Testing group category AND provide a framework for an over-time and cross-project view of testing results.
1. Cluster Cost Optimization - Tools that provide alerting and dashboarding for attached Kubernetes cluster cost optimization.
1. Observability - Tools that immediately increase our capabilities in tracing and digital experience management. We are specifically focused on improving the out-of-the-box experience for instrumenting and setting up triage workflows of users' applications.

Additional options:
1. Smart Feedback Loop - Tools that enable the continuous improvement of applications via efficient feedback loops from production to the Plan and Create stages, including product product analytics, user forums, suggestion mechanisms, and real-user monitoring. 
* Operations as Code - Tools and software that improve the ability to define observability systems and repeatedly deploy them alongside application code.
* Operations for All - Tools that help developers quickly understand complex infrastructure setups, including service catalogs, cluster cost optimization, and observability interfaces.

## Useful Resources
Below are some additional resources to learn more about Ops:
* [The DevOps Handbook: How to Create World-Class Agility, Reliability, and Security in Technology Organizations](https://www.amazon.com/dp/1942788002/)
* [Accelerate: The Science of DevOps: Building and Scaling High Performing Technology Organizations](https://www.amazon.com/Accelerate-Software-Performing-Technology-Organizations/dp/1942788339)

## Stages and Categories

The Ops section is composed of five stages, each of which contains several categories. Each stage has an overall strategy statement below, aligned to the themes for Ops. Each category within each stage has a dedicated vision page plus optional documentation, marketing pages, and other materials linked below.

<%= partial("direction/ops/strategies/verify") %>

<%= partial("direction/ops/strategies/package") %>

<%= partial("direction/ops/strategies/release") %>

<%= partial("direction/ops/strategies/configure") %>

<%= partial("direction/ops/strategies/monitor") %>

## What's Next

See what is coming across all of the DevOps stages in our [Upcoming Releases](/upcoming-releases/) page.