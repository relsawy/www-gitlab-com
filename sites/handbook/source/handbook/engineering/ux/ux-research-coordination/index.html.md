---
layout: handbook-page-toc
title: "UX Research Coordination"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Research Coordination at GitLab


### How many studies can a coordinator support per milestone?

The short answer is approximately 5-10. The long answer is that it depends on the type of study and the target population. When each study only requires 3-5 participants who are relatively abundant in our database and community, the number of studies recruited per milestone could be ~10. This is also contingent upon the Product Designer and Product Manager submitting their complete requests early enough to allow coordinators to juggle multiple recruiting efforts at once. When there are surveys that require 50+ respondents, or studies addressing new user groups or features, the number of studies recruited per milestone may be closer to 5. Those studies require sustained effort and creativity. 

Never hesitate to bring up a recruitment request - the coordinator will work with you to schedule the recruiting effort. 

### How does the coordinator plan ahead?

When the number of requests exceeds 10, the coordinator is at over capacity.  When this occurs, there are too many projects to successfully manage at once, and the following side effects are experienced internally and externally to GitLab:

* Timelines slip
* Outbound communications from the Research Coordinator are reduced, resulting in the feeling of going dark
* Inbound communications to the Research Coordinator are increased, from people wanting to know statuses and help with next steps
* Errors occur in the process

To deal with situations of over capacity, we have two plans of action:

#### 1) Quarterly planning

Given the nature of how we conduct research at GitLab, it’s difficult to have an accurate view into the pipeline at any given time to understand what research projects will be coming up when.  However, we can rely on research project volume data from the past and predict the cadence of some projects quarterly to plan accordingly.  An example of identified projects that require extra coordination are:

* SUS measuring
* Category Maturity Scorecard testing
* Large N surveys (n=200+)

A quarterly planning session is conducted at the start of each quarter.  The planning exercise maps out the upcoming research requests, by month, with known research projects, predicted research projects, upcoming events requiring research participants, anticipated changes to our processes (ex: new hires, unmoderated testing, etc), and coordinator unavailability.  In addition, check-ins at the beginning of each month take place between Research Coordination and the UX Research Director.

The goal with the quarterly planning exercise is to have a fairly accurate view into the quarter at any given time, be able to identify any potential delivery risks, and to start early on addressing those risks with mitigation plans.

The coordinator is responsible for driving and maintaining the research coordination quarterly planning exercise, the monthly check-ins, and keeping the Google Sheet up to date and accurate.

#### 2) A back-up plan

To keep research projects on track, each coordinator can fill-in for each other, should one become unavailable.  Additionally, at least 2 other UX Researchers are trained to work as coordinators when coordinators are unavailable or if they are at over capacity and need additional assistance.

The coordinator is responsible for: 

* Developing training materials to train UX Researchers 
* Conducting the training

Example coverage areas for training:

* Recruiting intake process (ex: how to respond to all parties, Service Level Agreement (SLA), expectations of a coordinator, etc)
* Communication guidelines when reaching out to panelists or recruitment sources (provide examples for each, special logins/accounts to use, etc)
* Handling gratuities for different countries
* Understanding the different types of issues created for a recruiting request vs. gratuity request - and wrapping those up

Ideally, a back-up plan is rarely used if proper planning has been done.

### When over-capacity happens

Even with accurate planning, over-capacity can still occur.  When this happens, coordinators have several actions they can take:

* **Inform the team - Be transparent.**  Let the #ux_research channel know, along with managers, that research coordination is over capacity at the moment along with an estimated timeframe on when they expect things to get back to normal. This automatically sets expectations for existing and new requests.  Lastly, follow-up with a post when capacity returns to normal to let the channel know.

* **Reset expectations on any deliverable dates** - Communicate to the key people on the projects impacted that their deliverable dates may change.

* **Inform participants - Be transparent.**  If participants are negatively impacted, let them know that we’re busy, express a sincere apology, and reset timing expectations with them.

* **Ask for help** - Research Coordinators need to ask their trained UX Research Team back-ups for help. Research Coordinators will need to delegate the right duties to the UX Researchers to result in the biggest impact.

* **New incoming requests will take longer** - We don’t stop taking on new research when we’re at over capacity.  Instead, we set expectations early with new requests.  New requests will be buffered accordingly to accommodate being at over capacity and project owners are educated as to why.

* **Encourage self-service** - Research Coordinators can point people to self-serve resources with a link to instructions in the handbook (ex: using Respondent and User Interviews to find participants, etc).

### When UXR Coordinators are unavailable

Since Research Coordinators have unique duties that are critical to the success of research at GitLab, when they are unavailable, recruitment efforts slow down.  This results in a series of problems for all parties.

To address this, two UX Researchers are available to act as back-up when coordinators are unavailable.  Back-ups complete any in-progress tasks, accept new requests, and keep existing requests on track.

Prior to the coordinator becoming unavailable, they create an issue to provide their assigned back-up(s) with a clearly detailed list of:

* Research projects in process
* Actions needed for each project - and when to do them
* Key contacts for each project
* Links to all applicable issues
* Flag exactly what needs to be done by the back-up Research Coordinators

It’s the responsibility of the coordinator to make sure their back-up understands their duties and responsibilities.

### Can we combine recruiting requests to make recruiting go faster?

In some cases, absolutely. This works best when two or more studies are quite similar (e.g. both require interviews on a similar timeline and have an identical screener). 

It is not usually advisable to combine recruitment for studies that are quite different from each other, for example a large survey and a set of user interviews. This is due to the different timeframes for these different studies. 

In order to get people scheduled for interviews, we generally want to keep the feedback loop as tight as possible - by inviting qualified participants to schedule for an interview asap after they respond to the screener. This means that the whole recruitment process might take only a matter of days. Conversely, surveys requiring a large number of responses may require multiple rounds of emails through Qualtrics over a period of weeks. By the time you remember to reach out to some users who responded early on, so much time has passed that you're likely to see a very low response rate. This results in us needing to recruit twice. 

### Recruitment methods

1. **GitLab First Look (formerly the UX Research Panel)** is a group of users who have opted in to receive research studies from GitLab. To find out more or to join, please visit [GitLab First Look](/community/gitlab-first-look/index.html). This is a good fit for studies that are aimed at GitLab users who are software developers and related roles. The panel currently does not have many security professionals, nor senior engineering leaders. 

1. **Respondent.io** is a recruitment service that is a good choice for studies aimed at software professionals who are not necessarily GitLab users. This has been a decent source of security professionals and some other harder-to-reach users. 

1. **Social outreach.** Social posts may go out on GitLab's brand channels. The Research Coordinator uses the Social Request template in the corporate marketing project to request this type of post. This is a good choice for studies primarily aimed at GitLab users. Product Managers, Product Designers, and other teammates are highly encouraged to help promote their studies to their networks.

1. **Direct Sourcing.** We can utilize LinkedIn to approach and source potential participants. Anyone at GitLab is able to request a [LinkedIn Recruiter license](/handbook/hiring/sourcing/#upgrading-your-linkedin-account). This [Unfiltered video](https://youtu.be/rc2IX1e2sQ8) and [slide deck](https://docs.google.com/presentation/d/1LI9qXLRQSnikPiHztDQBapGrDn5Nimsf-K8g1r3j9Do/edit#slide=id.g29a70c6c35_0_68) provide an overview on how to use Linkedin Recruiter to source participants for your study.

### Thank you gifts

As a small token of thanks (and to ensure a higher participation/completion rate) we send gifts to research participants. These can be requested by opening an issue in the [UX research project](https://gitlab.com/gitlab-org/ux-research/blob/master/.gitlab/issue_templates/Incentives%20request.md).

* User interviews, usability testing, or 'buy a feature' research: $60 (or equivalent currency) Amazon gift card per 30 minutes.

* Surveys, beta testing, card sorts, and tree tests: Opportunity to win 1 of 3 $30 (or equivalent currency) Amazon Gift cards, or a GitLab swag item. 

* Design evaluations: Unpaid.

* Swag can also be sent on an ad hoc basis, as requested by researchers, PMs, and others. This is a nice touch and an opportunity for personalization after a particularly good conversation. Coordinators can reach out to the corporate events team (Emily Kyle) for swag codes.

#### Fulfillment

Requests for thank you gifts should be fulfilled at least twice per week so that users receive their gift promptly after participating in research. It's important to maintain a customer service mindset when interacting with participants - receiving their gift is the end of the research cycle and another touchpoint in their relationship with GitLab.

##### Tango Card

Tango Card can be used to send gift cards to users around the world. 

The accounting team funds the account with a lump sum amount from the pre-approved research incentives budget. We issue cards from that prepaid amount. The research coordinator sends a report by the 1st of every month to `ap@gitlab.com` with the following information:

* How much the account was funded at the beginning of the month
* How much was spent on issued cards
* The final balance in the account

The report should be for the previous month (e.g., the report should be sent by 11/1 for cards send between 10/1 and 10/31).

##### Respondent.io

Respondent requires us to pay participants directly through the platform. 

##### Directly through Amazon

**This should not be necessary while Tango Card is operational.** Amazon gift cards are country specific. When purchasing a gift card, ensure you use the appropriate Amazon store for a user's preferred country.

When attempting to send incentives internationally, your credit card or PayPal may be denied. You can offer to send an Amazon.com card (not in the user's country's store) or use [transferwise](https://transferwise.com/), which requires bank routing information. 


### Email tips

When sending emails to GitLab First Look, coordinators may customize emails in a few ways. 
* Always send yourself at least one test email
* Add an opening line above the study details. This can be a way to reference the time of year or add other personalization not in the template.  
* Feel free to lightly re-write parts of the email template to make them sound more like your voice. Some First Look participants receive many emails from us, so changing them up slightly will be nice reinforcement that a human is actually on the other side of the process.
* Experiment with subject lines (`We have a new study for you!`; `See if you're a match with our new study`; etc.)
* Adjust the `sent from` display name to `Name from Gitlab`
* If your response rate is low, but you think that your target participants are in your segment, send one reminder email rather than immediately starting a new segment.
    - Send the reminder no sooner than 4 days after your first email was sent 
    - Add a brief note to the top of the email. Otherwise Qualtrics will just send a link to the survey
    - Write a new subject line (`We still want to hear from you!`; `Reminder: Take our quick survey`)
    - If you suspect that not many of your target participants are in your segment (i.e., because they are security professionals or others that we've struggled to recruit) don't send a reminder. Sending too many reminders increases the risk of unsubscribes. 

Below are email/message templates that you can use for communicating with research participants. Whenever possible, we strongly encourage you to customize each message for the individual and circumstance.

#### Invitation to partake in a user interview, following successful screening

```
Hi! We have a study coming up, and I was wondering if you might be interested in participating with us. Based on your response to our survey, you look like a great fit! Sessions are taking place from `[XX-XX]`, and they last about `[XX]` minutes over Zoom (videoconference).

For this round of testing, we’ll be chatting about `[Replace with research subject. Example: What tools you use, what your process is like, etc.]`.

Participants who complete a session will be compensated with a `[Replace with compensation. Example: $60 Amazon gift card, or approximate value in your home currency]`.

If you are interested, go here `[Link to your Calendly]` and choose one time and day that works for you as soon as possible. There are limited spots available.

Please let me know if you have any questions.
```

#### Outreach internal customers (GitLab) in Slack

```
Hi all! :wave: We are in the process of `[Replace with research subject]` to `[Replace with research goals for context]`.
We need internal customers to answer a few questions. If you would like to help us out, please reply to this survey `[Link to research survey]`. Thank you!
```

### UX research promotion

Coordinators should set aside time to interact with users on social. It's recommended to post any new UX blog posts and search `gitlab ux` at least once per week. 

When appropriate, link to GitLab First Look (and [grab an image](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/tree/master/design/social-media/ads-share-images/gitlab-first-look/png) to upload) in responses to comments about GitLab's UX. This is important for raising awareness of the research program, growing the panel, and performing research with users who may have had negative experiences with GitLab. 

Some sample responses:
* (To a positive comment) `Thank you so much! Would you consider joining our research program if you haven't already? You can specify which areas of the product you're interested in: https://about.gitlab.com/community/gitlab-first-look/`
* (To a negative comment) `I'm so sorry to hear you've had a negative experience! Would you consider giving feedback through our research program? You can specify exactly which parts of the product you're interested in: https://about.gitlab.com/community/gitlab-first-look/`

#### Leveraging the UX Research Group Conversation

At the conclusion of each UX Research Group Conversation, the Research Coordinator will choose one of the presented research projects to showcase both internally and externally to GitLab. The goal of showcasing our work is to not only let the community know about the great work the team is doing, but to also let our participants and users know how their valuable feedback has impacted the user experience within the GitLab product. 

Projects that we want to showcase are chosen on the following criteria: 

- Enough is there to tell an engaging story 
- The research yielded strong, actionable research insights
- A clear path, with examples, from user feedback to resolution

The Research Coordinator will take the following steps to showcase a research project:

1. **Write up and publish a blog post** - this effort will require close collaboration with the Researcher who led the research. Often, screenshots and images will be used to supplement the story being told.  [See an example](https://about.gitlab.com/blog/2020/09/01/a-tale-of-two-editors/) of a published blog post.
1. **Post in Slack** - post in the #whats-happening-at-gitlab, #customer-success, #ceo, and #product Slack channels.
1. **Post on Twitter** - post on Twitter. [See an example](https://twitter.com/EmvonHoffmann/status/1301609713755287553?s=20) of a Twitter post.
1. **Send a note via Qualtrics to our First Look panel** - Qualtrics has a ‘send a thank you note’ option, which should be used to send a link to the blog post to participants who responded to the survey associated with that particular study.

### GDPR and CAN-SPAM requirements

At GitLab, when we communicate with our research study participants, we take [GDPR](https://about.gitlab.com/gdpr/) and CAN-SPAM requirements seriously.

* Anyone can submit a [GDPR request](https://about.gitlab.com/handbook/support/workflows/gdpr_account_deletion.html) to see which lists they are subscribed to, and be removed from any and all of them. The UX research coordinator routinely processes these requests by searching for the contact in the UX research Qualtrics directory. If the contact is not present, the coordinator comments in the issue to document. If the contact is present in the directory, the coordinator deletes the contact, then confirms in the issue that the contact has been removed.
* Every email sent to the research panel members contains an unsubscribe link. When a recipient clicks the link, Qualtrics [automatically confirms](https://www.qualtrics.com/support/survey-platform/distributions-module/email-distribution/emails-overview/#UsingTheOptOutLink) that they have been unsubscribed from the mailing list.

### Participant database pilot

We are piloting the creation of a participant database for the duration of the 13.5 & 13.6 milestones. The pilot will only include studies related to the Secure & Protect stages. 

The goal of the pilot is to make it easier for us to: 

* Identify, tag, and recruit participants with an above-average level of knowledge in a particular technology that is useful for studies in Secure/Protect
* Curate a panel that contains only the highest quality participants

The reimbursement sheet in the Recruiting issue for studies in the Secure & Protect stages will contain three additional columns `rating`, `expertise`, and `notes`.  Each column is optional. It is the responsibility of the team member who adds the participant to this sheet to grade the participant (if they choose to do so). 

`Rating` can be thought of as a 1-5 star quality scale. For the pilot, we will **only** use the **1** rating. Participants will be considered a **5** by default and the field will be **left empty**. 

When any of the following statements are true... 

The participant…

* Had poor internet connection
* Didn't answer questions clearly
* Didn’t follow instructions 
* Is not suitable for a segment within Secure/Protect now or in the foreseeable future
* Was very late
* Was a no show and failed to provide an adequate excuse for missing the interview slot 

You can mark the `rating` collumn with a `1`. This would result in this participant's exclusion from future solution validation or interview-type studies. 

`Expertise` will be used to tag participants who possess a higher than average level of knowledge in a specific area. We will use the following `Expertises`

* Fuzzing
* DAST
* SAST
* Alert Management 

More than one `Expertise` can be added to a participants profile by using commas to separate your choices in the `Expertise` column. 

`Notes` will be used to explain the reason behind the rating.

UX Research Coordinators will be responsible for inputting the data as an embedded data field in the participant's First Look profile. 

UX Research Coordinators will **not** invite anyone with a **1** grade to solution validation or interview-type studies. 

UX Research Coordinators will use the `Expertise` field when priortising who to contact when recruiting for a particular study. 

A question will be added to the discussion guide `Have you signed up to our First Look panel?` because we are unable to add attributes to anyone not present in First Look. If the participant answers no, you are encouraged to share the signup link with them and encourage them to join after the session.





