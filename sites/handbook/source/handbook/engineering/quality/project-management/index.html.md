---
layout: handbook-page-toc
title: "Quality Team's Project Management"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Projects

The Quality team currently works cross-functionally and our task ownership spans multiple projects.

* **GitLab.org**
  * [Gitlab QA](https://gitlab.com/gitlab-org/gitlab-qa/)
  * [GitLab-Triage](https://gitlab.com/gitlab-org/gitlab-triage)
  * [GitLab](https://gitlab.com/gitlab-org/gitlab/)
  * [GitLab CustomersDot](https://gitlab.com/gitlab-org/customers-gitlab-com)
  * **Quality Group**
    * [Quality team tasks](https://gitlab.com/gitlab-org/quality/team-tasks/)
    * [Triage-Ops](https://gitlab.com/gitlab-org/quality/triage-ops/)
    * [Nightly tests](https://gitlab.com/gitlab-org/quality/nightly)
    * [Staging tests](https://gitlab.com/gitlab-org/quality/staging)
    * [Canary test pipeline](https://gitlab.com/gitlab-org/quality/canary)
    * [Production test pipeline](https://gitlab.com/gitlab-org/quality/production)
    * [Known QA failures](https://gitlab.com/groups/gitlab-org/quality/-/issues?scope=all&utf8=%E2%9C%93&state=all&label_name[]=bug)

### Reviewers and maintainers

Upon joining the Quality department, team members are granted either developer, maintainer, or owner access to a variety of core projects. For projects where only developer access is initially granted, there are some criteria that should be met before maintainer access is granted.

For general information, please see the [engineering workflow page](/handbook/engineering/workflow/code-review/#how-to-become-a-maintainer) about how to become a maintainer.

* [GitLab (/qa)](https://gitlab.com/gitlab-org/gitlab/-/tree/master/qa)
  * Quality team members are code owners for this directory. If you are tagged by a developer and don't yet feel comfortable approving a merge request, please tag another member of the Quality department to help review.
  * In general, we expect that team members will generally feel comfortable and will be granted maintainer access once they have:
    * Authored and reviewed 3 MRs related to improvements to the framework.  
    * Authored and reviewed 5 MRs related to adding new end-to-end tests or making substantial improvements to existing ones.
* [GitLab-QA](https://gitlab.com/gitlab-org/gitlab-qa)
  * Authored and reviewed 5 merged MRs that demonstrate good understanding of the codebase.
  * Reviewed 5 MRs that make substantial improvements.
* [GitLab Triage](https://gitlab.com/gitlab-org/gitlab-triage/)
  * Completed 5 merged MRs with all MRs reviewed by the Engineering Productivity team and resulting in no reverts.
  * Reviewed 5 MRs.
  * After completing the above requirement the maintainer should be vetted by an existing maintainer in the Engineering Productivity team. An issue should be created in the project outlining the reasons why this person should be a maintainer. List all 10 MRs in the issue to help aid with review.
  * After the issue has been reviewed and approved by manager of the Engineering Productivity team, an access request will be created to grant the engineer maintainer role.
* [Triage Ops](https://gitlab.com/gitlab-org/quality/triage-ops/)
  * Engineers who authored and reviewed at least 10 merged MRs should be given Maintainer access.
* [GitLab CustomersDot (/qa)](https://gitlab.com/gitlab-org/customers-gitlab-com/-/tree/staging/qa/)
  * Quality Growth team members are code owners for this directory. If you are tagged by a developer and don't yet feel comfortable approving a merge request, please tag another member of the Quality Growth team to help review.
  * In general, we expect that team members will generally feel comfortable and will be granted maintainer access once they have:
    * Authored and reviewed 5 MRs related to improvements to the framework.  
    * Authored and reviewed 10 MRs related to adding new end-to-end tests or making substantial improvements to existing ones.

## Project Management

Our team's [Quality: Development board (top level board)](https://gitlab.com/groups/gitlab-org/-/boards/425899) can span 10k+ issues and it's not easy to work on that level.
As a result, it's only meant to capture the current workload of the team. The board shows who currently owns what in the entire GitLab.org space.

The board is meant to be read-only. We don't manage the project on that level.

We have sub-boards at the project level that are used for project management, triaging and scheduling issues.

Each project has 3 boards each for a given dimension of the project management component: `Development`, `Prioritization`, and `Scheduling`

### Development

This board shows the current ownership of workload / issues with assignees as the dimension.

![Development.png](Development.png)

### Prioritization

This board is for prioritization with priorities as the dimension (`~priority::1` `~priority::2` `~priority::3` `~priority::4`).

Most important is left most and gradually moves to least urgent.

![Priorities.png](Priorities.png)

### Scheduling

This board is for scheduling with milestones as the dimension.

Earliest milestone is left most and gradually moves into later milestones.

![Milestones.png](Milestones.png)

## How to use the boards

Each project planning, scheduling and triaging process will happen in the projects' boards.

The boards are using a consistent configuration and is the same across all of our projects. This means that anyone on the team can work using the same set of tools everywhere.

Think of all these projects as different class of objects with stable interface methods that is consistent and cross-compatible.

This also ensures that the data rolled up to the top level board is consistent.

### Board Overview

![Mermaid.png](Mermaid.png)

### Board Links

* **[GitLab.org top Level board](https://gitlab.com/groups/gitlab-org/-/boards/425899)**
   * [GitLab-QA](https://gitlab.com/gitlab-org/gitlab-qa/)
     * [`Development`](https://gitlab.com/gitlab-org/gitlab-qa/boards/2922)
     * [`Priorities`](https://gitlab.com/gitlab-org/gitlab-qa/boards/787592)
     * [`Scheduling`](https://gitlab.com/gitlab-org/gitlab-qa/boards/787593)
  * [GitLab-Triage](https://gitlab.com/gitlab-org/gitlab-triage/)
     * [`Development`](https://gitlab.com/gitlab-org/gitlab-triage/boards/316854)
     * [`Priorities`](https://gitlab.com/gitlab-org/gitlab-triage/boards/788523)
     * [`Scheduling`](https://gitlab.com/gitlab-org/gitlab-triage/boards/788524)
  * [GitLab](https://gitlab.com/gitlab-org/gitlab/): GitLab issues with `~Quality`
    * [`Quality: Development`](https://gitlab.com/gitlab-org/gitlab/boards/793784)
    * [`Quality: Priorities`](https://gitlab.com/gitlab-org/gitlab/boards/793788)
    * [`Quality: Scheduling`](https://gitlab.com/gitlab-org/gitlab/boards/793791)
  * **[Quality group](https://gitlab.com/gitlab-org/quality)**
    * [Triage-Ops](https://gitlab.com/gitlab-org/quality/triage-ops/): GitLab-Triage setup
      * [`Development`](https://gitlab.com/gitlab-org/quality/triage-ops/boards/701857)
      * [`Priorities`](https://gitlab.com/gitlab-org/quality/triage-ops/boards/793763)
      * [`Scheduling`](https://gitlab.com/gitlab-org/quality/triage-ops/boards/793764)
    * [Quality Tasks](https://gitlab.com/gitlab-org/quality/team-tasks) Roadmap and initiative planning
      * [`Roadmaps`](https://gitlab.com/gitlab-org/quality/team-tasks/boards/548459)
      * [`Initiatives`](https://gitlab.com/gitlab-org/quality/team-tasks/boards/793708)
    * [Nightlies](https://gitlab.com/gitlab-org/quality/nightly)
      * Use the [default board](https://gitlab.com/gitlab-org/quality/nightly/boards)
    * [Staging](https://gitlab.com/gitlab-org/quality/staging)
      * Use the [default board](https://gitlab.com/gitlab-org/quality/staging/boards)
