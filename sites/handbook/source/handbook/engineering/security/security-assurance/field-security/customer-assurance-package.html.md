---
layout: handbook-page-toc
title: "GitLab's Customer Assurance Package"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# GitLab's Customer Assurance Package
At GitLab, we believe that [transparency](https://about.gitlab.com/handbook/values/#transparency) is critical to our success- and security is no different. Our **Customer Assurance Package (CAP)** is designed to provide GitLab team members, users, customers, and other community members with the most current information about our Security and Compliance Posture. 

## Self-Service Resources  
We know that our Customers and Prospects have a lot of questions about GitLab's Security. And we are here to help answer them! Below is a collection of resources that captures the vast majority of commonly asked security questions. They are accessible to anyone and a Non-Disclosure Agreement (NDA) is **not required** to view.

* [GitLab Security Trust Center](/security/)
* [Cloud Security Alliance (CSA)
Consensus Assessments Initiative Questionnaire (CAIQ)](https://cloudsecurityalliance.org/star/registry/gitlab/)
* [GitLab's Security Control Framework](/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html)
* Information Security Policies and Procedures
    * [GitLab Internal Acceptable Use Policy](/handbook/people-group/acceptable-use-policy/)
    * [GitLab Password Policy](/handbook/security/#gitlab-password-policy-guidelines)
    * [GitLab Access Management Policy](/handbook/engineering/security/#access-management-process)
    * [GitLab Data Classification Policy](/handbook/engineering/security/data-classification-standard.html)
    * [GitLab Data Protection Impact Assessment Policy](/handbook/engineering/security/dpia-policy/)
    * [GitLab Penetration Testing Policy](/handbook/engineering/security/penetration-testing-policy.html)
    * [GitLab Audit Logging Policy](/handbook/engineering/security/audit-logging-policy.html)
     * [GitLab Security Incident Response Guide](/handbook/engineering/security/sec-incident-response.html)
     * [GitLab Business Continuity Plan](/handbook/business-ops/gitlab-business-continuity-plan.html)
* GitLab Architecture
    * [GitLab Application Architecture](https://docs.gitlab.com/ee/development/architecture.html)
    * [GitLab Production Architecture](/handbook/engineering/infrastructure/production/architecture/)
    * [High-level Network Diagram](/handbook/engineering/infrastructure/production/architecture/#network-architecture)
* GitLab Blog Post: [Securing your Instance Best Practices](/blog/2020/05/20/gitlab-instance-security-best-practices/)

Coming soon: Regulated Markets Customer Assurance Packages!

## Third Party Security Validation
In today’s security world, it isn’t enough to **say** you have a strong security posture, our Customers and Prospects want us to **prove** it. And we know that many of our customers utilize Third Party Application ratings as a deciding factor when contracting with vendors. These tools are designed to independently identify potential vulnerabilities and provide a public report of an organization's Security health. At this time, GitLab is working closely with both [Security Scorecard](https://securityscorecard.com/) and [BitSight](https://www.bitsight.com/) to ensure the most up-to-date and accurate information is refelcted in these scores. 

### BitSight
BitSight utilizes public information collected across multiple domains to provide a numeric score from 250-900. GitLab publishes three reports:
  * [GitLab Production](https://bitsight.salesloftlinks.com/t/9165/c/6b948c41-4e8d-413b-8fd7-04cc0cf9d737/NB2HI4DTHIXS643FOJ3GSY3FFZRGS5DTNFTWQ5DUMVRWQLTDN5WS6YLQOAXWG33NOBQW46JPMY4TEOJZMI2DOLJXG4YDGLJUMZRWCLJYGZQTELLDGM2GCNBRMRRDEYRXGIXW65TFOJ3GSZLXF4======/service-bitsighttech-com-app-company-f9299b47-7703-4fca-86a2-c34a41db2b72-o)- this report represents the Security Posture of any URL or IP associated with supporting our [Production Architecture](/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html#what-is-considered-production). Our goal is to maintain a rating of 700 or higher. Alerts from BitSight are monitored in real time and addressed as part of our [Vulnerability Management](https://about.gitlab.com/handbook/engineering/security/application-security/vulnerability-management.html) program. 
  * GitLab Sub-Production- this report represents the Securtiy Posture of any URL or IP that does NOT support the Production Environment noted above. This generally includes tests instances and/or instances that are purposely kept out-of-date for Quality Assurance Activities. This score is not monitored by GitLab.
  * GitLab User-Managed- many of our customers utilize static pages as part of gitlab.io. These pages are managed solely by our customers. However, from time to time, these pages are associated with GitLab erroneously. As such, we have moved any page on the GitLab.io domain to this report. This score is not monitored by GitLab and does not represent GitLab's Security Posture. 

### Security Scorecard
[Security Scorecard](https://securityscorecard.com/) monitors public information across 10 risk factors (such as DNS health, IP reputation, network security and patching cadence) and publicly reports an A-F rating. Due to the way Security Scorecard publishes information, Gitlab does not monitor this score regularly.GitLab will review reported items spordically with a goal of maintaing a B or higher. 

### Other Third Party Secuirty Applications

We recognize that different customers may utilize other Third Party Security Applications. If you utilize an application that we are not already partnered with (see above), you can submit a request for evaluation. 

* Existing and Prospective Customers: Please contact your Technical Account Manager
* GitLab Team Members: [Open an issue](https://gitlab.com/gitlab-com/gl-security/field-security/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) on our issue board.


Each application will be assessed based on the following criteria:

* How does the application capture and monitor its data? 
* How many GitLab customers utilize the applications? 
* How are findings reported to GitLab and what is the remediation process?
* How much does the application cost for GitLab to continuously monitor it? 

The Risk and Field Security Team will review this along with a “current snapshot” from the requested applications to determine if it will provide valuable information. Oftentimes these applications will report the same information which would lead to redundant findings and inefficient processes. It is important to note that just because an application is approved for usage, it does not mean that GitLab is agreeing to any of the findings or remediation activities.

## Additional Evidence
In line with specific laws, regulations and contractual requirements, there are certain items that we require a **Non-Disclosure Agreement** to provide.  

* SOC2 Type 1 Report 
    * You can request a copy of the GitLab SOC2 Type 1 report by following [these instructions.](/handbook/engineering/security/security-assurance/security-compliance/soc2.html#requesting-a-copy-of-the-gitlab-soc2-type-1-report)
* GitLab Annual Third Party Penetration Test Results 
    * You can request a **Detailed Letter of Engagement** that includes a summary of the testing performed and high level results by following [these instructions](/security/#external-testing).

## Questions?
* If you have any further questions that aren't answered here please:
    * If you are not a GitLab team-member, contact security@gitlab.com.
    * If you are a GitLab team-member, reach out to Field Security via slack [#sec-fieldsecurity](https://gitlab.slack.com/archives/CV5A53V70), or [open an issue](https://gitlab.com/gitlab-com/gl-security/field-security/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) on our issue board.

    
