---
layout: handbook-page-toc
title: "All You Need Tu-Know-Ki"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}
[GitLab Handbook](/handbook)>[GitLab Handbook: Engineering](/handbook/engineering)>[GitLab Handbook: Security Department](/handbook/engineering/security)>[GitLab Handbook: Security Assurance](/handbook/engineering/security/#assure-the-customer---the-security-assurance-sub-department)>[GitLab Handbook: Field Security](/handbook/engineering/security/security-assurance/field-security)

## All You Need Tu-Know-Ki

**ALl You Need Tu-Know-Ki** is an internal only system of curated security and privacy relevant question and answer pairs maintained by the Risk and Field Security Team. This tool is designed to increase speed and efficiencies when completeing [Customer Security Assessments](/handbook/engineering/security/security-assurance/field-security/customer-security-assessment-process.html).

### Users and Responsibilities
There are three main types of users:
* **GitLab Team Members**- Individuals interested in or looking for answers to questions. This will mainaly be Solutions Architects, Technical Account Managers, and/or Account Executives. 
* **Subject Matter Experts**- Individuals who are either stakeholders in the development of an answer or are the Control Owner directly linked to the question. This could be the Data Protection Officer, Infrastructure Team,  Security Specialty Teams, etc.  
* **Owners/Maintainers** - Individuals responsible for operation the system including triage of new questions, proposal of answers, coordination with SME users, and long term accuracy. This is the repsonsibility of the Risk and Field Security Team.

### Access and Instructions

**All You Need Tu-Know-Ki** is currently in Beta testing. Watch the [`What's Happening At GitLab`](https://gitlab.slack.com/archives/C0259241C) Slack Channel for the offical Go-Live. 

