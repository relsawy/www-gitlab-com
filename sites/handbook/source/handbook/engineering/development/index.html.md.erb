---
layout: handbook-page-toc
title: Development Department
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Vision

A world class development team of software engineers and managers who make our customers happy when using our product(s).   Our products should contain broad rich features, high availaiblity, high quality, fast performance, trustworthy security, and reliable operation.

## Mission

The development department strives to deliver MRs fast.  MR delivery is a reflection of:
* providing new product requirements
* resolution of customer issues/bugs
* fixing security problems
* increasing availability, quality, and reliability
* fostering open source community contributions
* Improving user experience
* fostering best agile practices for fast iterations

The department also focuses on career development and process to make this a preferred destination for high performing software engineers.

We use data to make decisions.  If data doesn't exist we use anecdotal information.  If anecdotal information isn't available we use first principles.

The development team is responsible for developing products in the following categories:

* [Dev](/handbook/engineering/development/dev/)
* [Enablement](/handbook/engineering/development/enablement/)
* [Fulfillment](/handbook/engineering/development/fulfillment/)
* [Growth](/handbook/engineering/development/growth/)
* [Ops](/handbook/engineering/development/ops/)
* [Secure](/handbook/engineering/development/secure/)
* [Threat Management](/handbook/engineering/development/threat-management/)


## Team Members

The following people are permanent members of the Development Department:

<%
departments = ['Verify', 'Package', 'Release', 'Protect', 'Dev' , 'Enablement', 'Fulfillment', 'Growth', 'Ops', 'Secure', 'Fellow']
department_regexp = /(#{Regexp.union(departments)})/
%>

<%=  direct_team(role_regexp: department_regexp, manager_role: 'VP of Development') %>


## Stable Counterparts

The following members of other functional teams are our stable counterparts:

<%= stable_counterparts(role_regexp: /[,&] Development/, direct_manager_role: 'VP of Development') %>

## Team Composition

This is the breakdown of our department by section and by stage.

<% if ENV['PERISCOPE_EMBED_API_KEY'] %>
  <div>
    <embed width="100%" height="100%" style="min-height:300px;" src="<%= signed_periscope_url({ chart: 8817664,dashboard: 673088 , embed: "v2"}) %>">
  </div>
  <% else %>
    <p>You must set a <code>PERISCOPE_EMBED_API_KEY</code> environment variable to render this chart.</p>
<% end %>

<% if ENV['PERISCOPE_EMBED_API_KEY'] %>
  <div>
    <embed width="100%" height="100%" style="min-height:300px;" src="<%= signed_periscope_url({ chart: 8817717,dashboard: 673088, embed: "v2"}) %>">
  </div>
  <% else %>
    <p>You must set a <code>PERISCOPE_EMBED_API_KEY</code> environment variable to render this chart.</p>
<% end %>

This is the stack-up of our engineers, by level.

<% if ENV['PERISCOPE_EMBED_API_KEY'] %>
  <div>
    <embed width="100%" height="100%" style="min-height:300px;" src="<%= signed_periscope_url({ chart: 8737993,dashboard: 673088, embed: "v2"}) %>">
  </div>
  <% else %>
    <p>You must set a <code>PERISCOPE_EMBED_API_KEY</code> environment variable to render this chart.</p>
<% end %>

## How We Work

### Onboarding
Welcome to GitLab! We are excited for you to join us.
Here are some curated resources to get you started:

* [Joining as an Engineer](/handbook/developer-onboarding/)
* [Joining as an Engineering Manager](/handbook/engineering/development/onboarding/manager/)

### Cross Functional Collaboration

#### Working across Stages

Issues that impact code in another team's product stage should be approached collaboratively with the relevant Product and Engineering managers prior to work commencing, and reviewed by the engineers responsible for that stage.

We do this to ensure that the team responsible for that area of the code base is aware of the impact of any changes being made and can influence architecture, maintainability, and approach in a way that meets their stage's roadmap.

#### Architectural Collaboration

At times when cross-functional, or cross-departmental architectural collaboration is needed, the [GitLab Architecute Evolution Workflow](/handbook/engineering/architecture/) should be followed.

### Development Headcount planning

Development's headcount planning follows the Engineering [headcount planning](/handbook/engineering/#headcount-planning) and [long term profitability targets](/handbook/engineering/#long-term-profitability-targets).  Development headcount is a percentage of overall engineering headcount.  For FY20, the headcount size is 271 or ~58% of overall engineering headcount.

We follow normal span of control both for our managers and directors of [4 to 10](/company/team/structure/#management-group).  Our sub-departments and teams match as closely as we can to the [Product Hierarchy](/handbook/product/product-categories/#hierarchy) to best map 1:1 to [Product Managers](/handbook/product/).

### Daily Duties for Engineering Directors

The following is a non exhaustive list of daily duties for engineering directors, while some items are only applicable at certain time, though.
* Review engineering metrics boards in Sisense
  * [Development KPIs](https://app.periscopedata.com/app/gitlab/504639/Development-KPIs)
  * [Sub-department MR metrics](https://app.periscopedata.com/app/gitlab/533956/Development-Section-MR-Metrics)
  * Sub-department and group specific boards, for example [Dev Sub-department Overview Board](https://app.periscopedata.com/app/gitlab/561630/Dev-Section-Overview-Dashboard)
* Review hiring dashboards
* Personal todo list
* Personal GitLab board(s) if any
* [Working groups](/company/team/structure/working-groups/) that the director drives or participates in
  * Action items in agenda documents
  * Issue boards
  * Slack channel
* [Availability & Performance refinement](/handbook/engineering/workflow/#availability-and-performance-refinement)
  * Follow up open questions and ensure appropriate handling of issues with regard to priority and severity
  * [Agenda document](https://docs.google.com/document/d/1SanPUz86cIyRQR5kRmXyCLLE8sZVpx0auu_W6jY94W4/edit)
  * [Infradev board](https://gitlab.com/groups/gitlab-org/-/boards/1193197?label_name[]=infradev)
  * [Performance board](https://gitlab.com/groups/gitlab-org/-/boards/1233204?label_name[]=performance-refinement)
* Follow active [Engineering Rapid Action(s)](#rapid-action-issue) that the director sponsors
  * Standup/status update document
  * Issue board


### Developing and Tracking OKRs

In general, OKRs flow top-down and align to the company and upper level organization goals. 

#### Managers and Directors
For managers and directors, please refer to a good [walk-through example of OKR format](/company/okrs/#example-developments-approach-to-okrs) for developing team OKRs. Consider stubbing out OKRs early in the last month of the current quarter, and get the OKRs in shape (e.g. fleshing out details and making them [SMART](https://en.wikipedia.org/wiki/SMART_criteria)) no later than the end of the current quarter.

It is recommended to assess progress **weekly**. 

1. Append the percentage score to the subject of Objective epics and Key Result issues.
1. Set the [Health status](https://about.gitlab.com/company/okrs/#maintaining-the-status-of-okrs) of epics and issues.
1. In the case where weekly assessment is impractical, an assessment shall be made by the end of each month.

#### Staff Engineers, Distinguished Engineers, and Fellows

Below are tips for developing individual's OKRs:

1. Align OKRs to team goals. However, it's unnecessary to derive from all organizational OKRs. Simply decide what makes sense to your personal situation.
1. Follow the same timeline of managers and directors, i.e. stubbing out early and bring OKRs in shape by the end of the current quarter.
1. Refer to the same [walk-through example of OKR format](/company/okrs/#example-developments-approach-to-okrs).
1. Make [SMART](https://en.wikipedia.org/wiki/SMART_criteria) OKRs - Specific, Measurable, Achievable, Relevant, Time-bound.
1. Follow the same progress assessment instructions above.

#### Examples
1. [Engineering](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7253)
1. [Development](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7148)

## Learning Resources

### Secure coding best practices

It is important that all developers are aware of [secure coding best practices](/handbook/engineering/security/secure-coding-training.html) and refresh this knowledge periodically.  This is tracked via [Secure Coding Training Guidelines](secure-coding/).

### Ruby on Rails Performance Training

The materials from an earlier Ruby on Rails performance workshop can be found on [internally shared Google drive](https://drive.google.com/drive/search?q=in:0ACCfBKgYFjLvUk9PVA).

#### Video Sessions

| Day | Topics | Video Links |
| --- | --- | --- |
| Session - Day 1 | Intro and overview | [Monday](https://drive.google.com/drive/search?q=title:%22day%201%20session%22%20parent:15EaLvFMexoJu7pHQQdTDuInhFRk_lOLq) [Wednesday](https://drive.google.com/drive/search?q=title:%22day%201%20session%22%20parent:1dJ08oeWdff4BpcrlVjuRQIPARYXE-LGY) |
| Session - Day 2 | Tools | [Monday](https://drive.google.com/drive/search?q=title:%22day%202%20session%22%20parent:15EaLvFMexoJu7pHQQdTDuInhFRk_lOLq) [Wednesday](https://drive.google.com/drive/search?q=title:%22day%202%20session%22%20parent:1dJ08oeWdff4BpcrlVjuRQIPARYXE-LGY) |
| Session - Day 3 | SQL and N+1 Troubleshooting | [Monday](https://drive.google.com/drive/search?q=title:%22day%203%20session%22%20parent:15EaLvFMexoJu7pHQQdTDuInhFRk_lOLq) [Wednesday](https://drive.google.com/drive/search?q=title:%22day%203%20session%22%20parent:1dJ08oeWdff4BpcrlVjuRQIPARYXE-LGY)|
| Session - Day 4 | Queueing Theory | [Monday](https://drive.google.com/drive/search?q=title:%22day%204%20session%22%20parent:15EaLvFMexoJu7pHQQdTDuInhFRk_lOLq) [Wednesday](https://drive.google.com/drive/search?q=title:%22day%204%20session%22%20parent:1dJ08oeWdff4BpcrlVjuRQIPARYXE-LGY) |

### Database

Here is the information of a PostgreSQL query optimization bot at GitLab - Joe: [Blueprint](/handbook/engineering/infrastructure/library/database/postgres/query-optimization-bot/blueprint/) and [Design](/handbook/engineering/infrastructure/library/database/postgres/query-optimization-bot/design/).

### Frontend

#### Frontend Masters

[Frontend Masters](frontendmasters.com) allows you to advance your skills with in-depth, modern frontend engineering courses.

GitLab has an account with Frontend Masters and team members can gain access to it by creating an [Access Request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) and select the best option for your situation ([single user](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request), [bulk user](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Bulk_Access_Request), etc.) and, once approved by your manager, assign to the Access Request Provisioner listed in the [Tech Stack](https://docs.google.com/spreadsheets/d/1mTNZHsK3TWzQdeFqkITKA0pHADjuurv37XMuHv12hDU/edit#gid=0) for this system. Once your access has been provisioned, you will receive an email to activate your account.

You can also join the [#frontendmasters](https://gitlab.slack.com/archives/C0151MXP6JH) Slack channel for course recommendations and discussion.

## Continuous Delivery, Infrastructure and Quality Collaboration

In late June 2019, we moved from a monthly release cadence to a more continuous
delivery model.  This has led to us changing from issues being
concentrated during the deployment to a more constant flow.  With the adoption
of continuous delivery, there is an organizational mismatch in cadence between
changes that are regularly introduced in the environment and the monthly
development cadence.

To reduce this, infrastructure and quality will engage development via
[Availability & Performance
Refinement](https://gitlab.com/groups/gitlab-org/-/boards/1193197) which
represents critical issues to be addressed in development from infrastructure
and quality. Issues on this board are tagged with `gitlab.com` and `infradev`
labels. A director from development will be assigned to refine the board, work
with product/infrastructure/quality to set priority/severity of issues, make sure they
are assigned and worked, and escalate where necessary for resolution.

Refinement will happen on a weekly basis and involve a member of infrastructure,
quality, product management, and development.

### Rapid Action Issue

Rapid Action, as the name implies, is the process we use when a critical situation arises needing immediate attention from various stakeholders.  When the situation is identified as a potential Rapid Action the following guidance is recommended.

* Create an issue or epic labeled with `rapid action`
* Identify the stakeholders involved and cc them on the issue/epic
* Create a doc using [this](https://docs.google.com/document/d/1ZIJvWgo2W4Tw3mmXs_WDJK5Di-zfANtgYgiTZ5Afdyc/edit) template to track the progress of the rapid action
on a daily basis
* (Optional) create a slack room dedicated to this rapid action
* In the document and tracking issue list the context, stakeholders and exit criteria

It is recommended that daily progress is updated in the agenda.  Once the exit criteria has been met, remove the `rapid action` label,  close the rapid action issue and prepend the agenda document with "Closed" or "Deprecated" to indicate its status.

### Email alias and roll-up
1. Available email alias (a.k.a. Google group):

   Managers, Directors, VP's teams: each alias includes everyone in the respective organization.

1. Naming convention:

   team@gitlab.com, examples below -
   * Managers: configure-be@gitlab.com includes all the engineers reporting to the Configure backend engineering manager.
   * Directors: ops-section@gitlab.com includes all the engineers and managers reporting to the director of engineering, Ops.
   * VP of Development: development@gitlab.com includes all engineers, managers, and directors reporting to the VP of Development.

1. Roll up:

   Teams roll up by the org chart hierarchy -
   * Engineering managers' aliases are included in respective Sub-department aliases
   * Sub-department aliases are included in Development alias

### Development Escalation Process ###
* [General information](./processes/Infra-Dev-Escalation/)
* [Process outline](./processes/Infra-Dev-Escalation/process.html)

## Books

Note: books in this section [can be expensed](/handbook/spending-company-money).

Interested in reading this as part of a group? We occassionally self-organize [book
clubs](/handbook/leadership/book-clubs/) around these books and those listed on our [Leadership page](/handbook/leadership/#books).

1. [The Principles of Product Development Flow](https://www.amazon.com/Principles-Product-Development-Flow-Generation/dp/1935401009/)
2. [Software Engineering at Google](https://gitlab.com/gitlab-com/book-clubs/-/issues/10)

## Common Links

* [Development department board](https://gitlab.com/gitlab-com/www-gitlab-com/-/boards/1008667?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Development%20Department)
* [Current OKR's](https://gitlab.com/gitlab-com/www-gitlab-com/-/boards/1008667?scope=all&utf8=✓&state=opened&label_name[]=Development%20Department&label_name[]=OKR)
* Slack channel [#development](https://gitlab.slack.com/messages/C02PF508L)
