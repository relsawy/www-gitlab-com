---
layout: handbook-page-toc
title: "Brand and Digital Design Handbook"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Brand & Digital Handbook
{:.no_toc}

Brand and Digital is part of the Marketing Growth department, and is comprised of the Brand and Digital teams. Our mission is to create brand and marketing experiences that attract, convert, nurture and delight customers throughout the customer journey, one MVC at a time.


### Brand team

The Brand team’s primary responsibilities are:

- Developing and managing GitLabs brand and visual identity.
- Developing and managing campaign brands and identities.
- Enabling Everyone to Contribute with a consistent voice and look and feel.
- Content marketing design support, such as editorial and video content.

Quick Links:
- [Request support from Brand](#working-with-us)
- [Meet the Brand team](#meet-the-brand-team)
- [Brand standards and guidelines](/handbook/marketing/growth-marketing/brand-and-digital-design/brand-guidelines/)
- [Image guidelines](/handbook/marketing/growth-marketing/brand-and-digital-design/image-guidelines/)
- [Reviewing merge requests](/handbook/marketing/website/merge-requests/)


### Digital team

The Digital team’s primary responsibilities are:

- Developing and maintaining [about.gitlab.com](https://about.gitlab.com/).
- Optimizing online marketing funnels and other digital experiences for conversion.

Quick Links:
- [Request support from Digital](#working-with-us)
- [Meet the Digital team](#meet-the-digital-team)
- [Digital definitions and explanations](/handbook/marketing/growth-marketing/brand-and-digital-design/digital-definitions/)
- [Brand and Digital: Foundations Agenda](/handbook/marketing/growth-marketing/brand-and-digital-design/foundations-agenda/)

### What we do

The Brand and Digital team works with our GitLab peers and the wider GitLab community to rapidly co-design and release value generating experiences throughout the marketing funnel -from raising brand awareness and attracting strangers, to converting visitors to leads, leads to customers and customers to promoters:

- **Attract** - Blog, Keywords, Social Media
- **Convert** - Forms, CTAs, Landing Pages
- **Nurture** - Ebooks, White-papers, Case Studies, Webinars, Guides, Videos, Email Newsletters and Drip Campaigns, Free Trials, Demos
- **Delight** - Feedback Loops, Personalization


### Our focus

The Brand and Digital team’s [mission](#mission), [work](#how-we-work), and solutions revolve around:

- **GitLab’s** **CREDIT Values** - We incorporate [Gitlab's CREDIT values](/handbook/values/#credit) into everything we do
- **Data-driven decisions**  - We use data to plan, design and measure success
- **Understanding the Problem** - We strive to understand the *who*, *what* and *why,* so we can best solve the how
- **Value-driven** **MVCs** - We rapidly plan, design and release results-driven MVCs, and measure our success
- **Customer focus** - We promote customer-centric strategies and solutions throughout the org
- **Conversion, conversion, conversion** - We optimize for conversion throughout the buyer journey
- **Story telling** - We build our brand through compelling story telling
- **Decisive Collaboration** - We facilitate an open and inclusive, decision-driven design process
- **Cohesive** **Experiences** - We build cohesive, omni-channel experiences across diverse media and devices, one MVC touchpoint at a time
- **Everyone Can Contribute** - We support GitLab’s Everyone Can Contribute model by creating contribution paths aligned to our brand and business goals
- **Efficiency via DRY** - In the spirit of Don’t Repeat Yourself (DRY), we leverage strategic reuse to promote consistency and speed
- **Lean UX** - We employ Lean UX prototyping to iterate and learn at 1/10th the cost of dev-based iteration, and mitigate development delays and rework


### Contact us

- [Slack](https://gitlab.slack.com/app_redirect?channel=marketing-brand-and-digital)
- [Email Brand and Digital](mailto:brand-and-digital@gitlab.com)

# Mission

Our mission is to rapidly release value generating MVCs, together. To realize our mission, we strive to be collaborative, innovative, informative, efficient, and results-driven in all things we do:

- **Innovative** - We emphasize GitLab’s disruptive business and product innovation, and embrace new ideas and ways to achieve our goals.
- **Informative** - We support evidence-based solutions via brand and conversion design best practices, competitive and user research, and customer feedback loops.
- **Efficient** - We solve efficiently “boring” solutions, Lean UX tools and techniques, and design reuse (e.g. design systems, reusable assets, and self-serve templates).
- **Results focused** - We deliver results fast by rapidly releasing value-generating, customer-centric MVCs and measuring success via performance indicators (PIs).
- **Collaborative** - We facilitate an open and iterative design process, that welcomes contributions from Gitlabbers and the wider GitLab community end to end.


# Working with us

To work with Brand and Digital, please:
- Use the following issue templates and labels to submit your issue requests
- Submit issue requests with adequate lead time (the sooner the better)
- Use Growth Boards and Brand and Digital Boards to track your request

## Labels Overview
Please see the Growth Marketing Handbook section on [labels](/handbook/marketing/growth-marketing/#labels) for status and communication labels. Below is more detailed labels specific to Brand and Digital Design.

## Brand issues & labels

#### Brand issues
To request support from Brand, please select the Brand issue template best aligned to your needs from the [Requesting Support](/handbook/marketing/growth-marketing/#requesting-support) section of the Growth Marketing Handbook.


Note: All new requests must be submitted via a Brand issue template.

#### Brand labels
When you use a Brand issue template, the `design` and `marketing status` labels are automatically added to your issue request. Please review your issue to ensure relevant labels are added:

-  `Design`
    - If you create an issue where Brand services are the primary need, please use this label.
    - Helps us find and track issues relevant to the Design team
- Group labels (ex: `Strategic Marketing`, `Corporate Marketing`)
    - Denotes it could be part of a Growth Marketing Sub-team scope
    - Is not a label to denote the status of an issue
    - Note: These labels must exist in the root `GitLab.com` group or the `www-gitlab-com` repository
- Subject matter labels (ex: `blog`,  `SEO`)
    - Identifies relevant team(s) and subject matter(s)
    - Note: These labels must exist in the root `GitLab.com` group or the `www-gitlab-com` repository

## Digital issues & labels

#### Digital issues
To request support from Digital, please select the Digital issue template best aligned to your needs from the [Requesting Support](/handbook/marketing/growth-marketing/#requesting-support) section of the Growth Marketing Handbook

- [Team backlog refinement](https://gitlab.com/gitlab-com/marketing/corporate-marketing/-/issues/new?issuable_template=brand-and-digital-design-grooming) - <description and recommended lead time>


#### Digital labels
When you use a Brand issue template, the `mktg-website` and `mtg-status::triage` labels are automatically added to your issue request. Please review your issue to ensure relevant labels are added:

-  `mktg-website`
    - If you create an issue where the Website services are the primary need, please use this label.
    - Helps us find and track issues relevant to the Design team
- Group labels (ex: `Strategic Marketing`, `Corporate Marketing`)
    - Denotes it could be part of a Growth Marketing Sub-team scope
    - Is not a label to denote the status of an issue
    - Note: These labels must exist in the root `GitLab.com` group or the `www-gitlab-com` repository
- Subject matter labels (ex: `blog`,  `SEO`)
    - Identifies relevant team(s) and subject matter(s)
    - Note: These labels must exist in the root `GitLab.com` group or the `www-gitlab-com` repository


# Tracking work

We use issue boards to track work that has been triaged by Growth Marketing and assigned to Brand and Digital by milestone.  We offer a variety of issue boards so everyone can easily track Brand and Digital work that is relevant to them:

- **All Marketing Tracking** - All work assigned to Brand and Digital
- **Group Tracking** - Ex: All Remote, Corporate Marketing
- **Specialty Tracking** - Ex: OKR, CMO, vendors and design-handbook

Note: Issues must have the appropriate labels to be visible and trackable by Brand and Digital and other stakeholders.


| Team or subject         | Brand                                                          | Digital                                                          | Prioritization lead       |
| ----------------------- | -------------------------------------------------------------- | ---------------------------------------------------------------- | ------------------------- |
| All of Marketing        | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1511332) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1483370) | Todd Barr                 |
| Account Based Marketing | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1690658) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1691347) | Emily Luehrs              |
| All Remote              | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1571555) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1485066) | Jessica Reeder            |
| Blog                    | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1571570) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1483337) | Erica Lindberg            |
| Blocked                 | -                                                              | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1485169) | -                         |
| Brand & Digital Team    | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1485124) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1485124) | Michael Preuss            |
| CMO                     | -                                                              | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1486533) | Todd Barr                 |
| Content Marketing       | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1571561) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1483354) | Erica Lindberg            |
| Corporate Marketing     | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1541149) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1485085) | Danielle Morrill          |
| Design Handbook         | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1498563) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1498563) | Luke Babin            |
| Digital Handbook         | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1498563) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1498563) | Michael Preuss            |
| Diversity, Inclusion and Belonging | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1621520) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1632756) | Candace Byrdsong Williams |
| Events                  | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1541174) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1485090) | Emily Kyle                |
| Field Marketing         | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1541162) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1533790) | Leslie Blanchard          |
| Marketing Ops           | -                                                              | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1485111) | Dara Wade                 |
| Marketing Programs      | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1571580) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1580521) | Jackie Gragnola           |
| OKR                     | -                                                              | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1483333) | Michael Preuss            |
| Social                  | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1571585) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1485179) | Natasha Woods             |
| Strategic Marketing     | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1571417) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1489415) | Ashish Kuthiala           |
| Talent Brand            | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1571573) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1580510) | Betsy Church              |
| Developer Evangelism    | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1596489) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1485182) | -                         |
| Vendor                  | [Brand](https://gitlab.com/groups/gitlab-com/-/boards/1511334) | [Digital](https://gitlab.com/groups/gitlab-com/-/boards/1502288) | Michael Preuss            |


# How we work


### Project Management

#### Issue boards
Brand and Digital project planning, management, and tracking is primarily facilitated through [issue boards](#tracking-work).

#### Roadmap planning

- Process Marketing Growth assignments
    - Marketing Growth triages all Brand and Digital issue requests.
    - Issues approved by Marketing Growth for Brand and Digital engagement are scheduled on the project roadmap based on priority
- Sprint Roadmap
    - Issues are assigned sprint milestones based on the week we expect to work on the issue (format is `Fri: Apr 17, 2020`), typically placed in future milestones
    - Issues milestone assignments may be changed to address new asks, blockers and or priority pivots
    - If issues fall out of priority, they are placed in the Brand and Digital backlog
- Prioritization - In order to ensure we’re focused on what matters most, we prioritize our roadmap and MVC requirements based on:
    - Input from Growth Marketing
    - Input from other stakeholders
    - Project deadlines
    - ROI based on prioritization criteria:
        - **Common**
            - Frequency of use - How often is the single application diagram used? Web traffic to this page?
            - Number of people impacted - How many GitLabbers would benefit from this asset? How many unique users would benefit from this page?
        - **Critical**
            - High customer risk - If we don't do this, what are the risks to customers? How severe are those risks?
            - High business risk - If we don't do this, how might it create risk for our business? Could it create a large volume of support calls? Make us non-GDPR compliant?
            - Business criticality - Part of high ROI opportunity or other business critical initiative?
            - Impact to important stakeholders - CEO or CMO request? Impacts bottom of funnel (BOFU) prospects very close to buying? Impacts key partners or customers?
        - **Differentiator**
            - Brand and or product differentiator - Creates value by positioning our brand and or product against competition.
        - **Reusable**
            - Can we reuse - If we build this, can we reuse it elsewhere to get more ROI. Perhaps it's low value score for this project, but high "lifetime" value via reuse.
        - **Time & Cost**
            - Time and cost required to complete the work.
        - **Deadlines**
            - Are there any hard deadlines due to contract or event obligations?
            - Assign to Sprints

#### Sprint planning
Before the sprint starts, Brand and Digital teams break high-level issue requests into task-based implementation issues, as needed. Implementation issues are assigned, weighted and related to other issues to denote relationships and dependencies (i.e. blocking issues).

- Implementation issues  
    - [**Strategy Issue**](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/new?issuable_template=team-implementation-strategy) - Issue to define strategic brief that defines project hypothesis, scope, business and brand requirements, target audience, CTAs and any other information key to success.
    - [Conversion Checklist Issue](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/new?issuable_template=team-implementation-conversion) - Issue that’s opened at the start of a conversion project, completed by design, development and testers throughout the project, and closed at the end of the project.
    - [**Prototyping Issue**](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/new?issuable_template=team-implementation-prototyping) - Issue to facilitate co-design sessions using experience mapping and or lo- to mid-fi prototyping to rapidly align on big picture experience and value-driven MVCs.
    - [**Visual Design Issue**](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/new?issuable_template=team-implementation-visualdesign) - Issue to theme the approved conversion design prototypes.
    - [**Development Issue**](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/new?issuable_template=team-implementation-developement) - Issue to develop the approved design.
    - [**Testing Issue**](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/new?issuable_template=team-implementation-testing) - Issue to define, conduct and report outcome of tests.
- Weighting
    - We use issue weight to plan and manage our work better. Team members are not held to weights, they’re simply a planning and learning tool. Brand and Digital weights are based on the following point scale: 1pt = .5 day
- Issue Assignments
    - All sprinted implementation issues are assigned to designer or developer. He or she is responsible for leading the issue end-to-end, including facilitating cross-functional completion, keeping the SSOT it up to date, responding to contributions, and completing requirements.
- Right-sizing Sprints
    - We strive to plan sprints we can complete. When we don’t, that is ok, so long as we learn from the experience and continually improve.  In order to achieve predictable sprint outcomes we focus on:
        - Can I complete this work in a week?
        - Have we been under or over estimating this type of work? Should we adjust the weight accordingly?
        - Do I need to break this MVC into smaller MVCs?
        - Do I anticipate any blockers, like cross-functional dependencies or technical challenges? If so, are they manageable or should I pivot to an alternate priority?

#### Sprint cycle

- **Weekly Meetings**
    - **Sprint Kick-off** - At the beginning of each week, we kick-off the sprint and each team member:
        - Shares their priorities for the weekly sprint in the meeting agenda. Priorities should be reflected on Brand and Digital issue boards.
        - Notes any blockers.
    - **Slack** **Stand-ups**
        - We’re experimenting with async Slack stand-ups. If you’re not participating in Slack stand-ups and would like to, please reach out to your leader.
    - **1:1’s**
        - Leaders check-in with team members on sprinted projects to provide feedback, address blockers and or pivot project priorities as needed.
        - Its recommended team members issue boards be used to facilitate these discussions.
    - **Team Reviews**
        - Gathering iterative feedback is an essential part of the design and development process. Team reviews, allow team members to get input and ideas before work begins, quickly solicit feedback on works in progress (WIP), and validate final solutions. Because GitLab is an all-remote company, team reviews are also important for building shared understanding with the broader team and company.
            - Brand and Digital managers will incorporate peer reviews into their team processes, and provide feedback on MVC scope, processes, and WIP and final designs and development solutions.
            - Brand and Digital team members will present at least once a fiscal quarter, and record and share with the Marketing Growth team for awareness and async contribution.
            - Optionally, and only with presenter approval,  team review recordings can be posted  to GitLab Unfiltered.
            - Be sure to frame discussions around the customer and the problem being solved, not the ”pretty” visual design or functionality.  As the discussion unfolds, continually tie everything back to how we can best achieve Gitlab (ex: conversion) and target audience needs (ex: easily getting valuable content from trustworthy source).
    - **Demo Day!**
        - Team members showcase something they have completed during the sprint.
        - Peers have the opportunity for Q&A async, or verbally if time permits.
- **Close out** - We close out the week's milestone each week by processing:
    - Closed issues - Closing completed issues.
    - Rollover issues - Moving issues forward if they were not closed out. Issues that consistently rollover may require attention.


### Strategy


#### Define the opportunity

- Work with your stakeholders to clearly define *who* we're solving for, *what we want to achieve*, and *why* we’re solving for it.
- Help your stakeholders define the *who*/*what*/*why* as a user story. For example, "As a (who), I want (what), so I can (why/value)." If you’re asked to implement a non-evidence-based *how* (i.e. a specific solution), then ask the requestor to focus on the who/what/why, so everyone can work together to find the best *how*.
- Help stakeholders define [MVC](/handbook/values/#minimal-viable-change-mvc) success criteria, prioritizing MVC “must-haves” and non-MVC “should-haves” and “could-haves.” (Note that this success criteria is subject to change based on new learning from the iterative design process and customer feedback.)


### Design process


#### Before you design

**Generate ideas**
Part of the role of Brand and Digital is to lead and facilitate idea generation with stakeholders. We are all very busy working with stakeholders to solve known problems, but remember that there are also undiscovered problems out there that are definitely worth solving. Here are a few activities and resources to inspire you!

- Run a sync, async, or design session to generate ideas. Define the scope and goals for the session, and invite diverse participants for the best results.
- Reach out to other GitLabbers who may represent the target audience or simply have unique perspective and insights, by inviting them to meetings or via Slack, to generate new idea and or get feedback on existing ideas.
- Spin up a sync or async session to share competitive and comparative solutions, and then use them as an “iteration 0” like/dislike session with stakeholders.

#### Understand the space

- Consider lightweight competitive research an analysis to inform your work.
    - Google and or social search the subject you’re working on
    - Checkout competitor and comparative solutions
    - Put yourself in the target audiences shoes, as you get a sense of the competitive and market landscape.
    - Identify “boring” design conventions customers expect, such as interactive patterns or subject matter iconography, that we can embrace for marketing and release speed reasons. Only stray from industry conventions with strategic intent, such as capitalizing on [disruptive innovation](https://www.economist.com/the-economist-explains/2015/01/25/what-disruptive-innovation-means) opportunities.
    - Screen capture competitive and comparative solutions, you can use to generate ideas.
- Consider creating user flows or journey maps to help ensure you and stakeholders understand how the touchpoint you’re working on fits into the broader buyer journey. Where are prospects coming from? Where will they go next? How do we optimize the overall experience for conversion?
- Read the content you’re designing, so you can maximize the message with meaningful photos, illustrations and diagrams that are informative and engaging.

#### Investigate possible dependencies

It is our responsibility as Brand and Digital to ensure the touchpoints and experiences we deliver are well integrated into GitLab’s experiences.

- Proactively reach out to other Brand and Digital GitLabbers to align your work to theirs and ensure the solution delivered is on brand and conversion optimized.
- Identify the DRIs, cross-discipline peers and stakeholders early and make sure they’re aware of your work, their role and that they have what they need from you to contribute and avoid delaying your delivery.


### Design


#### Ideate and iterate

- Share design ideas in the lowest fidelity that still communicates your idea. To keep the cost of change low, only increase fidelity as design confidence grows and implementation requires.
- Ask for feedback from stakeholders throughout the design process to help refine your understanding of the problem and align on the best possible solution.
- Be sure to frame discussions around the customer and the problem being solved, not visual design or functionality. When presenting, walk through the proposed solution from the target audience’s point of view. As the discussion unfolds, continually tie everything back to how well proposed solutions achieve Gitlab and target audience goals.
- Ask for feedback from your Brand and Digital team via Slack or in a Team Design & Development Review <link> to help improve your work. At minimum, you'll get objective feedback and new ideas that lead to better solutions. You might also get context you didn’t know you were missing, such as related GitLab initiatives and solutions you can factor into your solution.
- Engage Digital peers early and often. Their insight into technical costs and feasibility is essential to determining viable designs and MVCs.
- Collaborate with your Content Marketing group early and often, to ensure design and copy work together to create a cohesive, compelling and conversion optimized experience.
- For important project, like a key content piece and or conversion path, include your leadership in feedback, as they might have input into the overall direction of the design or knowledge about initiatives that might impact your own work.
- Work with Growth Marketing peers to align on how to measure the success of the overall initiatives you’re a part of, and your MVCs.
- Make sure your solutions are aligned to design and dev standards and best practices, such as conversion best practices.

#### Refine MVC

- MVC issues have a tendency to expand in scope. Work with your stakeholders to revisit which aspects of the solution are “must haves” versus those that can be pushed until later. Document non-MVC requirements in new issues, and relate the new issues to the original issue. If you’re ever unsure how to split apart large issues, work with your leader.
- If development needs to begin before you have completed your design, and a planning pivot to another issue is not an option, then look for high confidence and low risk elements dev can start work on while you finish the remainder of the design. To mitigate these scenarios and the dev delays and waste they can create, everyone should work together to plan ahead.
- Devs should be able to build an MVC is one sprint. If an MVC is too large to build within one release, work with your leadership and peers to split the MVC into smaller MVCs that can be closed at the end of the sprint. Note: MVC may be completed in a sprint, but released later due to dependencies on other dev MVCs.

#### Final MVC

- After you've facilitated and open and inclusive process, present your final design solution in the Design tab.
- When sharing asynchronously in an issue, make sure your audience has the context necessary to understand how your proposal delivers on the who, what and why and our brand and business goals, and anything you need from them. Is it clear who will use the solution and how it meets Gitlab and target audience goals? Is this just the final interactive design or is visual design also final? Do you need feedback or assistance from stakeholders, like final content changes? To make reviewing easier, have you highlighted how you addressed final change requests, questions and concerns.
- Set the issue marketing status scope label to  `mktg-status::design-review` to open the solution up to final review and approval. If you are not able to get approval from your leadership, despite best efforts, do not let that hold up delivery.
- Anticipate questions that others might have, and try to answer them in your proposal comments. You don’t have to explain everything, but try to communicate a bit of your rationale every time you propose something. This is particularly important when proposing changes or challenging the status quo, because it reduces the feedback loop and time spent on unnecessary discussions. It also builds the UX Department’s credibility, because we deal with a lot of seemingly subjective issues.
- Keep the SSOT updated with what’s already agreed upon so that everyone can know where to look. This includes images or links to your design work.
- If you are proposing a solution that will introduce a design, or change an existing one, please consider the following:
    1. Will this design or interaction pattern be inconsistent with like experiences?
    2. Will like experiences need to be updated to match?
    3. Is upgrading this design or interaction pattern worth the cost?

#### Deliver

- Once your work is complete and all feedback is addressed, make sure that the issue description and SSOT are up to date, you’ve validated that you have followed any required best practices, and relevant parties are informed of what to do next.   
- As applicable, commit all final design assets and files to appropriate repositories.
- If the solution needs to be broken out into smaller issues for implementation, work with your Digital peer to do so.

#### Follow through

- Encourage developers to scope down features into multiple merge requests for an easier, more efficient review process.
- When breaking down features into multiple merge requests, consider how the UX of the application will be affected. If merging only a portion of the total changes will negatively impact the overall experience, consider using a feature branch or feature flag to ensure that the full UX scope ships together.
- When breaking solutions into smaller MVCs, make sure customers do not get fragmented or incomplete experiences. Make sure everyone understands the full picture so dependent MVCs are released together.
- Keep the issue description updated with the agreed-on scope and requirements, even if doesn’t impact your work. This is everyone’s responsibility. The issue description and design files must be the Single Source Of Truth (SSOT), not the discussion or individual comments. If the developer working on the issue ever has any questions on what they should implement, they can ask the designer to update the issue description with the design.
- For obvious changes, make the SSOT description update directly. [You don't need to wait for consensus](/handbook/values/). Use your judgement.
- When the issue is actively being worked on, make sure you are assigned and subscribed to the issue. Continue to follow both the issue and related merge request(s), addressing any gaps or concerns that arise.

#### Design reviews of coded solutions

Ideally, design is part of the dev review process. Any MR that makes a significant change that is user-facing should be reviewed by a designer.

- UX reviews of coded product are a high priority. Tackle them as quickly as you are able.
- Test coded product, do not rely on screenshots.
- Be thorough. There should be as little back and forth as possible.
- If you are asked to review an MR for an issue you were not assigned to, remind the author who the assigned designer is and assign to original designer for review.
- When reviewing an MR, please use the following order of importance:
    - Functionality first: Does it work?
    - Edge cases: Are there any unexpected edge cases?
    - Visual consistency: Does it conform to brand identity?
- Remember to stick to the issue. Create issues for further updates to avoid scope creep.
- Once you have completed the review process, note your approval. You can then un-assign yourself from the MR.


### Development process

(coming soon)

#### Working in modules

##### What is a module?

1. A module is a section where the presentation, goal, and required functionality remains the same, but content can be updated (wording, imagery, links, etc).
1. A module is a block, box, or section of the page, generally kept as small as possible. It's  usually a horizontal slice of layer cake across the page but can also be a chunk of a sidebar.
1. Modules often have configurable options to facilitate reuse with different configurations. It might not always be desirable to have a title block or buttons might need to expire after a date.

##### Why is it important for a module to be reusable?

1. In order to facilitate updates, the code needs to be reusable. It's not an easy update if you have to build it again.
1. Implementing the same thing over and over again is not an efficient use of resources.
1. In order to keep the codebase clean, navigable, and easy to understand it's important to maintain SSOT.
1. If the same code is implemented several times in several spots then the chance for bugs increases. One of those spots might have a bug where the others don't.
1. Much of what goes into building code is unseen on the page. This includes things like optimizing performance, setting up tracking, preparing assets such as formatting images & videos, building responsive views and layouts, human physiology (fingers on a touchscreen, eyes and perception, etc). Testing and building all of these things takes time, so it's important to reuse and reduce code as much as possible.

##### Why is it important for a module to have a single-purpose?

1. In order to facilitate updates, the code needs to be easy to operate.
1. Having a clearly defined purpose for each module enhances the goals of the page and assists with navigation and conversion goals. If a module tries to do 5 things or there are 3 different modules on the page doing the same thing it's easy to spot.
1. An end user might be confused if duplicate modules are on the page.
1. If a module gets too large then it becomes harder to understand the code. Keeping a module single-purpose keeps the module small.
1. Tracking the performance of a module becomes more difficult the more a module changes.
1. When examining from a distance, it's hard to know what module to use if the modules all have several different purposes, sometimes overlapping purposes. "Do I use this module or that one?"

### Testing process and merge requests

For best practices regarding testing and reviewing merge requests, please see our related handbook page for [reviewing merge requests](/handbook/marketing/website/merge-requests/).

### Video bands

This is a link to our [documentation on how to implement a video band](/handbook/marketing/growth-marketing/brand-and-digital-design/video-bands/).

# Vendor Management

The Brand and Digital team is working with vendors to increase our capacity and capabilities, as needed, to meet GitLab’s brand and business objectives. The following are roles and responsibilities for vendor engagement, management, invoicing and reporting.

### Vendor recruitment

If you know of a brand or digital vendor, whether “an agency of one” (aka freelancer) or agency, please share the contact with your manager for further consideration.
    
### Vendor on-boarding

1. Manager creates [Access Request issue](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Individual_Bulk_Access_Request) for relevant GitLab projects and Slack channels
2. Manager creates Intro to GitLab issue <link>
3. Intro call with Manager, Vendor, and Vendor Buddy to set expectations, outline workflow, share relevant handbook links, and introduce first project(s).
4. Vendor completes Intro to GitLab issue, including standard and design and or dev on-boarding tasks (depending on vendor type).
5. Help any developer vendors if they have trouble setting up their code environment and please familiarize them with our codebase.
6. Questions about on-boarding, please see handbook, your manager, and or thge [#marketing_brand_contractor_onboarding](https://gitlab.slack.com/archives/C0114QFK8M6) channel.

### Vendor project management

#### Team

- **Assigning Issues to Vendors**
    - Assign the vendor and team member who will be the vendor’s buddy.
    - Ensure following labels are on the issue:
        - `vendor`
        -  `mktg-growth`, `mktg-status`
        - `design` and or `mktg-website`
    - Assign the issue to a milestone, as appropriate.
    - Confirm the issue appears on the [vendor board](https://gitlab.com/groups/gitlab-com/-/boards/1511334?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=mktg-growth&label_name[]=outsource). 
    - Capture opportunities to improve vendor management in the [vendor management epic](https://gitlab.com/gitlab-com/marketing/growth-marketing/brand-and-digital/brand-and-digital/-/issues/248).
        

#### Vendor buddy

- **Managing Vendor Work**
    - Ensure you and vendor are assigned to the issue.
    - Check-in regularly to ensure progress and clear blockers.
    - Ensure projects to which you’re assigned are maintained on the [vendor board](https://gitlab.com/groups/gitlab-com/-/boards/1511334?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=mktg-growth&label_name[]=outsource), just as you maintain your own projects.
    - Communicate with vendor in issues and or the vendor-contract slack channel, [#brand-contractors](https://gitlab.slack.com/archives/C011PDBLWLC), per GitLab’s async communication best practices. Also be aware GitLab’s async model may be new to contractors. Be open to synchronous meetings as vendors acclimate to async work.
    - Meet standard GitLab and Brand and Digital work practices, including open and inclusive design and **weekly status updates**.
    - Review vendor deliverables for quality assurance.
- **Addressing Concerns**
    - Should issues arise, provide vendors feedback in private. Be specific on the concern, how it can be resolved, and set a date for when it will be resolved.
    - Share critical concerns or patterns of issues with your manager.
    - Immediately report urgent concerns to your manager, such as significant project delays or poor quality of work.

#### Managers

- **Vendor Boards** - Ensure [vendor board](https://gitlab.com/groups/gitlab-com/-/boards/1511334?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=mktg-growth&label_name[]=outsource) is maintained.
- **Vendor Issues** - Ensure the `vendor` label is being applied to issues we *might* outsource.
- **Vendor ROI -** Ensure we are planning sprints to maximize vendor output while minimizing vendor overhead.
- **Monthly Vendor Work Report** - Provide end of month reports on what each vendor completed, and any issues encountered that require stakeholder attention.


### Vendor invoicing, payment and reporting


#### Vendor buddy

- Ensure you know enough about the [invoicing process](/handbook/finance/accounting/#invoice-accounting) to assist when directing invoicing questions from the vendor to the appropriate party.
    - Please don’t drop the ball, we want vendors to be happy and get paid.
    - Please follow any questions from start to finish to keep things moving along.
        - Ex: “I’m having trouble with Tipalti, who can help me?” (Lori Lamb)
        - Ex: “Do I need a purchase order number for my invoice?”

#### Managers

- **Vendor contracts** - Negotiate and execute [vendor contracts](/handbook/finance/procure-to-pay/), with Director’s approval. Goals are to maximize value delivered within our allocated vendor budget.
- **Invoicing** - Ensure vendors:
    - [Set themselves up properly in Tipalti](/handbook/finance/accounting/#3-vendor-master-management)
    - Adhere to GitLab’s invoicing standards
    - Invoice all remaining hours by the end of month
- **Monthly vendor work report**
    - Provide director with end of month report of vendor projects, including projects in progress and those completed within the past month.
    

#### Director

- **Approve invoices** 
    - Approve invoices in Tipalti throughout the month and at end of month.  
    - Validate vendor invoices and deliverables with  Brand and Digital managers.
    - Ensure projects are billed to the appropriate budgets.
- **Approve contracts**
    - Engage vendors in alignment with brand and business objectives.
    - Manage overall budget spend.
- **Monthly vendor work report** 
    - Add spend to Monthly Vendor Work Report and distribute to stakeholders for awareness. Stakeholders include Growth leadership, Finance, and Marketing team leads working with vendors.



# Digital FAQ

<details markdown="1">

<summary>Why isn't this form working?</summary>

### Why isn't this form working?

Many of our forms are served through a third party tool, Marketo. Sometimes Marketo is blocked by an ad blocker or a strict web browser such as [Brave](https://brave.com/). GDPR and CCPA (among other laws) require us to obtain consent before setting cookies and those cookies are required for many third party functions.

If you are having trouble using a form, please try <a href="javascript:Cookiebot.renew();">updating your cookie settings</a> to allow **personalization (personal information) cookies**. If that does not work, please try a different browser with a less strict adblocker and ensure our domain and subdomains (gitlab.com, about.gitlab.com, and page.gitlab.com) are not blocked.

If you have tried the above solutions but are still having trouble using a form, please [file a bug report](https://gitlab.com/gitlab-com/www-gitlab-com/issues/new?issuable_template=-website-bug-report). Please note that if you do not provide all of the requested information we might be unable to reproduce the bug and therefore unable to fix it.

</details>

<details markdown="1">

<summary>Why can't I see something in the review apps?</summary>

### Why can't I see something in the review apps?

This is potentially due to [cookiebot](/handbook/marketing/marketing-operations/cookiebot). Because of how our infrastructure is setup, certain third party content (such as youtube embeds) or advanced javascript may not appear on review apps. If you are unsure of the cause, please [contact us](#contact-us) so we can help review what might be impacting your project.

If you need to **temporarily** preview an item in the review app before release, you can try to add the following attribute to the frontmatter of the file: `manual_cookiebot: true`. **Do not commit that change without removing it before your final merge.** Alternately, you can follow this tutorial for steps on [how to use developer tools to view a review-app video blocked by cookiebot](https://drive.google.com/file/d/17pveEE_M7TXzar7b69ZhuwFKhKLgRZ8n/view?usp=sharing).

</details>

<details markdown="1">

<summary>What logos do we have permission to use?</summary>

### What logos do we have permission to use?

On the about.gitlab.com website we have approval to use the customer logos lisited at the following link, [Approved customer logos for promotion](https://about.gitlab.com/handbook/marketing/product-marketing/customer-reference-program/#approved-customer-logos-for-promotion)

</details>

<details markdown="1">

<summary>Can I request a specific project release time?</summary>

### Can I request a release time?

* Due to how CI/CD deployment pipelines work, release times will NOT be exact. Please plan accordingly. We try to have releases live within 1 hour of the requested timeframe.
* When requesting a release time, please specify a timezone.
* Reasons we can't guarantee a release time include:
  * We don't have a dynamic server, all items are pre-compiled and static.
  * Pipelines might have hundreds of people already in the queue before you.
  * Pipelines might be broken.
  * It takes an unknown amount of time for pipelines to allocate resources, build, run tests, and deploy.
  * It takes time for our CDN to propagate any changes across their network.
  * We don't have dedicated QA resources to ensure that things will happen as expected.
* If planning to release AT a specific time...
  * Request a time an hour before the expected release.
  * Plan for your item to appear before the expected release time. This might mean supplying alternate visuals or copy.
* If planning to release AFTER a specific time...
  * Plan for the preexisting content to cover that time range.
* Please ensure requested times are during normal business hours for the person making the changes.
  * If any changes are requested outside of their normal business hours, please ask before hand if that is possible or if someone else who is available can work on it to ensure that it releases in a timely fashion.

</details>




# Our People

<details markdown="1">

<summary>show/hide this section</summary>

<details markdown="1">

<summary>Meet the Brand team</summary>

## Meet the Brand team

[**Luke Babb**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#luke)

* Title: Manager, Creative
* Email: luke@gitlab.com
* GitLab handle: @luke
* Slack handle: @luke

[**Matt Salik**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#msalik)

* Title: Senior Brand Designer
* Email: msalik@gitlab.com
* GitLab handle: @msalik
* Slack handle: @matt

[**Monica Galletto**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#monica_galletto)

* Title: Associate Production Designer
* Email: mgalletto@gitlab.com
* GitLab handle: @monica_galletto
* Slack handle: @monicagalletto

[**Vic Bell**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#vicbell)

* Title: Senior Illustrator
* Email: vbell@gitlab.com
* GitLab handle: @vicbell
* Slack handle: @vic

</details>

<details markdown="1">

<summary>Meet the Digital team</summary>

## Meet the Digital team

[**Michael Preuss**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#mpreuss22)

* Title: Senior Manager, Digital Experience
* Email: mpreuss@gitlab.com
* GitLab handle: @mpreuss22
* Slack handle: @mpreuss22

[**Brandon Lyon**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#brandon_lyon)

* Title: Website Developer and Designer
* Email: skarpeles
* GitLab handle: @brandon_lyon
* Slack handle: @Brandon Lyon

[**Lauren Barker**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#laurenbarker)

* Title: Fullstack Engineer
* Email: lbarker@gitlab.com
* GitLab handle: laurenbarker
* Slack handle: lbarker

[**Stephen Karpeles**](https://about.gitlab.com/company/team/?department%5Cu003dops-section/#skarpeles)

* Title: Website Developer and Designer
* Email: skarpeles@gitlab.com
* GitLab handle: @skarpeles
* Slack handle: @Stephen Karpeles

</details>

</details>















# Other

<details markdown="1">

<summary>show/hide this section</summary>

## Calendars
{:.no_toc}

<details markdown="1">

<summary>show/hide this section</summary>

[ TODO : Document ]

* [TODO: Homepage merchandising schedule](#)

</details>

## Tools

<details markdown="1">

<summary>show/hide this section</summary>

- [Adobe Creative Cloud / Suite](https://www.adobe.com/): Adobe Creative Cloud is a set of applications and services from Adobe Inc. that gives subscribers access to a collection of software used for graphic design, video editing, web development, photography, along with a set of mobile applications and also some optional cloud services.
- [Sketch](https://www.sketch.com/): Create, prototype, collaborate and turn your ideas into incredible products with the definitive platform for digital design.
- [Mural](https://mural.co/): MURAL is an Online Virtual Collaboration Space, Easy to Use Specially Designed for Teams. You can Post Stickies, Share Ideas, Brainstorm and Run Product Sprints.
- [Sisense ( previously Periscope )](https://www.sisense.com/product/data-teams/): Sisense for Cloud Data Teams (previously Periscope Data) empowers data teams to quickly connect to cloud data sources, then explore and analyze data in a matter of minutes. Extend cloud investments with the Sisense analytics platform to build, embed, and deploy analytics at scale.
- [Launch Darkly](https://launchdarkly.com/): LaunchDarkly is a Feature Management Platform that serves over 100 billion feature flags daily to help software teams build better software, faster.
- [Swiftype](https://swiftype.com/): Swiftype is our search provider for the about site and handbook site. We are on a legacy "business" plan where we are allowed 100,000 documents to index, 3 engines, 24 hours for partial recrawls (edited documents), and 7 days for full recrawls (new & deleted documents).

</details>
