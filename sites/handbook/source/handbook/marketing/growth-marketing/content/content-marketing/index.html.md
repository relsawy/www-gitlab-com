---
layout: markdown_page
title: "Content Marketing"
---

## On this page
{:.no_toc}

- TOC
{:toc}

The content marketing team is responsible for strategic and resourceful content creation. The goal of the content marketing team is to drive inbound and organic traffic with SEO by optimizing webpages using keyword clusters, gap analysis research, target keyword updates, and aligning with reader intent. The team provides readers with end-to-end content experiences in order to garner trust, create source credibility, and generate customer engagement. 

The content team is using SEO-infused data to create quality definitions, web articles, and topic know-how in order to enhance the existing webpages, add more pages, and drive more traffic to the site. Working closely with the inbound marketing team, the content team has created a content strategy to ensure that people searching for topics like open source, version control, code management, CI/CD, and DevSecOps will find the best possible resources on GitLab’s website.

## Who is our audience?

The content team is specifically addressing the developer audience and those who are committed to learning more about a particular software workflow, application, standard, or methodology. Our content is created to be a trusted source for [Sasha, the software developer](/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer) who is looking to advance their career by learning new skills.

The content marketer’s goal is to steer readers through the buyer’s journey -- capture organic traffic through awareness level content and engage readers by providing links to solutions-based knowledge. The content team relies heavily on [user personas](/handbook/marketing/product-marketing/usecase-gtm/ci/#user-persona) and [use cases](/handbook/marketing/product-marketing/usecase-gtm/), developed by the product marketing team, in order to understand who our readers are and how best to support them in their software development journey. Personas, along with SEO research, help us to navigate content strategies.

## Content creation strategy

We use content pillars and topic clusters to organize and plan our work. Pillars are definied within epics on a quarterly basis and work is broken out into milestones and issues. We use content pillars to plan our work so we can provide great digital experiences to audiences. The content team aligns to themes to ensure we are executing strategically and meeting business goals. Pillars allow us to narrow in on a specific topic and audience, while topic clusters help to inform us of what search terms people are using within that subject. Each content marketer focuses on a specific part of the software development lifecycle, then creates content clusters to support the topic. 

### What is a topic cluster?

Topic clusters are pieces of content that share a common subject or subtopic and are used to enhance organic search engine results. A topic cluster begins with a pillar page, consisting of the prominent subject matter. The content team, along with detailed input from inbound marketing, works to design insightful topic clusters. Our [Topics pages](/topics/) act as pillar pages. Web articles, also called cluster pages, will support and enhance the topics pages, using primary keyword placement, search volume research, and semantic and secondary keywords. Our topic clusters are listed below. 


![Topic cluster image](/images/handbook/growth-marketing/version-control-cluster.png)


**Existing topic clusters:**
1. **[Version control & collaboration](https://docs.google.com/spreadsheets/d/1kUvj1liGbBb3cOnYqhnPGBgKNGAKO-4guwJ6LqUlSSU/edit?usp=sharing)**
2. **[Continuous integration](https://docs.google.com/spreadsheets/d/1eqFcadC0zoUn7bB9r76bBmNylMcB1ifALq_Ij-Hwpog/edit?usp=sharing)**
3. **[DevSecOps](https://docs.google.com/spreadsheets/d/13w0u994Y19RGxC6Nz3xhd55CBf--lWr_EBhj76oHgSg/edit?usp=sharing)**

### What is a content pillar?
 
A content pillar is a go to market content strategy that is aligned to a high-level theme executed via content sets. Often, content pillars are defined based on [value drivers and customer use cases](/handbook/marketing/#Go_to_market:_value_drivers and_customer_use_cases) in order to support go to market strategy. For example, "Just commit to application modernization" is a content pillar about improving application infrastructure in order to deploy faster. Within this pillar, many topics can be explored (CI/CD, cloud native, DevOps automation, etc.) and the story can be adapted to target different personas or verticals. Each set created should produce an end-to-end content experience (awareness to decision) for our audience.

![Content pillar diagram](/images/handbook/marketing/corporate-marketing/content-pillar.png)


#### What's included in a content set?

A content set is the collection of work that is be created each quarter. It covers all three stages of the buyers journey. The content marketers are responsible for awareness level and some consideration level content, and the product marketers cover the purchase and some consideration content.

Here's an example of what's included in a content set:

| Stage | Content Type | DRI |
| ------ | ------ | ------ | ------ |
| Awareness | Thought leadership blog post | Content marketing |
| Awareness | Topic page | Content marketing |
| Awareness | Web article | Content marketing |
| Awareness | Resource | Content marketing |
| Consideration | Case study | Content marketing |
| Consideration| Technical blog post | Content marketing |
| Consideration | Whitepaper | Product & technical marketing |
| Consideration | Solution page | Content & product marketing |
| Consideration | Webcast | Product & technical marketing |
| Purchase | Demo | Technical marketing |
| Purchase | Data sheet | Product marketing |

**Sources:**

- [What Is a Content Pillar? The Foundation for Efficient Content Marketing](https://kapost.com/b/content-pillar/)
- [How to Create Pillar Content Google Will Love](https://contentmarketinginstitute.com/2018/04/pillar-content-google/)
- [What Is a Pillar Page? (And Why It Matters For Your SEO Strategy)](https://blog.hubspot.com/marketing/what-is-a-pillar-page)
- [Content for Each Buying Stage of the Consumer Purchase Cycle](https://contentwriters.com/blog/content-consumer-purchase-cycle/)
- [AlsoAsked.com](https://alsoasked.com/)

## Meet the Content Marketing team

**[Erica Lindberg](https://gitlab.com/erica)**

Title: Sr. Manager, Global Content

GitLab handle: @erica

Slack handle: @erica

**[Brein Matturro](https://gitlab.com/bmatturro)**

Title: Manager, Content Marketing

GitLab handle: @bmatturro 

Slack handle: @Brein

**[Suri Patel](https://gitlab.com/suripatel)**

Title: Sr. Content Marketing Manager

Area of focus: Dev, Version control and collaboration

GitLab handle: @suripatel

Slack handle: @suri

**[Valerie Silverthorne](https://gitlab.com/vsilverthorne)** 

Title: Sr. Content Marketing Manager

Area of focus: Sec, DevSecOps

GitLab handle: @vsilverthorne

Slack handle: @ValSilver

**[Chrissie Buchanan](https://gitlab.com/cbuchanan)** 

Title: Content Marketing Manager

Area of focus: Ops, CI/CD

GitLab handle: @cbuchanan

Slack handle: @chrissie


## Planning timeline

Pillar strategy is planned annually and reviewed quarterly. Content sets are planned quarterly and reviewed monthly. When planning, we follow the 80/20 rule: 80% of capacity is planned leaving 20% unplanned to react to last minute opportunities.

- **2 weeks prior to start of the quarter:** Proposed content plans are published to the content schedule. Product marketing, digital marketing, and corporate events marketing give feedback on the plan.
- **1 week prior to the start of the quarter:** Kickoff calls are held.
- **1st day of the quarter:** Content plans are finalized.
- **1st of each month:** Progress reviews are held. Plans are adjusted if needed.
- **Last day of the quarter:** Cross-functional retrospective is held.

## Gated asset creation workflow 
Gated assets are created and written by the content team. The process will vary, however the content strategy and timeline should be consistent. Several other teams are involved in the planning process for whitepapers and eBooks. To learn more about the campaign and marketing strategy, the [Marketing Programs Management section](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/#overview)) of the handbook has an overview.

All gated assets should have an epic. There are many teams involved in creating the final product and an epic helps keep al realted issues together. The epic is the single source of truth for workflow deadlines. Gated assets include eBooks, whitepapers, demos, and webcasts. All gated assets can be found on the [Resources](/resources/) page.

Visit the [Gated Content page](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/#gated-content) for detailed instructions on [how to structure your epic, issues, and timeline.](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/#timeline-and-process-alignment) 

### Content types: Definitions and workflows

#### Blog post

A post on the GitLab blog is used during the Awareness or Consideration stage of the buyer's journey. A blog post can educate, entertain, tell a story, take an opinionated stance, etc. (See: [Attributes of a successful blog post](/handbook/marketing/blog/#attributes-of-a-successful-blog-post).) A blog post is dated, so it only reflects thoughts, ideas, and processes from a specific period of time. For communicating long-term/evergreen ideas or processes, consider using a [topic page](/handbook/marketing/growth-marketing/content/content-marketing/#topic-page) or [web article](/handbook/marketing/growth-marketing/content/content-marketing/#web-article) instead.

**Content Marketer workflow**
1. To suggest a blog post, follow the **[How to pitch a blog post](/handbook/marketing/blog/#how-to-pitch-a-blog-post)** process in the handbook. It's ideal to do this as far in advance of when you hope to publish as possible, so there's plenty of time to discuss the ideal angle or approach to optimize for GitLab's blog audience. Be sure to include any context the Editorial team might need when reviewing your pitch (e.g. Is the post supporting a campaign? Is there a specific reason why you chose this topic? Does anyone else need to approve the post before we can go ahead?)
2. Once the Editorial team has provided feedback and approved the blog topic and publish date, consider opening an issue in the [Content Marketing project](https://gitlab.com/gitlab-com/marketing/growth-marketing/global-content/content-marketing/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) to capture your work during the writing and collaboration process (See: [Every task should be an issue](/handbook/marketing/project-management-guidelines/issues/#every-task-should-be-an-issue)).
3. Once blog copy has been reviewed by the required SMEs/strategic marketing counterparts, close the Content Marketing issue and [create a merge request](/handbook/marketing/blog/#when-your-post-is-formatted-and-youre-ready-to-create-your-merge-request) from your www-gitlab-com issue. Please submit your MR to the Editorial team a week prior to the publish date; this allows sufficient time for working through feedback and making improvements to ensure your post has the best chance of getting lots of unique views. If there is a delay, your MR needs to be in a _minimum of 2 working days_ before the publish date.


#### Whitepaper

A whitepaper is a technical and focused topic study intended to educate a prospective buyer during the Consideration or Purchase stages of a campaign. The whitepaper offers a problem and solution instance in a granular, technical tone. The content team member should collaborate closely with their product marketing counterpart when researching and writing the asset so that the content reaches appropriate technical standards for the intended audience. 

**Examples:** 
1. [A seismic shift in application security](/resources/whitepaper-seismic-shift-application-security/)
2. [How to deploy on AWS from GitLab](/resources/whitepaper-deploy-aws-gitlab/)

**Timeline:** 1-4 weeks to plan, research, and draft a whitepaper. Use [the content resource request template](https://gitlab.com/gitlab-com/marketing/growth-marketing/global-content/content-marketing/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#) for planning with team members and to define a specific deadline. All whitepapers should have a launch date on the [editorial calendar](/handbook/marketing/growth-marketing/content/editorial-team/#blog-calendar).

**Deliverables:**
* Week 1: Create and share a detailed outline of the whitepaper with involved team members.
* Week 2: Share a first draft with the team and include review parameters and deadlines.
* Week 3: PMM and subject matter expert (SME) reviews are completed. The whitepaper content track changes are reviewed/accepted by the content DRI and shared with content team reviewer(s).
* Week 4: Reviews are completed. You may need an additional week and second round of reviews if significant content changes are made.

#### eBook

An eBook tends to be broader in scope than a whitepaper and provides a clear definition of the topic, along with various industry standard best practices. eBooks typically provide awareness-level content, but can dive deeper into GitLab if it's intended for late-stage consumption. These are identified in the design template as Beginner, Intermediate, or Advanced ebooks. 

The content team member takes the lead on developing eBook content, with input and review from their product marketing counterpart. More technical or instructive eBooks might require more collaboration with product marketing.

**Examples:**
1. [An intermediate guide to GitOps, Kubernetes, and GitLab](/resources-ebook-gitops-kubernetes-gitlab.html)
2. [The GitLab Remote Playbook](/resources/ebook-remote-playbook/)
3. [The benefits of single application CI/CD](/resources/ebook-single-app-cicd/)

**Timeline:** 1-3 weeks to plan, research, and draft an eBook. Use [the content resource request template](https://gitlab.com/gitlab-com/marketing/growth-marketing/global-content/content-marketing/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#) for planning with team members and to define a specific deadline. All eBooks should have a launch date on the [editorial calendar](/handbook/marketing/growth-marketing/content/editorial-team/#blog-calendar).

**Deliverables:**
* Week 1: Share a detailed outline with involved team members.
* Week 2: Share a first draft with the team and include review parameters and deadlines.
* Week 3: Reviews are completed. You may need an additional week and second round of review if significant content changes are made.

#### Infographic

An infographic is an illustrated overview of a topic or process, and is typically an ungated asset. Infographics should tell a story using data, diagrams, and text. It can be used to discuss industry trends, relate insights, or explain different stages of a project. When planning an infographic, it's important to meet with the design DRI before writing the content for the asset. Together, figure out a structure for the graphic, and ask the design DRI for recommended copy lengths.

**Example:**
1. [Git cheat sheet](/images/press/git-cheat-sheet.pdf)

#### Topic page

A topic page is a high-level explanatory "pillar" page dedicated to a specific topic, such as [version control](/topics/version-control/), [application security](/topics/application-security/), or [cloud native](/solutions/cloud-native/). Topic pages should explain what the subject is, why it is important, and explain the basic concepts of the subject. Topic pages should include links to additional related resources, such as blogs, web articles, videos, and case studies, as well as at least one CTA to a gated asset. 

#### Web article

Web articles are educational, informational content, designed to support topic pages using keywords and search terms. They are similar to blogs in length, but differ in that they are not dated and the content is evergreen (see [more about blog posts](#blog-post)). Web articles should be linked on one or more topic pages, and should serve as a deep-dive into a specific sub-topic. Web articles are created in accordance with SEO research and campaign goals that are listed in the topics clusters spreadsheets. Complete the [SEO web article template](https://docs.google.com/document/d/1ON2CZ7uufl9gtqh-P67AxU5Y42ctQ3mRtPsgJRlep7Y/edit?usp=sharing) when creating a new piece of content.

#### Case study

Case studies are in-depth customer stories that provide insight as to how GitLab has resolved significant software workflow problems for a company. The case study tells the story using quotes from customer interviews and straightforward metrics that broadly show the impact of adopting GitLab. 

[Case studies](/customers/) are created in partnership with the customer reference team. The customer reference team has a process in place for how they add new references and provide a list of [customer value drivers](/handbook/marketing/product-marketing/customer-reference-program/#customer-case-studies). The content team is responsible for writing the case study asset using [this template](https://docs.google.com/document/d/1UbhW2AEP7BfEZJGcp7w4LS6-W-xSeGET4K4NDr7bP0E/edit?usp=sharing), [publishing the case study](/handbook/marketing/product-marketing/customer-reference-program/#publishing-to-the-website) to the website, [adding the customer logo](/handbook/marketing/product-marketing/customer-reference-program/#adding-customer-logo-and-case-study-to-customers-grid) to the customer logo grid, and ensuring that the case study goes through proper social media and newsletter steps. 

**Examples:**
1. [How Hotjar deploys 50% faster with GitLab](/customers/hotjar/)
2. [Axway aims for elite DevOps status with GitLab](/customers/axway-devops/)
3. [Security provider KnowBe4 keeps code in-house and speeds up deployment](/customers/knowbe4/)

### Epic template

```
**Epic title:** [asset type] Working Title - Launch date

## Launch date: TBD

* Content DRI:   
* MPM DRI: 

### Deadlines

* [ ]  2020-XX-XX Open copy issue and complete asset brief (-27 business days from launch)
* [ ]  2020-XX-XX Landing page copy due (-27 business days from launch)
* [ ]  2020-XX-XX Final copy due  (-15 business days from launch)
* [ ]  2020-XX-XX Final design due (-5 business days from launch)
* [ ]  2020-XX-XX MR ready to launch (-3 business days from launch)
* [ ]  2020-XX-XX Asset added to pathfactory (-3 business days from launch)
```


