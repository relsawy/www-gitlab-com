---
layout: handbook-page-toc
title: "Field Marketing"
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Field Marketing during COVID-19 Pandemic

The health and safety of our team members continue to be a top priority. Given the continuing situation with COVID-19 worldwide, our eGroup has decided that we will not invest in any in-person marketing events for the remainder of our fiscal year, ending January 31, 2021, globally. If you have any questions please feel free to reach out to the VP of Revenue Marketing, the Director of Field Marketing, or your direct line manager.

Not to fear, the Field Marketing team is busying still helping to create pipeline via our other tactics. Check out all the awesome work on the [regional issue boards](/handbook/marketing/revenue-marketing/field-marketing/#whats-currently-scheduled-in-my-region).

# Field Marketing

The role of field marketing is to support marketing messages at a regional level through in-person and virtual interactions (quality vs. quantity) coupled with multi-touch activities. Field Marketing programs are focused on relationship building with customers and prospects to support land and expand opportunities as well as pulling pipeline through the funnel more quickly.

# Field Marketing Goals

- Sales Acceleration
    - Engaging with existing customers
    - New growth opportunities
- Demand
    - Education
- Market Intelligence
    - Test out new messaging or positioning

# Account Based Marketing

Account based marketing is separate but sits next to field marketing. For info on account based marketing, please head over to the [ABM page](/handbook/marketing/revenue-marketing/account-based-marketing/).

# Types of Programs Field Marketing Runs

## In-Person Event Changes Due to COVID-19

### Owned Event Turned Virtual

- FMM to notify MPM as soon as possible - this process takes considerable time to deconstruct existing setup and begin setting up as virtual
- MPM to follow [this process](/handbook/marketing/events/#when-owned-offline-events-turn-to-virtual-owned-events)

### Field Event Turned Virtual

- FMM to notify MPM as soon as possible - this process takes considerable time to deconstruct existing setup and begin setting up as virtual
- MPM to follow [this process](/handbook/marketing/events/#when-offline-field-events--conferences-turn-to-virtual)

## GitLab Owned Field Events

### [GitLab Connect](https://www.youtube.com/watch?v=aKwpNKoI4uU)

GitLab Connect is a full or half day event with both customers and prospects in attendance sharing stories & lessons learned about GitLab. SAL's will be responsible for asking customers to speak and Marketing, through a combination of SDR outreach, database and ad geotargeting will drive attendance to the event. If you would like to propose a GitLab Connect in your city, please follow [these instructions](/handbook/marketing/events/#suggesting-an-event) for requesting an event. Interested in seeing a GitLab Connect in action? [Check it out.](https://www.youtube.com/watch?v=aKwpNKoI4uU)

### GitLab-Hosted Workshops

Field Marketers will work with the Customer Success teams, Product Marketing & Solution Engineers to build various types of workshops, depending on the needs of the region. These workshops are designed to accelerate pipeline and attract new customers to GitLab by providing a hands-on experience. 

* Field Marketing + SA Workshops available to date can be found [here](https://drive.google.com/drive/folders/1qAymFTiXFEk-lRSNreIhaZ6Z62fdo_y2).
* Technical Product Marketing led workshop content is [available here](https://gitlab.com/gitlab-workshops)

#### Detailed information on GitLab-Hosted Virtual Workshops
Please visit out [workshop handbook page](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/virtual-workshops/).

## 3rd Party Events

We will sponsor regional 3rd party events in an effort to build the GitLab brand and also to gather leads. Different type of 3rd party events include, but are not limited to:

- DevOps Days
- Agile Events
- City run technology meetings
- Customer/prospect run DevOps events on invite
- Executive relationship building events via companies like Apex Assembly & Argyle Executive Forum

#### Executive Roundtables

Executive Roundtables can be run virtual or in-person and differ from webcasts/events mainly due to the size of the audience and the interactivity level. A roundtable is run as an open discussion between the host (usually 3rd party vendor), GitLab presenter and the audience. The host would open with an introduction of themselves and the topic for the session, then introduce the GitLab presenter and have them give an overview of GitLab followed by the host asking questions directly to specific people in the audience for them to openly answer and discuss. The advantage of a roundtable is that you can document the meeting more closely and understand more about an organization’s pains and problems.

Below are best practices when running a roundtable:

- Content creation - FMM to work with Sales Managers to decide on most relevant or hot topics in the region
- Speaker request - Put through a Strategic Marketing Support Request and fill out the template
- [Example of scripts](https://docs.google.com/document/d/1iy7AreiMM7seZM_EidixqdUw-OG__-cnl6pihqH72eY/edit#heading=h.btpndxf36cvl) for host and GitLab presenter
- [Example of meeting notes](https://docs.google.com/document/d/1T2m3Izn66u9xuWGDHmR5_YJcWdUhKupfVlWVPjWGLL0/edit) to document conversations during the session
- Ensure questions are prepared between host and GitLab presenter beforehand to lead the conversations within the session
- Assign notes of each delegate to list leads for upload onto SFDC
- Pre-analysis of delegates - check to see if their organization is currently a user of GitLab, whether they’re CE or EE customers. This could be a great way to start or dig into deeper conversations with the delegate.

### How to operationally set up an in-person event

Field Marketing works closely with our Marketing Program Managers to functionally set up and execute our in person events. For complete details on our shared process, including the epic/issue creation process, please review our [event execution page](/handbook/marketing/events/#event-execution).

## Other Tactics

Being the marketing experts for the region, GitLab Field Marketers are also responsible for using other tactics to help generate leads to build pipeline. We have options such as survey tools, both of current employees and past employees based on the 3rd party vendor we use, webcasts, direct mailings, and also various digital tactics.

### Alyce Direct Mail 

#### Overview
Alyce is a direct mail campaign used to land and accelerate deals to fuel pipeline progression using personalized gifts from information pulled via social media accounts as a thank you meeting with our Sales team. Our strategy will be limited to digital only during covid-19 using email campaigns, social media, and other SDR outreach methods until we can safely return to physical mailing as our prospects move back into the office.

Note: Gift recipients will always be able to donate the value of their gift to a meaningful charity or exchange for something different with equal or lesser value.

#### Regions that use Alyce 
At this time, Alyce is only used by our AMER Field Marketing team via a pilot program. 

#### Successful Usage
Strategic success is contingent upon close alignment and communication between Marketing and Sales. Alyce believes that prospecting by building rapport, earning trust and driving loyalty are the keys to success and have built their entire platform from this model. SDRs will leverage templates available in the Alyce tool that they are able to customize.

**We will work together to complete the following:**  
  
- Determine our list of top prospects   
- Create account-centric plan per SAL  
- Understand Alyce platform and how to use it  
- Review Alyce use-cases to optimize success  

#### SDR Allocation per Gifting Rules
     
- MQL Score < 30 = $35 gift allowance  
- MQL Score 31-75 = $50 gift allowance  
- MQL Score 76-100 = Up to $100 gift allowance  
- For a higher gift allowance a very specific justification should be presented and would need to be approved by FMM DRI  

#### SDR Requirements for Booking IQMs via Alyce

- Meetings that are booked by SDR leveraging Alyce should use the following naming convention when creating an IQM Event: IQM - Prospect Name, Account Name - Alyce. This will allow FMM's and SDR Managers to filter reports by IQMs booked via Alyce platform. 

#### Training and Helpful Videos
To be successful, there will be a series of training sessions to enable the Sales and Marketing team success. 
- [Introductory Training/Sales Enablement](https://gitlab.zoom.us/rec/share/OgjnGu92hWJJO0wOD2YqFXQDEBt9VYIgDwOXbrQQBJBslvWAkbPWMxLPs0kgvRCC.2ioRU-qJbvioHhVJ?startTime=1596812685000) 
- [SDR Enablement Training](https://gitlab.zoom.us/rec/share/1uDOxHhjx6C15dwko_u_6BdCK6L6tNtKJn52AK6Wwt3Q2_UifMa2jUEehedxHjMZ.WT9ikakQyMlfrFJF?startTime=1598983434000)
- [Alyce Quick Start Video](https://gitlab.zoom.us/rec/play/mEBrvrZfMPrzjZhmUtndNJU9_ZWrlU0wfjoWxGX1gNv6ZkzP1WVafvDyGYjUAUR4NFsExNb2S9C1_k6X.aE3YZYdS_rKq23W8?continueMode=true)

#### Reporting and Dashboards
Alyce defines influence as engagement, which would be any opportunity/pipeline with an Alyce gift that passed through viewed and/or accepted. We have integrated the Alyce dashboard into SFDC for easy access [HERE](https://gitlab.my.salesforce.com/01Z4M000000oXdn). There is also a report folder for Alyce, which can be [found here](https://gitlab.my.salesforce.com/00O?fcf=00l4M000001AD1I). 

#### ROI & Metrics
- Total number of engagements
- Total number of meetings
- Total number of SAOs
- Pipeline progression 
- 90 or 180-day progression 

#### Personas
Our focus will be geared towards key-decision makers or fellow team members who can help push to key decision makers. This opens up our outreach to anyone in DevOps, DevSecOps, IT, and other related fields with titles ranging from Developer Team Lead to Executives.  

Building rapport with a personalized experience that’s relatable, relevant, and respectful will be our cornerstone framework for any persona. Preferably, we’ll have had previous conversation before using Alyce.

#### Sales Acceleration
Every deal has multiple layers and multiple teams involved. This can lead to opportunities stalling unless you can re-engage and advance the conversation. Once a SAL has identified that they need to move the needle, our team will engage specifically with Alyce to move the conversation along. 

If the SAL or the SDR has already started early stages discussion, a post-meeting follow-up will keep the relationship moving forward. By using information learned at their earlier communications they can send a personal thank you.

Once a target person has been identified within the Alyce system, the system needs 6-12 hours to come to you with suggested gifting options specific to that person. 

### Printfection Direct Mail Giveaways

The below steps can be followed to set up and run a giveaway campaign using Printfection for field marketing. If you come across any steps that you can’t follow in Printfection, post in the [#swag Slack channel](https://app.slack.com/client/T02592416/C66R8N98F) so a Community Advocate can update your permissions levels.

1. Register and send the items you will be using to Printfection by following [these steps](/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling/#send-items-to-printfection-warehouse)
1. Bundle your items together by creating a [Giveaway Kit](https://help.printfection.com/hc/en-us/articles/360006335613-How-to-start-a-new-kit) in Printfection
    - The picture of the Giveaway Kit is what the customer will see. If you’d like a different image to show than what Printfection puts together as a preview, be sure to provide this to the support team
    - Select Standard packaging or choose to create a custom box (a new custom box will incur additional expenses). If you’d like to use already created custom packaging (such as the GitLab branded Poly Mailers), check with the advocates in the #swag slack channel to make sure it’s okay for you to use
1. Set up a [Giveaway Campaign](/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling/#create-a-new-giveaway-campaign)
    - [Add the kit](https://help.printfection.com/hc/en-us/articles/360026589734-Using-kits-in-Giveaway-campaigns) you created as an item in the Giveaway Campaign
    - The name of the giveaway campaign will be visible to the customer
    - Change from Paused to Running when you’re ready to run the campaign
1. Add a Printfection code column to your event spreadsheet with your leads. Copy over unique codes (generated above in the Printfection Giveaway Campaign) to assign a code to each lead
1. Work with your MPM to set up the Printfection URL & unique codes in your email copy (if desired, steps 5-7 are only relevant if sending a Marketo email rather than individually distributing the redemption links):
    - Place the Giveaway Campaign URL in your email copy (found on the Overview tab of the campaign in Printfection)
    - A custom Marketo field will be used to append the unique Printfection codes to the URL for each lead (example of how it works [here](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/1195#note_325404319)) and to place the code under the URL
1. Complete list upload prior to the email send date (example list upload [here](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/2343))
1. Work with your MPM to determine post redemption email needs. The options are follow up email and an automated confirmation email. The confirmation email triggers upon redemption of codes, the follow up email is sent at one time to a group of recipients. If an automated confirmation email is needed, define what status in SFDC should trigger the follow up email
using the Direct Mail campaign status progressions. (for example, when a person's campaign status changes to `Queued` it could trigger a confirmation email and when the status changes to `Shipped` the SDRs will begin follow up sequence (example [here](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/1195#follow-up-planned-from-field-marketing-fmc))
1. Once all above steps are complete, the Giveaway Campaign is ready to run. Make sure the Giveaway is changed from Paused to Running if you did not do this already in Step 3
1. When the campaign is running, monitor Redemptions, Shipping, and Costs under the Giveaway Campaign Overview in Printfection
    - Update redemptions/shipments in event spreadsheet if needed to notify SDRs (add additional columns to track)
    - Update SFDC status for leads once they redeem a code or item is shipped, depending on what was decided in Step 7
1. When a campaign is over and you no longer need the items in Printfection, follow [these steps](/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling/#removing-products-from-printfection) to remove items from Printfection. If inventory is not yet 0, you will need to ship the items back to yourself/desired recipient before archiving. Until this is done, $25/month will still be charged per item. If relevant, the MPM must be instructed as to when the printfection codes are no longer needed on person records. This will signal that the MPM can now run a batch smart campaign to remove the printfection codes from the person records, thus freeing up the custom field for use in the future.

**Fees Involved:**

- Cost of the items you are using in the giveaway
- Fulfillment and shipping fees for each order placed once the giveaway is running (there is a Cost Estimate under campaign settings but please note actuals have ended up higher than their estimate so far) These will be on a Printfection invoice, more details to come on the invoice process.
- $25/month per item stocked in Printfection. This will not hit your field marketing campaign tag, but is an overall cost to GitLab to consider and be mindful of (noted in Step 12 [here](/handbook/marketing/community-relations/community-advocacy/workflows/merchandise-handling/#creating-the-send-order)

### EMEA Giveaway/SWAG Campaigns

- When printfection is not an option, EMEA Field Marketing utilise the services of vendor [Ten&One](https://www.tenandone.com/) to source, fulfill and ship giveaways/swag [EMEA SWAG](/handbook/marketing/events/#emea-field-marketing-swag). Please create an issue utilizing the [EMEA Swag Request Template](https://gitlab.com/gitlab-com/marketing/field-marketing/-/blob/master/.gitlab/issue_templates/EMEA_SWAG_Request_template.md) or contact the EMEA FMC for further information and options.

## Digital Tactics

- To run plays where we are targeting a specific geography or where we would like to propose content syndication, we work through our Digital Marketing Programs team. Please create an issue utilizing DMPs [Paid Digital Request Issue Template](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/new?issuable_template=paid-digital-request) if you'd like them to do work for you.
- To run plays where we are targeting a specific list of accounts, we work with our ABM team utilizing [this issue template](https://gitlab.com/gitlab-com/marketing/account-based-marketing/-/blob/master/.gitlab/issue_templates/Demandbase_Campaign_Request_Template.md).

## Account-Centric Micro-Campaign

The purpose of the regional integrated micro-campaign is to build and drive a specific strategy in a region (with a specific list of targeted accounts/look-a-like accounts). This could be based on intent data pulled from our ABM tool, DemandBase, or it could be based on accounts you’ve agreed to target with your SAL. A micro-campaign must include a minimum of 2 different account-centric tactics.

**For example:**

- If you are planning a virtual Lunch & Learn and want to utilize LinkedIn InMail as well, those 2 account-centric tactics together make up a micro-campaign. However, if you are planning a Lunch & Learn on its own without any additional tactics, that is just considered an [Individual Tactic](/handbook/marketing/revenue-marketing/field-marketing/#individual-tactics).

### What does Account-Centric mean?

At times, Field Marketing will run an account-centric approach to its in-region campaigns. Account-Centric is an approach where both sales and marketing are focusing their efforts on a very specific list of targeted accounts in order to increase engagement and ultimately pipeline creation within this list of accounts. The SAL/FMM/SDR are responsible for outlining which accounts are account-centric and accounts can easily pop in and out of being in the account-centric play based on the strategy that’s being executed.
i.e. If a FMM is targeting to get 15 people into an interactive roundtable discussion, that will greatly impact the number of accounts that are targeted for that specific tactic vs if they were trying to 100+ people into that same discussion.

### How to track the ROI of your Account-Centric approach

In order to track engagement, you need to start with a baseline and over time, track the progress on how engaged that account is with us. Right now this is a manual process where this [SFDC report](https://gitlab.my.salesforce.com/00O4M000004e4Ll) should be modified by pulling in all the accounts that are in the account-centric GTM Strategy per rep and then you must take a snap shot in time (SFDC does not allow for historical data pulls) by exporting the data and saving to a gsheet. [Example of gsheet](https://docs.google.com/spreadsheets/d/1m7xxoKcXW3Lq8eXIPg5KIOMGXIItp7MEZeBXAFfszOI/edit#gid=206509375&range=A1). Depending on your campaign, you can export this data on a weekly or monthly cadence to monitor progress.

In order to change the GTM Strategy field for accounts in SFDC from `Volume` to `Account-Centric`, you can employ two options: a) Change the GTM Strategy field for each account manually if you have <20 accounts in the campaign, or b) Create a MOps request to have all accounts changed from `Volume` to `Account-Centric` in bulk. To request the bulk adjustment, please utilize [this MOps template](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/new?issuable_template=abm_bulk_update_request).

Account-Centric is different from Account Based Marketing in the sense that ABM focuses on accounts as markets of one versus account-centric targeting a group of accounts. At GitLab we run a 3-Tiered approach to ABM focused around our ICP and target 100-150 accounts in our ABM strategy at any given time. For complete details on how GitLab runs its Account Based Marketing, please head to the [GitLab ABM page](/handbook/marketing/revenue-marketing/account-based-marketing/#what-is-account-based-marketing).

At GitLab, we utilize Bizible for attribution tracking. In order to track the touchpoints of your campaign, modify [this linear IACV SFDC report](https://gitlab.my.salesforce.com/00O4M000004aDSN) . This report shows Opportunity: Incremental ACV and Linear iACV.

- If your campaign has a SFDC campaign with leads associated to it, then you will pull in the SFDC Field `Salesforce Campaign` equals .
- If your campaign does NOT have a SFDC Campaign with leads associated OR you also are wanting to track the opportunities from your digital spend, then you would add in the SFDC `Ad Campaign Name` equals . We use the same UTM per channel, so all channels (paid ads, LinkedIn Mail, DemandBase, etc.) will pull all influenced iacv. Stay tuned for more details on Bizible.

### Account-Centric Micro-Campaign Team

- Field Marketing Manager - overarching DRI responsible for opening [the micro-campaign issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=Micro_Campaign_and_Individual_Tactics), reviewing the intent data from the ABM DRI, and building the strategy based on what the intent data is suggesting (i.e. are a lot of folks searching to Kubernetes? Is it digital transformation? What call to action do we have? Do you want a direct mail piece involved? Do you want to drive to an On-Demand or scheduled webcast?), owning, and driving the mico-campaign, including epic and issue creation and communicating timelines and actions needed of other team members with relevant SLAs from request to delivery.
- Account Based Marketing (ABM) Manager - will work with FMM to produce target account list based on intent data and known targets & will also lead strategy session with Demandbase Campaign Manager, should the FMM decided its part of the strategy to deploy ads via Demandbase.
- Sales Development Rep (SDR) - Will assist in building out the actual target people & build Outreach cadence to support agreed upon CTA.
- Digital Marketing Programs - Can assist in deploying paid digital strategies (ex: display ads, paid search, paid social, and/or paid publisher placement) and geo focused ads via our digital agency. They can also help get our digital agency involved should the FMM want to engage with the agency.
- Marketing Program Managers - Setup of Marketo and Salesforce campaigns with correct program type to support the tactic. Guidance on system tracking, setup of Marketo email follow up, and addition of needs to existing email nurture programs (all as needed). Facilitate new program types with MOps and other MPMs as necessary.

### Steps for the Creation and Organization of Micro-Campaign Epics & Issues

#### 1. FMM opens overarching issue

<i>FMM opens a field marketing issue using the [Micro-Campaign and Individual Tactics Template](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=Micro_Campaign_and_Individual_Tactics)</i>, listing the tactics to be used from the list below in step 2. At this point, the issue is in `PLAN` stage.

**WHY? The purpose of opening the overarching micro-campaign issue is to discuss the plan with any teams that would require work and gain agreement on SLAs before opening epics and issues.** _Remember - as of today, epics cannot be made confidential, they cannot show up on issue boards, and there is no such thing as an epic template._

**Here is what needs to be included before moving from `PLAN` to `WIP`:**

- Agreement on SLAs and scope between other teams involved in selected tactics - [SLA workback doc](https://docs.google.com/spreadsheets/d/1YXriQ1clvYyBn-TDbbCVvNP6NEbrAF-0w6tIHKhDeZM/edit#gid=1983708280)
- Formulation of a clear strategy and understanding of customer journey
- Completion of all fields in the `Details` section of the micro-campaign template

_For anything that’s listed in the `Tactics included` section of the template the FMM needs to be clear about their specific ask & what the desired outcome will be. If the FMM does not know, it's ok to set up a call to talk through ideas and strategies._

#### 2. FMM opens specific tactic issues

_FMM opens additional issues relevant to their micro-campaign, if required._

**WHY? The purpose of opening the issue is so that each individual tactic shows up on the FMM boards.** _Remember - as of today, Epics cannot be made confidential, they cannot show up on issue boards, and there is no such thing as an epic template._

**Issue Templates for Each Tactic:**       
Please see the Field Marketing Issue Templates and Epic Codes list [**HERE**](/handbook/marketing/revenue-marketing/field-marketing/#field-marketing-issue-templates-and-epic-codes) for a comprehensive list of templates to be utilized for each tactic.

#### 3. FMM creates tactic epics

_FMM follows the instructions for epic creation based on the tactics decided._

**WHY? The purpose of opening the epics and issues is to organize the campaign in GitLab with the epic being equivalent to the project/campaign and issues being equivalent to the tactical execution steps to complete the campaign.**

**How to create the epic:**

- Where to create the epic: [Marketing Epic Repo](https://gitlab.com/groups/gitlab-com/marketing/-/epics?sort=created_desc)
- How to create the epic: Click "New Epic" at the top right of the screen
- What to name the epic: The naming convention is listed in the epic code for each specific tactic.
- What to choose as the due date: this should be the same as your micro-campaign issue due date. For example - If your micro-campaign involves 3 different tactics and the last tactic is set to close out on December 5, 2020, that should be your due date.
- How to link the epic to the issue: In your epic click on Epics + Issues - Add - Add an existing issue - Link your issue

**Micro-Campaign Epic Code**
This epic will include the general details of your micro-campaign.

```
<--- Name this epic using the following format, then delete this line: [Micro-Campaign Tactic Name] - [3-letter Month] [Date], [Year] --->

## [Micro-Campaign Issue >>]()

## :notepad_spiral: Details 
(copy and pasted from your micro-campaign issue)

/label ~"mktg-status::wip" ~"Field Marketing" ~"FMM-MicroCampaign" ~"FMM-Other Tactics"
```
**Epic Codes for Each Tactic:**    
Please see the Field Marketing Issue Templates and Epic Codes list [**HERE**](/handbook/marketing/revenue-marketing/field-marketing/#field-marketing-issue-templates-and-epic-codes) for a comprehensive list of epic codes to be utilized for each tactic.

#### 4. FMM creates relevant issues and assigns DRIs and due dates

_At this point the FMM has the main issue (for agreement by teams), the tactic issue(s) (to show up on boards), and the tactic epics (to capture overarching tactic plan and organize issues). Now FMMs will create the issues to request the work and timeline of teams involved in each tactic._

In each epic code (from step 3), there are issue template links included that indicate what issues need to be created (example - follow up email, facilitate tracking, etc.) - if they do not say "optional" then they are required. The FMM will create each issue and link to the epic so that all tactical issues are organized together clearly in GitLab.

The FMM will designate DRI as indicated and set due dates based on [SLAs](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/#timeline-guidelines) for each item that is being requested. [SLA workback sheet](https://docs.google.com/spreadsheets/d/1YXriQ1clvYyBn-TDbbCVvNP6NEbrAF-0w6tIHKhDeZM/edit#gid=1983708280) is available to help you.

**Note: This is essentially creating the work-back execution plan using issues, DRIs, and due dates.**
If the date of the tactic is changed, the FMM is responsible for updating the due dates of all issues accordingly. It is also important to remember that the DRIs on the issues are not notified when due dates are added. If you create an issue and then add due dates at a later time (or change due dates), please remember to ping the DRIs in the issue so they are aware dates have been added or changed.

#### 5. Organizing Epics

Your micro-campaign issue and epic is an overview of your micro-campaign strategy and each individual tactic supporting the micro-campaign requires its own issue and epic. When you add individual tactic epics to the micro-campaign epic, the micro-campaign epic automatically becomes the [Parent Epic](https://docs.gitlab.com/ee/user/group/epics/#relationships-between-epics-and-issues). Each individual tactic epic, when nested under the micro-campaign epic, automatically becomes the [Child Epic](https://docs.gitlab.com/ee/user/group/epics/#relationships-between-epics-and-issues). Whichever epic you nest the other epics under will automatically become the Parent Epic, so remember to always add your individual tactic epics to the micro-campaign epic and not the other way around.

For example - You have created a micro-campaign that will include a self-service event with promotion, a survey, LinkedIn InMail, and DemandBase targeted ads.

- You will create an epic for your micro-campaign per the [above instructions](/handbook/marketing/revenue-marketing/field-marketing/#3-fmm-creates-tactic-epics).
- Your self-service event with promotion will require a separate issue with its own epic.
- Your survey will also require a separate issue with its own epic.
- Your LinkedIn InMail will require its own issue (the only DMP tactic that currently requires an epic is Content Syndication).
- Your DemandBase ads will require its own issue (ABM tactics do not currently require epics).

**To organize your various tactic epics under your micro-campaign epic:**

- Click into your micro-campaign epic
- Scroll down to Epics & Issues
- Click Add an Epic
- Paste the epics from your other tactics into this section and GitLab will automatically organize the tactic epics under your micro-campaign epic

## Individual Tactics

Remember, an account-centric micro-campaign consists of at least 2 different account-centric tactics. If you are planning an individual tactic (account-centric or not), follow the below steps. Refer to the information provided in [Steps for the Creation and Organization of Micro-Campaign Epics & Issues](/handbook/marketing/revenue-marketing/field-marketing/#steps-for-the-creation-and-organization-of-micro-campaign-epics--issues), for _how_ to create the issues & epics if needed.

1. Open tactic issue (see [here](/handbook/marketing/revenue-marketing/field-marketing/#2-fmm-opens-specific-tactic-issues) for issue links)
1. Open tactic epic (see [here](/handbook/marketing/revenue-marketing/field-marketing/#3-fmm-creates-tactic-epics) for epic code)
1. Create relevant issues (from links in the epic), assign DRIs and due dates

Reminder for what what needs to be included before moving from PLAN to WIP:

- Agreement on SLAs and scope between other teams involved in selected tactics - [SLA workback doc](https://docs.google.com/spreadsheets/d/1YXriQ1clvYyBn-TDbbCVvNP6NEbrAF-0w6tIHKhDeZM/edit#gid=1983708280)
- Completion of all fields in the Details section of the template

### Examples of Individual Tactics

#### Content Syndication

You decide you would like to create a Content Syndication piece. Here is how you would utilize the above [Steps for the Creation and Organization of Micro-Campaign Epics & Issues](/handbook/marketing/revenue-marketing/field-marketing/#steps-for-the-creation-and-organization-of-micro-campaign-epics--issues) to do so:

1. Content Syndication is a DMP tactic and per the [instructions for opening a tactic issue](/handbook/marketing/revenue-marketing/field-marketing/#2-fmm-opens-specific-tactic-issues) you would follow the Digital Marketing Program Tactics instructions and open a [Micro-Campaign and Individual Tactics Template](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=Micro_Campaign_and_Individual_Tactics).
1. Since Content Syndication is the only DMP tactic that requires an epic, you would then follow the [epic creation instructions](/handbook/marketing/revenue-marketing/field-marketing/#3-fmm-creates-tactic-epics) and create the [Content Syndication](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/content-syndication/) epic.
1. Finally, you would follow the instructions for [creating relevant issues and assigning DRIs and due dates](/handbook/marketing/revenue-marketing/field-marketing/#4-fmm-creates-relevant-issues-and-assigns-dris-and-due-dates).

#### Self-Service Virtual Event With or Without Promotion

You decide you would like to host a Self-Service Virtual Event. Here is how you would utilize the above [Steps for the Creation and Organization of Micro-Campaign Epics & Issues](/handbook/marketing/revenue-marketing/field-marketing/#steps-for-the-creation-and-organization-of-micro-campaign-epics--issues) to do so:

1. Per the [instructions for opening a tactic issue](/handbook/marketing/revenue-marketing/field-marketing/#2-fmm-opens-specific-tactic-issues) you would follow the Self-Service Virtual Event instructions and open a [Self-Service Virtual Event Issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=self_service_event).
1. You would then follow the [epic creation instructions](/handbook/marketing/revenue-marketing/field-marketing/#3-fmm-creates-tactic-epics) and create the [Self-Service Virtual Event Epic](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/self-service-ve-with-without-promotion/#self-service-virtual-events-with-or-without-promotion) (please note there are two different epic codes listed, dependent on whether you are utilizing promotion or not).
1. You would then would follow the instructions for [creating relevant issues and assigning DRIs and due dates](/handbook/marketing/revenue-marketing/field-marketing/#4-fmm-creates-relevant-issues-and-assigns-dris-and-due-dates).
1. Finally, you would also continue through the additional steps for setting up your Self-Service Virtual Event that are listed following the Self-Service Virtual Event epic instructions [here](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/self-service-ve-with-without-promotion/#best-practices).

#### Micro-Campaign Training Video

[Video training on how to set up a Micro-Campaign](https://drive.google.com/drive/folders/1nTQOO5jszlwIivy_bCjqA0a6RXnKwQtt). This video walks you though how to set up a Self Service webcast with promotion and LinkedIn InMail. Two tactics = Micro Campaign.

#### Example Micro-Campaign for Training

- [Micro-Campaign Epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1072)
    - [Micro-Campaign FM Issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/1396)
        - [Self-Service Virtual Event with promotion Epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1073)
            - [Self-Service Virtual Event with Promotion FM Issue](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/1397)
            - [Add to Nurture Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/2960)
            - [Follow Up Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/2959)
            - [Invites and Reminders Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/2958)
            - [List Clean and Upload Issue](https://gitlab.com/gitlab-com/marketing/marketing-operations/-/issues/2712)
            - [Facilitate Tracking Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/2957)
        - [Paid Survey Epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1074) (No separate FM issue since information is in overall Micro-Campaign Issue)
            - [Gated Content Request Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/2965)
            - [Add to Nurture Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/2963)
            - [Follow Up Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/2962)
            - [Facilitate Tracking Issue](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/-/issues/2961)
        - [LinkedIn Inmail Issue](https://gitlab.com/gitlab-com/marketing/account-based-marketing/-/issues/136) (No epic required)
        - [DemandBase Targeted Ads Issue](https://gitlab.com/gitlab-com/marketing/account-based-marketing/-/issues/136) (No epic required)

## FAQ's - Micro Campaigns and Other Tactics

1. If I am only running 1 tactic, do I need to create the micro campaign issue? Answer: No, you do not. A microcampaign includes at least [2 account centric tactics](/handbook/marketing/revenue-marketing/field-marketing/#account-centric-micro-campaign).
1. If I want to run ads through our ABM vendor do I need to create a micro campaign issue? Answer: No, you do not. A microcampaign includes at least [2 account centric tactics](/handbook/marketing/revenue-marketing/field-marketing/#account-centric-micro-campaign). Ads with our ABM vendor is only 1 tactic.

## Reminders - Micro Campaigns and Other Tactics

1. **Labels** - be aware of your labels. Please be mindful that you're adding the correct labels.
1. Epic label issue - There is a glitch right now which may keep the labels from auto-assigning. In order to have the labels added to your epic, you'll need to copy the /label line from the epic code into a comment to pull over all labels. [Quick video how-to](https://drive.google.com/open?id=1gK8G4CBaqcgWgB6b2A1BRSQCma5GfJyt). 2 mins
1. If you create an epic & sub issues and DO NOT have a date, then when you go back and add dates then you MUST ping the assigners - could be FMC, MPM, OPS, etc.
1. When you go from plan to WIP that's when everything starts - from the SLA workback as well.
1. The details section is so important - what that tells the MPM/FMC how this program should flow. You can't move your issue to the WIP stage until the details are added - This is where if attention to detail isn’t your BFF, you need to make it your BFF. ;)

# Field Marketing Issue Templates and Epic Codes 

Field Marketing utilizes the below issue templates and epic codes for virtual events and other campaign tactics. 
 
**Buyer Progression Webcast**
* [Tactic Details](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/webcast/#buyer-progression-webcasts)  
* [Issue Template](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=Sponsored_Virtual_Conference)  
* [Epic Code](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/webcast/#organizing-buyer-progression-webcast-epics-and-issues)  

**Content Syndication**
* [Tactic Details](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/content-syndication/)
* [Issue Template](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=Micro_Campaign_and_Individual_Tactics)
* [Epic Code](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/gated-content/content-syndication/#code-for-epic)

**Direct Mail**
* [Tactic Details](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/direct-mail/)
* [Issue Template](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=Micro_Campaign_and_Individual_Tactics)
* [Epic Code](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/direct-mail/#code-for-direct-mail-epic)

**Executive Roundtable**
* [Tactic Details](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/external-virtual-events/#-executive-roundtable)
* [Issue Template](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=Executive_Roundtable)
* [Epic Code](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/external-virtual-events/#code-for-epic-2)

**GitLab-hosted Webcast**
* [Tactic Details](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/#webcast)
* [Issue Template](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=Virtual_Workshop_Template)
* [Epic Code](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/webcast/#organizing-campaign-webcast-epics-and-issues)

**Self-Service Virtual Event With or Without Promotion**
* [Tactic Details](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/#self-service)
* [Issue Template](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=self_service_event)
* [Epic Code (without promotion)](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/self-service-ve-with-without-promotion/#epic-code-for-self-service)
* [Epic Code (with promotion)](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/self-service-ve-with-without-promotion/#epic-code-for-self-service-with-promotion)

**Sponsored Virtual Conference**
* [Tactic Details](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/external-virtual-events/#types-of-external-virtual-events) 
* [Issue Template](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=Sponsored_Virtual_Conference)
* [Epic Code](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/external-virtual-events/#code-for-epic-1)

**Survey**
* [Tactic Details](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/surveys/)
* [Issue Template](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=Micro_Campaign_and_Individual_Tactics)
* [Epic Code](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/surveys/#code-for-epic)

**Vendor Arranged Meetings**
* [Tactic Details](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/external-virtual-events/#-vendor-arranged-meetings)
* [Issue Template](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=Vendor_Arranged_Meetings)
* [Epic Code](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/external-virtual-events/#code-for-epic-3)

**Virtual Hands-on Workshop**
* [Tactic Details](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/virtual-workshops/)
* [Issue Template](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=Virtual_Workshop_Template)
* [Epic Code](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/virtual-events/virtual-workshops/#workshop-epic-code)

## In-Person Event Templates

Please note the below templates are only to be used for potential future in-person events. These templates are not currently updated and should not be utilized at this time.

* [AMER Field Marketing Event](https://gitlab.com/gitlab-com/marketing/field-marketing/-/blob/master/.gitlab/issue_templates/AMER_Field_Marketing_Event.md)
* [APAC Field Events Template](https://gitlab.com/gitlab-com/marketing/field-marketing/-/blob/master/.gitlab/issue_templates/APAC_Field_Events_Template.md)
* [EMEA Event Field Marketing](https://gitlab.com/gitlab-com/marketing/field-marketing/-/blob/master/.gitlab/issue_templates/EMEA_Event_Field_Marketing.md)
* [DevOpsDays Template](https://gitlab.com/gitlab-com/marketing/field-marketing/-/blob/master/.gitlab/issue_templates/DevOpsDays_Template.md) 
* [GitLab Connect](https://gitlab.com/gitlab-com/marketing/field-marketing/-/blob/master/.gitlab/issue_templates/GitLab_Connect.md)
* [Field Marketing Event Meeting Setting](https://gitlab.com/gitlab-com/marketing/field-marketing/-/blob/master/.gitlab/issue_templates/Field%20Marketing%20Event_Meeting%20Setting.md)

# Suggested workflow to ensure your campaigns are accurate

After Field Marketing has run a campaign, the Field Marketing Manager is responsible for ensuring that the campaign has operationally fully run to include the following:

- The campaign DRI reviews and cleans up list following the guidelines for [list imports](/handbook/marketing/marketing-operations/list-import/)
- If a follow-up email was to go out to all attendees and no-shows, the FMM is responsible for ensuring that emails were sent.
- Did the campaign respondents receive the correct amount of MQL points they should have received for the activity the took?
    - Person score is available to be viewed in our `Custom Links` section of each SFDC campaign. This allows you to view the MQL score of the campaign members, whether they be a lead or contact, all within one view.
- Did a lead/contact hit our MQL threshold and have the SDRs followed up with this record and moved them beyond the [MQL stage](/handbook/marketing/marketing-operations/marketo/#mql-scoring-model)?
- Update all the relevant tabs in the field marketing campaign event planning sheet.

# ROI tracking for Field Marketing

For a complete picture of GitLab Marketing Metrics, please refer to our [Marketing Metrics page](/handbook/marketing/marketing-operations/marketing-metrics/).

This section will go into specifics on the workflow for a Field Marketer to check their results.

At the highest level, Field Marketing is tracking its contribution to the sales pipeline and the marketing qualified leads (MQL) that we bring in.

## Useful links

- [Linear Attribution Dashboard](https://app.periscopedata.com/app/gitlab/556414/Marketing-Linear-Attribution)
- [Marketing Metrics Dashboard](https://app.periscopedata.com/app/gitlab/431555/Marketing-Metrics)
- [WIP:Field Marketing Dashbaord](https://app.periscopedata.com/app/gitlab/628217/WIP:Field-Marketing-Metrics)
- [Revised Marketing Metrics Dashboard](https://app.periscopedata.com/app/gitlab/628196/Revised-Marketing-Metrics)
- [Digital spend Dashboard via PMG Agency country detail](https://datastudio.google.com/u/0/reporting/1NL1FxHvoXRul4vlQJCVfWWfwsu8Jz1XZ/page/kmnGB)
- [Field Marketing Specific digital spend Dashboard via PMG agency](https://datastudio.google.com/reporting/17t7s-cbcFUghpBHD1xlU8hBe8P7IHZGK/page/mPgDB)
- [SAOs accepted by last touch = Field Marketing Campaigns](https://gitlab.my.salesforce.com/00O4M000004FYB0)
- [First Order SAOs by GTM Segment](https://gitlab.my.salesforce.com/00O4M000004aSMQ)
- [CONTACT MQL report by GTM Segment](https://gitlab.my.salesforce.com/00O4M000004aSCB)
- LEAD MQL report by GTM Segment - WIP, will be forth coming 
- [Sales Pipeline Report stages 1-3](https://gitlab.my.salesforce.com/00O4M000004aJh9)
- [WW SFDC Field Marketing Digital Report](https://gitlab.my.salesforce.com/00O4M000004aA0V)
- List of campaign members with Outreach stages listed - to help facillitate the FMM<>SDR convo on lead status follow-up. If you are a FMM reading this, then we suggest you add `Campaign Owner Name` to filter to your specific campaigns. Feel free to play around with the filters and save your own copy as well!  
   - [AMER FMM Campaign Members](https://gitlab.my.salesforce.com/00O4M000004aNoL)
   - [APAC FMM Campaign Members](https://gitlab.my.salesforce.com/00O4M000004aQLL) 
   - [EMEA FMM Campaign Members](https://gitlab.my.salesforce.com/00O4M000004aQLV) 
- Reports in the following SFDC Folders:
    - SSOT WG Sales and Marketing
    - SFDC folder: Sales Ops Reports (Verified)

### Contribution to sales pipeline

This can be calculated by heading to the [Marketing Linear Attribution Dashboard](https://app.periscopedata.com/app/gitlab/556414/Marketing-Linear-Attribution). NOTE: Ensure you’ve got your filters properly dialed in.
* WIP SECTION AS THE REPORT IS CURRENTLY UNDER CONSTRUCTION
Go to XYZ report and download the data - filter by budget holder, then run a % to determine what % of the pipeline came from Field Marketing - our target is 30% of pipeline.

### MQLs

This can be calculated by heading to the [Marketing Metrics Dashboard](https://app.periscopedata.com/app/gitlab/431555/Marketing-Metrics) and looking at the `MQLs by Date by Initial Source - MQL Date chart`. NOTE: Ensure you’ve got your filters properly dialed in.

- WIP SECTION AS THE REPORT IS CURRENTLY UNDER CONSTRUCTION
- The FY21 Field Marketing MQL goals were built on data from FY20 using the leads `initial source`. Field Marketing has plans (in alignment with Marketing Ops plans) of moving to a lead attribution model that accounts for initial source, conversion point (what activity did the record take that pushed them over the MQL threshold) and any MQL that was influenced by a Field Marketing activity.

### How to track the ROI of an in person event

- SAOs accepted - Reference the SAO accepted SFDC report.
- Contribution to pipeline - Reference the Linear Attribution dashboard.

If you spent money on digital ads to drive registration, then you’ll also want to check on the ROI of your spend. Head down to the How to track the ROI of your Digital Tactics section, which is relevant for all digital spend.

### How to track the ROI of your Digital Tactics

#### 3rd Party digital agency

In order to track engagement for any work we do with PMG (our global digital marketing partner), a campaign UTM code will be created by the DMP team. The DMP team uses this UTM code to create a SFDC UTM Report. Please note a SFDC UTM Report is not the same as a SFDC Campaign (which pulls in registrations for events, webinars and content syndication leads). It is a dedicated report for our paid digital efforts only. For more information on UTM's at GitLab [can be found here](handbook/marketing/revenue-marketing/digital-marketing-programs/digital-marketing-management/#utms-for-url-tagging-and-tracking).

You will follow this process when you are working with our 3rd party digital agency to serve your target audience ads/LinkedIn InMails.

At the highest level, it's interesting to see the spend, clicks, impressions, CPC (cost per click), inquiries, and CPI (cost per inquiry). This is done by going to the Field Marketing Specific digital spend Dashboard via PMG agency, linked in the `Useful Links` section and searching for your campaign using the campaigns UTM code. Here is the report as well, because we know sometimes you just want the link in the exact place you are looking for it in ;) . [WW SFDC Field Marketing Digital Report](https://gitlab.my.salesforce.com/00O4M000004aA0V)

Inquiries (people who end up registering for your event or engaging with your ad) are the most important to look into and really the status of them attending your event or interacting with your campaign, eventually leading to an SAO and then pipeline!

If you were driving people to register for something, then hop over to your SFDC campaign. Then go down to the `Custom Links` section and click on the `View All Campaign Members` report.

You’ll then want to sort by `Ad Campaign Name (FT)`, which answers the question “What was the 1st touch ad this record interacted with?” and also the `Ad Campaign Name (LC)`, which answers the question “What ad created this lead?”.

If you did not have a specific SFDC Campaign you were driving to, and you wanted to see the success of your campaign, then you would still refer to the [WW SFDC Field Marketing Digital Report](https://gitlab.my.salesforce.com/00O4M000004aA0V), add in your campaigns UTM there, using the filter `Ad Campaign Name` [contains] and add your UTM.

Please note that whilst you can track leads via SFDC campaigns or UTM reports, pipeline generated should be viewed on [Sisense](https://app.periscopedata.com/app/gitlab/556414/Marketing-Linear-Attribution) only, as SFDCs last touch model is different from our multi touch attribution model.

#### DemandBase

Via our Account Based Marketing team, Field Marketers have access to serve ads to specific accounts via DemandBase.

At the highest level, all ROI on any digital ad can be found in our favorite [WW SFDC Field Marketing Digital Report](https://gitlab.my.salesforce.com/00O4M000004aA0V), we should note, this report does take time to load. It's not you, it's the report. So if you are planning to show this data, we recommend you load the report well in advance. If you used DemandBase to serve an ad and we received an inquiry (form fill), then you will see that information in that report, and the `Touchpoint Source` will = `ddbase`.

If you want to dig into the reporting details in DemandBase, you can really geek out and have some real fun there, but be sure you have time allocated and can come out with actionable next steps to provide whomever you are sharing the data with.

Once in DemandBase, head over to the `Targeting` section. Be sure your date range is accounting for the dates you actually care about.

Select your campaign name.

From there, you can see the spend to date - this is in near real time, so that’s pretty cool! You can also see the percentages of accounts that have been lifted since the campaign has started, and you can see the impressions. You can also see the stages (according to DemandBase) that your campaign target accounts are in. Today, the downside with this reporting, is that you can’t actually click in to see the names of the accounts in the stages. You can only see the closed won opportunities.

In order to see the closed won opportunities, you will need to click into `Performance` and then you’ll look for the `Opportunities Closed/Won` as well as the `New CRM/Opportunities` column. (If you don’t immediately see that opps closed/won, then click the blue MORE button and select it there!) If there is a 1 in that column, then boom! That's your opp. Unfortunately, right now, there is no link back to the opp in SFDC, so you would need to search a bit within the account in SFDC to find this specific opportunity.

It’s also useful to check out `ABM Analytics`, which you navigate to from the home page.

Again, date range. Super important here.

Select your date range and your campaign. Click compare.

Here within ABM analytics you can see the amount of total won opportunities, the close rate, total pipeline, and the average deal size, as well as information around deals that are more likely to engage and close. NOTE: this is reporting for all time. Not just the time frame of our campaign, which is what the `Targeting` section is reporting on.

Again, this is all great information. Be sure that you come out of deep dive with actionable next steps when talking with other team members.

#### Walk through videos

- High level video walking through [DemandBase](https://youtu.be/srv4WUv3-oQ).
- High level video walking through the [data studio and ad campaign SFDC report](https://youtu.be/r790WprOspo).
- Walk through on checking [ad inquiries at the SFDC campaign level](https://youtu.be/T8e_yJzIv84).

# FMC Process for Issues Moving from Plan to WIP

The following is the FMC process for when a Field Marketing issue moves from `mktg-status::plan` to `mktg-status::wip`.
 
- FMM moves the Field Marketing issue to `mktg-status::wip` (**Reminder:** Event Details and FMC Checklist must be filled out in full and line item complete on your budget tab to move an issue to WIP)
- FMM pings the FMC in the Field Marketing issue and requests epic and sub-issue creation
- FMC confirms the campaign tag has been created in [Netsuite](https://docs.google.com/spreadsheets/d/19Le1PrWE1JbqN6Wz3D6tGfy7Ee5PmoetfyLPdwinFV0/edit#gid=275839115)
- FMC creates the epic and sub-issues utilizing [this process](/handbook/marketing/revenue-marketing/field-marketing/#3-fmm-creates-tactic-epics)
- FMC adds the event to the appropriate virtual events calendar, if applicable (specified by the FMM in the FMC Checklist section of the issue)
- FMC adds the event to the [GitLab Events Page](/handbook/marketing/events/#how-to-add-events-to-aboutgitlabcomevents), if applicable (specified by the FMM in the FMC Checklist section of the issue)
- FMC pings the FMM in the Field Marketing issue to confirm completion of the above tasks
- FMC creates the [Marketo program and SFDC campaign](/handbook/marketing/marketing-operations/#marketo-program-and-salesforce-campaign-set-up) utilizing the Facilitate Tracking sub-issue previously created
- FMC pings the FMM in the Facilitate Tracking issue that the Marketo program and SFDC campaign have been created and closes issue

# FMM Localization Process

We do follow the global direction to translate content for [P0 and P1 priority countries](/handbook/marketing/localization/#priority-countries). For translating requests for P0 and P1 countries, work with our content team [following this process](/handbook/marketing/localization/#translating-content-for-campaigns). 

At times, there is a need to translate content outside of the priorities countries, please follow the process below. Keep in mind that only content used as part of a campaign and in support of a revenue goal will be approved.

For complete details on Smartling, our translation tool, please see the [Smartling page.](/handbook/marketing/marketing-operations/smartling/) 

## Outside of focus countries for GitLab owned events and campaigns
- FMM to create the Content translation issue request using the [`FMM_localization_request`](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=FMM_localization_request) issue template
- Provide a justification
- Outline how this content is to be used to achieve a revenue or opportunity created goal 
- Make sure it is 100% aligned to achieve sales goals
- Need functional leader approval

## Outside of focus countries for Channel for GitLab owned campaigns
- FMM to create the Content translation issue request using the [`FMM_localization_request`](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=FMM_localization_request) issue template
- For channel leadgen campaigns, we prefer that the Channel partner pays for the translation of assets we provide in English
- Provide a justification
- Outline how this content is to be used to achieve a revenue or opportunity created goal 
- Make sure it is 100% aligned to achieve partner goals
- Needs functional leader approval

# AMER Field Marketing Vendors and Tools

We sometimes work with third party vendors and use other tools for outreach, event production etc. Below is a list of whom we work with currently and the [FM vendor evaluation epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/441) that tracks whom we have evaluated/worked with in the past.

- Emissary.io - in an effort to help sales gain account intelligence
- Banzai - to supplement event recruiting
- [Thnks](https://www.thnks.com/how-it-works/) - platform to send gifts to recipients with the option to accept gift or donate to charity
    - There is a service fee on each order which sits around 16% (for example, a $50 giftcard will cost us $60 to send via Thnks)
    - `@lilphil` currently has an admin login and can add others as users, reach out to be added or for more info on using
    - Example Thnks [here](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/1380#package-swag-and-event-assets)
- Printfection - used for direct mail giveaways, see more details [here](/handbook/marketing/revenue-marketing/field-marketing/#printfection-direct-mail-giveaways)
- [Alyce](alyce.com) - direct mail platform to help create personal bonds with our prospects, customers, and partners that deliver results through one-to-one gifting
- [Jack Nadel](https://jacknadel.exhibitors-handbook.com/) - our AMER swag vendor who provides swag sourcing, warehousing and distribution, see more details [here](/handbook/marketing/events/#amer-field-marketing-swag)

# Sourcing New Field Marketing Vendors

Field marketers are always looking for new ways to support the sales teams within their regions and to provide unique, alternative ways to draw potential prospects closer to our company and show value to our current customers in order to expand within those accounts. Field marketers have a vast tool kit of marketing initiatives for precisely that. However, sometimes a gap is identified and field marketers will look to external third party vendors to help fill that gap when it comes to achieving their goals to their marketing metrics.

When souring and selecting a new third-party vendor, there are a few things that should be considered:

1. What are you trying to achieve? What is the gap you have identified? Is it filling the top of the funnel? Building relationships within a specific target audience (i.e. C-Level)? Maybe your goal is delivering on more qualified leads? Be clear on your end goal(s) from the beginning.
1. Can this vendor achieve your goal? Have the vendor show you proven use cases and references on your specific goals.
1. Have you sourced more than 1 vendor? A good point of reference is to research at least 2 or 3.
1. Is this vendor willing to proceed with a pilot program with you? Even with the best intentions, not all 3rd party vendors will fulfill your needs as originally discussed. A pilot program will allow you to try out the vendor and/or their product without requiring a full investment up front.
1. Identify all of the pieces of the contract. Ensure that you and the 3rd party vendor are aligned on the following:
    - Cost
    - Timeline of project
    - Timeline of onboarding
    - What is required when it comes to integration - Full Access, No Access or Partial Access
    - What are your deliverables?
    - What are their deliverables?
    - Identify who needs to be trained. What does that training involve?
    - Who will be the DRI for the project (always have 1 person who will support you during your contract process)?
    - What is the cost of renewal, or, if you are running a pilot, what is the cost of a full term contract?
    - If you're running a pilot, what are the goals you have in place to determine if you should invest beyond the pilot phase?

Once you feel confident that you have chosen the right 3rd party vendor, now is the time to begin the following review process.

# Software Vendor/Product Approval Process

1. FMM and FMC to review the [Procurement Handbook Page](/handbook/finance/procurement/#when-should-i-contact-procurement) so you are familiar with the procurement process and lead times. Allow [60-90 days](/handbook/finance/procurement/#when-should-i-contact-procurement) for processing all reviews, approvals and contract negotiations. Note that if the overall cost of the product is over $100k, 2-3 comparison bids are required.
1. FMM to open a Field Marketing Issue and detail out the justification for the potential new vendor/product. Include any presentations, slides and demos that have been provided to help explain the product. Tag your regional manager in the issue to review and discuss. Approval must be received to move forward. Make sure to attach all of the following issues to your main Field Marketing Issue for easy access.
1. FMM to open a [Contract Request](/handbook/marketing/revenue-marketing/field-marketing/#field-marketing-contract-requests) issue and assign to the regional FMC. 
1. FMC to process an NDA with the vendor, specified in the [Prior to Contacting Procurement](/handbook/finance/procurement/prior-to-contacting-procurement/) instructions in the Handbook. This NDA will be required in your Software Vendor Contract Request Issue. 
1. If you're requesting a new tool, FMM to open a [Marketing Ops Eval](/handbook/marketing/marketing-operations/#requesting-a-new-tool) issue. Marketing Ops will help evaluate the product and will assist in the procurement process. They will also help you determine if an integration is required with any products in our current tech stack and if so, which team will be the DRI for the integration.
1. If you're requesting a new tool, FMC to open a [Software Vendor Contract Request](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/issues/new?issuable_template=software_vendor_contract_request) and follow the instructions. Please note that you can open this issue even before a contract has been provided. This template details the following approvals:
    - NDA requirement
    - Privacy review (if applicable)
    - Security review
    - Tech stack request (for new software purchases)
1. If you're requesting a new tool, once the Software Vendor Contract Request has been approved by all teams and the contract has been fully executed, FMC to proceed with invoice processing.
1. If you're requesting a new tool, and if the new product requires integration with any of our existing products (SFDC, Marketo, Outreach, etc.) FMC to open an integration request issue with the appropriate team (see step above where the Marketing Ops team will help determine). Integration will not begin until all approvals are final and the contract has been fully executed, but opening the integration issue early will give the appropriate team advance notice for planning.

# AMER Field Marketing Event Venue Tracking

The below epic is for tracking venues we would like to utilize for future events, or as a way to evaluate event venues we have already worked with to note the pros and cons of various event space across different regions.

- [AMER Field Marketing Event Venues Epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/625)

# Field Marketing Contract Requests

In Field Marketing, the FMCs manage all [Procurement Issues](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/tree/master/.gitlab/issue_templates) for their regions. For contracts or invoices ready to submit for approval, the FMM will open an issue utilizing the [`Contract_Request`](https://gitlab.com/gitlab-com/marketing/field-marketing/issues/new?issuable_template=Contract_Request) template and follow the instructions to provide important details required before assigning to the regional FMC.

# Corporate Memberships Owned by GitLab Field Marketing

- [AFCEA](https://www.afcea.org/site/) - Membership is handled by the Public Sector Field Marketing Manager. Account information is stored in the marketing 1Pass vault.
- [ACT-ICT](https://www.actiac.org) - Membership is handled by the Public Sector Field Marketing Manager. Please ping the PubSec FMM for details if you'd like to join.
- [Charleston DCA](https://www.charlestondca.org/) - Membership is handled by the Public Sector Field Marketing Manager. Please ping the PubSec FMM for details if you'd like to join.
- [G2xExchange](https://www.g2xchange.com/join-today/) - Membership is handled by the Public Sector Field Marketing Manager. Please ping the PubSec FMM for details if you'd like to join.

# What's currently scheduled in my region?

| Region | FM DRI | GitLab User ID |
| ------ | ------ | -------------- |
| [AMER East NE & SE](https://gitlab.com/groups/gitlab-com/marketing/-/boards/915674?&label_name%5B%5D=East) | Ginny Reib | `@GReib` |

| [AMER East-Central](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1105137?&label_name[]=East%20-%20Central) | Rachel Hill | @rachel_hill |
| [AMER West-PacNorWest](https://gitlab.com/groups/gitlab-com/marketing/-/boards/933457?&label_name[]=West) | Rich Hancock | `@rhancock` |
| [AMER West - NorCal/SoCal/Rockies](https://gitlab.com/groups/gitlab-com/marketing/-/boards/933457?&label_name[]=West) | Rachel Hill | `@rachel_hill` |
| [AMER Public Sector](https://gitlab.com/groups/gitlab-com/marketing/-/boards/933456?&label_name[]=Public%20Sector) | Helen Ortel | `@Hortel` | 
| [APAC](https://gitlab.com/groups/gitlab-com/marketing/-/boards/933488?&label_name[]=APAC) | Pete Huynh | `@Phuynh` |
| [EMEA Southern Europe](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1426531?&label_name[]=Southern%20Europe) | Tina Morwani | `@tmorwani` | 
 [EMEA MEA](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1426540?&label_name[]=MEA) | Tina Morwani | `@tmorwani` |
| [EMEA Northern Europe](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1438252?&label_name[]=Northern%20Europe&label_name[]=Russia) | Kristine Setschin | `@ksetschin` |
| [EMEA UK/I](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1438265?&label_name[]=UK%2FI) | Kristine Setschin | `@ksetschin` |
| [EMEA Central Europe](https://gitlab.com/groups/gitlab-com/marketing/-/boards/1438243?&label_name[]=Central%20Europe&label_name[]=EMEA&label_name[]=Europe%20CEE) | Jeffrey Smits | `@JDSmits` |
| [EMEA CEE](hhttps://gitlab.com/groups/gitlab-com/marketing/-/boards/1440436?&label_name[]=EMEA&label_name[]=Europe%20CEE) | Jeffrey Smits | `@JDSmits` |
| [EMEA Russia](hhttps://gitlab.com/groups/gitlab-com/marketing/-/boards/1484015?&label_name[]=Russia) | Kristine Setschin | `@ksetschin` |

**NOTE:** to see the full list of events, you need to be logged into your GitLab account. There are times we make issues private.

To find out what upcoming events GitLab will be involved in please visit our [Events Page](/events/).

For details on how we handle events please visit the [GitLab Event Information](/handbook/marketing/events/) page.

# Suggesting an Event or Tactic

If you are interested in requesting Field Marketing's support for a particular in-person or virtual event or tactic, please follow the process listed on our [Events Page](/handbook/marketing/events/#suggesting-an-event).

# [Field Marketing planning](https://drive.google.com/drive/folders/1xtePKOl4RSINaPPr3InQNisYTQyOJ5JE)

It is our goal to have planning for each qtr completed four (4) weeks before the next quarter starts. At this time, we have the following time line set for regional plans:

- Q3 plan is due July 1
- Q4 plan is due Oct 1
- Q1 plan is due Jan 6

A standard deck will also be used to ensure consistency across the world in terms of how the plan is presented.

The SSOT of the plan remains in GitLab on our [various issue boards](/handbook/marketing/revenue-marketing/field-marketing/#whats-currently-scheduled-in-my-region).

- [FY21 Standard deck](https://docs.google.com/presentation/d/1mLaL7YeCDD8gD4r7NAbP6ui1_iDghZptpO-AOpQpV_E/edit#slide=id.g29a70c6c35_0_68).
    - Note: The deck is additive. You are only responsible for completing the slides that are due for the quarter we are in.
    - As the slides are due one (1) month before the end of the quarter, if you have a campaign/tactic that you want to highlight on the recap slide that has not been completed, please share this with your manager and propose a date when you will have that slide completed.
    - The completed deck should be stored in your correct regions [shared folder](https://drive.google.com/drive/folders/1xtePKOl4RSINaPPr3InQNisYTQyOJ5JE). Anyone within GitLab has access to search and find this folder.

# Field Marketing/Channel Marketing/Channel Account Managers (CAMs)

In April 2020, as part of our GTM strategy, GitLab launched its [channel sales program](/handbook/resellers/). As part of that program, each Channel Account Manager (CAM) will have a maximum of [3-5 Select Channel partners](https://docs.google.com/spreadsheets/d/1-EE7vChGkDeyJxoM-LjVmUdwYwboxBmq8_42hjHGw_w/edit#gid=241847859&range=A2) for [P0 and P1 regions](/handbook/marketing/localization/#priority-countries). After the CAM has the 3-5 Select Channel partners identified (note: FMM could work with Open partners depending on their MDF proposal or partners outside of P0 and P1 regions, and that will be handled on a case by case basis), it's the responsibility of the regional FMM to work with the channel team in the following manner:

1. Include Select Channel partners as a portion of their overall territory plan with the focus being lead generation.
1. Channel Partner Enablement (how well does partner know and understand the GitLab solution and how equipped they are to sell it)
    - In FY21 - [Channel Partner Enablement team](https://about.gitlab.com/job-families/sales/program-manager-field-enablement/) will be the DRI. It's the responsibility of FMM to assess how enabled the partner is and escalate to the program manager if necessary for improvement.
1. In order for FMM to engage in lead generation activity, the CAM needs to fill out the [Channel Lead Gen request issue template](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=Channel_LeadGen_Req).
1. Lead Management in the Channel: This is a WIP in process. As we start to work with channel partners, we will refine this process.
    - GitLab will own the leads and lead management if the Channel Partner is participating in a GitLab led initiative.
    - The Channel Partner will own leads and lead management if GitLab is participating in a Channel Partner led initiative.
    - Both GitLab and the Channel partner will work the leads and lead management if GitLab asks a Channel partner to sponsor (paid sponsorship) a GitLab led initiative. GitLab and the Channel partner will collaborate and agree on a clearly laid out plan for how the leads will be jointly worked in advanced of the activity taking place.
    - The Channel Partner will own leads and lead management if GitLab MDF is used for the lead gen activity
    - The Channel Partner will own leads and lead management if the lead gen activity is a joint GTM push where both the partner and GitLab are a part of the solution (i.e. GitLab software + partner implementation services + partner integration services)

- Please remember that from a sales perspective, channel deals (MQLs, SOAs, PIOs and won deals) are considered comp neutral so both the partner and the GitLab sales field team gets compensated. So for every deal/lead/PIO/Closed partner deal we ALL get 'credit'.
- The Channel team is currently exploring how we will prevent 'channel conflict' (i.e. who owns the deal GitLab or Partner), more more details to come.

1. Regional MDF management
As outlined in the [Channel Handbook - MDF section](https://about.gitlab.com/handbook/resellers/#the-market-development-funds-mdf-program), GitLab Open and Select partners have access to the proposal-based GitLab Marketing Development Funds (MDF) Program.
    - In FY21 - DRI for budget tracking and ROI measurement will be the Channel Marketing team with Field Marketing being the DRI for the 3rd step in the approval of the MDF proposal in region. Step 1 in the approval process is the CAM approving the MDF request and then step 2 is the Channel Marketing team approving the request. The full approval process can be [reviewed here](/handbook/marketing/product-marketing/analyst-relations/channel-marketing/#-channel-partner-mdf-program).

For more details on what the Channel Programs team does, please head to [their page](https://about.gitlab.com/handbook/sales/channel/).

## Here’s a quick rundown (not exhaustive) of what the Channel Program team handles

1. Manage/create SPIFFs
1. Manage Partner Advisory Boards
1. Set up MDF program
1. Manage the updates of the handbook
1. Operationally Manages Channel Partner Portal

If you are a FMM receiving requests to add a partner logo to our website, then please direct the CAM or partner to Channel Marketing.

If you are a FMM receiving questions about the Channel Portal, then please direct the CAM or partner to Channel Programs.

## Channel Marketing Handles

Please head over to [their page](/handbook/marketing/product-marketing/analyst-relations/channel-marketing/) to check out their work and process.

# Field Marketing - Public Sector

## Charitable Donations for Event Attendance

As incentive for attending an event, GitLab can promote and process a monetary donation per participant to a charitable organization. For example: GitLab promotes a $25 donation per attendee for attending an event, 100 attendees participate in the event and GitLab donates $2,500 to the selected charity. **Please Note**: Donations are not to be made in a participant's name or reference a participant's organization.

## Public Sector Team Lead - Staff Field Marketing Manager

Within our public sector team we have a team lead. Our PubSec Team Lead will:

- Lead PubSec Field marketing strategy and direction
- Responsible for the defense GTM strategy, tactics, execution
- Supports channel - Alliance, SI’s DoD
- Leads content collaboration efforts from Field Marketing with Strategic Marketing
- Represents Field Marketing team on the public sector team call, speaking and documenting content to be discussed
- Leads GitLab Connect or major event in the DC area when pulling in multiple public sector verticals
- Leads the bi-weekly call with Director of Public Sector
- Leads common work process issues as they arise - i.e. lead routing
- Manages budget for overarching PubSec campaigns

# GitLab Company Information (Including Tax ID)

[Useful Company Information](https://gitlab.com/gitlab-com/finance/wikis/company-information)

# AMER Field Marketing iPad Purchasing and Setup Instructions

## Purchasing Details

[iPad Pro 12.9 inch/256GB/wifi](https://www.apple.com/shop/buy-ipad/ipad-pro/12.9-inch-display-256gb-space-gray-wifi)
[iPad Pro 12.9 inch Smart Keyboard Folio](https://www.apple.com/shop/product/MU8H2LL/A/smart-keyboard-folio-for-129-inch-ipad-pro-3rd-generation-us-english)

- When purchasing, please utilize the GitLab Business Account for corporate discounts. The Apple store/online representative will look up the GitLab Business Account associated with GitLab's 268 Bush St., San Francisco, CA 94104 address.
- Do not purchase AppleCare
- iPads can only be purchased by the regional Field Marketing Manager

## iPad Tracking

To ensure we know who within the company currently has a company owned ipad, [please see here](https://gitlab.com/gitlab-com/marketing/field-marketing/issues/1031).

## Setting Up Logins

**iPad Password**

- Utilize the password listed in the notes section of the AMER Field Marketing Apple ID located in the Marketing 1Pass

**Field Marketing Apple ID**

- Log in to the AMER Field Marketing Apple account utilizing the username and password listed under the AMER Field Marketing Apple ID located in the Marketing 1Pass. Please take note of the additional information listed in the notes section regarding cell phone verification.

**Marketo Check-in App**

- Download the Marketo app from the App Store. Sign in using your Marketo login and follow the instructions [HERE](/handbook/marketing/events/#marketo-check-in-app) for details on using the app during events.

**Google Drive and Slides**

- Follow the instructions [HERE](/handbook/marketing/product-marketing/demo/conference-booth-setup/#ipads) to set up Google Drive and Slides

# Working with Field Marketing

## Requests to Field Marketing leadership team

If you are a Field Marketing Manager and you would like to make a request in order to achieve better business results aligned to our values, please submit an issue using the [Field Marketing Leadership request issue template](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=leadership_request).

FM leadership team will review all requests on a bi-weekly basis, and will prioritize as necessary or will share with you relevant issues if the request is already being worked on.

**Why we use this template:**
Funneling similar requests from a global team and addressing shared pain points in a single issue with a [DRI](/handbook/people-group/directly-responsible-individuals/) will help to reduce noise and churn for other teams we collaborate with and ideally helps to improve a process that works for the WW Field organisation.

This issue should not be used as a shortcut to get questions answered but for request where FM leadership can help to improve a process which does contribute to our success as a team.

# Field Marketing Campaign Tags

The Field Marketing team will always associate a charge with an [event campaign tag](/handbook/marketing/events/#all-events---setting-up-the-campaign-tag) when applicable. If a charge is not associated with a specific event, we will then utilize the below regional tags. 

**Swag Tags** - These tags are utilized for swag purchases not associated with a specific event (example - a bulk order of pens) as well as overall swag management costs (warehouse storage fees, order entry fees, inventory management, etc.).
`swag_AMER_marketingfield`
`swag_APAC_marketingfield`
`swag_EMEA_marketingfield`

**General Tags** - These tags are utilized for any general purchase not related to swag or associated with a specific event (example - banner stands, table throws, popup displays, etc.).
`general_AMER_marketingfield`
`general_APAC_marketingfield`
`general_EMEA_marketingfield`

There might be campaigns we manage where we ask someone to attend or we get an invitation to speak at an event where we do not expect to receive any leads, therefore we do not need to setup additional tracking issues, SFDC campaigns, or campaign tags. 

We may be given a sponsorship or have someone speak where we DO receive the leads and in that case, we create a line item in the QTR budget file with a $0 USD amount and use that tag.

# Field Marketing Labels in GitLab

The Field Marketing team works from issues and issue boards. If you need our assistance with any project, please open an issue and use the `Field Marketing` label anywhere within the GitLab repo.

**General Field Marketing Labels:**

- `Field Marketing`: Issue initially created, used in templates, the starting point for any issue that involves Field Marketing
- `FY21-Q1`, `FY21-Q2`, `FY21-Q3`, `FY21-Q4`: What event or activity is set to take place or be due in this quarter
- `mktg-status::plan`: work that is proposed, in an exploratory state
- `mktg-status::wip`: work in progress that has been accepted and assigned to a DRI
- `Events` : Issues related to events - this label is held at the gitlab.com level
- `FMM-Other Tactics`: Issues related to non-event tactics
- `FMM-MicroCampaigns`: Issues related to regional integrated micro-campaign, which are built to drive a specific strategy in a region
- `workshop`: Issue related to hands on GitLab workshop

**Regional Field Marketing Labels:**

- `APAC`: Issues that are related to the APAC sales team
- `EMEA`: Issues that are related to the EMEA sales team
- `Central Europe`: Issues that are related to the EMEA Central Europe (DACH) sales team
- `Europe CEE`: Issues that are related to the EMEA Central Eastern Europe sales team
- `Southern Europe`: Issues that are related to the EMEA Southern Europe (France, Italy, Spain, Portugal, Greece, Cyprus) sales team
- `Northern Europe`: Issues that are related to the EMEA Northern Europe (Nordics & Benelux) sales team
- `UK/I`: Issues that are related to the EMEA UK & Ireland sales team
- `Russia`: Issues that are related to the EMEA Russia & Ukraine sales team
- `MEA`: Issues that are related to the EMEA MEA (Middle East & Africa) sales team
- `East`: Issues that are related to the East sales team
- `East - Central`: Issues that are related to the US East-Central sales team
- `East - NE`: Issues that are related to the US East- NE sales team
- `East - SE`: Issues that are related to the East-SE sales team
- `WEST`: Issues that are related to the US West sales team
- `WEST - Bay Area`: Related to the US WEST Bay Area sales team
- `WEST - BigSky`: Issues that are related to the US WEST Midwest sales team
- `WEST - FM Planning`: Issues related to West FM planning
- `WEST - FMM`: Issues related to the West FMM
- `WEST - PacNorWest`: Issues that are related to the US WEST Pacific North West sales team
- `WEST - SW`: Issues that are related to the US WEST Southwest and SoCal sales team
- `Public Sector US`: Any issues related to US Public Sector
- `FMM AMER PubSec`: All issues and tasks related to the FMM AMER for PubSec
- `AMER - DOD`: AMER DOD tactics
- `AMER - CIV`: AMER Civilian tactics
- `AMER - SLED`: AMER SLED tactics
- `AMER - NSP`: AMER National Security tactics
- `FMC AMER NE/SE/PubSec`: Issues related to the FMC AMER East & PubSec
- `FMC AMER NE/SE/PubSec - Tracking`: Deadline tracking related to FMC AMER East & PubSec
- `FMC AMER NE/SE/PubSec - Swag`: FMC AMER East & PubSec tracking for swag and event assets
- `FMC AMER - West/East-Central::Watching`: Issues that the FMC AMER West is tracking
- `FMC AMER - West/East-Central::Working`: Issues that the FMC AMER West is actively working on
- `FMC EMEA`: Apply this label for the attention of the Field Marketing Coordinator-EMEA
- `FMC EMEA - Event Tracking`: Event/deadline tracking related to FMC EMEA

For more information on how Field Marketing utilizes GitLab for agile project management, please visit [Marketing Project Management Guidelines](/handbook/marketing/#-marketing-project-management-guidelines).

# The Field Marketing Budget

[**Budget Doc**](https://docs.google.com/spreadsheets/d/1QC6P0VRWwJheOlGB-9bX8JIF8_4UY3h1cGVT_gacv5M/edit#gid=184318451&range=A1) -
Field Marketing manages its budget in a separate doc, that our Finance Business Partners pull into the master marketing forecast. Specific instructions on how we manage our expenses:

1. Each FMM will have a qtrly allocation of money to spend based on territory pipeline needs.
1. Procurement issues will NOT be approved unless all column details are filled in. Field Marketing is the DRI for all columns.
1. Every month on t -6 days (so 6 business days before the start of the next month) finance will do a pull of this data and push it into the company rolling 4 qtr forecast. Field Marketing Managers must always keep their budgets up to date, but it is essential the budget is accurate for the month prior to t -6 days, as FMM Leadership reviews the budget vs. actuals on a monthly basis.
1. Direct Mail accrual process - Spend should be planned and expensed in the month in which they are sent out to recipients vs. the month in which the recipient could use the direct mail piece, as we have no control over when a gift card (as an example) is used. 
1. Survey accrual process - If you are using a 3rd party to complete a survey, it’s the responsibility of the FMM DRI to account for the percent of work that was completed in that month in the budget. As an example, if you signed a contract with a vendor to complete 100 surveys, and in month 1, 50 of the 100 contracted for surveys were completed, then you would account for 50% of the expense in month 1. 
   * This update in monthly expense is due to be updated directly in the budget document, in the correct month, in advance of the t -6 days before the end of the month, which is when Finance does a pull of our budget doc to run a budget vs. actuals comparison  
1. Content Syndication accrual process - If you are using a 3rd party to run a content syndication play, it’s the responsibility of the FMM DRI to account for the percent of work that was completed in that month in the budget. As an example, if you signed a contract with a vendor to complete 100 leads, and in month 1, 50 of the 100 contracted for leads were delivered, then you would account for 50% of the expense in month 1. 
   * This update in monthly expense is due to be updated directly in the budget document, in the correct month, in advance of the t -6 days before the end of the month, which is when Finance does a pull of our budget doc to run a budget vs. actuals comparison 
1. The entire cost of the program is to be forecasted for - Sponsorship + lead scanners + monitor + any other auxiliary cost that the company will incur.
1. If the event is just 1 day, then the start and end date would be the same.
1. The Campaign tag auto populates based off of the program name and the starting date (ISO format) of the campaign. The max of 31 characters that can be entered into NetSuite is already taken into consideration in the formula.
1. Created Netsuite tags can be found [here](https://docs.google.com/spreadsheets/d/19Le1PrWE1JbqN6Wz3D6tGfy7Ee5PmoetfyLPdwinFV0/edit?ts=5daf2dad#gid=248514497).
   - For more details on campaign tags, please reference [this section](/handbook/marketing/revenue-marketing/field-marketing/#field-marketing-campaign-tags) of the FM handbook.
1. Any expense that is less than $5,000 USD and is NOT related to an event with a date, will be expensed against your budget in the month it is paid. This is in alignment with our [GitLab Prepaid Expense Policy](/handbook/finance/accounting/#prepaid-expense-policy). It is essential that you have this expense in the correct month in the budget document.
1. Also in alignment with our Accounting rules, we recognize the expense in the month where the work happens, NOT when we the cash leaves our doors. As such, only when you are running paid digital ads to support campaign work, must you account for the spend in a separate line item. You will need to keep the same campaign tag for the digital spend, but you will need to account for the different vendors. The line item for your digital spend will have a different date than the event you are driving the digital ad to, and that is ok. [See here for an example](https://docs.google.com/spreadsheets/d/1QC6P0VRWwJheOlGB-9bX8JIF8_4UY3h1cGVT_gacv5M/edit#gid=1885631257&range=A2). NOTE: Because there are formulas built into our spreadsheet, when you copy the main campaign tag, you will need to `paste values only`.
1. If you are running a campaign that has more than one tactic, and there are specific dates for the tactics, then each tactic will need its own line item in the budget and its own campaign tag.
    - As an example: If you are running a country specific campaign on CI/CD and there are 2 webinars and a workshop included in that campaign with a different specific date, then each webinar and the workshop will need its own tag, in addition to its own line item in the budget.
1. Each tab is protected based on the managers/DRI's for each region. Only those with permissions will be able to edit each tab. Please reach out to the marketing finance business partner if edit access needs to be changed.
1. Only the "FY 21 Spend" column (O) needs to be filled out with the total spend of the program. Equations are in the monthly columns (P-AA) that will evenly spread the total amount over the months that are in the Start and End date columns. However, the equation can be deleted and dollar values can be hard-coded into the cells if the spend is not expected to be evenly spread over the months of the event.
1. The MQL target is built off of a formula based on the type of activity the spend is. CXO focused = 70% of the leads will convert. Regional tradeshow = 30% of the leads will convert. ABM = 50% of the leads will convert.
1. There will be a swag line item per region per qtr, so each sub region will not need to take this into consideration when it comes to the cost of the activity. AMER: Reminder that the Nadel portal spits out swag costs upon order completion.
1. At times, expenses are shared across an entire region and we need to account for this shared expense in an overall tab vs. a subregion tab.
    - An example of this could be run rate swag, stickers, tradeshow assets not tied to a specific event, or on online or in person event that all regions will benefit from.
    - Today, AMER & EMEA have their own regional tabs. (In addition to the subregion tabs each FMM is the DRI for.)
    - When splitting costs between regional teams, make sure you split all fields based on the percentage being split. For example - If NE/SE/East-Central Named/East-Central Geo are splitting an event evenly between all sub-regions, make sure the cost, MQLs, reach, leads, etc. all split out evenly in all 4 line items. Each sub-region will get its own line item for the event but will utilize the same campaign tag. Remember to specify the correct territory for each sub-region if you are copying and pasting details.
    - The Manager of the region will manage this tab and expense, although reach tactic or event will have its own DRI.
    - If the amount allocated to the region needs to be changed, the Manager of the regional budget will create an issue in the [Finance project](https://gitlab.com/gitlab-com/finance) for the Financial Analyst to update the total that should be allocated to the overarching tab. That money will be taken out of the other subregional tabs and added into the regional tab, so that the quarterly totals remain aligned to the budget.
1. For budgeting purposes we need to stipulate the amount in USD (as we currently operate in USD) in the FMM budget
    - When converting from a different currency, we minimally round up. For example, if the conversion of $500 AUD is currently $342.98 USD, then round up and quote $350 USD.
1. VAT and GST does not need to be included into budget planning as these do not attribute to program expenses, however sales tax in the US does.
1. As it relates to procurement issues being approved, it is essential that the contract details are submitted with the procurement issue, that way the approver is clear about what they are signing off on.
    - In FY21, we aim for an MQL to not cost us more than $500, should a contract be submitted that is over the $500 goal, there needs to be a documented reason + path to success shown in the procurement issue. It is the responsibility of the FMM Country Manager to ensure this documentation is listed in the procurement issue before the issue is sent on to further leadership to review/approve. For C-level and other campaign where we target decision makers, the customer journey needs to be documented and we acknowledge that the MQL might be higher.
1. Working through our Partner Marketing organization, Field Marketing can submit activities that can be considered for market development funds (MDF). In doing this, the entire amount of the activity needs to be budgeted for in the Field Marketing budget. We do this because the percentage covered by the channel partner will come through our revenue account, not our prepaid account and the transactions need to be kept separate. This also may mean you are over budget. If that is the case, then you will need to add a comment in the budget document and also clearly share this with your manager so that your manager may communicate this with Finance and Accounting in our monthly review meetings.

# Legal Approval

Field Marketing executes contracts with outside vendors to support the work we do. We do so by following the company [procure to pay process](/handbook/finance/procure-to-pay/#vendor-and-contract-approval-workflow).

We have an expectation of a 5 business day turnaround for a GitLab executed contract (could be longer depending on the amount of the contract & how many chains of approval the contract needs to go through).

If an issue has been sitting for more than 5 business days and does not have approval from legal, the FMC managing the issue is to ping legal in the issue reminding them to please review. If an FMC will be out of the office while a contract review is pending, it is their responsibility to assign a delegate to manage. 

It should be a rare occasion (and not a result of poor planning), that a contract would need to be urgently (less than 5 business days) turned around. Only after all FMM approvals have happened, should the FMC ping legal on slack asking them to please approve an urgent ask.

# Field Marketing Communications

## Slack Rooms we use

- [#fieldmarketing](https://gitlab.slack.com/archives/CCWDAJ8PK) - open to all teams to ask questions to field marketing & where weekly stand-up reports get posted to.
    - If you're an employee at GitLab trying to reach Field Marketing, please head over to our slack channel.
        - **NOTE**: If you're requesting we sponsor an event, instructions can be found [here](/handbook/marketing/revenue-marketing/field-marketing/#whats-currently-scheduled-in-my-region).
        - If a region specific question is asked in the [#fieldmarketing](https://gitlab.slack.com/archives/CCWDAJ8PK) slack room, please tag the [regional Field Marketing leader](#whats-currently-scheduled-in-my-region), as that person is the DRI.
- [#fieldmarketing-fyi](https://gitlab.slack.com/archives/C01502JC5BK) - Official channel for Field Marketing Leadership to post announcements to the Field Marketing team.
    - Restricted permission levels to only FMM Leadership posting the announcements
    - Announcements can be submitted to your manager if you would like to something shared
    - All members have comment access to allow clarifications if needed on a thread
    - You are to react with an emoji (thumbs-up, eyes…) to signal to managers that you’ve seen an announcement
    - There should be no need to cross-post announcements (such as posting a link from another slack channel to the #fieldmarketing channel)
        - With a #company-fyi and the [#fieldmarketing-fyi](https://gitlab.slack.com/archives/C01502JC5BK), all relevant info should be successfully communicated by managers to team members.
    - Default to issues first, then public channels when possible to avoid siloing work information to private discussions.
        - Use DMs for personal, private conversation rather than work communication (unless you are certain it is only relevant to the 2 of you or you need immediate attention).
- #fieldofdreamsteam: private room only accessible to FMMs - funny gifs, private questions for the team.
- Please keep in mind we have detailed instructions on [how GitLab uses slack](/handbook/communication/#slack).

## Monthly Calls

- The World Wide Field Marketing team has one standing call on the calendar.
- The team meets on the 1st Wednesday of each month.
- All team members are invited to each call although, if the call is scheduled outside of the team member's normal working hours, the team member is NOT expected to attend. The calendar invite simply serves as a reminder to the team member to check out the recording of the meeting the next working day and to also review the agenda.
- The purpose of the team call is to share relevant company, marketing, and Field Marketing announcements.
- Anyone on the team should feel empowered to add content to the team meeting agenda.
- We will also have a specific `lessons learned` section where each Country Field Marketing Manager will select 1 FMM event or other tactic event recap to review.
    - The FMM who is the DRI for the event or other tactic we will be reviewing as a team will add the `Event Recap` link from the `Event Planning and Recap` to the agenda
    - We review the recap prior to the call, the FMM DRI briefly gives a run down of the event or other tactic, then we dialogue.
    - Each region will be represented - AMER, APAC, and EMEA - so we will review 3 each call.
- We also discuss use cases - could be how you've worked with social media, how you built a report in SFDC to help the team be more efficient, etc,
- Guest Speakers - at times we will also invite other colleagues from the company to address our team as a whole
- As a handbook first company, if you are going to bring a topic to the team, please think twice on if you should add just an agenda item, or if you should add an agenda item that links to an MR or a handbook page.
- If you're not on the Field Marketing team at GitLab and you're a GitLab employee who's interested in joining our team meeting, please feel free to ask in the #fieldmarketing slack room, we'd welcome the opportunity to host you!

## Async Status Updates

Since we are a [remote](/company/culture/all-remote/) company, we utilize a Slack plugin called [Geekbot](https://geekbot.io/) to coordinate status updates. Field Marketing world wide currently conducts 1 weekly standup. Geekbot shares this update in the public #fieldmarketing slack room.

## Field Marketing and GitLab teams Sync

Monthly Sync with PMM Team:

To boost communication and encourage new ideas via cross-team collaboration, the Field Marketing Team will be teaming up with the Product Marketing Team once per month to focus on content creation, vertical marketing, use-cases and prioritizing field marketing needs. This meeting will focus on what's currently available and what our best next steps forward are.
Updates will be shared in the #fieldmarketing channel on Slack and stored in the meeting doc.

### Agenda

1. Review current available and upcoming content
1. Discuss Field Marketing requests

Meeting Notes
All meeting notes can be reference [here](https://docs.google.com/document/d/13v0aXrE4jUcXWgmOjBjCs8audP_TvJ1X7QzEmNwSOXM/edit?usp=sharing)

## Weekly Status Update

The **Weekly Status Update** is configured to run at 9 AM local time on Monday, and contains a series of rotating questions. Below is an example of questions you may see:

1. _**What was your favorite part of the weekend?**_
The goal with this question is for you to get to know your colleagues and for you to be able to share what excited you from the previous few days when you weren't working.
1. _**What's happening in your life?**_
Is there anything you'd like the team to know about what's going in your life? Feel free to share as much or as little as you feel comfortable sharing.
1. <i>**Are you traveling anywhere this week? If so, where and why.**</i> <--- This question is paused for the moment. :)
As Field Marketers, we travel up to 50% of the time. Sharing where you are is important, especially if are in a different timezone.
1. _**What are your top 1-3 priorities for the next week?**_
These top 3 priorities should be focused on what you plan to accomplish that week.
1. _**Anything blocking your progress?**_
Of those 1-3 items listed, do you need any roadblocks removed in order to accomplish the priorities?

You will be notified via the Geekbot plug in on slack at 9 AM your local time on Mondays, as stated above. It is important to note, that unless you answer all questions in the Geekbot plug in, your answers will NOT be shared with your colleagues, so please be sure to complete all questions!

# Field Marketing MR process

Everyone can contribute, although not everyone has merge rights. Within the Field Marketing team, when someone submits an MR, the submitter needs to assign to their direct line manager to review, and then their manager will assign to the Director of Field Marketing to review. Once the Director has reviewed/approved, it is their responsibility to ensure all threads are addressed and resolved and all merge conflicts are resolved, then the Director will assign the MR to the VP of Revenue Marketing for the MR to be merged.

Should a Country Manager of Field Marketing or Director of Field Marketing submit a process change in the handbook, then all Country Managers should sign off via approving the MR or commenting their approval. At which point, the Director ensures all threads are addressed and resolved and all merge conflicts are resolved, then the Director will assign the MR to the VP of Revenue Marketing for the MR to be merged.

# Other pages to review for a full understanding of how Field Marketing at GitLab operates

- [Events at GitLab](/handbook/marketing/events/)
- [Marketing Program Management](/handbook/marketing/revenue-marketing/digital-marketing-programs/marketing-programs/)
- [Marketing Operations](/handbook/marketing/marketing-operations/)
- [Sales Development](/handbook/marketing/revenue-marketing/sdr/)
- [Links GitLab Field Marketers find useful](https://docs.google.com/spreadsheets/d/1gjJghF8Va-G0lYWsDaYXKG7JPtADLS2Jhrh8IVHkizQ/edit?ts=5d249a33#gid=1748424259&range=A1)
- [Field Marketing onboarding videos](https://drive.google.com/open?id=1m8ReMIiymMTqqk5PJAG7u_IG-Q5pkusV) - NOTE - these are also in the Field Marketing Onboarding issue that is kept in the [Marketing onboarding project](https://gitlab.com/gitlab-com/marketing/onboarding#onboarding)
