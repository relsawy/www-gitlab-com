---
layout: handbook-page-toc
title: "Most commonly used sales resources"
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The purpose of this page is to be a _quick reference_ to help the sales team navigate and find resources to help them be more efficient and effective. This is NOT every possible resource, but a curated list of what we believe is **most valuable**. If you find a broken link - either **update it** or **contact the PMM team**. Ultimately, these resources should ALL be available on our main [resources page](/resources/). This is a work in progress resource. **Sales Team**: if you have comments, suggestions, or feedback about this page, this [issue- #366](https://gitlab.com/gitlab-com/marketing/product-marketing/issues/366) is open for ongoing feedback.

## Top 10 resources

- [Customer Deck](https://docs.google.com/presentation/d/1SHSmrEs0vE08iqse9ZhEfOQF1UWiAfpWodIE6_fFFLg/) (GitLab-internal) the GitLab value driver narrative and supports a discussion about GitLab differentiators.
- [Pitch Deck](https://docs.google.com/presentation/d/1dVPaGc-TnbUQ2IR7TV0w0ujCrCXymKP4vLf6_FDTgVg/) (GitLab-internal) the GitLab narrative.
- 2019 Forrester Cloud CI Wave™: [public link](https://about.gitlab.com/analysts/forrester-cloudci19/) — [GitLab-internal](https://docs.google.com/spreadsheets/d/1NK_0Lr0gA0kstkzHwtWx8m4n-UwOWWpK3Dbn4SjLu8I/edit#gid=0)
- 2019 Gartner Peer Insights "Voice of Customer" Report, Enterprise Agile Planning Tools: [public link](https://about.gitlab.com/resources/report-gartner-peer-insights-eapt/) — [GitLab-internal](https://www.gartner.com/doc/reprints?id=1-1OF99UBS&ct=190826&st=sb)
- [Security and Compliance Capabilities Deck](https://docs.google.com/presentation/d/1WHTyUDOMuSVK9uK7hhSIQ_JbeUbo7k5AW3D6WwBReOg/edit?usp=sharing) (GitLab-internal)
- [Goldman Sachs case study](https://about.gitlab.com/customers/goldman-sachs/) - ENT, NORAM and EMEA
- [Axway case study](https://about.gitlab.com/customers/axway-devops/) - ENT, NORAM
- [Paessler AG case study](https://about.gitlab.com/customers/paessler/) - MM, EMEA
- [Data Sheet - Digital Transformation with GitLab](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/uploads/da184544b2ec5b808bd9fbb237459208/Digital_Transformation_with_GitLab_-_unbranded.pdf)
- [Capabilities Statement](https://about.gitlab.com/images/press/gitlab-capabilities-statement.pdf)

## Command of the Message

- The most valuable pieces of sales collateral are our resources for Command of the Message (CoM) and the GitLab Value Framework. You can find links to these resources [in the CoM section of the sales handbook](/handbook/sales/command-of-the-message)

## Pricing and tiers

- [Why Ultimate Page](/pricing/ultimate/), [Why Ultimate Deck](https://docs.google.com/presentation/d/1TP5cXH5Nr0VkH7mE6M_-DFXT_Jnq7o5LPxuMUz2paI4/edit)
- [Why Premium](/pricing/premium/)
- [10 Reasons to NOT sell Starter](https://docs.google.com/presentation/d/1pJ3qrDh7fd4UQ9njs1K4LrQG2UL2TwlWUJjPGgQrmS0/edit#slide=id.p)
- [Choosing between GitLab.com and self-managed subscriptions](/handbook/marketing/product-marketing/dot-com-vs-self-managed/)
- [Self-managed pricing](/pricing/#self-managed), [Self-managed feature comparison](/pricing/self-managed/feature-comparison/)
- [GitLab.com pricing](/pricing/#gitlab-com), [GitLab.com feature comparison](/pricing/gitlab-com/feature-comparison/)
- [License FAQ](/pricing/licensing-faq/)
- [Pricing handbook](/handbook/ceo/pricing/)
- [Past releases sorted by tier](/releases/), [Upcoming releases sorted by stage](/upcoming-releases/), [upcoming features sorted by tier](/direction/#paid-tiers)
- [Product maturity](/direction/maturity/)

## GitLab Usecases

Here is a list of GitLab usecases. Visit the [Usecase handbook page](/handbook/use-cases/) for the full list.

- [Version Control and Collaboration (VC&C)](/about.githandbook/marketing/product-marketing/usecase-gtm/version-control-collaboration/)
- Continuous Integration (CI) - [WIP buyer's journey collateral map](https://docs.google.com/spreadsheets/d/1-VmrzB7T1b-UXBoP54j1pYMbYE78TGeKflFom3KNfU8/edit#gid=711048571)
- [Continuous Integration](https://about.gitlab.com/handbook/marketing/product-marketing/usecase-gtm/ci/)
- [Continuous Delivery](https://about.gitlab.com/handbook/marketing/product-marketing/usecase-gtm/cd/)
- DevSecOps
- Project Management
- Simplify DevOps
- Cloud Native Development
- GitOps

## Presentations and Messaging

- [Elevator Pitch and Messaging](/handbook/marketing/product-marketing/messaging/) - Overall GitLab value proposition and messaging
- [Pitch Deck:](https://docs.google.com/presentation/d/1dVPaGc-TnbUQ2IR7TV0w0ujCrCXymKP4vLf6_FDTgVg/edit?usp=sharing) - The primary GitLab overview presentation.
- [NEW Customer deck based on GitLab value drivers](https://docs.google.com/presentation/d/1SHSmrEs0vE08iqse9ZhEfOQF1UWiAfpWodIE6_fFFLg/edit?usp=sharing)
- [Customer Needs & Solution Pitch Deck](https://docs.google.com/presentation/d/1tixVN2g-QU6_HwdLBudVgKPutnWzg2Pv7K8gU9eour8/edit#slide=id.p1) - Deck to present customer pain points and a GitLab Solution.
- [Product Vision, Strategy, and FY20 Plan](https://docs.google.com/presentation/d/19o720CqP9S-xRQoT9y8FF7DgtRxuJhQedaaQQQygYx8/edit#slide=id.g57cef2563b_0_0)
- [Professional Services Deck](https://docs.google.com/presentation/d/1CFR8_ZyE9r4Dk_mjoWGe4ZkhtBimSdN0pylIPu-NAeU/edit?usp=sharing)
- [Security Overview](https://docs.google.com/presentation/d/1WHTyUDOMuSVK9uK7hhSIQ_JbeUbo7k5AW3D6WwBReOg/edit?usp=sharing)
- [Agile Overview Slides](https://docs.google.com/presentation/d/1MmPPao_382wBYYbQ_DLiBGuxsYNRzodiZ0MydXeeYU8/edit#slide=id.g5d6196cc9d_2_113)
- [Continuous Delivery Overview](https://docs.google.com/presentation/d/1bGdjQNfHxmYKYz_ZsrtyhEyXLGlv8UoTavi_aGl3UNc/edit)
- [Accelerating Delivery Short Talk](https://drive.google.com/open?id=15PgY9Dm0emVlWZdBjJX15nVxDR47l1MwhOF-tGbzY5Y)
- [Accelerating Delivery + Digital Transformation](https://drive.google.com/open?id=1_fCQW8-K_QaUAtTMfvrTSmjRRYRdpaZp78ljndEsk0c) and [recording](https://www.youtube.com/watch?v=MwSJuKYXAy4&feature=youtu.be)
- [Kubernetes 101](https://docs.google.com/presentation/d/1fWjiVgSNMKTHyC6_nWDY5rDhvdm-zEQMQytZysIXAzk/edit)
- [Digital & Cloud Native Transformation (GitLab Benefits) Deck](https://docs.google.com/presentation/d/1mcg5E4dztQZQTN4WvJ4sdqnRGZ6Wz7IRDa8_o8-p4vg/edit#slide=id.g51da45aefc_0_5)
- [Cloud Native Transformation (vendor agnostic speaker deck)](https://docs.google.com/presentation/d/15FN3tebw8LZMYkOMtqaPmHV17NDVZy0K6Tou1-TS-Ac/edit#)
- [Multi-cloud Serverless](https://docs.google.com/presentation/d/16TgspGl7jxVRMStfNHytZsDFQK1Mr5QWn5UP06N3JnA/edit)
- [Why Ultimate](https://docs.google.com/presentation/d/1TP5cXH5Nr0VkH7mE6M_-DFXT_Jnq7o5LPxuMUz2paI4/edit)
- [End to End Insight & Visibility Differentiator - Dashboards & Analytics in GitLab](https://docs.google.com/presentation/d/1gGJP3cil0MNi2hMzJSXc-5MDLGuult2KncmuziEJ76w/edit#slide=id.p)

## Help with RFPs

- [RFP Archive](https://drive.google.com/drive/u/0/folders/0B0JubjRNovkvNVJTVmNJdXJNZEk)
- [Request for SOC2 Type 1 Compliance Report](/engineering/security/soc2.html#requesting-a-copy-of-the-gitlab-soc2-type-1-report)

## GitLab by the Stages

Key resources by stage.

### Manage

- [Geo and DR](/solutions/geo/)
- High Availability via our [Reference Architectures)](/solutions/reference-architectures/)
- [Value Stream Management](/solutions/value-stream-management/)

### Plan

- [Agile Delivery Overview](/solutions/agile-delivery/)
- [Agile Teams Video](https://youtu.be/VR2r1TJCDew)
- [GitLab and Scaled Agile Framework Video](https://www.youtube.com/watch?v=PmFFlTH2DQk&feature=youtu.be)
- [Agile Overview Slides](https://docs.google.com/presentation/d/1MmPPao_382wBYYbQ_DLiBGuxsYNRzodiZ0MydXeeYU8/edit#slide=id.g5d6196cc9d_2_113)
- [Best Practices to Architect the Project Management Function Using GitLab](https://docs.google.com/document/d/1Q1A0mI2WWmQ5UQ38PwIuCy0FxZrPGamvwmjP9W1zn4E/edit?usp=sharing)

### Create

- [SCM Overview](/product/source-code-management/)

### Secure

- [Dev Sec Ops Overview](/solutions/dev-sec-ops/)
- [Security Demo Video](/resources/video-gitlab-security-demo/)
- [Whitepaper GitLab App Sec Workflow](/resources/downloads/gitlab-seismic-shift-in-application-security-whitepaper.pdf)
- [WhiteSource integration](/handbook/marketing/product-marketing/enablement/security-integrations-whitesource/)

### Verify, Package, Release, Configure

Customers aren't going to think about or ask for different "stages". They are going to say things like

- We want to adopt DevOps
- We want to adopt CI/CD
- We want to adopt Kubernetes (same thing as saying "We want to go cloud native")

#### CI/CD Resources

- [CI/CD overview](/stages-devops-lifecycle/continuous-integration/)
- [Scalable app deployment with Google Cloud and GitLab video](https://www.youtube.com/watch?v=uWC2QKv15mk) (Great Kuberentes 101 content for customers that are unfamilar. Also Demos Auto DevOps.)
- [Auto DevOps 101 video](https://www.youtube.com/watch?v=pPRF1HEtQ3s&t=923s)
- [Automating Kubernetes Deployments video](https://www.youtube.com/watch?v=wEDRfAz6_Uw) (For folks familiar with Kubernetes or Cloud Native. This is a webinar hosted by the CNCF. Goes into both business value and more technical detail on GitLab CI/CD)
- [GitLab + Kubernetes](/solutions/kubernetes/)
- [What is Cloud Native?](/cloud-native/)
- [What are microservices?](/topics/microservices/), [Business value of microservices](/topics/microservices/#business-value-of-microservices)
- [What is serverless?](/topics/serverless/), [Business value of serverless](/topics/serverless/#business-value-of-serverless)

### Monitor

- [Monitor overview](/stages-devops-lifecycle/monitor/)
- [Integrated APM for Cloud Native Applications](/solutions/apm/)
- [Monitor Demo video](https://www.youtube.com/watch?v=mm_8wVjn808&t=2s)
- [Incident Management video](https://www.youtube.com/watch?v=f4wNjjF9NhQ)

### Protect

- [Threat Management Open Source Projects](/handbook/engineering/development/threat-management/#open-source-projects) including:
    - ModSecurity based Web Application Firewall featuring the [ModSecurity Core Rule Set](https://owasp.org/www-project-modsecurity-core-rule-set/)
    - Container Network Security for Kubernetes
    - Vulnerability Management
- [Protect Container Security Overview Slides](https://docs.google.com/presentation/d/1xLBQljc6yRtG8ENGwjX9I7LgGj-ShtjaARZPY9WbtQs/edit#slide=id.g29a70c6c35_0_68)

## [Analyst Reports](/analysts/)

Internal and external links to analyst reports can be found in the [Resource Links](https://docs.google.com/spreadsheets/d/1NK_0Lr0gA0kstkzHwtWx8m4n-UwOWWpK3Dbn4SjLu8I/edit#gid=0) doc (GitLab-internal access only). Direct links to assets can be shared with customers and prospects. Only public, external links should be shared on social media.

- [Overview: Forrester Continuous Integration Tools](/analysts/forrester-ci/) and Report: GitLab and The Forrester Wave™: Continuous Integration Tools, Q3 2017
- [Overview: Forrester Value Stream Management](/analysts/forrester-vsm/) and [Report: GitLab and The Forrester New Wave™: Value Stream Management Tools, Q3 2018](/resources/forrester-new-wave-vsm-2018/)
- [Overview: Gartner Application Release Orchestration](/analysts/gartner-aro/) and Report: GitLab and the Gartner Magic Quadrant for Application Release Orchestration 2018
- [Overview: IDC Innovators Agile Code Development Technologies, 2018](/analysts/idc-innovators/) and [Report: IDC Innovators Agile Code Development Technologies, 2018](https://page.gitlab.com/rs/194-VVC-221/images/IDC-Innovators-Agile-Code-Development-Technologies-2018.pdf)
- [Analyst Program Overvew Page](/handbook/marketing/product-marketing/analyst-relations/)

## White Papers

Key Whitepapers:

- [A seismic shift in application security](/resources/downloads/gitlab-seismic-shift-in-application-security-whitepaper.pdf)
- [Bridging the divide between developers and management](/resources/downloads/201806_WP_Bridging_developers_and_management.pdf)
- [What is Concurrent DevOps](/resources/downloads/gitlab-concurrent-devops-whitepaper.pdf)
- [_Scaled CI and CD_](https://page.gitlab.com/rs/194-VVC-221/images/gitlab-scaled-ci-cd-whitepaper.pdf)
- [_Moving to Git: a guide_](https://page.gitlab.com/rs/194-VVC-221/images/gitlab-moving-to-git-whitepaper.pdf)
- [How GitLab is Enterprise Class](/solutions/enterprise-class/)
- [2018 Developer Report](/developer-survey/previous/2018/)
- [Reduce cycle time whitepaper](/resources/downloads/201906-whitepaper-reduce-cycle-time.pdf)
- [Speed to mission whitepaper](/resources/downloads/201906-whitepaper-speed-to-mission.pdf)
- [Forrester-Manage your toolchain before it manages you](/resources/whitepaper-forrester-manage-your-toolchain/)
- [How to deploy on AWS from GitLab](https://learn.gitlab.com/c/deploy-to-aws?x=04KSqy)

## Datasheets

- [GitLab Capabilities](/images/press/gitlab-capabilities-statement.pdf)
- [GitLab Data Sheet](/images/press/gitlab-data-sheet.pdf)
- [GitLab Data Sheet (2020 Update, Unbranded)](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/uploads/da184544b2ec5b808bd9fbb237459208/Digital_Transformation_with_GitLab_-_unbranded.pdf)
- [GitLab on AWS Concurrent DevOps Solution Brief](/resources/downloads/GitLab_AWS_Solution_Brief.pdf)

## [Customer References](/customers/)

- [Customer proof points](https://about.gitlab.com/handbook/sales/command-of-the-message/proof-points/)
- [Goldman Sachs improves from two daily builds to over a thousand per day](/customers/goldman-sachs/)
- [How Wag! cut their release process from 40 minutes to just 6 minutes](/blog/2019/01/16/wag-labs-blog-post/)
- [Axway realizes a 26x faster release cycle by switching from Subversion to GitLab](/customers/axway/)
- [Paessler AG switches from Jenkins and ramps up to 4x more releases](/customers/paessler/)
- [Trek10 provides radical visibility to clients](/customers/trek10/)
- [Particle physics laboratory uses GitLab to connect researchers from across the globe](/customers/cern/)
- [iFarm plants the seeds for operational efficiency](/customers/ifarm/)
- [Connecting the cosmos with Earth - How the European Space Agency uses GitLab to focus on space missions](/customers/european-space-agency/)
- [How GitLab CI supported Ticketmaster’s ramp up to weekly mobile releases](/blog/2017/06/07/continous-integration-ticketmaster/)
- [The Cloud Native Computing Foundation eliminates complexity with a unified CI/CD system](/customers/cncf/)
- [Worldline hosts 14,500 projects and has 3,000 active users on their GitLab platform](/customers/worldline/)
- [Equinix increases the agility of their DevOps teams with self-serviceability and automation](/customers/equinix/)
- [The Paul G. Allen Center for Computer Science & Engineering gains control and flexibility to easily manage 10,000+ projects](/customers/uw/)
- **[ALL Customer Case Studies](/customers/)**
- [Overall Customer Reference Program](/handbook/marketing/product-marketing/customer-reference-program/)

## [Comparisons](/devops-tools/)

Here are links to a few **KEY comparisons** and then a link to main comparison page where over 50 comparisons are available.

- [GitHub](/devops-tools/github-vs-gitlab.html)
- [BitBucket](/devops-tools/bitbucket-vs-gitlab.html)
- [AWS Codestar](/devops-tools/codestar-vs-gitlab.html)
- [Jenkins](/devops-tools/jenkins-vs-gitlab.html)
- [Azure](/devops-tools/azure-devops-vs-gitlab.html)
- [Jira](/devops-tools/jira-vs-gitlab.html)
- [ALL Comparisons](/devops-tools/)

## Alliance Partnerships

Here are a few useful links respective to our alliance partners that provide more visibility into our details of our partnership, GTM strategy, and joint solution.

### AWS

- [Public Tracker](https://gitlab.com/gitlab-com/alliances/aws/public-tracker) - this page has the most up to date blog posts, joint content and links.
- [Public AWS Solution Page](https://about.gitlab.com/solutions/aws/) - this page is our AWS customer facing site that shows how we are the best DevOps tool for specific AWS services.
- [AWS Marketplace Listings](https://aws.amazon.com/marketplace/seller-profile?id=9657c703-ca56-4b54-b029-9ded0fadd970&ref=dtl_B071RFCJZK) - these are the 5 user self service listings, we can create a Private Offer to support your customer unique needs. If you are unfamiliar with a Private Offer, it is a way for GitLab and a customer to agree to custom terms (product tier, number of users, or length of contract) and then charge it to the customers AWS bill.
- [Internal Tracker](https://gitlab.com/gitlab-com/alliances/aws/gitlab-tracker/-/boards/1346218) (stuff we are working on with AWS)
- [Register your Opportunities here to connect with your AWS sales peer](https://docs.google.com/forms/d/1yB8DEg6UB1XyIAEUjE2vNjHaQiJ4PX9-jEbZ6gTrFRs/edit?usp=sharing)

### Google Cloud

- [go/gitlab](https://sites.google.com/google.com/gogitlab/home)
    - This webpage primarily serves as the SST for Google sellers, but GitLab sellers will find useful as well to learn more about our GTM, how we co-sell and our joint integrations. The DRI for this page is `@mayanktahil`. This page is only accessible by `@google.com` and `gitlab.com` end users, autheticataed via G Suite credentials.
    - Check out the dedicated [Assets](https://sites.google.com/google.com/gogitlab/assets) page which lists a curated list of key joint assets between GCP and GitLab.
- [Google Cloud Solution Page](https://about.gitlab.com/solutions/google-cloud-platform/)
- [Google Cloud Joint GitLab Group](https://gitlab.com/gitlab-com/alliances/google/)
    - [Public Tracker](https://gitlab.com/gitlab-com/alliances/google/public-tracker) - holds public issues and shared private issues with Googlers
    - [Internal Tracker](https://gitlab.com/gitlab-com/alliances/google/gitlab-tracker/) - holds internal private issues only visible to GitLabbers.
    - [Sandbox](https://gitlab.com/gitlab-com/alliances/google/sandbox-projects) - holds public and private GitLab groups and projects for testing, demoing, and collaboration between Googlers, GitLabbers, and even the public as necessary.
- [External Shared G Drive](https://drive.google.com/drive/folders/0AE2YlFr0vUDTUk9PVA) - holds shared collateral between `@google.com` and `@gitlab.com` users which includes videos, notes, slides, etc. that both companies can reference. Please make sure to read the [Readme Doc](https://docs.google.com/document/d/1uFnhI_rYr3KvYC9korKS50q1L-HMxWUt8yjAA-qDYQc/edit) in the drive for more information.
- [GitLab EE in GKE Marketplace](https://console.cloud.google.com/marketplace/details/google/gitlab?q=gitlab) - Single click to deploy GitLab EE via helm to any GKE cluster on GCP, even Anthos.
- [GitLab EE for Private Offers on GCP Marketplace](https://console.cloud.google.com/marketplace/details/gitlab-public/gitlab-for-gcp?q=gitlab&id=0d5e298b-06b4-4424-b78d-9ab3a352ca82&project=gitlab-public) - This listing details instructions on how to percure GitLab through the GCP marketplace wiht integrated billing for customers. GitLab can only be purchased through the marketplace via private offers at this current time.

### VMware

- [Leveraging the GitLab/VMware Alliance](https://youtu.be/unpgyIR9yH0) (Oct 2020, 30 minutes)
- [VMware Joint GitLab Group](https://gitlab.com/gitlab-com/alliances/vmware)
    - [Public Tracker](https://gitlab.com/gitlab-com/alliances/vmware/public-tracker) - holds public issues and shared private issues with VMware
    - [Internal Tracker](https://gitlab.com/gitlab-com/alliances/vmware/gitlab-tracker) - holds internal private issues only visible to GitLab employees
    - [Sandbox](https://gitlab.com/gitlab-com/alliances/vmware/sandbox) - holds public and private GitLab groups and projects for testing, demos, and collaboration between VMware and GitLab.
- [GitLab EE on the VMware Marketplace](https://marketplace.cloud.vmware.com/services/details/129dc4e9-191d-405f-ab4d-803d56f366a9)

## Partner Ecosystem Integrations

- [WhiteSource integration](/handbook/marketing/product-marketing/enablement/security-integrations-whitesource/)
- [GitLab Technology Partners](https://about.gitlab.com/partners/)

## Demos

- [All Demo Videos](/handbook/marketing/product-marketing/demo/#videos)
    - SCM use case video
    - CI use case video
    - CI/CD use case video
    - [DevSecOps use case video](https://youtu.be/0dpwm0VwdAg)
    - Simplify DevOps use case video
    - Agile use case video
- [All Demo Click Throughs](/handbook/marketing/product-marketing/demo/#click-throughs)
- [All Demo Instructions](/handbook/marketing/product-marketing/demo/#live-instructions)

## Getting Started with GitLab (Services)

- [Professional Services](/services/)
- [Quick Start Implementation Package](/services/implementation/quickstart)
- [Dedicated Implementation Planning](/services/implementation/enterprise)
- [GitLab Training](/services/education)

## ROI

The ROI calculator is a work in progress - welcome contributions.

- [Overall ROI page](/roi/)
- [GitLab replacing other tools](/roi/replace/)

## Social Selling Basics

- [Social Selling Basics presentation](https://docs.google.com/presentation/d/1UCRF6PC6al8XxT8E_4rDKkQjkW6WGPA6gybWeuRIg7A/edit?usp=sharing)
- [Social Selling Basics video](https://youtu.be/w-C4jts-zUw) (20 minutes)
    - [Social Selling_Sales Enablement_2019-07-11](https://www.youtube.com/watch?v=Ir7od3stk70) (28 minutes)
- [LinkedIn Sales Navigator resources](https://docs.google.com/document/d/1UF69ieck4AdHadzgPmZ5X1GBs3085JhlYaMowLj0AOg/edit?usp=sharing)
- [LinkedIn Social Selling Index (SSI)](https://business.linkedin.com/sales-solutions/social-selling/the-social-selling-index-ssi)

## Enablement

- [GitLab Sales Training](/handbook/sales/training/)
- [Sales Enablement Sessions](/handbook/sales/training/sales-enablement-sessions/)
- [GitLab YouTube Sales Enablement playlist](https://www.youtube.com/playlist?list=PLFGfElNsQthYe-_LZdge1SVc1XEM1bQfG)
- [GitLab Unfiltered YouTube Sales Enablement playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrirMKe3CyWl4ZBCKna5rJX)
