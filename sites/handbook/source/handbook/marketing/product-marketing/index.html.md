---
layout: handbook-page-toc
title: Strategic Marketing
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Strategic marketing at GitLab

<table width="100%">
  <tr>
    <td><a href="/handbook/marketing/product-marketing/pmmteam/"><img src="/handbook/marketing/product-marketing/images/product-marketing.png" alt="Product Marketing"></a></td>
    <td><a href="/handbook/marketing/product-marketing/technical-marketing/"><img src="/handbook/marketing/product-marketing/images/technical-marketing.png" alt="Technical Marketing"></a></td>
    <td><a href="/handbook/marketing/product-marketing/competitive/"><img src="/handbook/marketing/product-marketing/images/competitive-intelligence.png" alt="Competitive Intelligence"></a></td>
    <td colspan="2"><a href="/handbook/marketing/product-marketing/mrnci/"><img src="/handbook/marketing/product-marketing/images/market-customer-research.png" alt="Market Research and Customer Refereneces"></a></td>
  </tr>
  <tr>
    <td colspan="5"><a href="/handbook/sales/training/"><img src="/handbook/marketing/product-marketing/images/enablement.png" atl="Enablement: Sales, Partners, Resellers"></a></td>
  </tr>
</table>

1. [Product marketing team](/handbook/marketing/product-marketing/pmmteam/)
1. [Technical marketing team](/handbook/marketing/product-marketing/technical-marketing/)
1. [Competitive intelligence team](/handbook/marketing/product-marketing/competitive/)
1. [Market research and customer insight team](/handbook/marketing/product-marketing/mrnci/)
    - [Analyst relations (AR)](/handbook/marketing/product-marketing/analyst-relations/)
    - [Customer reference program](/handbook/marketing/product-marketing/customer-reference-program/)

### What does Strategic Marketing do at GitLab

Strategic marketing is GitLab's interface to the market. The market is made up of customers, analysts, press, thought leaders, competitors, etc. Strategic marketing enables other GitLab teams such as Sales, Marketing, and Channel with narrative, positioning, messaging, and go-to-market strategy to go outbound to the market. Strategic marketing does market research to gather customer knowledge, analyst views, market landscapes, and competitor intelligence providing marketing insights inbound to the rest of GitLab.

![Strategic marketing functions](/handbook/marketing/product-marketing/images/ProductMarketingFunctions.png)

### Key content created by Strategic Marketing

Strategic marketing creates many types of content for communicating and positioning GitLab for multiple audiences. Some of the different kinds of content produced by Strategic Marketing are listed below.

![Strategic marketing functions](/handbook/marketing/product-marketing/images/PMMOutput.png)

[Product Marketing Messaging](https://about.gitlab.com/handbook/marketing/product-marketing/messaging/)

### Metrics

[Strategic Marketing Content Analytics](/handbook/marketing/product-marketing/metrics/)

### Some key resources

#### Customer facing presentations

Marketing decks linked on this page are the latest approved decks from Strategic Marketing that should be always be in a state that is ready to present. As such there should never be comments or WIP slides in a marketing deck. If you copy the deck to customize it please give it a relevant title, for example include the name of the customer and an ISO date.

- [Company pitch deck](https://docs.google.com/presentation/d/1dVPaGc-TnbUQ2IR7TV0w0ujCrCXymKP4vLf6_FDTgVg/)
The Pitch Deck contains the GitLab narrative.
- [Customer value deck](https://docs.google.com/presentation/d/1SHSmrEs0vE08iqse9ZhEfOQF1UWiAfpWodIE6_fFFLg/edit?usp=sharing)
The Customer deck contains the GitLab value driver narrative and supports a discussion about GitLab differentiators.
- [GitLab security capabilities deck](https://docs.google.com/presentation/d/1WHTyUDOMuSVK9uK7hhSIQ_JbeUbo7k5AW3D6WwBReOg/edit?usp=sharing)
This deck introduces GitLab's position and capabilities around security. It covers why better security is needed now and how GitLab provides that better security in a more effective manner than traditional tools. This deck should be used when talking to prospects who are asking about how GitLab can help them better secure their software via GitLab Ultimate.

To request updates to these decks see [requesting help from the strategic marketing department](#requesting-strategic-marketing-team-helpsupport)

#### Key Demos

##### Videos

- [GitLab in 3 minutes](https://youtu.be/Jve98tlZ394) (~3 mins)
- [Benefits of a Single Application](https://youtu.be/MNxkyLrA5Aw) (~2.5 minutes)
- [GitLab Overview](https://youtu.be/7q9Y1Cv-ib0) (~12 mins)
- [GitLab for Remote Teams](https://youtu.be/qCDAioq3eis) (~9 mins)

##### Click through Demos

- [GitLab Planning to Monitoring](https://tech-marketing.gitlab.io/static-demos/ptm-v14.html)
- [GitLab Secure Capabilities](https://docs.google.com/presentation/d/1fdTmdepdaq03OSfcA3pYduDxDnQEyvY4ARPqXEX8KrY/edit#slide=id.g2823c3f9ca_0_9)
- [GitLab Agile Project Management](https://docs.google.com/presentation/d/13Zj83pjpwyq3s4T2fPSTuKO8NwqdCdn827GB7S-3hW8/edit?usp=sharing)
- [GitOps with GitLab](https://docs.google.com/presentation/d/e/2PACX-1vQTBQAHx7zyNd4o3YIyKmFEsRJl8-BCdd2g6MdCKuJuFab_HNea_HYK7HDSzd3macx6LnVtYwIlCxV7/embed?start=false&loop=false&delayms=3000)

#### Print collateral

The following list of print collateral is maintained for use at trade shows, prospect and customer visits. The GitLab marketing pages should be downloaded as PDF documents if a print copy for any marketing purposes is needed.

- [GitLab DataSheet](/images/press/gitlab-data-sheet.pdf)
- [GitLab Federal Capabilities One-pager](/images/press/gitlab-capabilities-statement.pdf)
- [How GitLab is Enterprise Class](/solutions/enterprise-class)
- [Reduce cycle time whitepaper](/resources/downloads/201906-whitepaper-reduce-cycle-time.pdf)
- [Speed to mission whitepaper](/resources/downloads/201906-whitepaper-speed-to-mission.pdf)

#### Use case Go To Market

##### [Why use case driven GTM?](/handbook/marketing/product-marketing/usecase-gtm/)

##### Use case based messaging

The Strategic Marketing team also develops messaging and collaterals that aligns with a buyer's needs and a journey with GitLab supporting those needs. For example, a customer with a specific problem of SCM does not need to be sold the value of the single application. Rather these use case based messaging and collaterals will help when talking to customers to address their specific pain points.

- [Usecase: Simplify DevOps](/handbook/marketing/product-marketing/usecase-gtm/simplify-devops)
- [Usecase: Version Control & Collaboration](/handbook/marketing/product-marketing/usecase-gtm/version-control-collaboration)
- [Usecase: Continuous Integration](/handbook/marketing/product-marketing/usecase-gtm/ci/)
- [Usecase: Continuous Delivery](https://about.gitlab.com/handbook/marketing/product-marketing/usecase-gtm/cd/)
- [Usecase: DevSecOps](/handbook/marketing/product-marketing/usecase-gtm/devsecops)
- [Usecase: Agile Project Management](https://about.gitlab.com/handbook/marketing/product-marketing/usecase-gtm/agile/)
- Usecase: Cloud Native Development
- [Usecase: GitOps](https://about.gitlab.com/handbook/marketing/product-marketing/usecase-gtm/gitops/)

#### Speaker Abstracts

To encourage reuse and collaboration, we have a [shared folder of past abstracts](https://drive.google.com/drive/folders/1ODXxqd4xpy8WodtKcYEhiuvzuQSOR_Gg) for different speaking events.

#### Strategic marketing - team specific planning and reporting resources

- [Strategic marketing group conversation slides](https://drive.google.com/drive/folders/1fCEAj1HCegJOJE_haBqxcy2NYm0DS1FO)
- [Strategic marketing FY 2020 Vision](https://docs.google.com/presentation/d/1sbpBNy5OpO0QGvkAeobNyyIcEjTRGIkyApKeC1Oa8xY/edit#slide=id.g4a712342f9_0_852)
- [CMO Triage Board](https://gitlab.com/groups/gitlab-com/-/boards/1160244?label_name%5B%5D=CMO%20Staff%20Triage)
- [Strategic Marketing Staff Triage Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/1237365?label_name%5B%5D=sm_request)
- [Technical Marketing Issue Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/926375?&label_name%5B%5D=tech-pmm)
- [Partner Marketing Issue Board](https://gitlab.com/gitlab-com/marketing/general/-/boards/814970)
- [Customer Reference Board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/927283?&label_name%5B%5D=Customer%20Reference%20Program)
- [Case Studies Board](https://gitlab.com/groups/gitlab-com/marketing/-/boards/918204?&label_name%5B%5D=Case%20study)
- [Strategic Marketing Staff Triage Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/1237365?label_name%5B%5D=sm_request)
- [Sales Enablement Board](https://gitlab.com/gitlab-com/marketing/general/boards/465497?=&label_name%5B%5D=Sales%20Enablement)
- [SDR Coaching Board](https://gitlab.com/gitlab-com/marketing/general/boards/772948?label_name%5B%5D=XDR-Coaching)
- [PMM Budget Planning](https://docs.google.com/spreadsheets/d/1_MN8K9ixdgOp32DKiGPjT-208pr8s-rNKKuuYHsqCdI/edit#gid=1423447843)
- [PMM Hiring Planning](https://docs.google.com/spreadsheets/d/12mNijMwA8hIG5h3zV5JJ0oxsVh6xzk6-si0eT7L9mvM/edit#gid=1301737184)
- [Strategic Marketing Event and Travel Priorities](/handbook/marketing/product-marketing/events_travel/)

#### Other useful resources to help your productivity

- [Getting Started - GitLab 101 - No Tissues for Issues](/handbook/marketing/product-marketing/getting-started/101/)
- [Getting Started - GitLab 102 - Working Remotely](/handbook/marketing/product-marketing/getting-started/102/)
- [Getting Started - GitLab 103 - Maintaining common slides across decks](/handbook/marketing/product-marketing/getting-started/103/)
- [Getting Started - GitLab 104 - Building Project Issue Templates](/handbook/marketing/product-marketing/getting-started/104/)
- [Getting Started - GitLab 105 - Label Triage Bot - Automatic Hygiene](/handbook/marketing/product-marketing/getting-started/105/)
- [Getting Started - Strategic Marketing Project Management Overview](/handbook/marketing/product-marketing/getting-started/sm-project-management/)
- [Getting Started - How to Iterate](https://docs.google.com/presentation/d/14wjQoP8mXre-VWyX_e6F4Tp4D7kwvIetHHXEzheuqFM/edit#slide=id.g810dda6128_4_329)
- [Markdown style guide for about.gitlab.com](/handbook/markdown-guide/#markdown-style-guide-for-aboutgitlabcom)
- [Searching the GitLab Website Like a Pro](/handbook/tools-and-tips/searching/)
- [Other frequently used sales resources](/handbook/marketing/product-marketing/sales-resources/)
- [QBR Support Process](/handbook/marketing/product-marketing/qbr/)

### Requesting Strategic marketing team help/support

All Strategic Marketing Work is tracked and managed as issues in the Strategic Marketing Project. If you need support from the team, the simple process below will enable us to support you.

1. [Open an SM Support Request Issue](https://gitlab.com/gitlab-com/marketing/product-marketing/issues/new?issuable_template=A-SM-Support-Request) this link will the _A-SM-Support-Request_ template. PLEASE fill in what you know.
1. The Strategic Marketing leadership team will review the request, assign it to a SM Team, prioritize the work and plan how to support your requests.
1. If you need more immediate attention please send a message with a link to the issue you created in the `#product-marketing` slack channel. Add an `@reply` to the PMM responsible or you can ping the team with `@pmm-team`.

### Strategic marketing project management

The process for [how Strategic Marketing manages requests](/handbook/marketing/product-marketing/getting-started/sm-project-management/#sm-request-process) is on the [sm project management page](/handbook/marketing/product-marketing/getting-started/sm-project-management/)

### Sales and partner enablement

Strategic Marketing team members serve as subject matter experts and conduct sales trainings scheduled by the [Sales Training team](/handbook/sales/training/). For info go to the [Sales Enablement page](/handbook/sales/training/sales-enablement-sessions/).

### Strategic Marketing Hiring Process - What to Expect

1. Application Screening - Recruiter and Hiring Managers screen applications, resumes, and cover letters to select potential candidates.
1. Recruiter Screening - The recruiter schedules a brief call to discuss the role and the candidates potential fit.
1. Interviews - the typical sequence of interviews follows three stages.
    1. Hiring Manager Interview
    1. Team interviews - typically 3 - 4 separate interviews with different potential co-workers (product, sales, etc.)
    1. Executive interview with the Sr. Director of the Strategic Marketing team, the CMO, or even the CEO

## Meet the Strategic Marketing Leadership team

[**Ashish Kuthiala**](https://about.gitlab.com/company/team/#kuthiala)

- Title: Senior Director, Strategic Marketing
- Email: ashish@gitlab.com
- GitLab handle: kuthiala
- Slack handle: ashish

[**Colin Fletcher**](https://about.gitlab.com/company/team/#cfletcher1)

- Title: Manager, Market Research and Customer Insights
- Email: cfletcher@gitlab.com
- GitLab handle: cfletcher1
- Slack handle: Colin Fletcher

[**John Jeremiah**](https://about.gitlab.com/company/team/#johnjeremiah)

- Title: Manager, Product Marketing
- Email: jjeremiah@gitlab.com
- GitLab handle: johnjeremiah
- Slack handle: JohnJ

[**Mahesh Kumar**](https://about.gitlab.com/company/team/#mskumar)

- Title: Manager, Competitive Intelligence
- Email: mkumar@gitlab.com
- GitLab handle: mskumar
- Slack handle: mahesh

[**Dan Gordon**](https://about.gitlab.com/company/team/#dangordon)

- Title: Manager, Technical Marketing
- Email: dgordon@gitlab.com
- GitLab handle: dangordon
- Slack handle: Dan Gordon