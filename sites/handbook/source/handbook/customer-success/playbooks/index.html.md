---
layout: handbook-page-toc
title: "Customer Success Playbooks"
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [Customer Success homepage](/handbook/customer-success/) for additional Customer Success handbook content.

---


Customer Success playbooks assist in selling, driving adoption, and ultimately delivering more value to customers via GitLab's DevOps platform. The below playbooks will include original content as well as links to other parts of the handbook.

## Structure of Playbooks

**Procedure**: Each Playbook contains the outline of steps to position, apply discovery questions, lead value discussions, and drive adoption (TAM only). The will be driven and tracked [via Gainsight](/handbook/customer-success/tam/gainsight/).

**Positioning**: Stage and use case value proposition and positioning with supporting collateral linked within Gainsight CTAs and Playbooks.

- Market requirements, personas, and differentiators
- Top features, stages, and categories
- Competitive assessments
- Customer slides
- Proof points, blogs, and customer success stories

**Discovery**: Discussion tools (e.g., discovery questions) using Command Plan approach.

**Adoption**: Adoption-related reference materials to accelerate adoption.

- Adoption map, noting recommend recommended features / use cases and sequence of adoption (where applicable)
- Product Analytics attributes to track adoption
- Product documentation (Content owner: Product and Engineering Teams)
- Enablement and training assets
- Paid services

## Catalog of Playbooks

The following playbooks are aligned to our [customer adoption journey](/handbook/customer-success/vision/#high-level-visual-of-gitlab-adoption-journey) and support adoption of the related customer capability and [GitLab stage](/handbook/product/categories/). Within Gainsight, the TAM can manually add any Playbook ([instructions](/handbook/customer-success/tam/gainsight/#ctas)) to a Customer.

| Title | Live in GS? | Automated? | Gainsight Location | Type | Internal Reference Link | 
| ------ | ------ | ------ | ------ | ------ | ------ |
| Stage Adoption: Manage    | - | Manual | Success Plan | Stage Adoption | |
| Stage Adoption: Plan      | - | Manual | Success Plan | Stage Adoption | |
| Stage Adoption: Create    | **Yes** | Manual | Success Plan | Stage Adoption | [Source Code Management (SCM) / Create Stage](/handbook/marketing/product-marketing/usecase-gtm/version-control-collaboration/) |
| Stage Adoption: Verify    | - | Manual | Success Plan | Stage Adoption | [Continous Integration / Verify](/handbook/marketing/product-marketing/usecase-gtm/ci/) and [TAM CI Workshop](/handbook/customer-success/playbooks/ci-verify.html)|
| Stage Adoption: Package   | - | Manual | Success Plan | Stage Adoption | |
| Stage Adoption: Secure    | - | Manual | Success Plan | Stage Adoption | [DevSecOps / Security / Secure](/handbook/marketing/product-marketing/usecase-gtm/devsecops/) |
| Stage Adoption: Release   | - | Manual | Success Plan | Stage Adoption | [Continuous Delivery / Release](/handbook/customer-success/playbooks/cd-release.html) |
| Stage Adoption: Configure | - | Manual | Success Plan | Stage Adoption | |
| Stage Adoption: Monitor   | - | Manual | Success Plan | Stage Adoption | |
| Stage Adoption: Protect    | - | Manual | Success Plan | Stage Adoption | |
| Account Triage | **Yes** | Automated | CTA | Risk |      |
| Low License Utilization | **Yes** | Manual | CTA | Risk |      |
| Product Risk | **Yes** | Manual | CTA | Risk |      |
| Create Success Plan | **Yes** | Automated | CTA | Lifecycle |      |
| Executive Business Reviews | **Yes** | Automated | CTA | Lifecycle | [EBR in a Box](https://drive.google.com/open?id=1wQp59jG8uw_UtdNV5vXQjlfC9g5sRD5K)     |
| New Customer Onboarding | **Yes** | Automated | CTA | Lifecycle |      |
| Usage Ping Enablement | **Yes** | Manual | CTA | Lifecycle | [Usage Ping FAQ](/handbook/customer-success/tam/usage-ping-faq/) |
| Upcoming Renewal | **Yes** | Automated | CTA | Renewal |      |
| Prometheus & Grafana | Coming Soon | Manual | Not Available | Not Available |  [Internal link - existing Playbook](https://drive.google.com/open?id=1pEu4FxYE8gPAMKGaTDOtdMMfoEKjsfBQ)    |
| GitLab Days | Coming Soon | Manual | Not Available | Not Available | [Internal link - existing Playbook](https://drive.google.com/open?id=1LrAW0HI-8SiPzgqCfMCy2mf9XYvkWOKG)     |
| Account Handoff | **Yes** | Manual | CTA | Activity |      |
| New TAM Account Assignment | **Yes** | Automated | CTA | N/A |  |

TAMs create playbooks to provide a prescriptive methodology to help with customer discussions around certain aspects of GitLab. We currently have a [Stage Adoption Guideline](/handbook/customer-success/tam/stage-adoption/) to assist with understanding where a customer stands, and we are working on merging adoption maps into the use case pages.
