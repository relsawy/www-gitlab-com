---
layout: handbook-page-toc
title: "CEO"
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Intro

This page details processes specific to Sid, CEO of GitLab.
The page is intended to be helpful, feel free to deviate from it and update this page if you think it makes sense.
If there are things that might seem pretentious or overbearing please raise them so we can remove or adapt them.
Many items on this page are a guidelines for our [Executive Business Administrators](/job-families/people-ops/executive-business-administrator/) (EBAs).

### CEO Bio

Sid Sijbrandij is the Co-Founder and CEO of GitLab, the single application for the DevOps lifecycle and the world’s largest all-remote company. Sid’s career path has been anything but traditional. He saw the first Ruby code in 2007 and loved it so much that he taught himself how to program. It was during his time as a Ruby programmer that he first encountered GitLab, and quickly discovered his passion for open source.

In 2012, he helped commercialize GitLab, and by 2015, he led the company through Y-Combinator’s Winter 2015 batch. Under his leadership, the company has experienced 50x growth in the last 5 years, expanded from 9 to more than 1,300 remote team members across 65+ countries and regions and is now valued at $2.75 billion.

A champion of the open source community and a pioneer in scaling remote organizations, Sid is altering conventional wisdom on DevOps practice.

### Sijbrandij pronunciation hint

A pronunciation hint for `Sijbrandij`: It’s like when you have seen some distilled wine, and want to point it out: `Sid, see brandy`

## Related pages

- [CEO shadow program](/handbook/ceo/shadow)
- [CEO Job Description](/job-families/chief-executive-officer/)

## Favorite Restaurants

- Favorite places: [Heirloom Cafe](https://heirloom-sf.com/), [Ozumo](http://www.ozumo.com/) (seated in the bar area). [Hakkasan](https://hakkasan.com/locations/hakkasan-san-francisco/), [Mourad](http://mouradsf.com/), [Shizen](https://www.shizensf.com/) (can't make reservations), [Slanted Door](http://www.slanteddoor.com/)
- Favorite drinks place: [83 Proof](http://www.83proof.com/).

## Flaws

Transparency and directness are part of our [values](/handbook/values) and I want to live them by sharing the flaws I know I have.
I'm fully responsible for improving the things below, listing them is no excuse.
They are listed here for two reasons.
The first one is so that people know it is not them but my fault.
The second one is so I can improve, I hope that listing them lets people know I appreciate when people speak up about them.

1. I look serious all the time, it is OK to say 'maybe you can smile more.'
1. I love debating, it is OK to say 'please stop debating and start collaborating' or 'we should have a [dialectic](https://en.wikipedia.org/wiki/Dialectic) instead of a debate.'
1. My English pronunciation, choice of words, and grammar are not great. I'm taking lessons but I welcome corrections when we're having a 1:1 conversation and/or when it might confuse people.
1. When in a rush I will jump to conclusions, it is OK to ask 'can we take more time to discuss this.'
1. I sometimes make reports feel like I'm scolding them, as in being angry for a perceived fault. It is OK to say, I don't mind you making that point but your tone doesn't make me feel respected.
1. In my feedback I sometimes sound more like I'm giving an order instead of offering a suggestion, even when I mean the latter. It is OK to say 'that sounds like an order, I would have appreciated it more in the form of a suggestion.'
1. I sometimes fail to distinguish which of the [three levels of performance](#three-levels-of-performance) I'm talking about. It is OK to ask 'is that a commitment, an aspiration, or a possibility?'.
1. I come across as negative since I focus on what can be improved. It is OK to ask 'what recent improvements are you happy about'?

If you speak up about them I should thank you for it, it is OK to say 'this was on your list of flaws so I kinda expected a thank you'.
I'm sure I have more flaws that affect my professional life.
Feel free to send a merge request to add them or communicate them anonymously to one of our people operations team members so that they can send a merge request.

Not a flaw but something to know about me, I have [strong opinions weakly held](https://blog.codinghorror.com/strong-opinions-weakly-held/). Or as someone said, I come in hot but am open to new evidence.

## Strengths

Sid is easy to talk to on any subject. He is good at drawing people out and challenging them to grow, in a supportive way. He can meet anyone on their level and have a productive conversation. Watch a [quick video](https://www.youtube.com/watch?v=sRVfDNjavZY) from a CEO Shadow recounting her observations.

## Pointers from CEO Direct Reports

1. Sid takes hard feedback well, but he’s difficult to give feedback to because he can be intimidating. Build up your muster and don’t hold back.
1. Sid is worth managing up to. The learning curve he’s on is as steep as it gets, and he does learn/change/adapt readily, so you will see a return from investment.
1. Sid is GitLab's product visionary.
1. He’s the anchor of all-remote.
1. Sid is the source of our [transparency value](/handbook/values/#transparency).
1. Sid is also the driving force for our iteration value. For example, he holds [Iteration Office Hours](/handbook/ceo/#iteration-office-hours).
1. Sid really values 1:1 preparation.
1. Sid believes in “strong opinions, weakly held.” He doesn’t always seem like it, but he will change his mind quickly if you present him with compelling new information and a data driven perspective.
1. Sid loves naming things, and strongly believes in the power of clear language. Learn and use (and add to) our terminology e.g. `It’s not a “best practice”, it’s a “boring solution”`. The [product categories](/handbook/product/categories/) page is a good example. Sid advocates for using [MECEFU terms](/handbook/communication/#mecefu-terms) to keep communication efficient.

## Communication

Thanks to [Mårten Mickos](https://www.linkedin.com/in/martenmickos) for the inspiration for this section. All good ideas are his, all bad ones are mine.

I am a visual person much more than auditory, and I am a top-down person much more than bottom-up. This means that I love written communication: issues, email, Google Docs, and chat. Feel free to send me as many emails and chat messages as you like, and about whatever topics you like.

If you have a great new idea or suggestion for me, I appreciate if you can convey it in a picture or in written words, because I learn by seeing more than I learn by hearing. I don't mind if you send me or point me to plans that are in draft mode or not ready. I am happy if I can give useful feedback early. It doesn’t have to be perfect and polished when presented to me.

In written communication, I appreciate the top-down approach. Set the subject header to something descriptive. Start the email by telling me what the email is about. Only then go into details. Don't mix separate topics in the same email, it is perfectly fine to send two emails at almost the same time. Try to have a concrete proposal so I can just reply with OK if that is possible.

I get many email on which I am only cc'd on, I would very much appreciate if you started emails intended specifically for me with "Sid," or some other salutation that makes it clear that the message is for me.

I have accounts on [LinkedIn](https://www.linkedin.com/in/sijbrandij) and [Facebook](https://www.facebook.com/sytse). I will not send invites to team members on those networks since as the CEO I don't want to impose myself on anyone. But I would love to connect and I will happily accept your LinkedIn and Facebook friend request. You can also find me on Twitter as [@sytses](https://twitter.com/sytses), I won't request to follow private twitter accounts, I assume I'm welcome to follow public twitter accounts, if not please let me know.

### Communicating Follow Ups

Sometimes I will ask to be kept appraised of an action item or follow up. One common way for me to express this desire is by applying the "CEO Interest" label on a GitLab issue. When keeping me appraised please tend toward over-communication. The primary method to communicate your follow up to me should be via MR or issue updates posted in the #ceo slack channel including specific notification when an issue is completed or closed. It might feel like you are being bothersome or distracting, but it is not. If I ever feel like you are truly over-communicating, I will let you know.

### Communicating in Slack

I get a lot of @ mentions in Slack, often when I'm being discussed. Please only @ mention me when you need me to see something or approve something, when you just want to refer to me you can just say Sid. This saves time and enables increased efficiency.

## Please chat me the subject line of emails

I get a lot of email and I'm frequently not on top of it.
I appreciate if you sent me a chat message if I need to respond to something.
Please quote the subject line of the email in your chat message.

## Meeting request requirements

For scheduling a video call or meeting with me or other execs, please see the [EBA handbook page](/handbook/eba/).

## CEO Meeting Cadence

As part of my role, I participate in a variety of meetings both internal and external. Please see below for a general overview of these.

### Daily Meetings

1. [Group Conversation](/handbook/people-group/group-conversations/). 25 minutes.

### Weekly Meetings

1. [1-1s](/handbook/leadership/1-1/) with my direct reports and [EBA](/handbook/eba/). 25-50 minutes.
1. [E-Group](/company/team/structure/#e-group) Call. 50 minutes.
1. Recruiting Syncs on key Executive hires. 25 minutes.
1. Candidate Interviews. 25-80 minutes.
1. PR Interviews. 25-50 minutes.
1. Scaling. 25 minutes.
1. PM & Engineering. 50 minutes.
1. [Take A Break Call](/handbook/communication/#take-a-break-call). 25 minutes.

### Monthly Meetings

1. [Key Review](/handbook/finance/key-meetings/) with Executives and function leaders. 25 minutes.
1. [CEO Group Conversation](/handbook/people-group/group-conversations/). 25 minutes.
1. Informal Board Meeting with GitLab Board Members. 50 minutes.
1. [1-1](/handbook/leadership/1-1/) with CEO Coach. 80 minutes.
1. Industry Analyst Meetings. 25-50 minute meetings.
1. [Coffee Chats](/company/culture/all-remote/informal-communication/#coffee-chats). 25 minutes.
1. [Retrospective](/handbook/engineering/workflow/#retrospective). 25 minutes.
1. [Monthly Release Kick-off](/releases/). 25 minutes.
1. [CEO 101](/culture/gitlab-101/) 50 minutes.
1. [Iteration Office Hours](/handbook/ceo/#iteration-office-hours). 25 minutes.
1. Customer meetings or on-site visits. 25-90 minutes.
1. Executive AMA's (Ask Me Anything) 25 minutes.
1. Investor Relations meetings at the request of the CFO and/or VP, Investor Relations. Range from 25 minutes to 1+ days.

### Quarterly Meetings

1. [E-Group Offsite](/handbook/ceo/offsite/) Monday-Thursday. After QBR's and before the Board of Directors meeting.
1. [GitLab Board of Directors Meeting](/handbook/board-meetings/). 3.5 hours via Zoom. Usually preceded by a dinner or followed by a lunch for those who wish to attend in-person in San Francisco.
1. GitLab Board of Director Committee Meetings => [Audit Committee](/handbook/board-meetings/committees/audit/#audit-committee-charter), [Compensation Committee](/handbook/board-meetings/committees/compensation/#compensation-committee-charter), and [Nominating and Corp Governance](/handbook/board-meetings/#nominating-and-corporate-governance-committee) Meeting. 50 minutes.
1. [Skip Levels](/handbook/leadership/skip-levels/) with direct report's leadership team. 25 min.
1. [1-1](/handbook/leadership/1-1/) with GitLab Board Members. 25-50 minutes.
1. [OKR E-Group Planning](/company/okrs/#schedule) 50-90 minutes.
1. [OKRs How to Achieve Meeting](/company/okrs/#how-to-achieve-presentation). 25 minutes with each function Executive.
1. [Board Member AMA's](/handbook/board-meetings/#quarterly-ama-with-the-board). 25-50 minutes.

### Annual Meetings

1. [Contribute](/company/culture/contribute/). Sunday-Friday.
1. [Annual Planning](/handbook/finance/financial-planning-and-analysis/#annual-plan) with the GitLab Board of Directors. 50-80 minutes.
1. Fiscal Year Kickoff. 50 minutes.
1. Industry Conferences such as: DevOps Enterprise Summit, AWS re:Invent, KubeCon, Linux Summit

## Pick Your Brain interviews

To schedule a Pick Your Brain interview with me, please see the [EBA handbook page](/handbook/eba/ceo-scheduling/#pick-your-brain-meetings). To watch and read prior Pick Your Brain interviews about all-remote, please see the [Interviews page](/company/culture/all-remote/interviews/).

For scheduling a video call or meeting with me or other execs, please see the [EBA handbook page](/handbook/eba/).

## External Speaking Engagements

To schedule Sid to speak at an external engagement please contact the [EBA to the CEO](handbook/eba/#executive-business-administrator-team) with the details outlined in the [Executive External Event Brief](/handbook/eba/#meeting-request-requirements). The EBA to the CEO will gain approval from the function head of the requestor on whether the CEO will attend or not. Sid's preference is to not participate in panels with multiple speakers if there is no facilitation of time. 

### Requests for audio/visual check meetings 
If the organizer of an event requests a prep meeting with Sid to check audio and visual before a remote presentation, we can schedule for 5 minutes before the event for Sid to login and confirm that audio and visual is working as expected. A longer prep meeting is not required as Sid has a robust remote work set up. 

## Sending email

If someone else in the company wants to have me send an email they should email me and cc [my EBA](/handbook/eba/#executive-business-administrator-team) with:

1. Instruction: "Please email this, please bcc: me on the outgoing email, forward any responses, and cc: me on an further emails."
1. Recipient name
1. Recipient email
1. Email subject
1. Email body (text based, no html or rich text)

When receiving such an email my EBA should stage a draft email to the recipient and a draft answer 'done'.

The email should only be the body. Greetings and niceties are handled by the EBA.

## Sales meetings

- I love to talk to users or potential users of GitLab anytime.
- Traveling is not efficient because it can take 2 to 10 times the time of the meeting itself.

Some general guidelines of what travel is appropriate, these guidelines are not fixed, feel free to ask for exceptions:

1. Check my availability with the EBA, reschedule with the EBA, cancel with the EBA, **not me**.
1. I'll take any meeting via video conference or in our boardroom.
1. I'll take a meeting in the Bay Area as long as it is not an SMB organization.
1. I'll take a meeting outside the Bay Area but in the US with large or strategic organizations
1. I'll take a meeting outside the US with strategic organizations.

Consider the following to increase efficiency:

- Combine meetings with multiple organizations in the same location.
- Get meetings with multiple stakeholders in the same company.
- Apart from the formal meetings try to organize a meal with stakeholders.
- Record the meeting so you can distribute it to others in the organization.
- Please check with the EBA to look at my calendar to leverage my existing travel plans.
- Make sure to let the EBA know what you expect from me. E.g. arrive an hour before, do a pitch etc.
- Please plan audio only meetings only when the customer explicitly declines a video call.
- Any meeting that requires travel should be properly prepared and any lessons relayed to the marketing team.

## Conferences

When at conferences I want to achieve results for the company and be efficient with my time.
Please ask sales and/or marketing to set up meetings for me in advance.
I don't mind doing booth duty, presenting, or any other way I can contribute.
I do mind unscheduled time randomly wandering the hallways, I've found this to be ineffective.

Each year I want to attend the Linux Foundation Member Summit (formerly the Open Source Leadership Summit).
Please ensure:

1. We submit at least one talk.
1. We book the on-site hotel on the day it opens up.
1. We let the sales team know we would love to set up meetings and schedule these with my EBA at least a week before the conference.
1. There is same timezone EBA coverage during the conference.

If I am asked to keynote a conference, it is up to the [executive](/company/team/structure/#executives) of the function asking me to attend to decide.
For example, if the request is coming from marketing, the CMO decides; if the request is coming from Finance, the CFO decides.
Please follow the process outlined under [meeting request requirements](/handbook/eba/#meeting-request-requirements) and work with my EBA who will shepherd the decision about whether or not I will attend.

## Recording Content for Conferences

I'm always willing to record [video content](https://www.youtube.com/watch?v=Y310ksxsUoQ) for conferences I'm unable to attend.
Email [my EBA](/handbook/eba/#executive-business-administrator-team) to coordinate the recording.

## Transport

The CEO will pay for all transport expenses (flight, uber, etc.) personally.
By default flying business class.
On short flight with other team members fly economy if we can sit together, in this case still pay personally.

## House

If you are a GitLab team-member, you can stay at our home in Utrecht, the Netherlands for free with up to 5 guests. You can check availability and reserve the home by emailing my personal assistant, Anette at anette@sijbrandij.com. If you want more information on the home, please search in Drive for "Sid/Karen: Description of the NL House". If you cannot locate the document, please ask Anette or [my EBA](/handbook/eba/#executive-business-administrator-team) to send it to you. 
We hope you enjoy your stay!

## Three levels of performance

There are three levels of performance:

1. Commitment: what we promise to our stakeholders.
1. Aspiration: want to get to 70% of this, these are our [OKRs](/company/okrs/)
1. Possibility: what are intrigued by, inspired by, and what I talk about

I'm driven by what is possible, the aspiration, what can be.
Others' expectations of a person affect the person's performance with high expectations leading to better performance, this is called the [Pygmalion effect](https://en.wikipedia.org/wiki/Pygmalion_effect).
What is possible is more than what we are satisfied with or what we promised to our stakeholders.
We can be above what we promised and below what is possible and still have done a good job, we can win without doing everything we aspired to do, or everything that is possible.
It is unlikely that we win without doing what we promised.
I have to be clear in distinguishing these level when I discuss a goal with my reports.

## How do we keep shipping

One of the hardest things in business is not to slow down as the organization grows.
An applicant asked how we manage to do this and these are the factors that come to mind:

1. Don't ever slow down because it is very hard to recover from that. As soon as you stop shipping (for a big refactor, a security initiative, etc.) it is very hard to get back up to the old speed. The organization has accepted a slower rate and there are always enough reasons to go slower. You have to do the refactors and other things during the course of business, never slow down.
1. Everyone in the business wants to do the right thing for the existing users and customers. The problem is that people already using you prefer stability in scope over change in scope. You need to optimize for all people, both the current users and people not using GitLab yet because it is missing features.
1. Separate execution from goal-setting. At GitLab, product decides what to ship and engineering is responsible for shipping it. Both report in to the CEO. If you have a head of product that also runs engineering, they are more likely to slow down because it will make engineers' tasks easier.
1. Separate decision making from giving input. [As detailed in our handbook](/handbook/leadership/#making-decisions) we leave the decision to the person doing the work or their manager, this prevents the need for exponentially more coordination as we grow.
1. We [iterate](/handbook/values/#iteration) so that we keep learning quickly and reduce the risk of decisions.
1. We have [functional teams](/handbook/leadership/#no-matrix-organization) that make us efficient but as mentioned in that text we promote organic cross-functional collaboration by giving people stable natural counterparts.

## Evolution of the handbook

As the company keeps growing my use of the handbook is also changing.

1. Until 20 people I mostly did things myself.
1. From then on I focused on documenting things in the [handbook](/handbook/).
1. Then I asked people to document things.
1. Now we [documented to document it](/handbook/handbook-usage/).

## Iteration Office Hours

These monthly office hours are an opportunity for GitLab team members to discuss how to take a more iterative approach to a specific activity or to highlight how a more iterative approach helped drive results.
Iteration is one of the hardest things to learn about working at GitLab and these office hours are a great opportunity for me to help coach folks who are interested in better understanding it.
We learned iteration at YC, where we took our plan for the next 3 months and compressed it into 2 weeks.
Give yourself a really tight deadline and see what you can do.
The smaller we split things up, the smaller steps we take and the faster we can go.

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/jYYxi_bs1Qg" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/FAuwri0vsts" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## CEO scam

See [CEO and executive fraud](/handbook/security/#ceo--executive-fraud) in the security practices section of the handbook.
