---
layout: handbook-page-toc
title: Covid-19
description: Information on the support GitLab is offering in relation to COVID-19.
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Team Member and Manager Support During COVID

With the continued uncertainty of COVID 19 and the lack of visibility on how things will progress throughout the remainder of 2020 and into 2021 we wanted to offer additional guidance for how we continue to support our team members, managers and the GitLab business. Knowing that there is not a one size solution that fits all team members we are looking at what can we do collectively to ensure that communication, collaboration, engagement, performance and overall team member well-being is being addressed as we navigate these ever changing and challenging times.  We are a family and friends first company however we do also have a business to run so this section is to offer recommendations, suggestions, guidelines and best practices for all team members and managers.

### Manager Guidelines/Best Practices

Managers should always be checking in with their team members to determine how they are doing from a personal perspective not just from a task/role perspective.  If a manager is aware that a team member is struggling to balance work and home priorities they should reach out and schedule time with the team member to understand the concerns and work through potential options to help.  Ask the team member to do a [self evaluation](/handbook/total-rewards/benefits/covid-19/index.html#self-evaluation) of what is possible and explore different proposals for their working schedule.  Once the team member has completed a [self evaluation](/handbook/total-rewards/benefits/covid-19/index.html#self-evaluation) and you have identified options, reach out to your [aligned People Business Partner](/handbook/people-group/#people-business-partner-alignment-to-division) to discuss the best option that works for the team member that also aligns to local labor regulations. Furthermore as a manager it's important to help prioritise work if your team member has reduced availability. As a manager ask yourself: What things are highest priority? What things might be a low priority? What can temporarily be removed of the responsibilities if at reduced capacity? Document the expectations you've set with the team member and iterate as the situation might be changing.  

**If your team member is currently on a performance remediation plan, consult with your PBP on recommendations how to appropriately manage or adjust.**

### Team Member Guidelines/Best Practices

We want to acknowledge that many of our team members are now juggling various roles trying to balance full-time work, full-time parenting, full-time educating, full-time caregiving for an elderly parent, relatives, neighbors, or friends, etc.  These activities have blended into our daily lives and these very challenging times can create additional stress and burden for team members.  

If team members are feeling overwhelmed, stressed, unable to keep a normal work schedule and/or work full-time, they can reference the content below to help facilitate a discussion and develop a plan that works for all involved. 

#### Self-Evaluation

This is important and may be a hard thing for team members to undertake.  However, it is a critical step in the process of identifying and developing what a true working plan will look like to help you navigate the work life balance challenges.  It is ok to take a look at your current situation and identify what is working, what isn't working, what could work better and what would a successful working schedule look like during this time.  Once you have done your self evaluation set up time to discuss with your manager or [People Business Partner](/handbook/people-group/#people-business-partner-alignment-to-division) (PBP) regarding the support needed. Here are some examples, though not an exhaustive list of potential  work schedule alternatives.

- Team member is able to start their day earlier than normal, have a long mid-day break to manage their home situation and continue working until later the day to manage their responsibilities/acheive the same results. 
- Team member is unable to work in the start of their day, but instead starts work later and finishes later to manage their results/responsibilities. 
- Team member is able to shift the days they work due to additional support at home and works for example a flexible schedule of Wednesday-Sunday, Sunday to Thursday or Saturday to Wednesday.
- Team member is unable to work full-time due to home priorities, but is able to commit to a a reduced schedule. *(Note: There may be options based on local laws where a team member can go to a reduce work schedule and pay)*
- Team member is unable to work and would like to request a [leave of absence](/handbook/paid-time-off/#unpaid-leave-of-absence).  Eligibility and details for leaves of absenses will vary based on local laws. 

**Here is a [COVID-19 Impact Self-Evaluation](https://docs.google.com/document/d/10gxjLqW62jZF5ksp9AhEsIoNpm093IlxgfqhzfisvZE/edit#heading=h.5ahg1o4v53q7) template that you can optionally use to help guide your conversations.**

### Communication

Communication and collaboration is key to any relationship, particularly the working relationship.  Set up time with your manager to review your [self evaluation](/handbook/total-rewards/benefits/covid-19/index.html#self-evaluation) and talk through your different proposed schedule and work options.  Once you and your manager have agreed upon a proposed schedule/working agreement, reach out to your aligned [People Business Partner](/handbook/people-group/#people-business-partner-alignment-to-division) to review and come to a finalized working agreement.  Please note we know there is not a one size fits all for every team member.  However, with the right communication and collaboration we can ensure that team members needs are being supported as well as meeting GitLab goals.

We want to make sure that team members are doing the things necessary to take care of themselves. Please review the [/handbook/total-rewards/benefits/covid-19/#resources-for-covid-19](COVID-19 resources) content in the handbook and reach out to the [Employee Assistance Program: Modern Health](/handbook/total-rewards/benefits/modern-health/) as you see fit. We also want to encourage team members to continue to utilize our [PTO policy](/handbook/paid-time-off/) and take time off as needed.  We recognize that most team member's travel and vacation plans have altered dramatically during COVID, but a ["staycation"](https://www.merriam-webster.com/dictionary/staycation) and logging off from work is still strongly encouraged. Just work with your manager to make sure the appropriate coverage is available.


## COVID-19 Medical Leave Policy

GitLab has long believed in ["family and friends first”](/handbook/values/#family-and-friends-first-work-second) and we realize that our team members may require some additional time away from work in the event they or a dependent family member are directly affected by COVID-19. In order to best support our team members who find themselves or family member(s) directly affected by COVID-19, GitLab has created this COVID-19 Medical Leave Policy to allow for leave time explicitly and guaranteed.

GitLab team members directly affected by COVID-19 who need to take leave for one of the two reasons set forth below are eligible to take up to 12 weeks of leave with full pay after utilizing our Paid Time Off policy. In the event that the sick leave provided under your local law is more generous than this GitLab COVID-19 Policy, your local law will supersede this policy.
1. The team member has received a confirmed medical diagnosis of COVID-19 from a licensed healthcare provider.
1. The team member is caring for a [dependent family member](/handbook/total-rewards/benefits/covid-19/#faq-for-covid-19-medical-leave-policy) who has received a confirmed medical diagnosis of COVID-19 from a licensed healthcare provider.

You do not have to physically see your physician for this confirmation. If telehealth services are available, we highly encourage you to utilize those in line with your community’s regulations around physical distancing, quarantine and self isolation.

The ability to request COVID-19 Leave will run through October 31, 2020. That means, any requests for COVID-19 Leave must be made prior to October 31, 2020, however the leave itself may exceed that timeframe. By way of an example, if a team member requests COVID-19 Leave on October 31, 2020 for 4 weeks, the end date for their COVID-19 Leave would be November 28, 2020.

As the situation around COVID-19 continues to evolve, GitLab will continue to revisit this policy before the 25th of the month and may adjust it accordingly.

### How to apply for COVID-19 Leave

We encourage team members to only take the time they need to recover in accordance with the recommendation of your licensed healthcare provider. The maximum amount of time posted under this COVID-19 Leave Policy is 12 weeks, but medically, you may need less.

For some, our [Paid Time Off Policy](/handbook/paid-time-off/) of 25 consecutive calendar days will be suitable. Please keep your manager informed of any upcoming Paid Time Off. You are in no way obligated to release a personal medical diagnosis of COVID-19 to your manager.

If you meet the criteria set forth above and find that you need additional time past the 25 consecutive calendar days allowed under our Paid Time Off Policy, please follow these steps to apply for COVID-19 Leave:
* On or before day 20 of your Paid Time Off,  please email total-rewards@gitlab.com with the following to request COVID-19 Leave:
  * Start date of your Paid Time Off
  * Start date of your COVID-19 Leave (this will be the 26th day after your Paid Time Off began)
  * Anticipated end date of your COVID-19 Leave
  * A note from a licensed healthcare professional confirming the leave for yourself or a dependent family member which you will be caring for and the start and end dates of your COVID-19 Leave (which should be consistent with the anticipated dates you are providing within your request). The Total Rewards team will not ask for test results.
* The Total Rewards team will review the documents provided and communicate directly with the team member regarding any additional information needed (if necessary) and the result.
* If approved, the Total Rewards team will notify the PBP of the group and manager of the start and end dates of the COVID-19 Leave. The Total Rewards team will not specify to your PBP or manager the reason for the leave.
* The Total Rewards team will notify payroll of the leave dates.
* Stock Vesting, bonus payments, and benefits will not be affected by this leave during the 12 week time period.
* The Total Rewards team will follow up with the team member on a weekly basis to see how the team member is doing. If needed and in accordance with applicable law, the Total Rewards team will request additional documentation around inability to work due to COVID-19.
* The Total Rewards team will confidentially file all ongoing documentation of inability to work through the weekly check ins.

### FAQ for COVID-19 Medical Leave Policy

* Does this leave cover leave to care for children under the age of 18 who are home because their school or childcare is unavailable due to COVID-19?
  * No. We are reviewing the various legislation being passed in different jurisdictions and determining whether other forms of leave may be necessary to accommodate.
* Can the leave be intermittent (e.g., 4 hours/day or 3 days/week)
  * No. If, as this situation evolves and it is determined that recovery may allow work on an intermittent basis, the Total Rewards team will review and consider adjusting the policy as needed.
* Will this policy apply across all of GitLab or just U.S.?  
  * This policy is intended to apply globally.  Local laws or internal policies governing local jurisdictions that provide broader protections for team members will supersede this policy.
* How does this leave interact with other leave policies provided by GitLab?
  * As previously noted, local leave laws or policies will supersede this policy.  Additionally, the Military Leave policy will govern military specific leave needs.
* How does this policy work in conjunction with my 25 days of unlimited PTO?
  * This policy grants up to twelve weeks additional leave after you have used your initial 25 days PTO.  For many people, you will not need to use all of the 12 weeks to recover.
* What if I can’t get tested for COVID-19?
  * The Total Rewards team needs a note from your health care provider certifying that you cannot work because you or a dependent family member you care for has been diagnosed (either via test or presumptively) with COVID-19.  The Total Rewards team does not require you to provide testing results. You do not have to physically see your physician for this confirmation. If telehealth services are available, we highly encourage you to utilize those in line with your community’s regulations around physical distancing and self isolation.
* Will anyone at GitLab know that I am sick? How will my privacy be respected?
  * The Total Rewards team will maintain your privacy and keep any documentation private (it will not be shared with your manager or anyone on your team) and only shared within the Total Rewards team as needed to process your leave. Your manager and your team will have to be informed that you are on leave as you will not be available for work as normal, but will not provide the reason for the leave.
* What is a “dependent family member?”
  * A dependent family member is someone who you are directly responsible for as a caregiver. Examples can be: a spouse or significant other, dependent child; parent, sibling, etc
* What if I am on a commission plan?
  * If applicable, commissions are paid according to your applicable commission plan while on COVID-19 related leave based on the prior six months of performance with a cap at 100% of plan. On the day you return from leave and going forward, your commissions will be based on current performance only.
* What is the last day to apply for COVID-19 Leave?
  * The ability to request COVID-19 Leave will run through October 31, 2020. That means, any requests for COVID-19 Leave must be made prior to October 31, 2020, however the leave itself may exceed that timeframe. By way of an example, if a team member requests COVID-19 Leave on October 31, 2020 for 4 weeks, the end date for their COVID-19 Leave would be November 28, 2020. As the situation around COVID-19 continues to evolve, GitLab will continue to revisit this policy before the 25th of the month and may adjust it accordingly.

## Resources for COVID-19

GitLab is committed to the physical and mental wellbeing of all of our team members and wider community during the COVID-19 outbreak. We have put together this list to assist people in finding helpful resources when confronting different challenges during this time. This list is not exhaustive and contributions to it are encouraged. If you are experiencing difficulty breathing or any other life-threatening symptoms, please contact your country's emergency response system immediately.

### Simple Habit Free Trial

[Simple Habit](https://simplehabit.com/), a meditation and mental-wellness company, has offered all GitLab team members a free premium subscription through the month of April.

With this subscription, you will have unlimited access to Simple Habit's top wellness professionals, guided meditations, therapy, coaching, motivational talks and much more.

The code can be found in the following [shared doc](https://docs.google.com/document/d/1utRTqY0TkOK9He5zB3Iiia2ZQng5qoM9JzyBrV09P7Q/edit).

### Modern Health Webinars

Modern Health is hosting a series of free Community Support Sessions designed to equip you with meaningful strategies to overcome stress or anxiety in various areas of your life caused by the COVID-19, such as managing isolation and loneliness, maintaining a healthy lifestyle, financial confidence, work-from-home wellbeing, and practicing mindfulness.

In these sessions, coaches and therapists from our network will provide some advice on how to best approach these important areas of interest and then open up the floor to answer questions that you or the rest of the Modern Health community may have.

A full list of webinars taking place for the week and links to register can be found directly on [Modern Health's website](https://community.modernhealth.com/#For-Everyone).

For recordings of Sessions that have already taken place, please see [Modern Health's YouTube Channel](https://www.youtube.com/playlist?list=PLZoFVlbgk0-Nk-K8prckRd0vgJqT0ecmi).

### Sanvello COVID-19 Trial

Sanvello, an app dedicated to managing and treating stress, anxiety, and depression, has a trial during COVID-19 to July 15. If you would like to join this free trial, please sign up for Sanvello directly. We will be sending out a survey later in the year to evaluate usage of this by the GitLab community. 

Instructions for downloading trial:
1. Navigate to your phone's app store and search for "Sanvello" or navigate to the [online app](https://www.sanvello.com/app#/app/login). 
1. Register for an account (you do not need to provide a GitLab email to access this service). 
1. That's all you need to do to access the trial of Sanvello to July 15. 

#### Sanvello Premium Accesss through UHC

For more information on how to access Sanvello Premium if enrolled in a UHC medical plan (US team members only), please see the [UHC Premium Access](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#uhc-premium-access-to-sanvello) section of the US benefits page in the handbook. 

### Global Resources

* Our [Employee Assistance Program](/handbook/total-rewards/benefits/general-and-entity-benefits/#employee-assistance-program), Modern Health, is prepared to help with personalized coaching and therapy. They have a library of resources to help manage stress, cope with anxiety, and improve your mental well-being.

### United States

* Our medical plans through UHC and Kaiser both have a variety of resources to help their members:
   * Kaiser:
     * For [video visits](https://healthy.kaiserpermanente.org/), please sign into your account.
     * 24/7 Advice Line: 866-454-8855
     * Mail order prescriptions can be requested via the online portal or by calling the number at the top of your prescription label.
     * Emotional Support
       * [Wellness Resources](kp.org/selfcare).
       * [MyStrength](kp.org/selfcareapps) is an app for Kaiser members who may be experiencing anxiety or emotional distress and is available at no additional cost. This app provides a personalized, cognitive behavioral therapy-based program that includes interactive activities, in-the-moment coping tools, inspirational resources, and community support.
     * [Treatment Costs](https://healthy.kaiserpermanente.org/northern-california/health-wellness/coronavirus-information)
       * $0 out-of-pocket costs for COVID-19 screening or testing if referred by a Kaiser Permanente doctor.
       * If you test positive with COVID-19, out-of-pocket treatment costs will be waived through May 31, 2020.
     * For additional information: [Kaiser Coronavirus Resource Center](https://healthy.kaiserpermanente.org/northern-california/health-wellness/coronavirus-information).
   * UHC:
     * For [telehealth](https://www.myuhc.com/member/jsp/preMain.jsp), please sign into your account.
     * Mail order prescription delivery is available through Optum Home Delivers. Sign up through the UHC online portal.
       * Receive up to a 90-day supply of covered maintenance drugs (prescribed to treat chronic or long-term medical conditions, such as asthma, diabetes, high blood pressure).
     * [Treatment Costs](https://www.uhc.com/health-and-wellness/health-topics/covid-19).
       * $0 out-of-pocket costs (copay, coinsurance, deductibles) for COVID-19 testing and telehealth services, doctors visits for screenings, ER or hospital visits for screenings, and urgent care for screenings.
         * UHC has waived costs for COVID-19 testing and use of telehealth services through June 18, 2020.
         * UHC will also cover FDA-authorized COVID-19 antibody tests ordered by a physician or appropriately licensed health care professional without cost sharing (copayment, co-insurance, or deductible).
         * Waiving of COVID-19 testing and telehealth costs *includes* the HSA-eligible plan--even before you meet your deductible (essentially, it’s being treated as preventive care), per [IRS Notice 2020-15](https://www.irs.gov/newsroom/irs-high-deductible-health-plans-can-cover-coronavirus-costs). 
         * For more information on testing as it relates to your UHC coverage, please reference their [FAQ](https://www.uhc.com/content/dam/uhcdotcom/en/B2B-Newsletters/b2b-pdf/covid-19/faqs-testing.pdf).
       * If you test positive for COVID-19, your care will be covered and your standard plan benefits will kick-in. You will be responsible for any out-of-pocket costs related to your treatment.
     * For additional information: [UHC Coronavirus Resource Center](https://www.uhc.com/health-and-wellness/health-topics/covid-19).
* FirstMark and Ro are offering [free COVID-19 telehealth assessments](https://covid.ro.co/firstmark/).
* Emergency Dental Care - Cigna
   * Many dental offices are limiting appointments for emergency needs only or have closed due to state and local mandates. In the event of a dental emergency (severe pain, acute infection, swelling, and/or persistent bleeding), here’s how to get help:
     * First, contact your dentist to determine their care options.
     * If the dental office is closed, Cigna can help you find care. Call 1-800-244-6224 or go to [mycigna.com](mycigna.com).
     * View additional details from [Cigna](http://images.connecting.cigna.com/Web/CIGNACorporation/%7B639647dd-3a01-4125-b82d-c7ab21d09604%7D_Receiving_Emergency_Dental_Care_-_FOR_PDF.PDF).
* All US Team Members are automatically enrolled in [UHC's Employee Assistance Program](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#employee-assistance-program) by being enrolled in the Life/Disability insurance. This program offers help for difficult times including counseling services.

### United Kingdom

#### England

* The NHS provides an [online COVID-19 symptom checker](https://111.nhs.uk/covid-19/) which can give advice on self-isolation as well as providing links to booking COVID-19 testing.
* Residents in England registered with an NHS GP can book an appointment via the [NHS App](https://www.nhs.uk/using-the-nhs/nhs-services/the-nhs-app/).

#### Scotland

* Official advice for Scotland can be found on the [Scottish Government website](https://www.nhsinform.scot/illnesses-and-conditions/infections-and-poisoning/coronavirus-covid-19). This includes a symptom checker, advice on self-isolation and how to book a test. 

#### Wales

* Official advice for Wales can be found on the [Welsh Government website](https://gov.wales/coronavirus).

#### Northern Ireland
* Official advice for Northern Ireland can be found on the [nidirect website](https://www.nidirect.gov.uk/campaigns/coronavirus-covid-19).


### Canada

* Ontario: Recommends to take their [self-assessment](https://www.ontario.ca/page/2019-novel-coronavirus-covid-19-self-assessment) and if you answer yes to the questions, contact your primary care provider or Telehealth Ontario (1-866-797-0000) to speak with a registered Nurse.
* Quebec: Recommends to call 1-877-644-4545 if you have a cough or fever. [For more information](https://www.quebec.ca/en/health/health-issues/a-z/2019-coronavirus/#c46341).
* Manitoba: Recommends to call 204-788-8200 or 1-888-315-9257 if you have symptoms of COVID-19. [For more information](https://www.gov.mb.ca/covid19/).
* Saskatchewan: Recommends to take their [self-assessment](https://www.saskatchewan.ca/government/health-care-administration-and-provider-resources/treatment-procedures-and-guidelines/emerging-public-health-issues/2019-novel-coronavirus/covid-19-self-assessment) and contact HealthLine 811 if you have symptoms and have travelled outside Canada within the last 14 days. [For more information](https://www.saskatchewan.ca/coronavirus#utm_campaign=q2_2015&utm_medium=short&utm_source=%2Fcoronavirus).
* Alberta: Recommends to take their [self-assessment](https://myhealth.alberta.ca/Journey/COVID-19/Pages/COVID-Self-Assessment.aspx) and contact Health Link 811 if you have symptoms or have travelled outside Canada. [For more information](https://www.alberta.ca/coronavirus-info-for-albertans.aspx).
* British Columbia: Recommends to contact your healthcare provider or HealthLinkBC (811) if you have symptoms and have been in contact with someone who has tested positive.
* All Canada team members are enrolled in the [Employee and Family Assistance Program](/handbook/total-rewards/benefits/general-and-entity-benefits/canada-corp-benefits/#employee-and-family-assistance-program). This plan provides support for difficult times including counselling services. For more information, please consult the [brochure](https://drive.google.com/open?id=1d30c7T8dUa48ZR58H9-R84VRG2pChEGf).
* All Canada team members who are enrolled in the Canada Life benefit plan have access to [Akira Virtual Care](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/canada-corp-benefits/#akira---virtual-care). 

### Netherlands

* For information on COVID-19, there is a national information line: 0800-1351.
* If you believe you have COVID-19, it is recommended to call your family doctor especially if you have an elevated temperature or have been to a high-risk area. [For more information](https://www.rivm.nl/en/novel-coronavirus-covid-19).

### Germany

* If you suspect you have COVID-19, it is recommended to contact your [local health office](https://tools.rki.de/PLZTool/de-DE), contact your primary doctor, or contact the non-urgent medical assistance line: 116117.

### Australia

* For information on COVID-19, there is a Coronavirus Helpline: 1800 020 080.
* If you believe you may have COVID-19, you can use the [online symptom checker](https://www.healthdirect.gov.au/symptom-checker/tool/basic-details). It is recommended that you call your doctor in advance before going to their clinic if you think you may have COVID-19.

### Aotearoa New Zealand

* For COVID-19 health advice and information, contact the Healthline team (for free) on 0800 358 5453 or +64 9 358 5453 for international SIMS.
* Please see the [Ministry of Health information page](https://www.health.govt.nz/our-work/diseases-and-conditions/covid-19-novel-coronavirus) for updated information.

### India

* If you have symptoms of COVID-19 and have travelled to an affected country, you can call the Ministry of Health & Family Welfare, Government of India's helpline: 011-2397 8046. If you qualify for testing, they will direct you to a [government-approved lab](https://www.icmr.nic.in/). [For more information](https://www.mohfw.gov.in/).
* Central Helpline Number for corona-virus: +91-11-23978046
* [Helpline Numbers of States & Union Territories (UTs)](https://www.mohfw.gov.in/pdf/coronvavirushelplinenumber.pdf)

### Ireland

* For general information, you can contact HSELive: 1850 24 1850. If you believe you have COVID-19, it is recommended to call your General Practitioner.
* Up-to-date information and detailed guidance is available on the [HSE's Coronavirus portal](https://www2.hse.ie/coronavirus/) (also available in [Gaeilge](https://www2.hse.ie/gaeilge/coroinvireas/)).

### South Africa

* The Department of International Relations and Cooperation (DIRCO) has set up a 24-hour hotline number for all South African citizens that believe they might have COVID-19, the hotline number is 0800-029-999.

### Brazil

* For information on COVID-19 the national information helpline phone number is 136.
* The Ministry of Health provides up-to-date [Coronavirus information](https://coronavirus.saude.gov.br/) online and apps for [Android](https://play.google.com/store/apps/details?id=br.gov.datasus.guardioes) and [iOS](https://apps.apple.com/br/app/coronav%C3%ADrus-sus/id1408008382).

### Travel Guidance 
* For the company's current travel guidance during this time, please refer to the [travel handbook page](https://about.gitlab.com/handbook/travel/#travel-guidance-covid-19).
