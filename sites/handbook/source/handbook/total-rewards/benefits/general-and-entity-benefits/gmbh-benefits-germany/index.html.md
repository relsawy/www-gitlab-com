---
layout: handbook-page-toc
title: "GitLab GmbH (Germany) benefits"
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-operations).

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

## Specific to Employees Based in Germany

Currently, GitLab does not provide additional benefits over and above the mandatory state requirements. [General GitLab benefits](/handbook/total-rewards/benefits/general-and-entity-benefits/#general-benefits) still apply. As part of the [guiding principles](/handbook/total-rewards/benefits/#guiding-principles) this will be reviewed.

## German Social Security System

GitLab and team members contributions to pension, health insurance, care in old age insurance and unemployment insurance are mandatory, as required by the state system. The payments are calculated each month by payroll and are shown on the employee pay-slips.

Further information can also be found on the [Germany Trade & Invest Website](https://www.gtai.de/gtai-en/invest/investment-guide/employees-and-social-security/the-german-social-security-system-65600).

## Life Insurance

GitLab does not plan to offer Life Insurance Benefits because team members can access employer insurance and government pension schemes to help with payments in the event of a death of a family member.

## GitLab GmbH Germany Leave Policy

To initiate your Parental Leave, submit the dates in PTO Ninja under the Parental Leave category. This will prompt Total Rewards to process your leave. You can find out more information about our Parental Leave policy [here](https://about.gitlab.com/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave). 

**Maternity Protection**
Maternity protection is explained in the Maternity Protection Act (Mutterschutzgesetz). Generally, 6 weeks before the estimated date of birth and 8 weeks after the date of birth the pregnant employee is released from work. This period can differ in case of special circumstances (twins, preterm birth). The employee will receive full payment of her salaries from the employer. The employer receives a refund of these amounts through an application process with the state.

**Parental Leave**
An employee is entitled to parental leave until the child turns three. The employee is not obliged to work during this period and no payment is due by the employer. Parental leave can be taken by the mother and father, individually or jointly.

Parental allowance is a state benefit for parents who would like to look after the child themselves after the birth, and therefore are not in full-time work or are not working at all. If mothers and fathers share the parental allowance, they receive parental allowance for a maximum of 14 months. Each parent can draw a minimum of two and a maximum of twelve months' parental allowance. Lone parents are entitled to the full 14 months' parental allowance.


## Sick Time During COVID-19

During the COVID-19 Pandemic, per German labor law, team members are required to present a doctor's certificate if they need to take more than 3+ consecutive sick days. We encourage all team members to meet with a _virtual doctor_ for the certificate to avoid the need to leave home.
