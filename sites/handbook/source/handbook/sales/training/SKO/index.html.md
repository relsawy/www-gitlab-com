---
layout: handbook-page-toc
title: "Sales Kick Off"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# SKO Overview
This event sets the tone for the GitLab field organization for the new fiscal year with a focus on the following goals:
1.  **Motivate**: Be energized about GitLab’s vision & strategy and the incredible opportunities ahead (for each and every GitLab team member, the company, our customers & partners)
1.  **Celebrate**: Recognize and enjoy the successes of the prior fiscal year
1.  **Enable**: Ensure every field team member returns home with a clear understanding of what’s needed to plan and execute their business for the new fiscal year (based on functional enablement priorities from senior leadership) 

# Sales Kick Off 2021
SKO 2021 is scheduled to take place virtually during the week of Feb 8-12, 2021. More details coming soon!

## High-level SKO 2021 Agenda 
Virtual SKO 2021 will consist of half-day sessions each morning (PT) during the week of Feb 8-12, 2021. Below is a high-level agenda: 

*  Pre work: Embrace remote → learn about product strategy/roadmap prior to SKO. Complete first, segment-specific Field Certification course.
*  Mon, 2021-02-08: Keynotes, Q&As
*  Tue, 2021-02-09: Keynotes, Q&As
*  Wed, 2021-02-10: Awards ceremony, role-based breakout sessions, Partner Summit
*  Thu, 2021-02-11: Role-based breakout sessions
*  Fri, 2021-02-12: Role-based breakout sessions, regional team building
All subject to change. 

## SKO 2021 Tickets 
You must book a ticket for this event. This is how we keep track of who is attending and all other details. Please try to register before Dec 15. You can edit your pass after registering up until Jan 31. At this point, registration changes will be locked for planning purposes.

We will be validating all registrations on the back end, and any unapproved registrants will be canceled. Please do not book if you are not approved to attend this event.

**Ticket Types:**
- Sales - for all in the Sales org
- Other - for team members outside of the Sales org
- Guest - for guest speakers

## SKO 2021 FAQ 
**Q: What are the dates and location?**

A: Sales Kick Off (SKO) will take place virtually from 08-Feb through 12-Feb, 2021. 

**Q: Why are we having a virtual event versus an in-person SKO?**

A: The health and safety of our team is our top priority and a remote event allows us to keep planning with confidence while there is still uncertaintly about when and how to host large in-person events.

**Q: Will we still have an in-person Sales event in 2021?** 

A: We hope to! Sales leadership is looking into opportunities to get the team together in person either by region or with the entire team in 2H FY22. This is contingent on the status of the COVID-19 pandemic, and we will make all decisions with the health and safety of our team members as our top priority.

**Q: What platform(s) are we using for the virtual event?**

A: Please stay tuned for more information on our virtual event platform! 

**Q: When and how should I register?**

A: The SKO core team will reach out via email with registration links and update this page with registration details once they are available.  

**Q: How are we accommodating different timezones?**

A: We will have live and interactive sessions for all timezones. All sessions will be recorded and available for team members on-demand after the event.

**Q: Will there be any pre-work?**

A: In support of GitLab’s all-remote culture, there will be pre-work that all SKO participants will be asked to complete to make the best use of our time together in person. Details will be shared in early Q4. 

**Q: What will I be expected to participate in?**

A: SKO is a great opportunity for our team to come together to both celebrate our results and collaborate for a strong start in the new year. As such, the agenda is purposely crafted with numerous activities to learn, network and celebrate together. We expect that every SKO to treat it as a “work trip” and block your calendar to ensure you fully participate in every part of the event.

**Q: I just got hired – how do I attend SKO?**

A: All new hires after Oct 21 will have 2 weeks from their start date to register and book travel. This info will be included in all new hire welcome packets if they are expected to attend.

**Q: How will we make SKO 2021 exciting even though it's virtual?**

A: The SKO core team is preparing an engaging virtual event. Everything from the virtual platform to the agenda to the event theme is being carefully curated to ensure that our team feels connected and energized while participating in SKO. 

**Q: Will we still have an Awards Dinner and networking time?**

A: Rather than an Awards Dinner, we will host an Awards Ceremony on Wednesday. The SKO core team is planning fun and creative ways to ensure 2021 Sales Awards winners are celebrated. We have also built networking time into the agenda each day to give team members time to connect across the organization and with partners.

**Q: How will we include partners in SKO 2021?**

A: We will host a Partner Summit in tandem with SKO. Partners will be invited to participate in our Day 1 keynotes on Tuesday, and they will have their own Partner Summit-specific sessions on Wednesday. Partners will be available to virtually meet and greet with GitLab team members in the virtual Partner Booths each day of SKO after the half-day keynotes or breakout sessions have concluded.

**Q: Where can I ask questions related to SKO?**

A: If your question is not covered in the FAQ, please feel free to post in the [feedback issue](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/-/issues/535). Alternatively, you can visit the public slack channel #sko-virtual-2021 and ask your question there. If your question is not-public, you can email sales-events at gitlab dot com with questions. Members of the core SKO planning team, including Emily Kyle, David Somers and Monica Jacob are actively monitoring this email and will respond to your inquiry within 24 hours. 

We are actively planning this event and will update this FAQ as more information becomes available. 

Reference: [Company Code of Conduct](https://about.gitlab.com/community/contribute/code-of-conduct/)

----

# Sales Kick Off 2020
In February 2020, the entire sales organization and their supporting teams gathered in Vancouver, BC Canada for the inaugural GitLab Sales Kick Off. This page includes slide decks, videos, and pictures from the event.

## SKO 2020 Theme
**Level Up** - It works across nationalities / languages and has lots of ways we can tie it into the content. All about leveling up the business.

## SKO 2020 Day 1 Sessions

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| Welcome & FY21 Overall GitLab Strategy & Vision | [slides](https://drive.google.com/open?id=1mvqXfa3vIQec8mhBSzVB5gZDC7LJwdMhjHev0nkVj9g) (GitLab internal) | [video](https://youtu.be/kc3H0gMC7u4) (GitLab internal) |
| Product Strategy & Vision with Q&A | [slides](https://drive.google.com/open?id=1DfsjznCwqBz9MpueftriC1QS81QHuXcdAACqtdcFPU0) (public) | [video](https://youtu.be/fS5T-XOvXPk) (public) |
| FY21 Sales & Marketing Strategy & Vision with Q&A | [slides](https://drive.google.com/open?id=1bq_bxM07PTNF3rfAbFB_Fia7U8CszfLTEJUrF-bfilI) (GitLab internal) | [video](https://youtu.be/8vrp1AGRp1U) (GitLab internal) |
| CRO Staff Level Up Panel Discussion | no slides | [video](https://youtu.be/EYlC9uP3LoE) (GitLab internal) |
| Leveling Up with Partners at GitLab | [slides](https://drive.google.com/open?id=12j219pElrox1hUyMpiOwSSrTV6S6biLurahKuLjvH1Y) (GitLab internal) | [video](https://youtu.be/jMUzgPIfFXg) (GitLab internal) |
| Keys to Winning panel discussion | [slides](https://drive.google.com/open?id=1V1lDVIJyX1mMin2Hm7AaExps0WAgczi6vAB6UWFuR8c) (GitLab internal) | [video](https://youtu.be/ervvabavL2o) (GitLab internal) |
| Sales Kick Off Awards Ceremony | [slides](https://drive.google.com/open?id=1deR4D2GplTGan1E2ENZbhZ0rt4YiUzZEJGX7RLj83yY) (GitLab internal) | [video](https://drive.google.com/open?id=1_lbLGvYhhB6ynyaWngeuE6nkDUcUXv1A) (GitLab internal) |


## SKO 2020 Day 2 Sessions

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| Day 2 Welcome | no slides | [video](https://youtu.be/bI1XBeZajIs) (GitLab internal) |
| Articulating and Selling the GitLab Vision & Customer Journey | [slides](https://drive.google.com/open?id=14kO1iTqSwuvV-7CDHThslUv8_QP_EnaZg4yG-1romZc) (GitLab internal) | [video](https://youtu.be/DtL38mgpycE) (GitLab internal) |
| Getting Into Accounts That Say They Don’t Have a Problem | [slides](https://drive.google.com/open?id=1dRdKs7BkbyTzTfFZ4JwwjJUIwgTSSOjkG-DEGDGgN30) (public) | N/A (not recorded) |
| Proactively Competing Against Microsoft | [slides](https://drive.google.com/open?id=1WUdzaNn-tgm3Pa1W3JQyGjcR9JOR2a0rLUTA7zRNIt8) (GitLab internal) | [video](https://youtu.be/Ds_U8iiUOz8) (GitLab internal) |
| Customer Expectations and Storytelling | [slides](https://drive.google.com/open?id=1hnYT7ulTPpb7C3V_fKdOU9gajAA1oufzp9EiDYQC-mU) (public) | N/A (not recorded) |
| Finding and Defining the Customer’s Problem | [slides](https://drive.google.com/open?id=18NynGuJTEwSUnKkRR4A1fQa2xojPxpO3mlnvb3BmMXY) (public) | [video](https://youtu.be/_RSqUgYDjdQ) (GitLab internal) |
| One Lab, One Story: Leveling Up Our Demo Labs | [slides](https://drive.google.com/open?id=1x125pYEjAQQX5vDo5u2eanAoVtoz5R2muiaYqXL_xEE) (public) | N/A (not recorded) |
| Customer Success Plan Workshop | [slides](https://drive.google.com/open?id=15Qt-UcfRt9cX-4CV7zMsurojTZg_8Kf-u0dMYL16JXQ) (public) | N/A (not recorded) |
| Gaining Access and Pitching to the Economic Buyer | [slides](https://drive.google.com/open?id=166GA0LyvQLG6y-9qAuKoJ6iZUxmjpjklLSZ1J9wC6z0) (public) | N/A (not recorded) |
| Command Plan Excellence | [slides](https://drive.google.com/open?id=1Vjn5ICOpwxbZNFRZhvpwZc0REkHMeEKNSakpFV04EuM) (public) | [video](https://youtu.be/zN_0J6syxmM) (GitLab internal) |
| Technically Competing Against MSFT in CI Use Case | [slides](https://drive.google.com/open?id=1Rfsk5_h5O6DF14wjgmd7WrzQPCC1yGzYPPdYG_JVApg) (GitLab internal) | N/A (not recorded) |
| Next Level Engagements: Consulting Acumen | [slides](https://drive.google.com/open?id=1ahDNo93BpNSRonLj6C3iWKZfeJy1ZfJZosujpzSzo2U) (public) | N/A (not recorded) |
| Proactively Competing Against Jenkins | [slides](https://drive.google.com/open?id=1IvftLfaQyKn5-n1GLgCZokOoLU-FFzQ8LfJ9cf0FVeg) (GitLab internal) | N/A (not recorded) |
| Account Planning | [slides](https://drive.google.com/open?id=1XPhv3zqtR-q5sKgDx1HhnNkaQUSIVTO0pXj2wPdjafs) (public) | [video](https://youtu.be/-vPkzlWLZ8o) (GitLab internal) |
| From SCM and CI to Security: Paths to Ultimate | [slides](https://docs.google.com/presentation/d/1VTCfKcAZ4NS2xQu8C-fA6pugCdvUbb0MfQhimLiAGak/) (public) | N/A (not recorded) |
| Next Level Engagements: Consulting Methodology | [slides](https://drive.google.com/open?id=1TWg_grUCegOnJK8yCXxq7lRgVCByoJMCd59u3A1QjMo) (public) | N/A (not recorded) |


## SKO 2020 Wrap Up
* [SKO Wrap Up presentation](https://docs.google.com/presentation/d/1MwJRWCGl-U2qic_h3xHQxGDCUf1s0R23aKEIrXqcXW0/edit?usp=sharing) (public)
* [GitLab FY21 Sales Kick Off Wrap Up and Survey Results](https://youtu.be/_q9M9_nwNy4) video (public) 

## GitLab Infomercial
* [Public version on YouTube](https://youtu.be/gzYTZhJlHoI) featuring David Astor
* [Long version](https://drive.google.com/open?id=1yhG_JLh4rpayRvJYkYE4EJYMcNZkrUXd) (GitLab internal)

## SKO 2020 Pictures
* [Sales Kick Off photo album](https://photos.app.goo.gl/hcvEyzH3wDdccrQw7)
* [LinkedIn headshots](https://drive.google.com/open?id=1_Laql3qZBj9hp6CXrHCs7ovrCQOGR1gx) 
