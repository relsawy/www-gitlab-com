---
layout: handbook-page-toc
title: "SMB Account Executive"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## "You should expect excellence of yourself, teammates, and managers in your role. We will be excellent if we expect it." 
{: .no_toc}
{: .text-center}

Small and Medium Size Business Account Executives "SMB AE" act as Account Executives and the face of GitLab for SMB prospects and customers. They are the primary point of contact for companies that employ between [1 to 99 employees](/handbook/business-ops/resources/#segmentation). These GitLab team members are responsible for working new business in their territory as well as handling the customer journey for new and existing customers. SMB Account Executives will assist prospects through their GitLab evaluation and buying process and will be the customer's point of contact for any renewal and expansion discussions.

### Reaching the SMB team (internally)

##### General  
* **Main SMB Channel** = [`#smb`](https://app.slack.com/client/T02592416/CH4KPGS87) 
* **Commercial Sales Group** = [`commercial_global_all`](https://app.slack.com/client/T02592416/GG3M02L75)
* **Main Sales Channel** = [`#sales`](https://app.slack.com/client/T02592416/C02NE5PQM)

##### Team Channels 
* **EMEA + APAC** = [`#international-smb`](https://app.slack.com/client/T02592416/CN84VB75H)
* **AMER** = [`#smb-amer`](https://app.slack.com/client/T02592416/GN43QNUL9)

##### SMB Support 
* **SMB Training Support** = [`#smb_training_support`](https://app.slack.com/client/T02592416/CKYLWKGJU)
* **Sales Support** = [`#sales-support`](http://app.slack.com/client/T02592416/CNLBL40H4/thread/CKYLWKGJU)
* **Questions** = [`#questions`](http://app.slack.com/client/T02592416/C0AR2KW4B)

##### Closed deals 
* **Celebrate Closed Deals** = [`#win-key-deals`](https://app.slack.com/client/T02592416/CGWPJQJ07)

### Onboarding 
The goal of [onboarding](/handbook/sales/commercial/enablement) in all of this is to ensure that you feel confident and comfortable to hit the ground running. Our hope is that along this journey you are not only gaining the tactical skills needed to complete your job, but that you form connections with colleagues, gain a strong understanding of the GitLab culture, and begin to develop industry knowledge.

### Required 7 
All team members from Commercial Sales are expected to implement the [Required 7](/handbook/sales/commercial/#required-7) into their sales process. None of the 7 should be missing at any time. The Required 7 will help to stay on top of your territory, deliver what is promised to the prospect / customer and have quality meetings.

### SMB Processes

#### Territories and Account Owners 
Overview of the divided territories and owners for `SMB segment`. This territory page also shows Account owners for the other segments (`Mid Market` and `Strategic`)
* [EMEA](/handbook/sales/territories/#emea-2) 
* [AMER](/handbook/sales/territories/#amer-2)
* [APAC](/handbook/sales/territories/#apac-2) 
* Follow the [rules of engagement (ROE)](/handbook/sales/commercial/#account-ownership-rules-of-engagement-for-commercial-sales) when an account is wrongly routed into your name

#### Team Dashboards
These Dasboards are used to keep track on team performance globally as well as per region.
*  [Global](https://gitlab.my.salesforce.com/01Z4M000000skkt)
*  [EMEA & APAC](https://gitlab.my.salesforce.com/01Z4M000000oWy)
*  [NORAM](https://gitlab.my.salesforce.com/01Z4M000000slTA)

#### Call Preps

Though not mandatory, call preps are a great way to gain a different perspective on your opportunities during a sales cycle. Thus, allowing you to be more prepared for following meetings.  

* Put time on the calendar to connect with a fellow SMB team member or someone else of choice (Solution Architect, Technical Account Manager, Area Sales Manager)
* Be prepared with the Salesforce link to the opportunity, as well as your deck/call notes if applicable
* During or after the Call Prep, **log an activity** on the opportunity labeled `Prep w/ [team member name]`.

#### True Ups
True-ups are conversations that often come to the table with customers during renewal conversations. It's important to get yourself familiar with the term, how true-ups work - and how the SMB team is handling true-up conversations: 
* [True Up Policy](/handbook/ceo/pricing/#true-up-pricing)
* [What Causes True Ups?](/handbook/business-ops/business_systems/portal/troubleshooting/#user-counts-true-ups-add-ons-users-over-license)
* [How To Find True Ups](https://about.gitlab.com/pricing/licensing-faq/#what-does-users-over-license-mean)
* [Licensing and Subscription Troubleshooting ](/handbook/support/workflows/license_troubleshooting.html#licensing)
*  If you still have questions about a specific true up case, please write in the [`#smb_training_support`](https://app.slack.com/client/T02592416/CKYLWKGJU) channel for further assistance

### Solutions Architect & Technical Account Manager Rules of Engagement
- Follow the [Commercial Sales standards](/handbook/customer-success/comm-sales/#additional-engagement-guidelines-for-sales) for engaging a Technical Account Manager or Solution Architect into the conversation with a customer or prospect. 
- If limited on resources, encourage clients to submit questions to the following communities:
  - Community Forum: https://forum.gitlab.com/
  - Reddit: https://www.reddit.com/r/gitlab/

#### Renewals (WIP)
#### Discount Approval Process (WIP)
#### Forecasting (WIP)

### Tools
* [ZenDesk](/handbook/support/workflows/zendesk-overview.html) - Ticketing system
* [Version GitLab](https://version.gitlab.com/users/sign_in)- Customer Product Usage
* [Outreach](/handbook/marketing/marketing-operations/outreach/) - Email Sequencing 
* Salesforce - Customer Relationship Management 
* [LinkedIn Sales Navigator ](https://docs.google.com/document/d/1UF69ieck4AdHadzgPmZ5X1GBs3085JhlYaMowLj0AOg/edit) - Social Selling
* CaptivateIQ - Commission Calculator 
* [Clari](/handbook/sales/#clari-for-salespeople-instructional-videos) - Forecasting 
* [Datafox](/handbook/business-ops/tech-stack/#datafox) - Client Information
* [CustomersDot Admin](https://customers.gitlab.com/plans) (see overview video [here](https://youtu.be/G9JuHXqV5LM))
* [Chorus](/handbook/business-ops/tech-stack/#chorus) (For NORAM SMB ONLY) - Call/Demo Recording 
*  [Crayon ](https://app.crayon.co/intel/gitlab/battlecards/) - Competitor Insights
*  [Periscope](https://app.periscopedata.com/app/gitlab/403199/Welcome-Dashboard-%F0%9F%91%8B) - Data Visualization

### Training & Resources

#### Opportunity Consults
Opportunity Consults are weekly meetings that promote continued learning. This meeting is open to all commercial teams and is more of a framework for any sales person to leverage. Come to the meeting with a specific opportunity and let your teammates help you get creative with your opportunities.

**During the meeting Account Executive summarizes:**
* Context of what has happened on the oppty so far
* Who is your point of contact (title, who do they report to, role & responsibility)
* Why GitLab
* Initiatives or goals
* Stage & next steps (Why) 

**Team questions, input & critiques**
* Goal: find an original risk (What risks do we see, how can we mitigate them, how can we pre-emptively ask about these risks)
* Who is not involved? (How can we get them involved?)

**Meeting etiquette**
1. Be Respectful, though challenging
2. Be confrontational, though constructive
3. Be curious, and ask the question behind the question
4. Be committed
5. Everyone is encouraged to contribute
6. Everyone must present

#### Previous training sessions
- [Closing in difficult times](https://docs.google.com/presentation/d/1K5i4WeyruXD3EUXUhHxVM8UDJjrtQyZyQ1zYDV0RSXQ/edit?usp=sharing) (video [here](https://youtu.be/IwacdXWe37k))
- [Forecasting 101](https://docs.google.com/presentation/d/1r-J5Ya9DLtzxIkq0rqJou5c4vKBZkcL_y2zspEnAbaQ/edit?usp=sharing)
- [Adapting Command of the Message for SMB, part 1](https://docs.google.com/presentation/d/1ct5FksWRDtnsQeYkvceZLwi5IBlnZVx2dsA8tSzMHuY/edit?usp=sharing)
- [Adapting Command of the Message for SMB, part 2](https://docs.google.com/presentation/d/1EWXuIwFQ20r6-Efb4xdwTBBRFaqgzT7xjnqpEK3F9z0/edit?usp=sharing)
- [Mastering difficult conversations](https://docs.google.com/presentation/d/1M3PrP_h72qM7dPVIl8E1N98zYQR4-cKf51G3RCyXiDw/edit?usp=sharing) (video [here](https://youtu.be/6nCAyIIW_pE))

### Resources
*  [Recommended books for SMB team](https://docs.google.com/document/d/19KOw5A84uUvKLBI9zdspbGpP4t3OOqsQLLKj6eTBjFw/edit) 

*  [International G-drive with recordings of team meetings / trainings](https://drive.google.com/drive/folders/0ADQKNz5y48VuUk9PVA)

### Compensation
*  The global SMB team is measured monthly on shared [quota](/handbook/sales/commissions/#quotas-overview) attainment. All [iACV](/handbook/sales/#incremental-annual-contract-value-iacv) goes toward the global *shared quota* retirement.
*  Because SMB AEs operate as individual contributors toward a shared quota, [collaboration](/handbook/people-group/collaboration-and-effective-listening/#collaboration) is key. One teammate's success is everyone's success.

#### [Payment Schedule](/handbook/finance/payroll/#pay-date): 
*  AMER: [commissions](/handbook/sales/commissions/#base-commission-rate-bcr) based on prior month attainment are paid on the 2nd payroll cycle the following month.
*  EMEA & APAC: commission payout dates are specific to the country of residency. 
* you can find your adjusted payout statement and submit inquiries via [CaptivateIQ](captivateiq.com).

Any further questions related to SMB Compensation can be directed to the slack channel #total-rewards or your direct manager.

### Handbook Mantra 
GitLab is intentional about documenting in a manner that creates a single source of truth. [It operates handbook-first](https://about.gitlab.com/company/culture/all-remote/handbook-first-documentation/), and in valuing transparency, makes its handbook publicly accessible to all. SMB team members should always be striving to contribute and exemplify this value.
