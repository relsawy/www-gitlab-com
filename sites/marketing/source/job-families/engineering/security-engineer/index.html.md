---
layout: job_family_page
title: "Security Engineer"
---

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/owot2NPK1Jk" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

As a member of the security team at GitLab, you will be working towards raising the bar on security. We will achieve that by working and collaborating with cross-functional teams to provide guidance on security best practices.

The [Security Team](/handbook/engineering/security) is responsible for leading and implementing the various initiatives that relate to improving GitLab's security.

## Responsibilities for Security Engineer roles

- Develop security training and guidance to internal development teams
- Provide subject matter expertise on architecture, authentication and system security
- Assess security tools and integrate tools as needed, particularly open-source tools
- Assist with recruiting activities and administrative work
- Technical Skills
  * Familiar with common security libraries, security controls, and common security flaws that apply to Ruby on Rails applications.
  * Ability to discover and patch SQLi, XSS, CSRF, SSRF, authentication and   authorization flaws, and other web-based security vulnerabilities (OWASP Top 10 and beyond).
  * Knowledge of common authentication technologies including OAuth, SAML, CAs, OTP/TOTP.
  * Knowledge of browser-based security controls such as CSP, HSTS, XFO.
  * Experience with standard web application security tools such as Arachni, Brakeman, and BurpSuite.
  * There should also be time to participate in development of GitLab.
- Code quality
  * Proactively identify and reduce security risks.
  * Find and remove outdated and vulnerable code and code libraries.
- Communication
  * Consult with other Developers and Product Managers to analyze and propose application security standards, methods, and architectures.
  * Handle communications with independent vulnerability researchers and design appropriate mitigation strategies for reported vulnerabilities.
  * Educate other developers on secure coding best practices.
  * Ability to professionally handle communications with outside researchers, users, and customers.
  * Ability to communicate clearly on technical issues.
- Performance & Scalability
  * An understanding of how to write code that is not only secure but scales to a large number of users and systems.

## General Requirements for Security Engineer roles

- You have a passion for security and open source
- You are a team player, and enjoy collaborating with cross-functional teams
- You are a great communicator
- You employ a flexible and constructive approach when solving problems
- You share our [values](/handbook/values), and work in accordance with those values
- Ability to use GitLab

## Levels of Security Engineer roles

### Intermediate Security Engineer

* Leverage understanding of fundamental security concepts
* Triages/handles basic security issues
* Be positive and solution oriented
* Good written and verbal communication skills
* Constantly improve product security

### Job Grade

The Security Engineer is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Senior Security Engineer

The Senior Security Engineer role extends the [Intermediate Security Engineer](#intermediate-security-engineer) role.

* Leverages security expertise in at least one specialty area
* Triages and handles/escalates security issues independently
* Conduct security architecture reviews and makes recommendations
* Great written and verbal communication skills
* Interview security candidates during hiring process

***

A Senior Security Engineer may decide to pursue the [security engineering management track](/job-families/engineering/security-management) at this point, should they wish to. See [Engineering Career Development](/handbook/engineering/career-development) for more detail on the tracks available for Senior Engineers.

***

### Job Grade

The Senior Security Engineer is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Staff Security Engineer

The Staff Security Engineer role extends the [Senior Security Engineer](#senior-security-engineer) role.

* Recognized security expert in multiple specialty areas, with cross-functional team experience
* Make security architecture decisions
* Provide actionable and constructive feedback to cross-functional teams
* Implement security technical and process improvements
* Exquisite written and verbal communication skills
* Author technical security documents
* Author questions/processes for hiring and screening candidates
* Write public blog posts and represent GitLab as a speaker at security conferences

### Job Grade

The Staff Security Engineer is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

### Distinguished Security Engineer

TBD

### Job Grade

The Distinguished Security Engineer is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

## Specialties for Security Engineer roles

### Security Research

Security research specialists are subject matter experts (SME) that conduct research in their area of expertise to protect GitLab the product and GitLab company assets. They are also encouraged to participate in the larger security community through blog posts and participation in industry conferences. Responsibilities for this specialty include:

* Conduct research in their area of expertise to protect GitLab and GitLab.com assets.
* Research security posture of FOSS tools that are integrated with GitLab.
* Report findings to tool developers and track mitigation process, following [responsible disclosure guidelines](https://about.gitlab.com/security/disclosure/#disclosure-guidelines-for-vulnerabilities-in-3rd-party-software).
* Author blogs posts and presentations on vulnerabilities discovered and their area of expertise.
* Support other GitLab initiatives as a SME.
* Author documentation and/or tooling for security training.

### Application Security

Application Security specialists work closely with development teams, product managers (PM), and third-party groups (including the paid bug bounty program) to ensure that GitLab products are secure.

Application Security Responsibilities

- Perform vulnerability management and be a subject matter expert (SME) for mitigation approaches.
- Support and evolve the bug bounty program.
- Conduct risk evaluation of GitLab product features.
- Conduct application security reviews, including code review and dynamic testing.
- Participate in initiatives to holistically address multiple vulnerabilities found in a functional area.
- Develop security training and socialize the material with internal development teams.
- Develop automated security testing to validate that secure coding best practices are being used.
- Facilitate preparation of both critical and regular security releases
- Guide, advise, and assist product development teams as SMEs in the area of application security.
- Assist with recruiting activities and administrative work

Application Security Requirements

- Familiarity with common security libraries, security controls, and common security flaws that apply to Ruby on Rails applications
- Some development experience (Ruby and Ruby on Rails preferred; for GitLab debugging)
- Experience with OWASP, static/dynamic analysis, and common exploit tools and methods
- An understanding of network and web related protocols (such as, TCP/IP, UDP, IPSEC, HTTP, HTTPS, routing protocols)
- Familiarity with cloud security controls and best practices

### Security Automation

By leveraging diverse technologies and an automation first approach, the Security Automation team strives towards improving the efficiency, effectiveness, and accuracy within GitLab's Information Security program with a focus on cost savings. Examples include the creation of automated security issue triage and management solutions, automating handling of repetitive tasks, and defining re-usable security automation architectures. Additionally, the Security Automation team will assist other security specialty teams with automation efforts they are leading and developing through the assessment of automation tools, and integration tools and technologies to support automation efforts as needed.

Security Automation Responsibilities

- Design, engineer, deploy, and maintain custom automation products
- Build security tooling and automation for internal use that enable the Security Department to operate at high speed and wide scale
- Define and own metrics and key performance indicators to determine the effectiveness of the Security Automation program
- Collaborate with product teams to ensure that the GitLab product meets security automation requirements for ourselves and our users.


Security Automation Requirements

- Previous experience on a Security Operations, Software Development, or Automation team
- Scripting/coding experience with one or more languages - Python, Ruby, and/or Golang experience a plus
- Extensive knowledge of Internet security issues, automation or software engineering technologies, cloud architectures, and threat landscape concepts
- Solid understanding of the Software as a Service (SaaS) model
- Solid understanding of the DevOps model
- Experience with Cloud Computing Platforms - GCP experience a plus
- Experience with Kubernetes a plus
- Experience with infrastructure as code processes and tools a plus


### SIRT - Security Incident Response Team

SIRT Engineers are the firefighters of the GitLab Security Team.  As a Security Engineer in SIRT your daily duties will include incident response, log analysis, forensics, tooling and automation development, as well as contributing to strategic improvements to the GitLab products and GitLab.com services.  Successful Security Engineers thrive in high-stress environments and can think like both an attacker and defender, have the ability to engage with and mentor more junior Security Engineers, and can help come up with proactive and preventative security measures to keep GitLab and its user’s data safe.

More information about the SIRT role is described in the persona of [Alex, SIRT Engineer](https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas/#alex-security-operations-engineer)

SIRT Responsibilities

- Detect and respond to company-wide security incidents
- Log analysis
- Security forensics
- Develop and implement preventative security measures (detection, monitoring, exploitation)
- Build security tools that enable the GitLab Security Team to operate at speed and scale
- Incorporate current security trends, advisories, publications, and academic research
- Engineer CND technologies to monitor and analyze (e.g. IDSes, Data collection tools)
- Vulnerability management - triage and manage vulnerabilities identified through scanning and manual efforts
- Identify and mitigate complex security vulnerabilities before an attacker exploits them
- Communicate risks and mitigations across multiple audiences with varying levels of sensitivity
- Take part in the Security Operations on-call rotation

SIRT Requirements

- 5+ years of demonstrated experience in web or cloud security engineering, log aggregation, and/or penetration testing
- 2+ years of direct experience with incident response
- Experience with log analysis systems
- Engineer, not an analyst mindset
- In-depth knowledge of Linux tools/architecture and logging systems
- Experience with Google Cloud Platform (GCP), AWS, and/or Azure
- Experience with one or more programming languages  (Ruby on Rails, Go, PHP and/or Python)
- Proficiency to communicate over a text-based medium (Slack, GitLab Issues, Email) and can succinctly document technical details.

### Trust & Safety

Trust & Safety Engineers are the builders of the anti-abuse world. They develop the tools needed to monitor, mitigate and report on abusive behavior and are an essential part of our goal to be good internet citizens.

A successful candidate is someone who wants to make the internet a safer place and do the right thing because it’s right.

Your daily duties will include building tooling and automation for curbing abuse, assist with incident response, as well as contributing to strategic improvements to the GitLab products and GitLab.com services.

Trust & Safety Responsibilities

- Initiatives to curb known abusive activity on GitLab.com, and to identify new and unknown abuse vectors
- DMCA Notice and Counter-Notices (dmca@gitlab.com)
- Mitigation of abusive/non-responsive customers
- Verifying the proper classification of abuse reports
- Escalating to stakeholders while continuing to monitor
- Monitoring logs and queues for trends
- Research and prevention trending abuse methodologies

Trust & Safety Requirements

- 3+ years of demonstrated experience in a developer, system engineering, or security engineering role
- 2+ years experience in Anti-Abuse processes or mitigation
- Broad knowledge of technology, and be passionate about it. Able to discuss and explain popular, internet-based technologies with ease, and present their experience with them.
- Development, scripting, or automation experience - A successful candidate is a builder. They dislike repetitive tasks and have a history of automating their daily workflows to make their days more productive. They are comfortable writing in Python, Ruby, or similar scripting languages, while also being able to read and interpret code from other languages.
- Good communication and documentation skills
- Knowledge of Linux tools/architecture and logging systems
- Experience with SQL
- Nice to Have: Experience with Google Cloud Platform (GCP), AWS, and/or Azure

### Security Compliance

Security Compliance specialists enables Sales by achieving standard as required by our customers. This includes SaaS, self-managed, and open source instances.

*  Please refer to the [Security Analyst](https://about.gitlab.com/job-families/engineering/security-analyst/) page for additional information.

### Red Team

Red Team specialists emulate adversary activity to better GitLab’s enterprise and product security. The role requires the ability to think like an advanced persistent threat. Creativity is key. For example, develop attack plans and stealthily execute them to compromise sensitive information on GitLab.com such as private repos, or develop and distribute malware to GitLab team-members to demonstrate how the corporate enterprise could be compromised.

* Utilize threat modeling concepts and frameworks such as MITRE ATT&CK, STRIDE, etc. to continually identify ways to protect and defend GitLab assets by executing attacks that emulate a range of adversaries
* Focus on designing, researching, and executing attacks to challenge the blue team
* Strive to identify weaknesses within GitLab products and corporate network and demonstrate the associated risks
* Contribute to the GitLab Secure and Protect products
* Incorporate current security trends, advisories, publications, and academic research
* Understand CND technologies to bypass these security controls and stay undetected
* Report on the Red Team engagements providing an in-depth analysis of the security issues identified
* Identify complex security vulnerabilities and exploit them before an external attacker can exploit them
* Determine the level of effort required to compromise sensitive data
* Publish blog posts and present talks at security conferences
* Contribute to GitLab products by testing and proposing new features

## Security Engineer Hiring Process

All interviews are conducted using Zoom video conferencing software. Candidates for Security Engineer roles can expect the hiring process to follow the order below, with modifications to the process as required, based on specific situations. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

#### Screening call with Recruiter

### Round 1
- 60 Minute Interview with Hiring Manager

### Round 2
- 45 Minute Peer Interview
- 45 Minute Peer Interview

### Round 3
- 60 Minute Interview with Director of Security or VP of Security, or both

As always, the interviews and screening call will be conducted via a video call.
See more details about our hiring process on the [hiring handbook](/handbook/hiring).

## Career Ladder

For more details on the engineering career ladders, please review the [engineering career development](/handbook/engineering/career-development/#roles) handbook page.
