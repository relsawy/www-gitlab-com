---
layout: secure_and_protect_competitors
title: "GitLab vs Checkmarx"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

<div class="comparison-table comparison-page-content secure-and-protect" markdown="1">

## Summary

Checkmarx is a long-standing company with their roots in SAST.  They are recognized as a Leader in the Gartner Application Security Testing Magic Quadrant.

## Comparison to GitLab

Although Checkmarx has a more mature SAST offering, GitLab offers a much broader range of security testing capabilities, including DAST and Fuzz Testing.  GitLab’s capabilities come integrated with the rest of GitLab out-of-the-box and do not require any special integration to shift the workflow left to the development team.  GitLab customers report that GitLab generally has a better false positive rate than Checkmarx, which saves time when trying to find true vulnerabilities that really matter.  Checkmarx's established position in the security market and deep SAST capabilities are offset by GitLab’s lower price point and tighter integration with the rest of the software development lifecycle.

The Checkmarx [vision](https://www.youtube.com/watch?v=gTGvl_V5rOA&feature=youtu.be) is closest to GitLab among the AppSec vendors, but because they must integrate into the rest of the SDLC via APIs, their path toward execution is more limited. Also, like the other AppSec vendors, Checkmarx is expensive. It is priced per developer with a rough estimate of 12 Developers for $59k USD per year or 50 Developers for $99k USD per year. Checkmarx uses Whitesource for dependency scanning and charges an extra $12k USD per year for this open source scanning.  

Checkmarx excels in that they are context aware, meaning they can mark what is not exploitable based on path. GitLab lacks this capability.  On the other hand, GitLab automatically includes broad security scanning with every code commit including Static and Dynamic Application Security Testing, along with dependency scanning, container scanning, and license compliance. All of this is part of the single GitLab Ultimate application.

## Security Scanning

### Strengths and Weaknesses

</div>
<div class="comparison-table comparison-page-content secure-and-protect strengths-and-weaknesses" markdown="1">

| | <b>GitLab</b> | <b>Checkmarx</b> |
| --- | --- | --- |
| <b>Strengths</b> | <span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cost is significantly less expensive than Checkmarx</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Tight integration with developer workflow</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Complete range of application testing types (SAST, DAST, etc.) are included by default</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Comparatively low false positive rates</span> | <span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Strong offering across scanning types</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Good integration with IDEs and local developer environments</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Well known, market-leading SAST offering</span> |
| <b>Weaknesses</b> | <span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;GitLab’s SAST offering only scans code repositories today and cannot scan compiled binaries</span> | <span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SCA is essentially a brand new product and only available as an addon to their SAST product</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DAST is only available as a managed service via a partnership</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Fuzz testing is not offered</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Each kind of testing is a separate piece of software that must be licensed, managed, and integrated with the DevOps lifecycle separately</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Operating system support to run the Checkmarx software is limited to Windows</span><br><span>&nbsp;&nbsp;&bull;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Significant tuning is required to reduce false positives</span> |

</div>
<div class="comparison-table comparison-page-content secure-and-protect" markdown="1">

### Feature Lineup

</div>
<div class="comparison-table comparison-page-content secure-and-protect feature-lineup" markdown="1">

| | <b>GitLab</b> | <b>Checkmarx</b> |
| --- | :-: | :-: |
| SAST | &#9989; | &#9989; |
| DAST | &#9989; | managed service only |
| IAST | | &#9989; |
| SCA: Vulnerability Scanning | &#9989; | &#9989; |
| SCA: Open Source Audit | &#9989; | &#9989; |
| Fuzz Testing | &#9989; | |

</div>
