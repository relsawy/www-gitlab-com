---
layout: markdown_page
title: "ALM Octane"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## Summary
Micro Focus, which purchased the software business unit of Hewlett Packard Enterprise, has ALM Octane, that has both on-premise technologies and SaaS-based offerings. ALM Octane is a web-based application lifecycle management platform that enables teams to collaborate easily, manage the product delivery pipeline, and visualize the impact of changes.

GitLab enables portfolio planning and management through epics, groups (programs) and milestones to organize and track progress. Regardless of your methodology from Waterfall to DevOps, GitLab’s simple and flexible approach to planning meets the needs of small teams to large enterprises. GitLab helps teams organize, plan, align and track project work to ensure teams are working on the right things at the right time and maintain end to end visibility and traceability of issues throughout the delivery lifecycle from idea to production.

## Gaps
ALM Octane doesn't include built-in CI/CD, SCM and IDE, therefore integrations with third-party tools are required for the complete software development lifecycle. In contrast, GitLab is a complete DevOps platform, delivered as a single application. From project planning and source code management to CI/CD, monitoring, and security.

## Comments/Anecdotes
* [From IT Central Station ALM Octane customer](https://www.itcentralstation.com/products/comparisons/alm-octane_vs_jira): "When I manage projects that are being created in ALM, I have a standard template, but I don't have a template for them in Octane. I literally have to create the project from the ground up every time, which for an administrator, is a nightmare solution". In contrast, Gitlab allows creating projects from template, [more details](https://docs.gitlab.com/ee/user/admin_area/custom_project_templates.html).

* [From IT Central Station ALM Octane customer](https://www.itcentralstation.com/products/comparisons/alm-octane_vs_jira): "The Requirements Module could be better, to build up a better requirements process. There's a huge improvement from ALM.NET to Octane, but it's still not really facilitating all the needs of the product owners, to set up their requirements in Octane".

* [From TechValid ALM Octane Senior consulted](https://www.techvalidate.com/product-research/microfocus-mobile-center/facts?fact_search%5Branking_ids%5D%5B%5D=8&page=2): "The Jenkins dashboard (pipeline overview) is amazing. I have shown customers who have a strong anti-HPE bias and they were blown away by that feature alone".

## Resources
* [Micro Focus ALM Octane](https://www.microfocus.com/en-us/products/alm-octane/overview)
* [ALM Octane online help](https://admhelp.microfocus.com/octane/en/latest/Online/Content/Resources/_TopNav/_TopNav_Home.htm)
* [Pricing](https://aws.amazon.com/marketplace/pp/B079J6FD76)

## Comparison
