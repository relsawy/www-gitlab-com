---
layout: handbook-page-toc
title: "Objectives and Key Results (OKRs)"
description: "OKRs stand for Objective-Key Results and are our quarterly objectives. OKRs are how to achieve the goal of the Key Performance Indicators KPIs."
canonical_path: "/company/okrs/"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## Most recent OKRs

All our OKRs are public and listed on the pages below.

- [FY21-Q4](/company/okrs/fy21-q4/)
- [FY21-Q3 (active)](/company/okrs/fy21-q3/)
- [FY21-Q2](/company/okrs/fy21-q2/)
- [Previous OKRs](#okr-archive)

## What are OKRs?

[OKRs](https://en.wikipedia.org/wiki/OKR) stand for Objectives and Key Results and are our quarterly objectives.
OKRs are _how_ to achieve the goal of the Key Performance Indicators [KPIs](/handbook/ceo/kpis/).
They lay out our plan to execute our [strategy](/company/strategy/) and help make sure our goals and how to achieve that are clearly defined and aligned throughout the organization.
The **Objectives** help us understand *what* we're aiming to do,
and the **Key Results** help paint the picture of *how* we'll measure success of the objective.
You can use the phrase “We will achieve a certain OBJECTIVE as measured by the following KEY RESULTS…” to know if your OKR makes sense.
The OKR methodology was pioneered by Andy Grove at Intel and has since helped align and transform companies around the world.

OKRs have four superpowers:
* Focus
* Alignment
* Tracking
* Stretch

We do not use it to [give performance feedback](/handbook/people-group/360-feedback/) or as a [compensation review](/handbook/total-rewards/global-compensation/#annual-compensation-review) for team members.

The [E-Group](/company/team/structure/#e-group) does use it for their [Performance Enablement Reviews](/handbook/people-group/learning-and-development/career-development/#performance-enablement-review)

The [Chief of Staff](/job-families/chief-executive-officer/chief-of-staff/) initiates and guides the OKR process.

Watch EVP, Engineering Eric Johnson discuss the power of OKRs from his perspective:

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/aT66up3SyVU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

### OKRs are stretch goals by default

OKRs should be ambitious but achievable. If you achieve less than 70% of your KR, it may have not been achievable. If you are regularly achieving 100% of your KRs, your goals may not be ambitious enough.

Some KRs will measure new approaches or processes in a quarter. When this happens, it can be difficult to determine what is ambitious and achievable because we lack experience with this kind of measurement. For these first iterations, we prefer to set goals that seem ambitious and expect a normal distribution of high, medium, and low achievement across teams with this KR.

### Shared Objectives

If there is something important that requires two (or more) parts of our organization, all leaders involved should share the same or similar objective. They should have deconflicted key results so they can still acheive things within their sphere of control. This is in keeping with our concepts of [collaboration](/handbook/values/#collaboration) and [directly responsible individual (DRI)](/handbook/people-group/directly-responsible-individuals/#what-is-a-directly-responsible-individual).

### OKRs are what is different

The OKRs are what initiatives we are focusing on this quarter specifically.
Our most important work are things that happen every quarter.
Things that happen every quarter are measured with [Key Performance Indicators](/handbook/ceo/kpis).
Part of the OKRs will be or cause changes in KPIs.

### Pass-thru OKRs

It's acceptable for managers and reports to have an identical key result. For instance, something really important might need to happen at the executive level, but it's a manager or IC several layers apart who is doing the actual execution. Every person in that line of reporting should have the same key result.

While it can feel like double-counting, it is consistent with [Andy Grove's](https://en.wikipedia.org/wiki/Andrew_Grove) concept of Managerial Leverage outlined in his book [High Output Management](https://www.amazon.com/High-Output-Management-Andrew-Grove/dp/0679762884). This ensures that conversations happen in the relevant 1:1's, that everyone knows the latest status, and that the person executing does not accidentally get re-tasked. Please remember to recognize the person that acheived the result so there is no perception of "taking credit" for others' work.

### How do I prioritize OKRs in the light of other priorities?

OKRs do not replace or supersede core team member responsibilities or performance indicators. OKRs are additive and are essentially a high signal request from your leadership team to prioritize the work. They typically are used to help galvanize the entire company or a set of teams to rapidly move in the one direction together. They are expected to be completed unless you have higher priority work that is surfaced and agreed to by leadership.  When surfacing tradeoffs, team members should not factor in how an unmet OKR may impact your executive leadership bonus in their prioritization. They should instead focus on GitLab priorities. If your executive leader still feels that the OKR is more important, they will ask you to *disagree and commit*. 

## Dogfood portfolio management's strategic planning capabilities
OKRs are like [Portfolio Management](/handbook/product/product-categories/#portfolio-management-group) so we are [dogfooding](/handbook/values/#dogfooding) those features to track our OKR progress over the course of a quarter.
Work happens in GitLab, so we use GitLab to manage tracking the progress of that work.
We use epics to track objectives, and issues to track key results.
With [health statuses](/releases/2020/04/22/gitlab-12-10-released/index.html#epic-and-issue-health-tracking), each of the CEO OKRs maps to an epic, and the progress of all cascading OKRs can be seen at-a-glance.

![okr-epic-at-a-glance](/company/okrs/okr-epic-at-a-glance.png)

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/-KIr8lhYn7Y" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Who sets OKRs?

Generally, we do OKRs up to the team level.
As a company, we don't do [individual OKRs](https://hrblog.spotify.com/2016/08/15/our-beliefs/), but some functions may.
For example, in the Engineering Division Staff-level (and above) individual contributors have OKRs.
Also, individual contributors in the Engineering Division who are not required to do OKRs are welcome to do them with their manager. It's a useful way to prepare for a managerial career, or to align one's activities with the broader goals of the company.

An individual might also have OKRs if they represent a unique team.
For example, individual SDRs don't have OKRs, the SDR team does.
If Legal is one person but represents a unique function, Legal has OKRs.
Part of the individual performance review is the answer to: how much did this person contribute to the team objectives?

## OKR Process at GitLab

The EBA to the CEO is responsible for scheduling and coordination of the OKRs process detailed below.
Scheduling should be completed at least 30 days in advance of the beginning of the OKR process, which begins 5 weeks before the start of the [fiscal quarter](/handbook/finance/#fiscal-year).

Should you need to reschedule, please @ mention the EBA to the CEO in the `#eba-team` slack channel. 

### CEO initiates new quarter's OKRs

**Five Mondays** before the start of the fiscal quarter, the CEO and Chief of Staff initiate the OKR process.
The CEO has three objectives every quarter that map to our [strategy sequence](/company/strategy/#sequence):
1. Grow IACV
1. Popular next generation product
1. Highly performant team

Initiating the OKR process is best done in two distinct MRs. The first accomplishes the following:
* create that quarter's page in the handbook
* set the CEO's objectives as H2 tags on the page
* skeleton the other parts of the OKR page (e.g. How to Achieve meetings)

In a subsequent MR, the CEO proposes one to three Key Results for the quarter for each of those objectives.
At least one KR should loosely map to each executive, though this is not explicitly stated.
The CEO shares this MR in the #e-group channel in Slack and discusses it in each of his 1:1s with his direct reports in the next week.

### Executives propose OKRs for their functions

The following week, **four Mondays** before the start of the fiscal quarter, Executives propose OKRs for their functions  **via epics (for objectives) and issues (for KRs) through a Slack message in #OKRs channel**. The CEO and Chief of Staff should be at-mentioned. The CEO will confirm sign-off on objectives through commenting directly in them. While the CEO is the DRI, this responsibility may be the delegated to the CoS.

Each executive should aim for a maximum of 3 objectives however in Q4 each executive can have a maximum of 4 OKRs. Each objective has between 1 and 3 key results; if you have less, you list less. While OKRs are known for being ambitious or committed, we only have ambitious OKRs.

Function epics should cascade from one of the CEO's OKR EPICs.

Executives should consider how their OKR efforts can have the greatest impact on the organization. Functions can have objectives under any of the three CEO OKRs. For example, the People Team could have an objective under the CEO's IACV OKR if it identified that a specific enablement activity were key to driving sales or the Sales Team could have an objective under the CEO's Great Teams OKR if it were focused on improving team diversity. Functions should not be pigeonholed into the CEO OKR that appears to be most directly related to the function.

When ready for review or when changes to objectives or issues are made, epics and issues should be shared in the #okrs channel in Slack and at-mention the Chief of Staff and CEO.
The CEO is responsible for OKR approvals, but may delegate this responsibility to the CoS. 

How objectives cascade from CEO KRs in the handbook to functional Objectives and KRs in Epics and Issues:

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/J5fe0q9mZCU/" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

### OKR Draft Review Meeting

The week that begins **three Mondays** before the start of the fiscal quarter, there is an OKR Draft Review Meeting for the e-group.
This meeting is an opportunity for executives to get feedback from one another and highlight any dependencies on other functions to one another.
The agenda for this meeting is structured as follows:
1. Function
   1. Epic: link to the objective
   1. Dependencies: call out any dependencies

If additional action needs to be taken by the functional leader, the epic or issue should be re-shared in the #okrs channel in Slack when it's ready for final review.

### Cascade!

Now that Executive (function-level) OKRs are set
(as set as things are at GitLab; Everything is always in Draft!),
Executives shift their focus to finalizing OKRs to their team.

This is also the opportunity to create Executive OKRs in GitLab (epics and issues) and add them to the relevant CEO OKR Epics.

Notes for Pass-thru KRs: To avoid duplicate issues for the same KR due to the fact that an Issue can only inherit from a single Epic, make the KR issue a child of the Objective epic at the lowest level in the organization structure. The upper level Objective epics all refer to the single KR issue but not in the inheritance hierarchy.

### Dependency Commitments

Top level dependencies should be identified in the [OKR Draft Review Meeting](https://about.gitlab.com/company/okrs/#okr-draft-review-meeting), but it is likely that additional dependencies will be identified as OKRs cascade. We want to avoid situations where big asks are coming weeks into the quarter, or teams are being committed to do work without first having input. This makes it difficult for teams to manage their own priorities and succeed in their own initiatives. 

It is each team's responsibility to proactively identify dependencies in which the team cannot reach success without meaningful engagement from another team. In these instances, it is important that all teams required to make a significant contribution sign off on the KR and agree on the level of support to be provided. A boring solution is to create a sister KR for other departments that will need to be actively involved. KRs with dependencies should not be considered final until other teams have confirmed support, but other teams should also respect department KRs as the top priorities for the overall business and do what they can to support. 

### How to Achieve Presentation

The **week before** the fiscal quarter begins, each executive presents a How to Achieve presentation.
The presentation should outline how executives plan to achieve each of their Key Results.
The presenation is NOT a status update on previous quarter's OKRs.

EBA to the CEO schedules the 25-minute How to Achieve Meetings.
All executives, the CoST, and the Functional EBA are all invited, but only the CEO and Functional Executive are required.

Details on meeting preparation:
1. Functional Leaders should record their How to Achieve presentation before hand.
1. Videos should be shared in the #okrs channel in Slack at least 2 business days before the call.
This should be done at least two days before the scheduled call. If completed two days before, add the link to the recording in the #okr Slack channel.
1. Functional EBA may add a calendar invite on their leader's calendar for three days before to remind the leader to upload the YouTube video, if that would be useful.
1. EBA to the CEO to link agenda and notes doc to the calendar invite 24 hours in advance. We'll use this to track questions and action items.

Some executives have asked EBAs to ensure there is time on the calendar to review the videos before the meeting.
EBAs should coordinate with their Executives directly.

The How to Achieve **presentation** is recorded in a video and delivered beforehand. 
The How to Achieve **meeting** is the discussion/Q&A of the presentation.
The meeting is livestreamed to YouTube, as a private or public stream, as necessary.
The EBA who sets up the livestream will confirm with the presenter beforehand.
OKRs are public, and we are public by-default, so we should aim for as many public How to Achieve presentations as possible.

### The quarter begins

The Chief of Staff updates the OKR page for the current quarter to be active.
CEO OKRs may be included in the next formal or informal Board meeting.

## Format of OKR on the Handbook Page

Top level CEO KRs will appear in the handbook. OKRs have numbers attached to them for [ease of reference, not for ranking](/handbook/communication/#numbering-is-for-reference-not-as-a-signal). In order to maintain a [single source of truth](/handbook/documentation/#documentation-is-the-single-source-of-truth-ssot), starting in Q4 FY21, we're putting functional objectives and KRs in epics and issues that should link to the CEO KR epics that are linked on the handbook page. We made this change, because the handbook and epics were not being consistently updated, so there often wasn't a single source of truth. We were also struggling with merge conflicts as regular changes were being made to the handbook page. 

Functional leaders are responsible for updating the their objectives and KRs in their epics and issues before each [Key Meeting](/handbook/finance/key-meetings/#automated-kpi-slides).

## Format of Objectives and Key Results in Epics and Issues

Each functional epic should appear under the relevant CEO epic. Naming conventions are captured in the chart below.

<div class="panel panel-success">
**This is the format for OKRs added as issues and pics.**
{: .panel-heading}
  <div class="panel-body">
**1. [EPIC] Title: Objective as a sentence.**
   1. [ISSUE] Title KR: Key result
   1. [ISSUE] Title KR: Key result
   1. [ISSUE] Title KR: Key result
   </div>
</div>

In other words, all OKR-related epics should follow the naming convention of "Title: Objective as a sentence".
The CEO Epic will include the Fiscal Quarter in it. Cascading epics should not. 

## Example: Development's approach to OKRs

<details>
  <summary  markdown='span'>Detailed instructions with links to templates</summary>

Although the steps in this example reference the Development department these steps can be used by any department within GitLab.  You may simply need to change the labels associated with the links used in the example below.

### The Tracking Issue

For Development we use the [Development Department Issue Board](https://gitlab.com/gitlab-com/www-gitlab-com/-/boards/1008667?label_name[]=Development%20Department) to track OKRs, organized by each sub-department.  Each group creates their own tracking issue with the description of the issue linking out to [Objectives](#objectives) and [Key Results](#key-results).  Instructions for creating the tracking issue:

1. Create a tracking issue using the `engineering-okr` template ([link](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/new?issuable_template=engineering-okr&issue[title]=FYYY-QQ%20Group%20Name%20OKRs))
1. Note: The `engineering-okr` template can be used by any department, simply remove the ~"Engineering Management" label
1. Note: you will need to add your own department (e.g. ~Development) and sub-deparment label (e.g. ~"Enablement Sub-department" )
1. Update the title to the following format: FYYY-Q# Group Name OKRs (e.g. FY21-Q2 Growth Sub-department OKRs)
1. As you create [Objectives](#objectives) and [Key results](#key-results) following the instructions below, be sure to update the body of this tracking issue with the titles and links.

#### Objectives

The objectives for the quarter are defined in the [OKR section of the handbook](https://about.gitlab.com/company/okrs/).  In your Tracking Issue you will need to [create an epic](https://gitlab.com/groups/gitlab-com/-/epics/new) for each objective with the proper title [formatting](https://about.gitlab.com/company/okrs/#format-of-objectives-and-key-results-in-epics-and-issues).  There are currently no templates for these epics.  Steps to create an objective epic:

1. [Create an epic](https://gitlab.com/groups/gitlab-com/-/epics/new) for each objective that you wish to create a key result
1. [Format the epic title](https://about.gitlab.com/company/okrs/#format-of-objectives-and-key-results-in-epics-and-issues) to match the company OKR title
1. Optional - add labels for your group or sub-department or department
1. Add the newly created epic to your manager's objective in the same category (e.g. Product, Team, IACV).  This is so that all objective roll up to the CEO objective of the same category and everyone can see a cascading view of objectives
1. Update the [Tracking Issue](#the-tracking-issue) with the title and link of this newly created objective

#### Key Results

Each [Objective](#objectives) will contain one or more Key Result issues.  Instructions:

1. Create a [new issue](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/new?issuable_template=engineering-kr&issue[title]=FYYY-QQ%20Group%20Name%20KR:%20Text)
1. The title format is FYYY-QQ Group Name KR: Text (e.g. FY21-Q3 Protect Group KR: Protect our Code!)
1. Add the following labels: ~OKR ~"Engineering Management" and the label for your department (e.g. ~"Development Department" and sub-department (e.g. ~"Dev Sub-department" )
1. Add labels for your `group` (e.g. ~group::compliance)
1. Assign to yourself (or appropriate person)
1. Link to the appropriate Objective Epic
1. Add the newly created Key Result , under the correct [objective](#objectives) listing, in the [Tracking Issue](#the-tracking-issue)

</details>

## Maintaining the status of OKRs

Teams should update the Health Status of their KR issues and present them in their [Key Meeting](/handbook/finance/key-meetings).
When presenting the status of OKRs, we use the following terms to denote the status of a key result:
1. On track - the DRI is confident the key result will be achieved.
1. Needs attention - the DRI believes there is some risk the key result will be achieved. Elevated attention is required in order for the key result to be achieved.
1. At risk - the DRI does not expect the key result will be achieved. Urgent action is required in order for the key result to be achieved.

A Key Results issue's health status should be maintained as the SSOT on the status. This is something that should be able to be referenced at any point in order to get a clear view of progress against the objective. The issue's parent epic will roll up the health statuses of all relevant issues. 

During key meetings, team's should include [OKR slides](/handbook/finance/key-meetings/#okr-slides) that share status details and link to relevant epics and issues.

The final Key Meeting of the quarter should offer a clear scoring for each KR.

## Everyone can contribute

Everyone is welcome to a suggestion to improve any OKR.
To update please make a merge request and post a link to the MR in the #okrs channel in Slack and at-mention the Chief of Staff. If commenting on a functional objective or KR, comment directly in the epic or issue.

## OKR resources:
* [With Goals, FAST beats SMART](https://sloanreview.mit.edu/article/with-goals-fast-beats-smart/)
* [Measure What Matters by John Doerr](https://www.whatmatters.com)
* [A Modern Guide to Lean OKRs](https://worldpositive.com/a-modern-guide-to-lean-okrs-part-i-c4a30dba5fa1)

## OKR Archive

- [FY21-Q1](/company/okrs/fy21-q1/) 
- [FY20-Q4](/company/okrs/fy20-q4/)
- [FY20-Q3](/company/okrs/fy20-q3/)
- [FY20-Q2](/company/okrs/fy20-q2/)
- [FY20-Q1](/company/okrs/fy20-q1/)
- [CY18-Q4](/company/okrs/2018-q4/)
- [CY18-Q3](/company/okrs/2018-q3/)
- [CY18-Q2](/company/okrs/2018-q2/)
- [CY18-Q1](/company/okrs/2018-q1/)
- [CY17-Q4](/company/okrs/2017-q4/)
- [CY17-Q3](/company/okrs/2017-q3/)
