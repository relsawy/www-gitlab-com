---
layout: markdown_page
title: "Diversity, Inclusion & Belonging"
description: "Diversity, Inclusion & Belonging is fundamental to the success of GitLab. We include it in every way possible and in all that we do."
canonical_path: "/company/culture/inclusion/"
---

## On this page
{:.no_toc}

- TOC
{:toc}


![Our Global Team](/images/summits/2019_new-orleans_team.png){: .illustration}*<small>In May of 2019, our team of 638 GitLab team-members from around the world had our annual company trip in New Orleans!</small>*

## Diversity, Inclusion & Belonging Mission at GitLab

Diversity, Inclusion & Belonging is fundamental to the success of GitLab.  We include it in every way possible and in all that we do.  We strive for a transparent environment where all globally dispersed voices are heard and welcomed.  We strive for an environment where people can show up as their full selves each day and can contribute to their best ability.  And with over 100,000 organizations utilizing GitLab across the globe, we strive for a team that is representative of our users.

Diversity complements our other [values](/handbook/values/), specifically Collaboration, Efficiency and Results.
And diversity in our leadership [supports innovation](https://www.bcg.com/en-us/publications/2018/how-diverse-leadership-teams-boost-innovation.aspx), [promotes better decision making](https://www.cloverpop.com/hacking-diversity-with-inclusive-decision-making-white-paper) and [improves financial results](https://www.mckinsey.com/~/media/McKinsey/Business%20Functions/Organization/Our%20Insights/Why%20diversity%20matters/Why%20diversity%20matters.ashx).

## GitLab's definition of Diversity, Inclusion & Belonging

The phrase "Diversity, Inclusion & Belonging" (or DIB) refers to the terminology for the initiative to create a diverse workforce and an environment where everyone can be their full selves.

**Diversity** refers to characteristics of the people who make up GitLab and how they identify. Race, gender, age, ethnicity, religion, national origin, disability, sexual orientation are *some* examples of how the data might be categorized when looking at GitLab's diversity.
Sometimes we can see things that make us diverse and sometimes we can't.

**GitLab uses the term "underrepresented" and it is meant to be a way of recognizing that we need more of what we do not have so that we can be at our best.**

The context is "at GitLab" or "in a specific department or team at GitLab."
This term is generally used in the context of reporting on how GitLab is working on understanding and improving the sourcing, interviewing, hiring, and retention of those who either want to work or currently work at GitLab.
Institutes like the [National Science Foundation](https://www.nsf.gov/mps/dmr/diversity.jsp) use the word "underrepresented" when discussing research around diversity so we have chosen to use it as well in order to be able to set goals around the data we have and understand where we need to work harder.

   *  A single person **should not** be referred to as a "diverse person" or a "diversity hire" which would imply they are not included in the current community or that they are only employed because of a factor that is not directly related to their skills and their ability to do their job.
   *  People should not be singled out or "othered" by labels with cold terminology in personal interactions.

For additional information about how GitLab uses this data to make progress, please see our [handbook page](/handbook/incentives/#add-on-bonus-for-select-underrepresented-groups) with more details.

**Inclusion** is the ability to recognize, respect, and value differences in those around us.  It focuses on the understanding and action needed to navigate a diverse team. It requires skills such as empathy, openness, listening, etc.  This lays the foundation of an inclusive mindset.  The foundation of understanding gives way to the actions and being intentional about creating policies and practices that embrace diversity that in the end change the overall company culture to create an environment of inclusion.

It acknowledges that a company composed of a diverse group of people can lead to the possibility of conflict of ideas which, if productively engaged with, can build innovation.  Inclusion also means being aware of both positive and negative biases and how those biases impact who we hire, work with, and retain.

GitLab believes that many perspectives coming together creates a more innovative environment to work in with more satisfied teammates, leading to a better product and increased profitability.

**Belonging** is when you feel your insights and contributions are valued. It goes back to team members feeling they can bring their full selves to work. It’s not enough to simply include people to have a "seat at the table", but it’s important to amplify everyone's voices, remove barriers and appreciate each others for their unique backgrounds.
Embracing inclusion most times leads to more of a feeling of a **sense of belonging**. Team members become more engaged and are invested in the work they are doing, because they are able to see themselves in the work being accomplished with the company overall.

**A good way to look at Diversity, Inclusion & Belonging is:**

- **Diversity**
  - diversity dimensions/layers that make people who they are
  - knowing the layers and having a seat at the table
- **Inclusion**
  - having a voice
  - feeling empowered to use your voice
- **Belonging**
  - acknowledgment of your voice being heard
  - the feeling of being a part of something
  - creating an environment where team members feel secure to be themselves

We believe in empowering team members to get their work done efficiently and collaboratively by establishing clear [DRIs](/handbook/people-group/directly-responsible-individuals/) for all our work. [DRIs do not owe anyone an explanation for their decisions](/handbook/people-group/directly-responsible-individuals/#empowering-dris-no-explanation-needed), but DRIs can still acknowledge input by closing an issue and marking it `Won't Do` or commenting on an issue acknowledging that they have read all the comments.

All team members don't have to agree on the best course of action- we can [disagree, commit, and disagree](/handbook/values/#disagree-commit-and-disagree)- but everyone can contribute and it is on the DRI to acknowledge those.
Some other ways we actively cultivate a sense of Belonging at GitLab include creating and cultivating allies, welcoming family members in the background of a call, and sharing [negative feedback in 1-1 settings](/handbook/values/#negative-feedback-is-1-1).

## Values

Inclusive teams are naturally more engaged, collaborative and innovative.
We aim to align [our values](/handbook/values/) to be reflective of our company wide commitment to fostering a diverse and inclusive environment.

In addition, the very nature of our company is to facilitate and foster inclusion.
We believe in asynchronous communication, we allow flexible work hours. GitLab team members are encouraged to work when and where they are most comfortable.

## Fully distributed and completely connected

The GitLab team is fully distributed across the globe, providing our team the opportunity to connect with each others cultures, celebrations and unique traditions.
We collaborate professionally and connect personally!

Our unique all-remote team opens our door to everyone.
Candidates are not limited by geography and we [champion this approach](/company/culture/all-remote/), to the extent that it’s possible, for all companies!

By having no offices and allowing each GitLab team member to work and live where they are most comfortable, GitLab offers a uniquely inclusive culture.
   * All-remote means that you [will not sacrifice career advancement](/handbook/people-group/learning-and-development/) by working outside of the office, as even GitLab executives are fully remote.
   * All-remote creates a workplace where caregivers, individuals with physical disabilities, etc. are not disadvantaged for being unable to regularly commute into an office.
   * GitLab's approach to [Spending Company Money](/handbook/spending-company-money/) enables all team members to create a work environment uniquely tailored for them.
   * All-remote enables those who must relocate frequently for family and personal reasons to take their career with them.
   * All-remote allows movement and relocation to physical settings that contribute to an individual's health (e.g. moving to a location with an improved air quality index).


Learn more about GitLab's [all-remote culture](/company/culture/all-remote/).

## GitLab team member data

Please see our [identity data](/company/culture/inclusion/identity-data).


## What we are doing with Diversity, Inclusion & Belonging

### Recruiting initiatives
[This page provides an overview of our Diversity, Inclusion & Belonging Recruiting initiatives](https://about.gitlab.com/company/culture/inclusion/recruiting-initiatives/).

### Engineering Initiatives
[This page provides an overview of our Diversity, Inclusion & Belonging Engineering initiatives](https://about.gitlab.com/company/culture/inclusion/engineering-initiatives/)

### Sales Initiatives
[This page provides and overview of Diversity, Inclusion and Belonging Initiatives within Sales](https://about.gitlab.com/handbook/people-group/women-in-sales-mentorship-pilot-program/)

### Inclusive benefits
We list our [Pregnancy & Maternity Care](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#pregnancy--maternity-care) publicly so people don't have to ask for them during interviews. In addition GitLab offers an Employee Assistance Program to all team members via [Modern Health](/handbook/total-rewards/benefits/modern-health/), a one-stop shop for all tools related to mental well-being and self-improvement.

### Inclusive language
In our [GitLab Values](/handbook/values/#inclusive-language--pronouns) we list: 'Use inclusive language. For example, prefer "Hi everybody" or "Hi people" to "Hi guys". And speak about courage instead of [aggression](https://www.huffingtonpost.com/2015/06/02/textio-unitive-bias-software_n_7493624.html). Another example is to avoid terms like "gossip" that have [negative gender connotations](https://inthesetimes.com/article/the-subversive-feminist-power-of-gossip). Also see the note in the [management section of the leadership page](/company/team/structure/#management-group) to avoid military analogies.
   * For an additional resource, we also have a presentation on [Inclusive Language](https://docs.google.com/presentation/d/186RK9QqOYxF8BmVS15AOKvwFpt4WglKKDR7cUCeDGkE/edit?usp=sharing)

### Global Diversity, Inclusion & Belonging Advisory Group
We launched our Global Diversity, Inclusion & Belonging Advisory Group - A team of company influencers who can be instrumental in driving DIB efforts from a global perspective.  We are empowering employees with Team Member Resource Groups based on diversity dimensions
* The group will assist in implementing global diversity, inclusion and belonging strategy, policies and initiatives.
* Review and provide feedback on new initiatives
* Suggest/Propose and drive initiatives needed based on the region in which you reside.

> Members of the advisory board have a tag on the [team page](/company/team) and [there is also a full list](/company/culture/inclusion/advisory-group-members).
> Want to know more about how the group is guided?  Please review [DIB Advisory group guidelines](https://about.gitlab.com/company/culture/inclusion/advisory-group-guide/).

### TMRGs - Team Member Resource Groups

We have created several TMRGs and welcome interest in creating new ones. Would you like to sign up for an Team Member Resource Group, start an TMRG, or just learn more?  See our [TMRG Guide](https://about.gitlab.com/company/culture/inclusion/erg-guide/).


### Military veterans and spouses

GitLab welcomes military veterans from around the world, as well as military spouses, to learn more about [life at GitLab](/company/culture/#life-at-gitlab) and to apply for [vacancies](/jobs/). We recognize the values gained from military experience, and we foster an [inclusive atmosphere](/company/culture/all-remote/building-culture/) to thrive in when returning to civilian life.

Our [all-remote culture](/company/culture/all-remote/) provides an ideal work environment for military veterans and spouses. By empowering team members to live and work where they are most comfortable, veterans and spouses can work in a safe, nurturing environment that they [choose and design](/company/culture/all-remote/workspace/).

We encourage military veterans and spouses to [read testimonials](/company/culture/all-remote/people/#military-spouses-and-families) from GitLab team members to understand the benefits of all-remote when [joining the workforce](/company/culture/all-remote/getting-started/) following military service.

*GitLab is actively [iterating](/handbook/values/#iteration) within Diversity, Inclusion & Belonging and Recruiting to ensure that additional underrepresented groups are pursued, embraced, and positioned for success.*

### Diversity, Inclusion & Belonging Training and Learning Opportunities

*  [Live Inclusion training](https://www.youtube.com/watch?v=gsQ2OsmgqVM&feature=youtu.be)
*  [Live Ally training](https://www.youtube.com/watch?v=wwZeFjDc4zE&feature=youtu.be)
*  Understanding [unconscious bias](https://about.gitlab.com/company/culture/inclusion/unconscious-bias/)
*  Salesforce Trailhead has publicly available diversity training on topics such as [Cultivating Equality at Work](https://trailhead.salesforce.com/en/content/learn/trails/champion_workplace_equality), [Inclusive Leadership Practices](Ihttps://trailhead.salesforce.com/en/content/learn/modules/inclusive-leadership-practices), [Unconscious Bias](https://trailhead.salesforce.com/en/content/learn/modules/workplace_equality_inclusion_challenges), and [many other diversity related trainings](https://trailhead.salesforce.com/en/search?keywords=diversity)
* [Inclusive Language](https://docs.google.com/presentation/d/186RK9QqOYxF8BmVS15AOKvwFpt4WglKKDR7cUCeDGkE/edit?usp=sharing)
* [Delivering Through Diversity](https://www.mckinsey.com/~/media/McKinsey/Business%20Functions/Organization/Our%20Insights/Delivering%20through%20diversity/Delivering-through-diversity_full-report.ashx) McKinsey and Company research on Diversity and its value.
* [Salesforce Equality at Work Training](https://trailhead.salesforce.com/trails/champion_workplace_equality).
 To earn badges and save your responses, you'll need to sign up! Use your GitLab address to sign in using Google+.
* [Business Value of Equality.](https://trailhead.salesforce.com/trails/champion_workplace_equality/modules/workplace_equality_diversity_and_inclusion) This module has three units. The third is specific to Salesforce values and mission and is not required or suggested for our training.
* [Impact of Unconscious Bias](https://trailhead.salesforce.com/en/trails/champion_workplace_equality/modules/workplace_equality_inclusion_challenges)
* [Allyship](https://about.gitlab.com/handbook/communication/ally-resources/) and [Equality Ally Strategies](https://trailhead.salesforce.com/trails/champion_workplace_equality/modules/workplace_equality_ally_strategies)
* [Inclusive Leadership Practices](https://trailhead.salesforce.com/trails/champion_workplace_equality/modules/inclusive-leadership-practices)
* To be truly inclusive is to be aware of your biases as well as strategies for stopping the effects of those biases. As part of our efforts, we recommend everyone to partake in [the Harvard project Implicit test](https://implicit.harvard.edu/implicit/takeatest.html) which focuses on the hidden causes of everyday discrimination.
* [Diversity, Inclusion, and Belonging Training issue](https://gitlab.com/gitlab-com/diversity-and-inclusion/-/blob/master/.gitlab/issue_templates/diversity-inclusion-belonging-training-template.md). As we work to format an in-house DIB Certification, team members are welcome to take [LinkedIn's Diversity, Inclusion, and Belonging training](https://www.linkedin.com/learning/paths/diversity-inclusion-and-belonging-for-all). The training is available until the end of 2020. While *not mandatory*, anyone at GitLab is welcome to complete this training! A few groups have set due dates for completion:

| Group | Due Date |
| --------------- | ----------------- |
| Development Department | August 31, 2020 (People Managers only) |
| [Engineering Division](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8607) | October 31, 2020 (People Managers only) |
| #manager-challenge-pilot | September 8, 2020 (all participants) |


## Community

GitLab team members are distributed across the globe, giving us access to an array of opportunity.
We encourage collaboration with global organizations and programs that support underrepresented individuals in the tech industry.
GitLab also provides additional support through the [GitLab Diversity Sponsorship program](/community/sponsorship/).
We offer funds to help support the event financially, and if the event is in a city we have a GitLab team member, we get hands-on by offering to coach and/or give a talk whenever possible.

Community members from all backgrounds are encouraged to join our [First Look](/community/gitlab-first-look/) UX research participant panel. Feedback from our community is what makes GitLab lovable and anyone is [able to sign up](https://gitlab.fra1.qualtrics.com/jfe/form/SV_51J9D8skLbWqdil?Source=d&i) to our research program for invites to usability tests, user interviews, surveys, and more.

We encourage organizers of events that are supported through our [GitLab Diversity Sponsorship program](/community/sponsorship/) to share [this sign up link](https://gitlab.fra1.qualtrics.com/jfe/form/SV_51J9D8skLbWqdil?Source=d&i) with attendees. Everyone can contribute.

## Internal to GitLab and want to learn more?

*  Stay updated via our slack channel - `#diversity_inclusion_and_belonging`
*  Have questions or suggestions for diversity, inclusion and belonging?  Please email `diversityinclusion@gitlab.com`
*  Have anonymous feedback you would like to send?  Please do so via our form [here](https://docs.google.com/forms/d/e/1FAIpQLSdout99ZmWz0TIPcIU24qGfy3aYCEWHEXpDOHCxcv1iZra8zg/viewform?usp=sf_link) which does not collect the email address.
*  **Monthly DIB Initiatives Company Call**. This call will allow time for GitLab team members to gain an understanding of what we are doing with DIB here at GitLab. It is the second Wednesday of every month.  If you aren't able to attend live calls, and would like listen in on past calls, you can do so [here](https://www.youtube.com/playlist?list=PL05JrBw4t0KqB4N2ruo8nsCwQzBocw-Hn) with our DIB playlist.  You have to be logged into GitLab Unfiltered.  If you are not sure how to log into GitLab Unfileted, you can review how to [here](https://about.gitlab.com/handbook/marketing/marketing-operations/youtube/#unable-to-view-a-video-on-youtube).


## Definitions

*  [Gender and Sexual Orientation Identity Definitions and FAQ](https://about.gitlab.com/handbook/people-group/gender-pronouns/)
*  [Leadership](https://about.gitlab.com/company/team/structure/#organizational-chart) is defined as manager and above.
*  [Geographically](https://about.gitlab.com/company/culture/inclusion/identity-data/) is defined as those countries we use in our identity data.
*  [Women](https://gitlab.bamboohr.com/home/) are defined as how you identify in Bamboo HR.
*  **Privilege** is an unearned advantage given by society to some people but not all
*  **Race** - As we work to be inclusive and equitable in opportunities, promotions, etc throughout GitLab, it is imperative that we understand our representation.  We want to do our best in having categories for race that team members can select where they are able to in some way identify with the options available.  Below are the categories GitLab uses with expanded options for each:
    - **American Indian or Alaska Native** - A person having origins in any of the original peoples of North and South America (including Central America) and who maintains tribal affiliation or community attachment. This category includes people who indicate their race as American Indian, Alaska Native, Navajo, Blackfeet, Inupiat, Yup'ik, Central American Indian groups or South American Indian groups.

    - **Asian** - A person having origins in any of the original peoples of the Far East, Southeast Asia, or the Indian subcontinent including but not limited to: Cambodia, China, India, Japan, Korea, Malaysia, Pakistan, the Philippine Islands, Thailand, and Vietnam. This includes people who indicate their race as Asian Indian, Chinese, Filipino,Korean, Japanese, Vietnamese and or other not mentioned Asian identifies.
    - **Black** - A person having origins in any of the Black racial groups of Africa. Including people who indicate their race as Black or African American, Cape Coloreds, Carribean, Kenyan, Nigerian, or Haitian.
    - **Hispanic or LatinX**  - A person of Cuban, Mexican, Puerto Rican, South or Central American, or other Spanish culture or origin, regardless of race. The term, "Spanish origin", can be used in addition to "Hispanic or Latino".
    - **Native Hawaiian or Other Pacific Islander** - A person having origins in any of the original people of Hawaii, Guam, Samoa, or other Pacific Islands. Included but not limited to people who reported their race as Fijian, Guamanian, Chamorro, Marshallese,Native Hawaiian,Samoan,Tongan or Other Pacific Islander 
    - **Multiracial** - refers to 2 or more races other than White.
    - **White** - A person having origins in any of the original people of Europe, the Middle East, or North Africa. Included but not limited to people who indicate their race as: White, Irish, German, Italian, Lebanese, Arab, Moroccan, or Caucasian.
    - **Prefer not to answer**
    - **Ethnicity not captured in options available**


## Performance Indicators

When measuring diversity-focused performance indicators, we focus on top-of-funnel metrics, like pipeline, because they're [leading indicators](https://www.leadingagile.com/2018/02/leading-lagging-indicators/) which are better measures of where we are heading.
It also helps reduce risk that we hire to the performance indicator, instead of hiring the best candidate.

Like all performance indicators, our [Diversity Performance Indicators](https://about.gitlab.com/handbook/people-group/people-success-performance-indicators/#key-performance-indicators) are on the [People Success Performance Indicator Page](/handbook/people-group/people-success-performance-indicators/).

